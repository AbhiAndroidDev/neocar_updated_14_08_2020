package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.MyProfile_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

/**
 * Created by parna on 27/2/18.
 */

public class HomeProfileActivity extends AppCompatActivity  implements Profile_Interface {

    private CircularImageViewBorder civProfilePic;
    private TextView  tvTrips, tvMyVehicle, tvSettings, tvHelp, tvInviteFriends, tvManageCoupons, tvYourRating;
    private TextView tvAlertText,tvUsername,tvEmailHeader, tvPhoneHeader, tvEmail,tvPhone, tvGSTNo, tvGSTNoHeader;
    private RelativeLayout rlALert;
    private ImageView ibEdit;
    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private ImageView iVsmallLogo;
    private SharedPrefUserDetails sharedPref;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private RelativeLayout rlBackLayout;
    private RelativeLayout rlUserDetails;
    private BottomView bottomview = new BottomView();
    private ConnectionDetector cd;
    private Context context;
    private int resultValue = 11;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_profile);

        new AnalyticsClass(HomeProfileActivity.this);
//        System.out.print("output"+1/0);
        Initialize();
        Listener();

    }

    private void Initialize() {
        context = this;
        sharedPref = new SharedPrefUserDetails(HomeProfileActivity.this);
        cd = new ConnectionDetector(this);
        UserLoginDetails=new UserLoginDetailsModel();

        rlALert= findViewById(R.id.rlALert);
        tvAlertText= findViewById(R.id.tvAlertText);
        tvEmail= findViewById(R.id.tvEmail);
        tvEmailHeader= findViewById(R.id.tvEmailHeader);
        tvPhoneHeader= findViewById(R.id.tvPhoneHeader);
        tvUsername = findViewById(R.id.tvUsername);
        tvPhone= findViewById(R.id.tvPhone);
        tvGSTNo= findViewById(R.id.tvGSTNo);
        tvGSTNoHeader= findViewById(R.id.tvGSTNoHeader);
        civProfilePic = findViewById(R.id.civProfilePic);

        ibEdit = findViewById(R.id.ibEdit);
        tvTrips = findViewById(R.id.tvTrips);
        tvMyVehicle =  findViewById(R.id.tvMyVehicle);
        tvSettings = findViewById(R.id.tvSettings);
        tvHelp = findViewById(R.id.tvHelp);
        tvInviteFriends = findViewById(R.id.tvInviteFriends);
        tvManageCoupons = findViewById(R.id.tvManageCoupons);
        tvYourRating = findViewById(R.id.tvYourRating);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.app_name));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlBackLayout.setVisibility(View.INVISIBLE);
        iVsmallLogo = findViewById(R.id.iVsmallLogo);
        iVsmallLogo.setVisibility(View.VISIBLE);

        rlUserDetails = findViewById(R.id.rlUserDetails);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            if( getApplicationContext().checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ) {
//                ActivityCompat.requestPermissions(HomeProfileActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, resultValue);
//            }
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener() {

        rlUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                    Intent mybookingIntent = new Intent(HomeProfileActivity.this, ViewUserProfile.class);
                    startActivity(mybookingIntent);

            }
        });

        tvTrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    transitionflag = StaticClass.transitionflagNext;
                    Intent mybookingIntent = new Intent(HomeProfileActivity.this, MyBookingActivity.class);
                    startActivity(mybookingIntent);
            }
        });

        tvMyVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent myvehicleIntent = new Intent(HomeProfileActivity.this, MyVehicleActivity.class);
                startActivity(myvehicleIntent);
            }
        });

        tvSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent settingsIntent = new Intent(HomeProfileActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);

            }
        });

        tvHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent helpIntent = new Intent(HomeProfileActivity.this, HelpActivity.class);
                startActivity(helpIntent);

            }
        });

        tvInviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent invitefriendsIntent = new Intent(HomeProfileActivity.this, InviteFriendsActivity.class);
                startActivity(invitefriendsIntent);
            }
        });

        tvManageCoupons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent manageCouponsIntent = new Intent(HomeProfileActivity.this, ManageCouponsActivity.class);
                startActivity(manageCouponsIntent);
            }
        });

        tvYourRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag=StaticClass.transitionflagNext;
                Intent riderRatingIntent = new Intent(HomeProfileActivity.this, RiderRatingDetailsActivity.class);
                startActivity(riderRatingIntent);
            }
        });

        civProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeProfileActivity.this, ImageFullActivity.class);
                i.putExtra("imgUrl", UserLoginDetails.getProfile_pic());
                startActivity(i);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(HomeProfileActivity.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        StaticClass.BottomProfile= false;

        //Fetch my profile details
        if (cd.isConnectingToInternet()) {

            new MyProfile_Webservice().MyProfile(HomeProfileActivity.this);

        } else {
            startActivity(new Intent(HomeProfileActivity.this, NetworkNotAvailable.class));
            transitionflag = StaticClass.transitionflagBack;

        }

        if (StaticClass.isLoginFalg) {

            sharedPref.setClear();
            StaticClass.isLoginFalg=false;
            transitionflag = StaticClass.transitionflagBack;
            Intent i = new Intent(HomeProfileActivity.this, LoginStepOneActivity.class);
            startActivity(i);
            finish();

        } else {

            gson = new Gson();

            String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
            UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        }

        if(StaticClass.EditProfileIsSave){

            StaticClass.EditProfileIsSave = false;

            sharedPref.putBottomView(StaticClass.Menu_Explore);

//            Intent invitefriendsIntent = new Intent(HomeProfileActivity.this, ExploreActivity.class);
            Intent invitefriendsIntent = new Intent(HomeProfileActivity.this, ExploreActivity.class);
            startActivity(invitefriendsIntent);

        }else{

            sharedPref.putBottomView(StaticClass.Menu_profile);
            bottomview = new BottomView();
            bottomview.BottomView(HomeProfileActivity.this, StaticClass.Menu_profile);
        }


        ImageUtils.deleteImageGallery();

    }

    @Override
    public void MyProfile(UserLoginDetailsModel userLoginDetailsModel) {

        this.UserLoginDetails = userLoginDetailsModel;
        if (!userLoginDetailsModel.getIs_profile_complete().equals("") && !userLoginDetailsModel.getIs_profile_complete().equals("null")) {
            if(userLoginDetailsModel.getIs_profile_complete().equals("Y")){
                rlALert.setVisibility(View.GONE);
            }else{
                rlALert.setVisibility(View.VISIBLE);
                tvAlertText.setText(userLoginDetailsModel.getProfile_incomplete_message());
            }
        }

        tvUsername.setText(UserLoginDetails.getProfile_name());
        tvEmail.setText(UserLoginDetails.getEmail());
        tvPhone.setText(UserLoginDetails.getMobile());

        new PrintClass(UserLoginDetails.getProfile_pic());

        try {
            Picasso.get().load(UserLoginDetails.getProfile_pic()).into(civProfilePic);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
