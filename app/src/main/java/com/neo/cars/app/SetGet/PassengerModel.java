package com.neo.cars.app.SetGet;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parna on 10/5/18.
 */

public class PassengerModel  implements Parcelable {

    String passenger_name;
    String passenger_contact;


    public PassengerModel() {

    }

    public PassengerModel(Parcel in) {
        passenger_name = in.readString();
        passenger_contact = in.readString();
    }

    public static final Creator<PassengerModel> CREATOR = new Creator<PassengerModel>() {
        @Override
        public PassengerModel createFromParcel(Parcel in) {
            return new PassengerModel(in);
        }

        @Override
        public PassengerModel[] newArray(int size) {
            return new PassengerModel[size];
        }
    };

    public String getPassenger_contact() {
        return passenger_contact;
    }

    public void setPassenger_contact(String passenger_contact) {
        this.passenger_contact = passenger_contact;
    }

    public String getPassenger_name() {
        return passenger_name;
    }

    public void setPassenger_name(String passenger_name) {
        this.passenger_name = passenger_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(passenger_name);
        parcel.writeString(passenger_contact);
    }
}
