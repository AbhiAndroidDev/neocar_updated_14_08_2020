package com.neo.cars.app.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.neo.cars.app.Interface.TripCostSelectInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.Utils.StaticClass;

import java.util.ArrayList;

public class BrandOfVehicleAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> arrlistVehicleBrand;
    private int selectedPosition = 0;
    private int showMoreClickedCount = 0;
    private TripCostSelectInterface selectInterface;
    private LayoutInflater inflter;
    private TextView textView;

    public BrandOfVehicleAdapter(Context mContext, TripCostSelectInterface _selectInterface, ArrayList<String> _arrlistVehicleBrand,
                                 int _selectedPosition) {
        this.mContext = mContext;
        this.arrlistVehicleBrand = _arrlistVehicleBrand;
        this.selectedPosition = _selectedPosition;
        this.selectInterface = _selectInterface;

        inflter = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return arrlistVehicleBrand.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.item_vehicle_brand, null);

        textView = view.findViewById(R.id.tv_item_name);

        textView.setText(arrlistVehicleBrand.get(position));

        Log.d("selectedPosition**", ""+StaticClass.selectedPosition);
        Log.d("showMoreClicked**", ""+StaticClass.showMoreClicked);

        if(StaticClass.selectedPosition == position){

            textView.setBackgroundResource(R.drawable.base_yellow);

        }else{
            textView.setBackgroundResource(R.drawable.base);
        }
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticClass.selectedPosition = position;
                    textView.setBackgroundResource(R.drawable.base_yellow);
                    selectInterface.setTripCost(position, "vehicle_brand", showMoreClickedCount);
            }
        });
        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
