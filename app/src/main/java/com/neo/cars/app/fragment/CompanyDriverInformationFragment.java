package com.neo.cars.app.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.ImageFullActivity;
import com.neo.cars.app.Interface.CompanyDriverSave_interfae;
import com.neo.cars.app.Interface.MoreDriverInfo_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.BookingInformationModel;
import com.neo.cars.app.SetGet.CompanyAvailableDrivermodel;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.ViewCompanyprofile;
import com.neo.cars.app.Webservice.MoreDriverInfo_Webservice;
import com.neo.cars.app.Webservice.VehicleDriverAssociationWebService;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CompanyDriverInformationFragment extends Fragment implements MoreDriverInfo_Interface, CompanyDriverSave_interfae {

    private View mView;
    private Gson gson;
    private DriverDataModel driverdataModel;
    private BookingInformationModel bookingInformationModel;
    private String driverdata = "", bookingInformation = "", strBookingId = "", strDriverId = "";

    private ImageView civDriverPic;
    private TextView tvName,tvDOB,tvSex,tvAddress,tvNationality,tvEmploymntSince,tvFathersname,
            tvDrivingLicense,tvAdhaarCard,tvMobNo,tvAddl;
    private ImageView ivDriverTick, ivAdhaarTick;
    private FrameLayout flSpnrDriverSelection;
    private NoDefaultSpinner spnrDriverSelection;
    private ArrayList<String> arrlistOfDriver;
    private ArrayList<CompanyAvailableDrivermodel> arrListAvailableDrivermodel;
    private ArrayAdapter<String> arrAdapterDriver;
    private RelativeLayout rlParent;
    private CustomButtonTitilliumSemibold btnSave;
    final int PERMISSION_REQUEST_CODE = 111;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_view_driver_info, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            driverdata = getArguments().getString("driverdata");
            bookingInformation = getArguments().getString("bookingInformation");
            Log.d("d", "Get driver data: " + driverdata);

//            strShowDriver = getArguments().getString("showdriver");
//            strShowDriverMsg = getArguments().getString("showdrivermessage");

        }

        gson = new Gson();
        driverdataModel = new DriverDataModel();
        driverdataModel = gson.fromJson(driverdata, DriverDataModel.class);

        bookingInformationModel = new BookingInformationModel();
        bookingInformationModel = gson.fromJson(bookingInformation, BookingInformationModel.class);

        Initialize();

        Listener();

        if (driverdataModel.getCan_assign_driver().equalsIgnoreCase("Y")){
            flSpnrDriverSelection.setVisibility(View.VISIBLE);
//            if (TextUtils.isEmpty(driverdataModel.getId())){
//                rlParent.setVisibility(View.GONE);
//            }else {
                setDriverSpinner();
//            }

        }else {
            btnSave.setVisibility(View.GONE);
        }

        if (driverdataModel.getDriver_associated().equalsIgnoreCase("Y")){
            rlParent.setVisibility(View.VISIBLE);
            SetData();
        }else{
            rlParent.setVisibility(View.GONE);
        }

        return mView;
    }

    private void Initialize(){
        civDriverPic=mView.findViewById(R.id.civDriverPic);
        tvName= mView.findViewById(R.id.tvName);
        tvDOB= mView.findViewById(R.id.tvDOB);
        tvSex= mView.findViewById(R.id.tvSex);
        tvAddress= mView.findViewById(R.id.tvAddress);
        tvNationality= mView.findViewById(R.id.tvNationality);
        tvEmploymntSince= mView.findViewById(R.id.tvEmploymntSince);
        tvFathersname= mView.findViewById(R.id.tvFathersname);
        tvDrivingLicense= mView.findViewById(R.id.tvDrivingLicense);
        tvAdhaarCard= mView.findViewById(R.id.tvAdhaarCard);
        tvMobNo= mView.findViewById(R.id.tvMobNo);
        tvAddl= mView.findViewById(R.id.tvAddl);

        ivDriverTick =  mView.findViewById(R.id.ivDriverTick);
        ivAdhaarTick =  mView.findViewById(R.id.ivAdhaarTick);

        flSpnrDriverSelection = mView.findViewById(R.id.flSpnrDriverSelection);
        spnrDriverSelection = mView.findViewById(R.id.spnrDriverSelection);

        rlParent = mView.findViewById(R.id.rlParent);
        btnSave = mView.findViewById(R.id.btnSave);
    }

    private void setDriverSpinner() {
        int pos=-1;
        if (driverdataModel != null){
            arrlistOfDriver=new ArrayList<>();
            arrListAvailableDrivermodel = driverdataModel.getArr_CompanyAvailableDrivermodel();

            Log.d("arrListSize", String.valueOf(arrListAvailableDrivermodel.size()));
            if(arrListAvailableDrivermodel.size() > 0){
                flSpnrDriverSelection.setVisibility(View.VISIBLE);
                btnSave.setVisibility(View.VISIBLE);

                for (int x=0; x<arrListAvailableDrivermodel.size();x++){
                    arrlistOfDriver.add(arrListAvailableDrivermodel.get(x).getDriver_name());

//                    if(arrListAvailableDrivermodel.get(x).getDriver_name().equals()){
//                        pos=x;
//                    }
                }

//                Log.d("d", "Array list vehicle type size::"+arrlistVehicleType.size());

                arrAdapterDriver = new ArrayAdapter<String>(getContext(), R.layout.countryitem, arrlistOfDriver);
                arrAdapterDriver.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrDriverSelection.setPrompt(getResources().getString(R.string.tvPlsSelectDriver));
                spnrDriverSelection.setAdapter(arrAdapterDriver);

                //spnrVehicleType.setSelection(0);

                spnrDriverSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        //  spCurrency.setTag(countryListModelArrayList.get(position));

//                        strVehicleTypeId = arrlistOfDriver.get(position).getId();
//                        Log.d("Vehicle type id: ", strVehicleTypeId);
                        strDriverId = arrListAvailableDrivermodel.get(position).getDriver_id();
                        Log.d("d", "strDriverId: " + strDriverId);

                        if(!strDriverId.equals("")) {
                            if (NetWorkStatus.isNetworkAvailable(getActivity())) {
                                new MoreDriverInfo_Webservice().driverInfo(getActivity(), CompanyDriverInformationFragment.this, strDriverId);

                            } else {
                                Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
                                startActivity(i);
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                spnrDriverSelection.setSelection(pos);
            }else {
                flSpnrDriverSelection.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
            }

        }else {
            new CustomToast(getActivity(), getResources().getString(R.string.tvVehicleTypeNotFound));
        }
    }

    private void SetData(){

        if(!TextUtils.isEmpty(driverdataModel.getRecent_photograph())) {
            Picasso.get().load(driverdataModel.getRecent_photograph()).transform(new CropCircleTransformation()).into(civDriverPic);
        }

        if(!TextUtils.isEmpty(driverdataModel.getFirst_name()) && !TextUtils.isEmpty(driverdataModel.getLast_name())){
            tvName.setText(driverdataModel.getFirst_name()+" "+driverdataModel.getLast_name());
        }else if (!TextUtils.isEmpty(driverdataModel.getName())){
            tvName.setText(driverdataModel.getName());
        }else {
            tvName.setText("N/A");
        }

        if(!"".equals(driverdataModel.getDob()) && !"null".equals(driverdataModel.getDob())){
            tvDOB.setText(driverdataModel.getDob());
        }else{
            tvDOB.setText("N/A");
        }

        if(!"".equals(driverdataModel.getGender()) && !"null".equals(driverdataModel.getGender()) ){
            tvSex.setText(driverdataModel.getGender());
        }else{
            tvSex.setText("N/A");
        }

        if(!"".equals(driverdataModel.getAddress()) && !"null".equals(driverdataModel.getAddress()) ){
            tvAddress.setText(driverdataModel.getAddress());
        }else {
            tvAddress.setText("N/A");
        }

        if(!"".equals(driverdataModel.getNationality()) && !"null".equals(driverdataModel.getNationality()) ){
            tvNationality.setText(driverdataModel.getNationality());
        }else{
            tvNationality.setText("N/A");
        }

        if(!"".equals(driverdataModel.getIn_employment_since()) && !"null".equals(driverdataModel.getIn_employment_since())){
            tvEmploymntSince.setText(driverdataModel.getIn_employment_since());
        }else{
            tvEmploymntSince.setText("N/A");
        }

        if(!"".equals(driverdataModel.getFathers_name()) && !"null".equals(driverdataModel.getFathers_name()) ){
            tvFathersname.setText(driverdataModel.getFathers_name());
        }else{
            tvFathersname.setText("N/A");
        }

        if(!"".equals(driverdataModel.getContact_no()) && !"null".equals(driverdataModel.getContact_no()) ){
            tvMobNo.setText(driverdataModel.getContact_no());
            tvMobNo.setTextColor(Color.parseColor("#1e65a6"));
            tvMobNo.setPaintFlags(tvMobNo.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }else{
            tvMobNo.setText("N/A");
        }

        if(!"".equals(driverdataModel.getAdditional_info()) && !"null".equals(driverdataModel.getAdditional_info()) ) {
            tvAddl.setText(driverdataModel.getAdditional_info());
        }else{
            tvAddl.setText("N/A");
        }

        ArrayList<UserDocumentModel> listOfDoc = driverdataModel.getArr_UserDocumentModel();
        if (listOfDoc != null) {
            for (int i = 0; i < listOfDoc.size(); i++) {
                if (listOfDoc.get(i).getDocument_type().equals("AC")) {
                    String[] parts = listOfDoc.get(i).getDocument_file().split("/");
                    tvAdhaarCard.setText(parts[parts.length - 1]);
                    if(listOfDoc.get(i).getIs_approved().equalsIgnoreCase("Yes")){
                        ivAdhaarTick.setVisibility(View.VISIBLE);
                    }

                } else if (listOfDoc.get(i).getDocument_type().equals("DL")) {
                    String[] parts = listOfDoc.get(i).getDocument_file().split("/");
                    tvDrivingLicense.setText(parts[parts.length - 1]);
                    if(listOfDoc.get(i).getIs_approved().equalsIgnoreCase("Yes")){
                        ivDriverTick.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else{
            tvAdhaarCard.setText("N/A");
            tvDrivingLicense.setText("N/A");
        }
    }

    private void Listener(){

        civDriverPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ImageFullActivity.class);
                i.putExtra("imgUrl", driverdataModel.getRecent_photograph());
                startActivity(i);
            }
        });



        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetWorkStatus.isNetworkAvailable(getActivity())) {
                    strBookingId = bookingInformationModel.getBooking_id();

                    new VehicleDriverAssociationWebService().vehicleDriverAssociationWebService(getContext(), getActivity(), CompanyDriverInformationFragment.this, strDriverId, strBookingId);

                } else {
                    Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
                    startActivity(i);
                }
            }
        });

        tvMobNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(driverdataModel.getContact_no())){

                    contactPermission(driverdataModel.getContact_no());
//                    callMethod(driverdataModel.getContact_no());
                }
            }
        });

    }

    private void contactPermission(final String contact_mobile){

        Dexter.withActivity(getActivity()).withPermissions( Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE)
                .withListener(new MultiplePermissionsListener()
                {
                    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
                    @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_mobile));
                            startActivity(intent);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showAlert();
                        }
                    }
                    @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                    {/* ... */

                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void showAlert(){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getContext().getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    private void callMethod(String passenger_contact) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                requestPermission();
            } else {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
            startActivity(intent);
        }

    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void MoreDriverInfo(DriverDataModel driverDataModel) {
        this.driverdataModel = driverDataModel;
        rlParent.setVisibility(View.VISIBLE);
        if (driverDataModel != null) {
            SetData();
        }
    }

    @Override
    public void companyDriverSave(String status, String msg) {
        if (status.equalsIgnoreCase(StaticClass.SuccessResult)){
            new CustomToast(getActivity(), msg);
            SetData();
        }
    }
}
