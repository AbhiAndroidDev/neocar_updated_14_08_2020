package com.neo.cars.app.Interface;


import com.neo.cars.app.SetGet.VoucherModel;

/**
 * Created by parna on 22/5/18.
 */

public interface UserVoucherRedeem_Interface {
     void userVoucherRedeem(VoucherModel voucherModel, String status);
}
