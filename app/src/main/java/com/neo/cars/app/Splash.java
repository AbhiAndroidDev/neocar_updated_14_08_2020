package com.neo.cars.app;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;

import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;

public class Splash extends RootActivity {

    private final int SPLASH_DURATION = 3 * 1000;
    private SharedPrefUserDetails sharedPref;
    private TextView tvgetstartedText;
    private int transitionflag = StaticClass.transitionflagNext;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    boolean isLocationPermissionAllowed = false, isContactPermissionAllowed = false;
    private Context context;
    private ConnectionDetector cd;
    private String TAG = Splash.class.getSimpleName();
    private boolean checkLocationPermission = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=this;
        new AnalyticsClass(Splash.this);// setting analytics

        cd = new ConnectionDetector(this);

//        System.out.print("output"+1/0);

        sharedPref = new SharedPrefUserDetails(this);
//        sharedPref.putBottomView(StaticClass.Menu_profile);
        sharedPref.putBottomView(StaticClass.Menu_Explore);

        tvgetstartedText=findViewById(R.id.tvgetstartedText);


//        if(isLocationEnable()) {
//
//            Next();
//        }
//        else {
//            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
//            dialog.setMessage("Enable Location");
//            dialog.setCancelable(false);
//            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                    // TODO Auto-generated method stub
//                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                    startActivityForResult(myIntent, 111);
//                    //get gps
//                }
//            });
//            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                    // TODO Auto-generated method stub
//                    finish();
//
//                }
//            });
//            dialog.show();
//        }

    }

    private void Next() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean s = sharedPref.getLoginStatus();
                String userType = sharedPref.getUserType();
                if (sharedPref.getLoginStatus()) {
                    if (sharedPref.getUserType().equalsIgnoreCase("U")){
//                        Intent i = new Intent(Splash.this, HomeProfileActivity.class);
                        Intent i = new Intent(Splash.this, SearchActivity.class);
                        startActivity(i);
                        finish();
                    }else if (sharedPref.getUserType().equalsIgnoreCase("C")) {
                        Intent i = new Intent(Splash.this, CompanyHomeProfileActivity.class);
                        startActivity(i);
                        finish();
                    }

                } else {
                    Intent i = new Intent(Splash.this, LoginStepOneActivity.class);
                    startActivity(i);
                    finish();
                }

//                startActivity(new Intent(Splash.this, MobileOtpVerificationActivity.class));

            }
        }, SPLASH_DURATION);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(resultCode == RESULT_OK){
//
//            if(requestCode == 111){
//
//                Next();
//
//                Toast.makeText(context, "Location Enabled", Toast.LENGTH_SHORT).show();
//
//            }
//        }
//    }

    @Override
    public void onPause() {
        super.onPause();
        new OnPauseSlider(Splash.this, StaticClass.transitionflagNext);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();

//            if(isLocationEnable()) {

                Next();
//            }else{
//                if(!checkLocationPermission) {

//                    checkLocationPermission = true;
//                    locationAlert();

//                }else{
//                    Next();
//                }
//            }


        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
        if (!cd.isConnectingToInternet()) {
            Intent i = new Intent(Splash.this, NetworkNotAvailable.class);
            startActivity(i);

        }else {

        }

        ImageUtils.deleteImageGallery();
    }

    private void locationAlert(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage("Enable Location");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, 111);
                //get gps
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
//                finish();


            }
        });
        dialog.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void marshmallowGPSPremissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && this.checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && this.checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            //if user deny once then dialog will be show to open Settings
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Permissions Required")
                        .setMessage("You have forcefully denied some of the required permissions " +
                                "for this action. Please open settings, go to permissions and allow them.")
                        .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

//                                Next();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();

                // Display UI and wait for user interaction
            }


            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

        }else {
//            Next();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            isLocationPermissionAllowed = true;
            Log.d(TAG,"LocationPermissionAllowed");

        }else{


            Log.d(TAG,"Permission denied");

        }

        if (requestCode == 2
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            isContactPermissionAllowed = true;
            Log.d(TAG,"ContactPermissionAllowed");

        }else{
            //if user deny
            //Added later to check only
//            grantCameraPermission();

            Log.d(TAG,"Permission denied");

        }
        if (isLocationPermissionAllowed && isContactPermissionAllowed){
            Log.d(TAG,"LocationContactAllowed");
//            Next(); //commented before open to check
        }
    }
}
