package com.neo.cars.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;

public class NetworkNotAvailable extends AppCompatActivity {

    private TextView tv_refresh;
    private ConnectionDetector cd;
    private SharedPrefUserDetails prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_not_available);

        new AnalyticsClass(NetworkNotAvailable.this);
        cd = new ConnectionDetector(this);

        prefs = new SharedPrefUserDetails(this);

        tv_refresh=findViewById(R.id.tv_refresh);

        tv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()){

                    finish();
                }else {

                    Toast.makeText(NetworkNotAvailable.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
    }
}
