package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.EmailConfirmationActivity;
import com.neo.cars.app.Interface.MobileOtpVerificationInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.ShareFcmDetails;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MobileOTPWebservice {

    private Activity mcontext;
    private String Status = "0", Msg = "";

    private SharedPrefUserDetails sharedPref;
    private ShareFcmDetails shareFcm;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;

    private String st_mobile="",otp="", unique_id="";
    private String userType="";
    private int transitionflag = StaticClass.transitionflagNext;
    private MobileOtpVerificationInterface otpVerification_interface;

    public void mobileOTPWebservice(Activity context, final String mobile, final String enteredOtp, final String userType,
                                    MobileOtpVerificationInterface _otpVerification_interface) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        otp = enteredOtp;

        sharedPref = new SharedPrefUserDetails(mcontext);
        shareFcm=new ShareFcmDetails(mcontext);

        UserLoginDetails = new UserLoginDetailsModel();
        otpVerification_interface = _otpVerification_interface;

        unique_id = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("d", "Device id::"+unique_id);

        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.mobile_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("mobile_no", mobile);
                params.put("otp", enteredOtp);
                params.put("device_type", "A");
                params.put("device_id", shareFcm.getFcmId());
//                params.put("user_type", userType);

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);


            Msg = jobj_main.optJSONObject("mobile_login").optString("message");
            Status= jobj_main.optJSONObject("mobile_login").optString("status");

            JSONObject details=jobj_main.optJSONObject("mobile_login").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)) {
                st_mobile=details.optString("mobile_no");
                UserLoginDetails.setMobile_verified(details.optString("mobile_verified"));
                UserLoginDetails.setEmail_verified(details.optString("email_verified"));
                UserLoginDetails.setUser_type(details.optString("user_type"));
                sharedPref.setUserType(details.optString("user_type"));


            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("mobile_login").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);

            }
        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }


        if (Status.equals(StaticClass.SuccessResult)) {

            transitionflag = StaticClass.transitionflagNext;
            mcontext.startActivity(new Intent(mcontext, EmailConfirmationActivity.class));
            mcontext.finish();


        }else if (Status.equalsIgnoreCase(StaticClass.ErrorResult)){
            otpVerification_interface.mobileOtpVerification(Status, st_mobile, "","");
        }
    }
}
