package com.neo.cars.app.SetGet;

/**
 * Created by parna on 23/4/18.
 */

public class PickUpLocationModel {

    String id;
    String pickup_location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

}
