package com.neo.cars.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.VehicleListAdapter_Interface;
import com.neo.cars.app.MoreDetailsActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by parna on 12/3/18.
 */

public class VehicleListAdapter extends RecyclerView.Adapter<VehicleListAdapter.MyViewHolder> {

    private ArrayList<VehicleTypeModel> vehicleTypeModelList;
    private Context context;
    private int transitionflag = StaticClass.transitionflagNext;
    private VehicleListAdapter_Interface vehicleListAdapterInterface;

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView civVehiclePic, ivVehicleStatus;
        CustomTitilliumTextViewSemiBold btnMore, btnDelete, btnActive;
        CustomTextviewTitilliumWebRegular tvLocation, tvDrivername, tvPlateNo;
        String vehicle_id="";
        RelativeLayout rlParent;

        public MyViewHolder(View view) {
            super(view);
            civVehiclePic = view.findViewById(R.id.civVehiclePic);
            btnMore = view.findViewById(R.id.btnMore);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvDrivername = view.findViewById(R.id.tvDrivername);
            tvPlateNo = view.findViewById(R.id.tvPlateNo);
            ivVehicleStatus = view.findViewById(R.id.ivVehicleStatus);
            btnDelete = view.findViewById(R.id.btnDelete);
            btnActive = view.findViewById(R.id.btnActive);
            rlParent = view.findViewById(R.id.rlParent);

        }
    }

    public VehicleListAdapter (Context context,VehicleListAdapter_Interface VehicleListAdapterInterface,ArrayList<VehicleTypeModel> myvehiclelist){
        this.context = context;
        vehicleListAdapterInterface=VehicleListAdapterInterface;
        vehicleTypeModelList = myvehiclelist;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.my_vehicle_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final  int position) {

        holder.tvLocation.setText(vehicleTypeModelList.get(position).getCity());
        holder.tvDrivername.setText(vehicleTypeModelList.get(position).getDriver_name());
        holder.tvPlateNo.setText(vehicleTypeModelList.get(position).getLicense_plate_no());

        Log.d("d", "Vehicle id:::" + vehicleTypeModelList.get(position).getVehicle_id());

        /******setting vehicle status icon for active and inactive vehicle add from admin end ******/
        if("I".equals(vehicleTypeModelList.get(position).getVehicle_status())){
            holder.ivVehicleStatus.setVisibility(View.VISIBLE);
            holder.btnActive.setVisibility(View.VISIBLE);
            holder.btnActive.setBackgroundResource(R.drawable.inactive);
            holder.btnActive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new CustomToast( (Activity) context , "Please first complete all necessary vehicle/driver information.");
                }
            });


        }else if("A".equals(vehicleTypeModelList.get(position).getVehicle_status())){
            holder.ivVehicleStatus.setVisibility(View.GONE);
            holder.btnActive.setVisibility(View.VISIBLE);
            if("N".equals(vehicleTypeModelList.get(position).getIs_active_by_owner())){
                holder.btnActive.setBackgroundResource(R.drawable.inactive);
            }else{
                holder.btnActive.setBackgroundResource(R.drawable.active);
            }

            holder.btnActive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String strDeactivateMsg = context.getResources().getString(R.string.OwnerDeactivate);

                    if("N".equals(vehicleTypeModelList.get(position).getIs_active_by_owner())){
                        strDeactivateMsg = context.getResources().getString(R.string.Owneractivate);
                    }else{
                        strDeactivateMsg = context.getResources().getString(R.string.OwnerDeactivate);
                    }

                    final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                            strDeactivateMsg ,
                            context.getResources().getString(R.string.yes),
                            context.getResources().getString(R.string.no));
                    alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialogYESNO.dismiss();
                            vehicleListAdapterInterface.OnOwnerDeactive(vehicleTypeModelList.get(position).getVehicle_id(),position,StaticClass.MyVehicleActiveDeactive);

                        }
                    });

                    alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialogYESNO.dismiss();
                        }
                    });

                    alertDialogYESNO.show();

                }
            });
        }

        Picasso.get().load(vehicleTypeModelList.get(position).getImage_url()).transform(new CropCircleTransformation()).into(holder.civVehiclePic);




        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag= StaticClass.transitionflagNext;
                Intent moreIntent = new Intent(context, MoreDetailsActivity.class);
                moreIntent.putExtra("vehicleId", vehicleTypeModelList.get(position).getVehicle_id());
                moreIntent.putExtra("DriverId", vehicleTypeModelList.get(position).getDriver_id());
                moreIntent.putExtra("VehicleStatus", vehicleTypeModelList.get(position).getVehicle_status());
                moreIntent.putExtra("DriverId", vehicleTypeModelList.get(position).getDriver_id());
                context.startActivity(moreIntent);

            }
        });


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strDeactivateMsg = context.getResources().getString(R.string.OwnerDelete);

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                        strDeactivateMsg ,
                        context.getResources().getString(R.string.yes),
                        context.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        vehicleListAdapterInterface.OnOwnerDeactive(vehicleTypeModelList.get(position).getVehicle_id(),position,StaticClass.MyVehicleDelete);

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicleTypeModelList.size();
    }

}