package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.fragment.MoreVehicleListPager;

/**
 * Created by parna on 20/4/18.
 */

public class MoreDetailsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    private TabLayout tabs;
    private ViewPager viewpager;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile;
    private ImageButton ibNavigateMenu, ibEditMenu;
    private CustomTextviewTitilliumWebRegular rightTopBarText;
    private RelativeLayout rlAddLayout;

    private int transitionflag = StaticClass.transitionflagNext;
    private RelativeLayout rlBackLayout;
    private String vehicleId="",DriverId="", strVehicleStatus="", strDriverId="";
    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_vehicle);

        vehicleId=getIntent().getExtras().getString("vehicleId");
        DriverId=getIntent().getExtras().getString("DriverId");
        strVehicleStatus = getIntent().getExtras().getString("VehicleStatus");
        strDriverId = getIntent().getExtras().getString("DriverId");

        if ("I".equals(strVehicleStatus)){
            if ("0".equals(strDriverId) || "".equals(strDriverId)){
                new CustomToast(MoreDetailsActivity.this, getResources().getString(R.string.strNodriver));

            }else {
                new CustomToast(MoreDetailsActivity.this, getResources().getString(R.string.strWaitingForAdminApproval));
            }
        }

        new AnalyticsClass(MoreDetailsActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvDetails));
        rlBackLayout = findViewById(R.id.rlBackLayout);
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);
        rightTopBarText= findViewById(R.id.rightTopBarText);
        rightTopBarText.setVisibility(View.GONE);
        ibEditMenu = findViewById(R.id.ibEditMenu);
        ibEditMenu.setVisibility(View.VISIBLE);

        viewpager = findViewById(R.id.viewpager);
        tabs = findViewById(R.id.tabs);
        //Adding the tabs using addTab() method
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.tvVehicleInfo)));
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.tvDriverInfo)));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        //Creating our pager adapter
        final MoreVehicleListPager adapter = new MoreVehicleListPager(getSupportFragmentManager(), tabs.getTabCount(),vehicleId,DriverId);
        //Adding adapter to pager
        viewpager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        tabs.setOnTabSelectedListener(this);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        bottomview.BottomView(MoreDetailsActivity.this,StaticClass.Menu_profile);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        ibEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag= StaticClass.transitionflagNext;
                Intent editIntent = new Intent(MoreDetailsActivity.this, MoreEditDetailsActivity.class);
                editIntent.putExtra("vehicleId", vehicleId );
                editIntent.putExtra("driverId", DriverId);
                startActivity(editIntent);
                finish();

            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(MoreDetailsActivity.this, transitionflag);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.MyVehicleAddUpdate) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile || StaticClass.MyVehicleIsDelete){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) MoreDetailsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
