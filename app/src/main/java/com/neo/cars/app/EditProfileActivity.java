package com.neo.cars.app;

import android.Manifest;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.EditProfileSave_Interface;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.EditProfile_Webservice;
import com.neo.cars.app.Webservice.MyProfile_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;


/**
 * Created by parna on 6/3/18.
 */

public class EditProfileActivity extends RootActivity implements CallBackButtonClick/*, StateListInterface , CityList_Interface */, Profile_Interface/*, DocumentListInterface*/ , EditProfileSave_Interface{

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private ScrollView scrollview;
    private RelativeLayout rlSpnrDocumentList, rlAadharCard, rlUploadDoc, rlParentRelativeLayout, rlNameRelativeLayout,
            rlLastNameRelativeLayout,  rlMobNoRelativeLayout, rlDobRelativeLayout, rlGenderRelativeLayout,
            rlNationalityRelativeLayout, rlEmailRelativeLayout, rlAddressRelativeLayout, rlFathersNameRelativeLayout,
            rlEmergencyContactNameRelativeLayout, rlEmergencyContactNoRelativeLayout, rlOccupationRelativeLayout,
            rlCompanyRelativeLayout;
    private CircularImageViewBorder civUserProfilePic;
    private CustomEditTextTitilliumWebRegular etFirstName, etLastName, etAddress, etFathersName,
            etEmergencyContactName, etEmergencyContactNo, etCompany;
    private CustomTextviewTitilliumWebRegular etOccupation, etDob, etMobNo, etEmail;
    private CustomButtonTitilliumSemibold btnProfileSave;
    private CustomTitilliumTextViewSemiBold tvConfPassword;

    private Context context;
    private File file, fileProPic, fileAadharCard, fileAadharCardBack, fileOtherDoc, fileOtherDocBack;
    private String strDocumentCode="", strDocumentName="", strStateId="", strStateName="", strCityId="", strCityName="", userChoosenTask="",
            mSelectedGalleryImagePath="", strName="", strMob="", strGender="", strNationality="", strEmail="", strAddress="",
            strFathersName="", strEmergencyName="", strEmergencyNo="", strCompany="", strDob="",
            strSelectedGender="M", strProfileEditable="", strProfileEditableMessage="";
    private FrameLayout flSpnrState,  flSpnrCity, flSpnrDoc;
    private NoDefaultSpinner spnrGender;
    private CustomTextviewTitilliumWebRegular spnrState,  spnrCity, spnrDoc;
    private ImageView ivAadhaarCard, ivAadhaarCardBack, ivSecDoc, ivSecDocBack, ivContactIconEditProfile, ivEmergencyContactIconEditProfile;
    boolean isIvContactIconEditProfileClicked = false, isIvEmergencyContactIconEditProfileClicked = false;

    private CustomTextviewTitilliumBold tvAdhaarCard, tvAdhaarCardBack, tvSecondaryDocument, tvSecondaryDocumentBack, etNationality;
    private TextView tvNameHeader, tvLastNameHeader,  tvMobNoHeader, tvDobHeader, tvEmailHeader, tvStateHeader, tvCityHeader, tvAddressHeader, tvEmergencyContactNameHeader, tvEmergencyContactNoHeader;
    private String strNameHeader="", strLastNAmeHeader="",  strOtherOccupation="", strMobNoHeader="", strDobHeader="", strEmailHeader="", strStateHeader="", strCityHeader="", strAddressHeader="";
    private String colored="", strOccupationId="", strOccupationName="";
    private SpannableStringBuilder builder;
    private int start, end;
    private String strFirstName="", strLastName="", strEmergencyContactNameHeader="", strEmergencyContactNoHeader="";

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int PICK_IMAGE_REQUEST = 901;
    public static final int StateListRequestCode = 701;
    public static final int CityListRequestCode = 702;
    public static final int DocumentListRequestCode = 703;
    public static final int NationalityListRequestCode = 704;
    public static final int OccupationListRequestCode = 705;
    private static final int REQUEST_CODE_PICK_CONTACTS = 101;

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;

    private int transitionflag = StaticClass.transitionflagNext;

    private SharedPrefUserDetails sharedPref;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;

    private String strFileType="";
//    private ArrayList<String> photos;
    private ConnectionDetector cd;
    private UserLoginDetailsModel M_userLoginDetailsModel;
    private String strUserLoginDetails;
    private ArrayAdapter<String> arrayAdapterGender;
    private UserLoginDetailsModel muserLoginDetailsModel;
    private RelativeLayout rlOtherOccupationRelativeLayout;
    private CustomEditTextTitilliumWebRegular etOtherOccupation;
    private Calendar mcalendar;
    private int day,month,year;

    private Uri uriContact;
    private String contactID;     // contacts unique ID

    private String[] gender = { "Male", "Female"};
    private ProgressDialog dialog;
    private Uri imageUri;

    private BottomView bottomview = new BottomView();

    private CheckBox cb_terms;
    private TextView tvByLoggingIntoText_privacypolicy;
    private TextView tvTermsAndCondition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.activity_editprofile);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        sharedPref = new SharedPrefUserDetails(EditProfileActivity.this);

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        new AnalyticsClass(EditProfileActivity.this);

        Initialize();
        Listener();

        //Fetch my profile details
        if (cd.isConnectingToInternet()){
            new MyProfile_Webservice().MyProfile(EditProfileActivity.this);

        }else{
//            new CustomToast(this, MessageText.Network_not_availabl);
            transitionflag = StaticClass.transitionflagBack;
            startActivity(new Intent(EditProfileActivity.this, NetworkNotAvailable.class));

        }
    }

    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);

        tvNameHeader = findViewById(R.id.tvNameHeader);
        tvLastNameHeader = findViewById(R.id.tvLastNameHeader);
        tvMobNoHeader = findViewById(R.id.tvMobNoHeader);
        tvDobHeader = findViewById(R.id.tvDobHeader);
        tvEmailHeader = findViewById(R.id.tvEmailHeader);
        tvStateHeader = findViewById(R.id.tvStateHeader);
        tvCityHeader = findViewById(R.id.tvCityHeader);
        tvAddressHeader = findViewById(R.id.tvAddressHeader);
        tvEmergencyContactNameHeader = findViewById(R.id.tvEmergencyContactNameHeader);
        tvEmergencyContactNoHeader = findViewById(R.id.tvEmergencyContactNoHeader);

        scrollview = findViewById(R.id.scrollview);
        rlParentRelativeLayout = findViewById(R.id.rlParentRelativeLayout);
        rlNameRelativeLayout = findViewById(R.id.rlNameRelativeLayout);
        rlLastNameRelativeLayout = findViewById(R.id.rlLastNameRelativeLayout);
        rlMobNoRelativeLayout = findViewById(R.id.rlMobNoRelativeLayout);
        rlDobRelativeLayout = findViewById(R.id.rlDobRelativeLayout);
        rlGenderRelativeLayout = findViewById(R.id.rlGenderRelativeLayout);
        rlNationalityRelativeLayout = findViewById(R.id.rlNationalityRelativeLayout);
        rlEmailRelativeLayout = findViewById(R.id.rlEmailRelativeLayout);
        rlAddressRelativeLayout = findViewById(R.id.rlAddressRelativeLayout);
        rlFathersNameRelativeLayout = findViewById(R.id.rlFathersNameRelativeLayout);
        rlEmergencyContactNameRelativeLayout = findViewById(R.id.rlEmergencyContactNameRelativeLayout);
        rlEmergencyContactNoRelativeLayout = findViewById(R.id.rlEmergencyContactNoRelativeLayout);
        rlOccupationRelativeLayout = findViewById(R.id.rlOccupationRelativeLayout);
        rlCompanyRelativeLayout = findViewById(R.id.rlCompanyRelativeLayout);
        rlAadharCard = findViewById(R.id.rlAadharCard);
        rlUploadDoc = findViewById(R.id.rlUploadDoc);
        rlSpnrDocumentList = findViewById(R.id.rlSpnrDocumentList);

        civUserProfilePic = findViewById(R.id.civUserProfilePic);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etMobNo = findViewById(R.id.etMobNo);
        etDob = findViewById(R.id.etDob);
        etNationality = findViewById(R.id.etNationality);
        etEmail = findViewById(R.id.etEmail);
        etAddress = findViewById(R.id.etAddress);
        etFathersName = findViewById(R.id.etFathersName);
        etOccupation = findViewById(R.id.etOccupation);
        etCompany = findViewById(R.id.etCompany);
        etEmergencyContactName = findViewById(R.id.etEmergencyContactName);
        etEmergencyContactNo = findViewById(R.id.etEmergencyContactNo);

        tvAdhaarCard = findViewById(R.id.tvAdhaarCard);
        tvAdhaarCardBack = findViewById(R.id.tvAdhaarCardBack);

        tvSecondaryDocument  = findViewById(R.id.tvSecondaryDocument);
        tvSecondaryDocumentBack  = findViewById(R.id.tvSecondaryDocumentBack);

        flSpnrState = findViewById(R.id.flSpnrState);
        flSpnrCity = findViewById(R.id.flSpnrCity);
        flSpnrDoc = findViewById(R.id.flSpnrDoc);

        mcalendar = Calendar.getInstance();

        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

        spnrState = findViewById(R.id.spnrState);
        spnrCity = findViewById(R.id.spnrCity);
        spnrDoc = findViewById(R.id.spnrDoc);

        ivAadhaarCard = findViewById(R.id.ivAadhaarCard);
        ivAadhaarCardBack = findViewById(R.id.ivAadhaarCardBack);
        ivSecDoc = findViewById(R.id.ivSecDoc);
        ivSecDocBack = findViewById(R.id.ivSecDocBack);

        btnProfileSave = findViewById(R.id.btnProfileSave);
        tvConfPassword = findViewById(R.id.tvConfPassword);



        spnrGender = findViewById(R.id.spnrGender);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvProfileHeader));

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        ivContactIconEditProfile = findViewById(R.id.ivContactIconEditProfile);
        ivEmergencyContactIconEditProfile = findViewById(R.id.ivEmergencyContactIconEditProfile);

        rlOtherOccupationRelativeLayout = findViewById(R.id.rlOtherOccupationRelativeLayout);
        etOtherOccupation = findViewById(R.id.etOtherOccupation);

        bottomview.BottomView(EditProfileActivity.this,StaticClass.Menu_profile);

        tvTermsAndCondition = findViewById(R.id.tvByLoggingIntoText_termsservices);
        cb_terms= findViewById(R.id.cb_terms);
        tvByLoggingIntoText_privacypolicy= findViewById(R.id.tvByLoggingIntoText_privacypolicy);

        etMobNo.setText(UserLoginDetails.getMobile());
        etEmail.setText(UserLoginDetails.getEmail());

        try {
            Picasso.get().load(UserLoginDetails.getProfile_pic()).into(civUserProfilePic);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Gender value
        arrayAdapterGender = new ArrayAdapter<>(EditProfileActivity.this, R.layout.countryitem, gender);
        arrayAdapterGender.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrGender.setPrompt(getResources().getString(R.string.tvPleaseSelectGender));
        spnrGender.setAdapter(arrayAdapterGender);
        spnrGender.setSelection(0);

        spnrGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                spnrGender.setSelection(position);
                strGender = (String) spnrGender.getSelectedItem();
                Log.d("d", "Selected Gender: "+strGender);

                if ("Male".equalsIgnoreCase(strGender)){
                    strSelectedGender = "M";
                }else if ("Female".equalsIgnoreCase(strGender)){
                    strSelectedGender = "F";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

    }

    private void Listener(){

        rlDobRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoBDialog();
            }
        });

        tvTermsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagNext;
                Intent termsIntent = new Intent(EditProfileActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);

            }
        });
        tvByLoggingIntoText_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagNext;
                Intent termsIntent1 = new Intent(EditProfileActivity.this, CmsPageActivity.class);
                termsIntent1.putExtra("cms", StaticClass.PrivacyPolicy);
                startActivity(termsIntent1);

            }
        });

        tvConfPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagNext;
                Intent updatepasswordactivity = new Intent(EditProfileActivity.this, UpdatePasswordActivity.class);
                startActivity(updatepasswordactivity);
                finish();

            }
        });

        civUserProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strFileType = "Profilepic";
                selectImage(getResources().getString(R.string.msgUpldUserPic), strFileType);
            }
        });

        tvAdhaarCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strFileType = "AadharCard";
                selectImage(getResources().getString(R.string.msgUploadAdhaarCard), strFileType);
            }
        });


        tvAdhaarCardBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strFileType = "AadharCardBack";
                selectImage(getResources().getString(R.string.msgUploadAdhaarCard), strFileType);
            }
        });


        tvSecondaryDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("d", "Doc code::"+strDocumentCode);
                strFileType = "Other";


                if (TextUtils.isEmpty(strDocumentCode)) {

                    final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                            "Please select secondary document type" ,
                            context.getResources().getString(R.string.btn_ok),
                            "");
                    alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialogYESNO.dismiss();

                        }
                    });

                    alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialogYESNO.dismiss();
                        }
                    });

                    alertDialogYESNO.show();

//                    focusView = tvSecondaryDocument;
//                    cancel = true;

                }else {

                    selectImage(getResources().getString(R.string.msgUploadSecondaryDoc), strFileType);
                }
            }
        });


        tvSecondaryDocumentBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("d", "Doc code::"+strDocumentCode);
                strFileType = "OtherBack";


                if (TextUtils.isEmpty(strDocumentCode)) {

                    final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                            "Please select secondary document type" ,
                            context.getResources().getString(R.string.btn_ok),
                            "");
                    alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialogYESNO.dismiss();

                        }
                    });

                    alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialogYESNO.dismiss();
                        }
                    });

                    alertDialogYESNO.show();

//                    focusView = tvSecondaryDocument;
//                    cancel = true;

                }else {

                    selectImage(getResources().getString(R.string.msgUploadSecondaryDoc), strFileType);
                }
            }
        });


        ivEmergencyContactIconEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isIvEmergencyContactIconEditProfileClicked = true;
                isIvContactIconEditProfileClicked = false;

            }
        });

        btnProfileSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cb_terms.isChecked()) {

                etFirstName.setError(null);
                etLastName.setError(null);
//                etMobNo.setError(null);
//                etDob.setError(null);
//                etAddress.setError(null);
//                etEmergencyContactName.setError(null);
//                etEmergencyContactNo.setError(null);

                boolean cancel = false;
                View focusView = null;

                strFirstName = etFirstName.getText().toString().trim();
                strLastName = etLastName.getText().toString().trim();
                strMob = etMobNo.getText().toString().trim();
                strDob = etDob.getText().toString().trim();
                strNationality = etNationality.getText().toString().trim();
                strEmail = etEmail.getText().toString().trim();
                strAddress = etAddress.getText().toString().trim();
                strFathersName = etFathersName.getText().toString().trim();
                strEmergencyName = etEmergencyContactName.getText().toString().trim();
                strEmergencyNo = etEmergencyContactNo.getText().toString().trim();
                strCompany = etCompany.getText().toString().trim();
                strOtherOccupation = etOtherOccupation.getText().toString().trim();

                //Mob no validation
//                if (!"".equalsIgnoreCase(strMob)) {
//                    if (!(new Emailvalidation().phone_validation(strMob))) {
//                        etMobNo.setError(getString(R.string.error_telphone_format));
//                        focusView = etMobNo;
//                        cancel = true;
//                    }
//                }

                //Emergency contact no validation
                if (!TextUtils.isEmpty(strEmergencyNo)){
                    if (!(new Emailvalidation().phone_validation(strEmergencyNo))) {
                        etEmergencyContactNo.setError(getString(R.string.error_telphone_format));
                        focusView = etEmergencyContactNo;
                        cancel = true;
                    }
                }

                //Email validation
//                if (!"".equalsIgnoreCase(strEmail)) {
//                    if (!(new Emailvalidation().email_validation(strEmail))) {
//                        etEmail.setError(getString(R.string.error_email_format));
//                        focusView = etEmail;
//                        cancel = true;
//                    }
//                }

                if(TextUtils.isEmpty(strFirstName)){
                    etFirstName.setError(getString(R.string.error_field_required));
                    focusView = etFirstName;
                    cancel = true;

                }else if (TextUtils.isEmpty(strLastName)){
                    etLastName.setError(getString(R.string.error_field_required));
                    focusView = etLastName;
                    cancel = true;

                }
//                else if (TextUtils.isEmpty(strMob)){
//                    etMobNo.setError(getString(R.string.error_field_required));
//                    focusView = etMobNo;
//                    cancel = true;
//
//                }else if (TextUtils.isEmpty(strEmail)){
//                    etEmail.setError(getString(R.string.error_field_required));
//                    focusView = etEmail;
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(strAddress)){
//                    etAddress.setError(getString(R.string.error_field_required));
//                    focusView = etAddress;
//                    cancel = true;
//
//                }
//                else if (TextUtils.isEmpty(strEmergencyName)){
//                    etEmergencyContactName.setError(getString(R.string.error_field_required));
//                    focusView = etEmergencyContactName;
//                    cancel = true;
//
//                }else if (TextUtils.isEmpty(strEmergencyNo)){
//                    etEmergencyContactNo.setError(getString(R.string.error_field_required));
//                    focusView = etEmergencyContactNo;
//                    cancel = true;
//
//                }

                else if (cancel) {
                    // form field with an error.
                    focusView.requestFocus();

                }else if(fileOtherDoc != null) {            //if secondary document is uploaded, document type will be mandatory.

                        saveEditProfile();

                }else {
                    saveEditProfile();
                }


                }else{
                    new CustomToast(EditProfileActivity.this, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));
                }



            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(EditProfileActivity.this,
                        EditProfileActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        EditProfileActivity.this.getResources().getString(R.string.yes),
                        EditProfileActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();

            }
        });

        spnrState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                Intent stateintent = new Intent(EditProfileActivity.this, StateList.class);
                startActivityForResult(stateintent, StateListRequestCode);

            }
        });

        spnrCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strStateId != null && !strStateId.equals("")) {
                    transitionflag = StaticClass.transitionflagNext;
                    Intent cityintent = new Intent(EditProfileActivity.this, CityList.class);
                    cityintent.putExtra("strStateId", strStateId);
                    startActivityForResult(cityintent, CityListRequestCode);
                }else {
                    new CustomToast(EditProfileActivity.this, "Please select State first!");
                }
            }
        });

        spnrDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                Intent stateintent = new Intent(EditProfileActivity.this, DocumentList.class);
                startActivityForResult(stateintent, DocumentListRequestCode);
            }
        });

        etNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                Intent stateintent = new Intent(EditProfileActivity.this, NationalityListActivity.class);
                startActivityForResult(stateintent, NationalityListRequestCode);
            }
        });

        etOccupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagNext;
                Intent occupationintent = new Intent(EditProfileActivity.this, OccupationList.class);
                startActivityForResult(occupationintent, OccupationListRequestCode);
            }
        });
    }

    public void saveEditProfile(){
        if (cd.isConnectingToInternet()) {
            Log.d("fileOtherDoc***", "" + fileOtherDoc);
            new EditProfile_Webservice().EditProfile(EditProfileActivity.this, strFirstName, strLastName, strMob, strDob, strSelectedGender,
                    strNationality, strEmail, strAddress, strFathersName, strEmergencyName, strEmergencyNo, strCompany, fileProPic, strStateId,
                    strCityId, strOccupationId, strOtherOccupation, fileAadharCard, fileAadharCardBack, fileOtherDoc, fileOtherDocBack, strDocumentCode);

        } else {
            Intent i = new Intent(EditProfileActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    public void selectImage(final String strMessage, String filetype) {


        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(EditProfileActivity.this,
                EditProfileActivity.this,
                EditProfileActivity.this,
                strMessage,
                EditProfileActivity.this.getResources().getString(R.string.Camera),
                EditProfileActivity.this.getResources().getString(R.string.Gallery));

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(permissions.length == 0){
            return;
        }
        boolean allPermissionsGranted = true;
        if(grantResults.length>0){
            for(int grantResult: grantResults){
                if(grantResult != PackageManager.PERMISSION_GRANTED){
                    allPermissionsGranted = false;
                    break;
                }
            }
        }
        if(!allPermissionsGranted){
            boolean somePermissionsForeverDenied = false;
            for(String permission: permissions){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                    //denied
                    Log.e("denied", permission);
                }else{
                    if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        Log.e("allowed", permission);
                    } else{
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        somePermissionsForeverDenied = true;
                    }
                }
            }
            if(somePermissionsForeverDenied){

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Permissions Required")
                        .setMessage("You have forcefully denied some of the required permissions " +
                                "for this action. Please open settings, go to permissions and allow them.")
                        .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }
        } else {
            switch (requestCode) {
                //act according to the request code used while requesting the permission(s).
            }
        }

    }

    public void DoBDialog(){
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" +monthOfYear);
                etDob.setText(year + "-" + (monthOfYear+1) + "-" +dayOfMonth );
                Log.d("d", "String Dob::"+etDob.getText().toString().trim());
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(context, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(mcalendar.getTimeInMillis());
        dpDialog.show();
    }


    //take photo through camera
    private void takePhoto() {

        //not getting path in mi device
//        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24

        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

//        try {
//
//            PhotoPicker.builder()
//                    .setPhotoCount(1)
//                    .setShowCamera(true)
//                    .setShowGif(false)
//                    .setPreviewEnabled(false)
//                    .start(this, PICK_IMAGE_REQUEST);
//
//        } catch (Exception e) {
//            //To catch unknown Null pointer exception inside library
//            e.printStackTrace();
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("resultCode: "+resultCode);
        if (resultCode == RESULT_OK) {
            System.out.println("requestCode: "+requestCode);
            System.out.println("data: "+data);
            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:
                    //added later code taken by tanmay da

                    dialog = new ProgressDialog(context);
                            try {

                                dialog.setMessage("Image processing...");
                                dialog.show();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        try
                                        {
                                            Bitmap fullImage = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);

                                            InputStream input = context.getContentResolver().openInputStream(imageUri);
                                            ExifInterface ei;
                                            if (Build.VERSION.SDK_INT > 23)
                                                ei = new ExifInterface(input);
                                            else
                                                ei = new ExifInterface(imageUri.getPath());

                                            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                            switch (orientation) {
                                                case ExifInterface.ORIENTATION_ROTATE_90:
                                                    fullImage = rotateImage(fullImage, 90);
                                                    break;
                                                case ExifInterface.ORIENTATION_ROTATE_180:
                                                    fullImage = rotateImage(fullImage, 180);
                                                    break;
                                                case ExifInterface.ORIENTATION_ROTATE_270:
                                                    fullImage = rotateImage(fullImage, 270);
                                                    break;
                                                default:
                                                    break;
                                            }

                                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                            fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                            byte[] byteArray = bytes.toByteArray();

                                            Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);


                                            // Before doing something that requires a lot of memory,
                                            // check to see whether the device is in a low memory state.
                                            ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();

                                            if (!memoryInfo.lowMemory) {
                                                file = ImageUtils.saveImage(scaledBitmap, context);
                                            }else{
                                                new CustomToast(EditProfileActivity.this, "Your device is running on low memory, Please free memory.");
                                            }



                                            if (file != null) {
                                                Log.d("d", "Camera image file:" + file);

                                                if (strFileType == "AadharCard"){
                                                    fileAadharCard=file;
                                                    Picasso.get().load(file).resize(200, 200).into(ivAadhaarCard);

                                                }if (strFileType == "AadharCardBack"){
                                                    fileAadharCardBack=file;
                                                    Picasso.get().load(file).resize(200, 200).into(ivAadhaarCardBack);

                                                }else if (strFileType == "Profilepic"){
                                                    fileProPic=file;
                                                    Picasso.get().load(file).resize(200, 200).into(civUserProfilePic);

                                                }else if (strFileType == "Other") {
                                                    fileOtherDoc=file;
                                                    Picasso.get().load(file).resize(200, 200).into(ivSecDoc);

                                                }else if (strFileType == "OtherBack") {
                                                    fileOtherDocBack=file;
                                                    Picasso.get().load(file).resize(200, 200).into(ivSecDocBack);
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                        }
                                        finally{
                                            dialog.dismiss();
                                        }
                                    }
                                }, 4000);  // 4 sec to allow processing of image captured

                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                            }

                    break;

                case PICK_IMAGE_REQUEST:

                    try {

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {
                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }


                        if (strFileType == "AadharCard"){
//                            Log.d("d", "Clicked on adhaar card....");
//                            mSelectedGalleryImagePath = "file://" + photos.get(0);
//                            Log.d("d", "Aadhar card image path : " + mSelectedGalleryImagePath);
//                            fileAadharCard= new File(photos.get(0));
                            fileAadharCard = file;
                            Picasso.get().load(file).resize(200, 200).into(ivAadhaarCard);

                        }if (strFileType == "AadharCardBack"){
//                            Log.d("d", "Clicked on adhaar card....");
//                            mSelectedGalleryImagePath = "file://" + photos.get(0);
//                            Log.d("d", "Aadhar card image path : " + mSelectedGalleryImagePath);
                            fileAadharCardBack = file;
                            Picasso.get().load(file).resize(200, 200).into(ivAadhaarCardBack);

                        }else if (strFileType == "Profilepic"){
//                            Log.d("d", "Clicked on profile pic....");
//                            mSelectedGalleryImagePath = "file://" + photos.get(0);
//                            Log.d("d", "Profile pic image path : " + mSelectedGalleryImagePath);
                            fileProPic = file;
                            Log.d("d", "Gallery Image File::" + fileProPic);
                            Picasso.get().load(file).resize(200, 200).into(civUserProfilePic);

                        }else if (strFileType == "Other") {

//                            mSelectedGalleryImagePath = "file://" + photos.get(0);
//                            Log.d("d", "Document path : " + mSelectedGalleryImagePath);
                            fileOtherDoc= file;
                            Picasso.get().load(file).resize(200, 200).into(ivSecDoc);
                        }else if (strFileType == "OtherBack") {

//                            mSelectedGalleryImagePath = "file://" + photos.get(0);
//                            Log.d("d", "Document path : " + mSelectedGalleryImagePath);
                            fileOtherDocBack= file;
                            Picasso.get().load(file).resize(200, 200).into(ivSecDocBack);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case StateListRequestCode:

                    if (!data.getExtras().getString("StateName").equals("") && !data.getExtras().getString("StateId").equals("")) {
                        strStateId =data.getExtras().getString("StateId");
                        Log.d("State id: ", strStateId);
                        strStateName = data.getExtras().getString("StateName");
                        Log.d("State Name: ", strStateName);

                        spnrState.setText(strStateName);
                        spnrCity.setText("");
                        strCityId = "";
                        strCityName = "";
                    }

                    break;

                case CityListRequestCode:

                    if (!data.getExtras().getString("CityName").equals("") && !data.getExtras().getString("CityId").equals("")) {
                        strCityId =data.getExtras().getString("CityId");
                        Log.d("CityId id: ", strCityId);
                        strCityName = data.getExtras().getString("CityName");
                        Log.d("CityName: ", strCityName);

                        spnrCity.setText(strCityName);
                    }

                    break;

                case DocumentListRequestCode:

                    if (!data.getExtras().getString("DocumentName").equals("") && !data.getExtras().getString("DocumentCode").equals("")) {
                        strDocumentCode =data.getExtras().getString("DocumentCode");
                        Log.d("CityId id: ", strCityId);
                        strDocumentName = data.getExtras().getString("DocumentName");
                        Log.d("CityName: ", strCityName);

                        spnrDoc.setText(strDocumentName);
                    }

                    break;

                case NationalityListRequestCode:

                    if (!data.getExtras().getString("Nationality").equals("")) {
                        etNationality.setText(data.getExtras().getString("Nationality"));
                    }
                    break;
                case OccupationListRequestCode:

                    if(!data.getExtras().getString("OccupationName").equals("") && !data.getExtras().getString("OccupationId").equals("")){
                        strOccupationId = data.getExtras().getString("OccupationId");
                        Log.d("Occupation id: ", strOccupationId);

                        strOccupationName = data.getExtras().getString("OccupationName");
                        Log.d("Occupation name: ", strOccupationName);

                        etOccupation.setText(strOccupationName);

                        if("0".equals(strOccupationId)){
                            rlOtherOccupationRelativeLayout.setVisibility(View.VISIBLE);

                        }else{
                            rlOtherOccupationRelativeLayout.setVisibility(View.GONE);
                        }
                    }
                    break;

                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();
                    getContactNumber(context, data);
                    break;
            }
        }
    }


    // Get a MemoryInfo object for the device's current memory status.
    private ActivityManager.MemoryInfo getAvailableMemory() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo;
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void getContactNumber(final Context context, Intent data) {
        String selectedNumber = "";
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";

        List<String> allNumbers = new ArrayList<>();
        int contactIdColumnId, phoneColumnID, nameColumnID;
        try {
            Uri result = data.getData();
//            Log.e(TAG, result.toString());
            String id = result.getLastPathSegment();
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);
                    Log.e("Phone", phoneNumber);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
                    allNumbers.size();
                    cursor.moveToNext();
                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (allNumbers.size() > 1 && !allNumbers.isEmpty()) {
                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        String selectedNumber = items[item].toString().replaceAll("[^0-9\\+]", "");
                        Log.d("numberOnClick", selectedNumber);

                        if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {

//                            if(isIvContactIconEditProfileClicked) {
//                                etMobNo.setText(selectedNumber);
//                                etMobNo.requestFocus();
//                                isIvContactIconEditProfileClicked = false;
//                            }else

                                if (isIvEmergencyContactIconEditProfileClicked){
                                etEmergencyContactNo.setText(selectedNumber);
                                etEmergencyContactNo.requestFocus();
                                isIvEmergencyContactIconEditProfileClicked = false;
                            }

                        } else {
                            new CustomToast(EditProfileActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                        }
                    }
                });
                android.app.AlertDialog alert = builder.create();
                alert.show();

            } else {
                selectedNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                Log.d("number", selectedNumber);
                if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {

//                    if(isIvContactIconEditProfileClicked) {
//                        etMobNo.setText(selectedNumber);
//                        etMobNo.requestFocus();
//                        isIvContactIconEditProfileClicked = false;
//                    }else

                        if (isIvEmergencyContactIconEditProfileClicked){
                        etEmergencyContactNo.setText(selectedNumber);
                        etEmergencyContactNo.requestFocus();
                        isIvEmergencyContactIconEditProfileClicked = false;
                    }

                } else {
                    new CustomToast(EditProfileActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                }
            }
        }
    }

    @Override
    public void MyProfile(UserLoginDetailsModel userLoginDetailsModel) {

        muserLoginDetailsModel=new UserLoginDetailsModel();
        muserLoginDetailsModel=userLoginDetailsModel;

        if(!"".equals(userLoginDetailsModel.getFirst_name()) && !"null".equals(userLoginDetailsModel.getFirst_name())){
            etFirstName.setText(userLoginDetailsModel.getFirst_name());
        }

        if(!"".equals(userLoginDetailsModel.getLast_name()) && !"null".equals(userLoginDetailsModel.getLast_name())){
            etLastName.setText(userLoginDetailsModel.getLast_name());
        }

//        if (! userLoginDetailsModel.getMobile().equals("") && !userLoginDetailsModel.getMobile().equals("null")){
//            etMobNo.setText(userLoginDetailsModel.getMobile());
//        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getDob()) && !userLoginDetailsModel.getDob().equalsIgnoreCase("0000-00-00")){
            etDob.setText(userLoginDetailsModel.getDob());
        }

        if(! userLoginDetailsModel.getSex().equals("") && ! userLoginDetailsModel.getSex().equals("null")){
            if ("F".equals(userLoginDetailsModel.getSex())){
                spnrGender.setSelection(1);

            }else if ("M".equals(userLoginDetailsModel.getSex())){
                spnrGender.setSelection(0);
            }
        }

        if (! userLoginDetailsModel.getNationality().equals("") && !userLoginDetailsModel.getNationality().equals("null")){
            etNationality.setText(userLoginDetailsModel.getNationality());
        }else{
            etNationality.setText(getResources().getString(R.string.tvIndia));
        }

//        if (! userLoginDetailsModel.getEmail().equals("") && ! userLoginDetailsModel.getEmail().equals("null")){
//            etEmail.setText(userLoginDetailsModel.getEmail());
//        }

        if (! userLoginDetailsModel.getAddress().equals("") && ! userLoginDetailsModel.getAddress().equals("null")){
            etAddress.setText(userLoginDetailsModel.getAddress());
        }

        if (! userLoginDetailsModel.getFather_name().equals("") && ! userLoginDetailsModel.getFather_name().equals("null")){
            etFathersName.setText(userLoginDetailsModel.getFather_name());
        }

        if (!userLoginDetailsModel.getEmergency_contact_name().equals("") && !userLoginDetailsModel.getEmergency_contact_name().equals("null")){
            etEmergencyContactName.setText(userLoginDetailsModel.getEmergency_contact_name());
        }

        if (!userLoginDetailsModel.getEmergency_contact_number().equals("") && !userLoginDetailsModel.getEmergency_contact_number().equals("null")){
            etEmergencyContactNo.setText(userLoginDetailsModel.getEmergency_contact_number());
        }

        if (!userLoginDetailsModel.getOccupation_info().equals("") && !userLoginDetailsModel.getOccupation_info().equals("null")) {
            etOccupation.setText(userLoginDetailsModel.getOccupation_info());
        }else if(!userLoginDetailsModel.getOther_occupation().equals("") && !userLoginDetailsModel.getOther_occupation().equals("null")){
            etOccupation.setText(userLoginDetailsModel.getOther_occupation());
        }else{
            etOccupation.setText("N/A");
        }

        if (!userLoginDetailsModel.getCompany().equals("") && !userLoginDetailsModel.getCompany().equals("null")){
            etCompany.setText(userLoginDetailsModel.getCompany());
        }

        spnrState.setText(userLoginDetailsModel.getState_name());
        spnrCity.setText(userLoginDetailsModel.getCity_name());
        spnrDoc.setText(userLoginDetailsModel.getLast_document_name());

        if (! userLoginDetailsModel.getIs_profile_editable().equals("") && !userLoginDetailsModel.getIs_profile_editable().equals("null")){
            strProfileEditable = userLoginDetailsModel.getIs_profile_editable();
            Log.d("d", "is Profile editable: "+strProfileEditable);

            if("N".equals(strProfileEditable)){
                strProfileEditableMessage = userLoginDetailsModel.getProfile_noneditable_message();
                Log.d("d", "Profile noneditable message: "+strProfileEditableMessage);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(strProfileEditableMessage)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                transitionflag = StaticClass.transitionflagBack;
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }else if ("Y".equals(strProfileEditable)){
                etNationality.setEnabled(true);
                etAddress.setEnabled(true);
                etFathersName.setEnabled(true);
                etEmergencyContactName.setEnabled(true);
                etEmergencyContactNo.setEnabled(true);
                etOccupation.setEnabled(true);
                etCompany.setEnabled(true);

                System.out.println("getAadhar_document_url**********"+userLoginDetailsModel.getAadhar_document_url());
                System.out.println("getLast_document_url**********"+userLoginDetailsModel.getLast_document_url());

                try {
                    if(!TextUtils.isEmpty(userLoginDetailsModel.getAadhar_document_url()))
                    Picasso.get().load(userLoginDetailsModel.getAadhar_document_url()).into(ivAadhaarCard);
                    if (!TextUtils.isEmpty(userLoginDetailsModel.getAadhar_document_back_url()))
                    Picasso.get().load(userLoginDetailsModel.getAadhar_document_back_url()).into(ivAadhaarCardBack);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!TextUtils.isEmpty(userLoginDetailsModel.getLast_document_url()))
                    Picasso.get().load(userLoginDetailsModel.getLast_document_url()).into(ivSecDoc);
                    if (!TextUtils.isEmpty(userLoginDetailsModel.getLast_document_back_url()))
                    Picasso.get().load(userLoginDetailsModel.getLast_document_back_url()).into(ivSecDocBack);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                strCityId = userLoginDetailsModel.getCity_id() == null ? "":userLoginDetailsModel.getCity_id();
                strStateId=userLoginDetailsModel.getState_id() == null ? "":userLoginDetailsModel.getState_id();
                strDocumentCode = userLoginDetailsModel.getLast_document_type() == null ? "":userLoginDetailsModel.getLast_document_type();

                M_userLoginDetailsModel=new UserLoginDetailsModel();

                strUserLoginDetails= gson.toJson(userLoginDetailsModel);
                M_userLoginDetailsModel=gson.fromJson(strUserLoginDetails, UserLoginDetailsModel.class);
            }
        }
    }

    @Override
    public void editProfileSaveInterface(String msg) {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                msg ,
                context.getResources().getString(R.string.btn_ok),
                "");
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogYESNO.dismiss();
//                startActivity(new Intent(EditProfileActivity.this, FilterOnBookActivity.class));
                startActivity(new Intent(EditProfileActivity.this, SearchActivity.class));
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(EditProfileActivity.this, transitionflag);

    }

    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(EditProfileActivity.this,
                EditProfileActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                EditProfileActivity.this.getResources().getString(R.string.yes),
                EditProfileActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void onButtonClick(String strButtonText) {
//        boolean result = CommonUtility.checkPermission(context);
        if (strButtonText.equals(EditProfileActivity.this.getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";

//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    takePhoto();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }

        } else if (strButtonText.equals(EditProfileActivity.this.getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";

//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    mCallPhotoGallary();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();

//            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) EditProfileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}