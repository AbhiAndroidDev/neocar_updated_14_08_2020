package com.neo.cars.app.SetGet;

import java.util.ArrayList;

/**
 * Created by joydeep on 26/10/17.
 */

public class UserLoginDetailsModel {

    String id;
    String email;
    String mobile;
    String user_type;
    String ad_username;
    String is_active;
    String profile_pic;
    String reg_type;
    String referral_code;
    String is_notification_active;
    String is_profile_complete;
    String profile_name;
    String full_name;
    String address;
    String dob;
    String nationality;
    String father_name;
    String sex;
    String emergency_contact_name;
    String emergency_contact_number;
    String occupation;
    String occupation_info;
    String other_occupation;

    String profile_info;
    String state_name;
    String state_id;
    String city_id;
    String city_name;
    String mobile_no;
//    ArrayList<UserDocumentModel> arr_UserDocumentModel;
//    String last_document_type;
//    String last_document_url;
//    String aadhar_document_url;

    String last_document_type;
    String last_document_name;
    String last_document_url;
    String aadhar_document_url;

    String is_profile_editable;
    String profile_noneditable_message;
    String first_name;
    String last_name;
    String company;

    String mobile_verified;
    String email_verified;
    String profile_incomplete_message;

    String company_name;
    String company_email;
    String company_phone_number;
    String company_mobile_number;
    String company_address;
    String company_gst_number;
    String company_pan_number;
    String primary_contact_name;
    String primary_contact_mobile;
    String primary_mobile_verified;
    String primary_contact_id;
    String primary_id_status;
    String secondary_contact_name;
    String secondary_contact_mobile;
    String secondary_mobile_verified;
    String secondary_contact_id;
    String secondary_id_status;
    String company_pan_card;
    String bank_account_number;
    String ifsc_code;
    String cancel_check;
    String aadhar_document_back_url;
    String last_document_back_url;
    String primary_id_back, secondary_id_back;

    public String getPrimary_id_back() {
        return primary_id_back;
    }

    public void setPrimary_id_back(String primary_id_back) {
        this.primary_id_back = primary_id_back;
    }

    public String getSecondary_id_back() {
        return secondary_id_back;
    }

    public void setSecondary_id_back(String secondary_id_back) {
        this.secondary_id_back = secondary_id_back;
    }

    public String getLast_document_back_url() {
        return last_document_back_url;
    }

    public void setLast_document_back_url(String last_document_back_url) {
        this.last_document_back_url = last_document_back_url;
    }

    public String getAadhar_document_back_url() {
        return aadhar_document_back_url;
    }

    public void setAadhar_document_back_url(String aadhar_document_back_url) {
        this.aadhar_document_back_url = aadhar_document_back_url;
    }

//    public String getCompany_email() {
//        return company_email;
//    }
//
//    public void setCompany_email(String company_email) {
//        this.company_email = company_email;
//    }

    public String getCompany_mobile_number() {
        return company_mobile_number;
    }

    public void setCompany_mobile_number(String company_mobile_number) {
        this.company_mobile_number = company_mobile_number;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPrimary_contact_name() {
        return primary_contact_name;
    }

    public void setPrimary_contact_name(String primary_contact_name) {
        this.primary_contact_name = primary_contact_name;
    }

    public String getPrimary_contact_mobile() {
        return primary_contact_mobile;
    }

    public void setPrimary_contact_mobile(String primary_contact_mobile) {
        this.primary_contact_mobile = primary_contact_mobile;
    }

    public String getPrimary_mobile_verified() {
        return primary_mobile_verified;
    }

    public void setPrimary_mobile_verified(String primary_mobile_verified) {
        this.primary_mobile_verified = primary_mobile_verified;
    }

    public String getPrimary_contact_id() {
        return primary_contact_id;
    }

    public void setPrimary_contact_id(String primary_contact_id) {
        this.primary_contact_id = primary_contact_id;
    }

    public String getPrimary_id_status() {
        return primary_id_status;
    }

    public void setPrimary_id_status(String primary_id_status) {
        this.primary_id_status = primary_id_status;
    }

    public String getSecondary_contact_name() {
        return secondary_contact_name;
    }

    public void setSecondary_contact_name(String secondary_contact_name) {
        this.secondary_contact_name = secondary_contact_name;
    }

    public String getSecondary_contact_mobile() {
        return secondary_contact_mobile;
    }

    public void setSecondary_contact_mobile(String secondary_contact_mobile) {
        this.secondary_contact_mobile = secondary_contact_mobile;
    }

    public String getSecondary_mobile_verified() {
        return secondary_mobile_verified;
    }

    public void setSecondary_mobile_verified(String secondary_mobile_verified) {
        this.secondary_mobile_verified = secondary_mobile_verified;
    }

    public String getSecondary_contact_id() {
        return secondary_contact_id;
    }

    public void setSecondary_contact_id(String secondary_contact_id) {
        this.secondary_contact_id = secondary_contact_id;
    }

    public String getSecondary_id_status() {
        return secondary_id_status;
    }

    public void setSecondary_id_status(String secondary_id_status) {
        this.secondary_id_status = secondary_id_status;
    }

    public String getCompany_pan_card() {
        return company_pan_card;
    }

    public void setCompany_pan_card(String company_pan_card) {
        this.company_pan_card = company_pan_card;
    }

    public String getBank_account_number() {
        return bank_account_number;
    }

    public void setBank_account_number(String bank_account_number) {
        this.bank_account_number = bank_account_number;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public void setIfsc_code(String ifsc_code) {
        this.ifsc_code = ifsc_code;
    }

    public String getCancel_check() {
        return cancel_check;
    }

    public void setCancel_check(String cancel_check) {
        this.cancel_check = cancel_check;
    }

    public String getCompany_pan_number() {
        return company_pan_number;
    }

    public void setCompany_pan_number(String company_pan_number) {
        this.company_pan_number = company_pan_number;
    }

    public String getCompany_gst_number() {
        return company_gst_number;
    }

    public void setCompany_gst_number(String company_gst_number) {
        this.company_gst_number = company_gst_number;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCompany_phone_number() {
        return company_phone_number;
    }

    public void setCompany_phone_number(String company_phone_number) {
        this.company_phone_number = company_phone_number;
    }

    public String getProfile_incomplete_message() {
        return profile_incomplete_message;
    }

    public void setProfile_incomplete_message(String profile_incomplete_message) {
        this.profile_incomplete_message = profile_incomplete_message;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getReg_type() {
        return reg_type;
    }

    public void setReg_type(String reg_type) {
        this.reg_type = reg_type;
    }

    public String getReferral_code() {
        return referral_code;
    }

    public void setReferral_code(String referral_code) {
        this.referral_code = referral_code;
    }

    public String getIs_notification_active() {
        return is_notification_active;
    }

    public void setIs_notification_active(String is_notification_active) {
        this.is_notification_active = is_notification_active;
    }

    public String getIs_profile_complete() {
        return is_profile_complete;
    }

    public void setIs_profile_complete(String is_profile_complete) {
        this.is_profile_complete = is_profile_complete;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAd_username() {
        return ad_username;
    }

    public void setAd_username(String ad_username) {
        this.ad_username = ad_username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getEmergency_contact_name() {
        return emergency_contact_name;
    }

    public void setEmergency_contact_name(String emergency_contact_name) {
        this.emergency_contact_name = emergency_contact_name;
    }

    public String getEmergency_contact_number() {
        return emergency_contact_number;
    }

    public void setEmergency_contact_number(String emergency_contact_number) {
        this.emergency_contact_number = emergency_contact_number;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getProfile_info() {
        return profile_info;
    }

    public void setProfile_info(String profile_info) {
        this.profile_info = profile_info;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getLast_document_type() {
        return last_document_type;
    }

    public void setLast_document_type(String last_document_type) {
        this.last_document_type = last_document_type;
    }

    public String getLast_document_name() {
        return last_document_name;
    }

    public void setLast_document_name(String last_document_name) {
        this.last_document_name = last_document_name;
    }

    public String getLast_document_url() {
        return last_document_url;
    }

    public void setLast_document_url(String last_document_url) {
        this.last_document_url = last_document_url;
    }

    public String getAadhar_document_url() {
        return aadhar_document_url;
    }

    public void setAadhar_document_url(String aadhar_document_url) {
        this.aadhar_document_url = aadhar_document_url;
    }

    public String getIs_profile_editable() {
        return is_profile_editable;
    }

    public void setIs_profile_editable(String is_profile_editable) {
        this.is_profile_editable = is_profile_editable;
    }

    public String getProfile_noneditable_message() {
        return profile_noneditable_message;
    }

    public void setProfile_noneditable_message(String profile_noneditable_message) {
        this.profile_noneditable_message = profile_noneditable_message;
    }

    public String getOccupation_info() {
        return occupation_info;
    }

    public void setOccupation_info(String occupation_info) {
        this.occupation_info = occupation_info;
    }

    public String getOther_occupation() {
        return other_occupation;
    }

    public void setOther_occupation(String other_occupation) {
        this.other_occupation = other_occupation;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }


    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }


    public String getMobile_verified() {
        return mobile_verified;
    }

    public void setMobile_verified(String mobile_verified) {
        this.mobile_verified = mobile_verified;
    }

    public String getEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(String email_verified) {
        this.email_verified = email_verified;
    }

}