
package com.neo.cars.app.SetGet;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("in_employment_since")
    @Expose
    private String inEmploymentSince;
    @SerializedName("fathers_name")
    @Expose
    private String fathersName;
    @SerializedName("contact_no")
    @Expose
    private String contactNo;
    @SerializedName("marital_status")
    @Expose
    private String maritalStatus;
    @SerializedName("name_of_spouse")
    @Expose
    private String nameOfSpouse;
    @SerializedName("recent_photograph")
    @Expose
    private String recentPhotograph;
    @SerializedName("name_of_childrens")
    @Expose
    private String nameOfChildrens;
    @SerializedName("additional_info")
    @Expose
    private String additionalInfo;
    @SerializedName("document")
    @Expose
    private List<Document> document = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getInEmploymentSince() {
        return inEmploymentSince;
    }

    public void setInEmploymentSince(String inEmploymentSince) {
        this.inEmploymentSince = inEmploymentSince;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getNameOfSpouse() {
        return nameOfSpouse;
    }

    public void setNameOfSpouse(String nameOfSpouse) {
        this.nameOfSpouse = nameOfSpouse;
    }

    public String getRecentPhotograph() {
        return recentPhotograph;
    }

    public void setRecentPhotograph(String recentPhotograph) {
        this.recentPhotograph = recentPhotograph;
    }

    public String getNameOfChildrens() {
        return nameOfChildrens;
    }

    public void setNameOfChildrens(String nameOfChildrens) {
        this.nameOfChildrens = nameOfChildrens;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<Document> getDocument() {
        return document;
    }

    public void setDocument(List<Document> document) {
        this.document = document;
    }

}
