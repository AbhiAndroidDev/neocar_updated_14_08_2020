package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.InboxModal;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 23/3/18.
 */

public interface InboxModalInterface {
    public void OnInboxModal(ArrayList<InboxModal> arr_InboxModal);
}
