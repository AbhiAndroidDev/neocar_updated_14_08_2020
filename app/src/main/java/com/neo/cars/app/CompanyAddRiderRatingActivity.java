package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.AddRiderRatingInterface;
import com.neo.cars.app.SetGet.AddRiderRatingModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.AddRiderRatingWebService;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;

public class CompanyAddRiderRatingActivity extends AppCompatActivity implements AddRiderRatingInterface {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private CustomEditTextTitilliumWebRegular writeReview;
    private RelativeLayout rlBackLayout;
    //    LinearLayout sendAnonymmousReviewll;
    private int transitionflag = StaticClass.transitionflagNext;
    private RatingBar ratingForRider;
    private CustomButtonTitilliumSemibold save, cancel;
    private CheckBox checkBox;
    private String isCheckedAnonymous = "N";
    private float riderRating;
    private String strWriteReview = "", riderId = "", bookingId = "";
    private Context context;
    private BottomViewCompany bottomview = new BottomViewCompany();
    private SharedPrefUserDetails sharedPrefUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_add_rider_rating);

        new AnalyticsClass(CompanyAddRiderRatingActivity.this);

        initialize();
        listener();
    }

    private void initialize() {
        context = CompanyAddRiderRatingActivity.this;
        sharedPrefUser = new SharedPrefUserDetails(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        riderId = getIntent().getExtras().getString("rider_id");
        bookingId = getIntent().getExtras().getString("booking_id");

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvRatings));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        ratingForRider = findViewById(R.id.rating_rider);
//        sendAnonymmousReviewll = (LinearLayout) findViewById(R.id.sendAnonymmousReviewll);
//        checkBox = (CheckBox) findViewById(R.id.check_anonymous);
        save = findViewById(R.id.btnSaveRating);
        cancel = findViewById(R.id.btnCancelRating);
        writeReview = findViewById(R.id.writeReview);

        bottomview.BottomViewCompany(CompanyAddRiderRatingActivity.this, StaticClass.Menu_MyBookings_company);
    }

    private void listener() {


        ratingForRider.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                riderRating = ratingBar.getRating();
                Log.d("riderRating", ""+riderRating);
            }
        });


        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(CompanyAddRiderRatingActivity.this,
                        CompanyAddRiderRatingActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        CompanyAddRiderRatingActivity.this.getResources().getString(R.string.yes),
                        CompanyAddRiderRatingActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkValidation();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    private void checkValidation() {
        strWriteReview = writeReview.getText().toString().trim();
        String strRiderRating = ""+riderRating;

        if (riderRating == 0.0){
            new CustomToast(CompanyAddRiderRatingActivity.this, "Rider rating is mandatory");

        }else if (NetWorkStatus.isNetworkAvailable(context)){

            new AddRiderRatingWebService().addRiderRating(CompanyAddRiderRatingActivity.this, bookingId,
                    "CW", sharedPrefUser.getUserid(), strWriteReview, strRiderRating,
                    riderId, isCheckedAnonymous);

        }else {
            Intent i = new Intent(context, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.BottomProfile){
            finish();
        }

        if( StaticClass.MyBookingListFlag){
            finish();

        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyAddRiderRatingActivity.this, transitionflag);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(CompanyAddRiderRatingActivity.this,
                CompanyAddRiderRatingActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                CompanyAddRiderRatingActivity.this.getResources().getString(R.string.yes),
                CompanyAddRiderRatingActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();

            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();

    }

    @Override
    public void onAddRiderRating(ArrayList<AddRiderRatingModel> arrListaddRatingModel) {

        if (arrListaddRatingModel.size() > 0){
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }
}
