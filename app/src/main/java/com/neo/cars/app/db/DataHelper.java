package com.neo.cars.app.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper {

	private static final String DATABASE_NAME = "Neocars.db";
	private static final int DATABASE_VERSION = 1;

	private Context context;
	private SQLiteDatabase db;
	public OpenHelper openHelper;
	Cursor cursor;

	private static DataHelper dataHelperObj;

	private DataHelper(Context context) {

		this.context = context;
		openHelper = new OpenHelper(this.context);
		try {
			this.db = openHelper.getWritableDatabase();
		} catch (Exception e) {
			Log.d(null, "error: " + e);
		}
		// this.insertStmt = this.db.compileStatement(INSERT);
	}

	public static synchronized DataHelper getDataHelper(Context context) {
		if (dataHelperObj == null) {
			dataHelperObj = new DataHelper(context);
		}
		return dataHelperObj;
	}

	public long insert(String tableName, ContentValues cv) {

		return db.insert(tableName, null, cv);
	}

	public void insert(String sqlcommand) {

		 db.execSQL(sqlcommand);;
	}

	public void deleteAll(String tableName) {

		this.db.delete(tableName, null, null);
	}

	public int deleteData(String tableName, String where) {

		return this.db.delete(tableName, where, null);
	}

	public void myRawQuery(String query, String[] where) {

		db.execSQL(query);

		// return cur;
	}

	public String[][] selectAll(String tableName, String[] cols, String where,
			String orderBy) {

		try {
			cursor = this.db.query(tableName, cols, where, null, null, null,
					orderBy);
			int resCount = cursor.getCount();
			int colCount = cols.length;
			String[][] resList = new String[resCount][colCount];
			Log.d(null, "resCount: " + resCount + ", colCount: " + colCount);
			if (resCount != 0) {
				if (cursor.moveToFirst()) {
					int row = 0;
					do {
						for (int i = 0; i < colCount; i++) {
							resList[row][i] = cursor.getString(i);
						}
						row++;
					} while (cursor.moveToNext());
				}

				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
				}
			}
			return resList;
		} catch (Exception e) {
			String[][] res = { { "Operation Failed" } };
			return res;
		}
	}

	public Cursor selectCursor(String tableName, String[] cols, String where,
			String orderBy) {

		try {
			cursor = this.db.query(tableName, cols, where, null, null, null,
					orderBy);
			return cursor;
		} catch (Exception e) {
			Log.d(null, "Operation Failed" + e);
			return null;
		}
	}

	public int countData(String tableName, String where) {

		cursor = this.db.query(tableName, new String[] { "_id" }, where, null,
				null, null, null);
		return cursor.getCount();
	}

	public boolean updateAll(String tableName, ContentValues cv, String where) {
		boolean res = false;
		try {
			this.db.update(tableName, cv, where, null);
			res = true;
		} catch (SQLiteException e) {
			Log.d(null, "Update SQLiteException: " + e);
		}
		return res;
	}

	public void close() {

		// cursor.close();
		db.close();
		dataHelperObj = null;
	}

	private  class OpenHelper extends SQLiteOpenHelper {

		Context myNewContext;

		OpenHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			myNewContext = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// export from sql file
			try {
				InputStream assetsDB = myNewContext.getAssets().open(
						"NeocarsSql");
				BufferedReader in = new BufferedReader(new InputStreamReader(
						assetsDB));
				String line = "";
				
				while ((line = in.readLine()) != null) {
					db.execSQL(line.substring(0, line.lastIndexOf(";")));
				}
				
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			/*
			 * Log.w("Example",
			 * "Upgrading database, this will drop tables and recreate.");
			 * db.execSQL("DROP TABLE IF EXISTS " + CATEGORY_TABLE_NAME);
			 * db.execSQL("DROP TABLE IF EXISTS " + SUBCATEGORY_TABLE_NAME);
			 * db.execSQL("DROP TABLE IF EXISTS " + WORD_TABLE_NAME);
			 * onCreate(db);
			 */
		}
	}

	public Cursor rawQuery(String query, String[] selectionArgs) {
		return db.rawQuery(query, selectionArgs);
	}
}
