package com.neo.cars.app;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.Interface.CallBackCustomTimePicker;
import com.neo.cars.app.Interface.FilterVehicleBrand_Interface;
import com.neo.cars.app.Interface.FilterVehicleType_Interface;
import com.neo.cars.app.SetGet.BookingHoursModel;
import com.neo.cars.app.SetGet.VehicleBrandModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.BookingHours_Webservice;
import com.neo.cars.app.Webservice.FilterVehicleBrand_Webservice;
import com.neo.cars.app.Webservice.FilterVehicleType_Webservice;
import com.neo.cars.app.dialog.CustomTimePickerDialog1;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by parna on 13/3/18.
 */

public class FilterActivity extends AppCompatActivity implements BookingHours_Interface, FilterVehicleType_Interface, FilterVehicleBrand_Interface, CallBackCustomTimePicker {

    private RangeSeekBar<Integer> seekBar;
    private CustomTextviewTitilliumWebRegular tvFromDate, tvPickUp, tv_toolbar_title, min_price, max_price,
            tvPassengerNo, tvBaggageNo,tv_toolbar_clear;
    private CustomButtonTitilliumSemibold btnApply;
    private Toolbar toolbar;
    private ImageButton ibNavigateMenu;
    private Calendar mcalendar;
    private int day,month,year;
    boolean DateSelectFlag=false;
    private ConnectionDetector cd;
    private Context context;
    private FrameLayout flSpnrFilterDuration, flSpnrFilterVehicleType;
    private NoDefaultSpinner spnrFilterDuration, spnrFilterVehicleType, spnrFilterVehicleBrand;
    private LinearLayout llMinus, llPlus;
    private ArrayList<String> listOfBookingHour, listOfVehicleType, listOfVehicleBrand;
    private ArrayAdapter<String> arrayAdapterBookingHour, arrayAdapterVehicleType, arrayAdapterVehicleBrand;

    private String strAvailableFor="", strBookingHourLabel="", strBookingHourValue="", strVehicleTypeId="", strVehicleBrandId="",
            strVehicleTypeName="", strVehicleBrandName="", strPassengerText="", strMinValue= Integer.toString(StaticClass.minPrice),
            strSelectedMinValue = "", strMaxValue = "", strSelectedMaxValue="", strOvertimeAvailableCheck="", strFromDate="",
            strToDate="", strPickUptime="", strBaggageAllowance="" ;

    private CheckBox ivCheck;
    private CustomTimePickerDialog1 customTimePickerDialog;

    private int transitionflag = StaticClass.transitionflagNext;
    private int intPassenger = 0;
    private int intBaggageAllowance = 0;
    private boolean isSelected = true;
    private String format;
    private RelativeLayout rlBaggageIncreaseDecrease, rlBackLayout;
    private LinearLayout llBaggageIncreaseDecrease, llBaggageMinus, llBaggagePlus;
    private BottomView bottomview = new BottomView();
    private ArrayList<BookingHoursModel> arrlistBookingHours;
    private ArrayList<VehicleTypeModel> arrlistVehicleType;
    private ArrayList<VehicleBrandModel> arrlistVehicleBrand;
    private String monthName, strDay, strMonthname;
    private boolean isClickedDateP = false, isClearClicked = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        strSelectedMinValue=getIntent().getExtras().getString("MinValue");
        strSelectedMaxValue=getIntent().getExtras().getString("MaxValue");
        strFromDate=getIntent().getExtras().getString("FromDate");
        strToDate=getIntent().getExtras().getString("ToDate");
        strBookingHourValue=getIntent().getExtras().getString("Duration");
        strPickUptime=getIntent().getExtras().getString("PickUpTime");
        strVehicleTypeId=getIntent().getExtras().getString("VehicleType");
        strVehicleTypeName=getIntent().getExtras().getString("VehicleTypeName");
        strVehicleBrandName=getIntent().getExtras().getString("strVehicleBrandName");

        strPassengerText=getIntent().getExtras().getString("PassengerNo");
        strBaggageAllowance=getIntent().getExtras().getString("BaggageAllowance");

        strMonthname = getIntent().getStringExtra("monthName");
        strDay = getIntent().getStringExtra("day");

        Log.d("d", "**strPassengerText**"+strPassengerText);
        Log.d("d", "**strBaggageAllowance**"+strBaggageAllowance);

        strOvertimeAvailableCheck=getIntent().getExtras().getString("OvertimeAvailable");

        new AnalyticsClass(FilterActivity.this);

        Initialize();
        Listener();

        if(cd.isConnectingToInternet()){
            new BookingHours_Webservice().bookinghoursWebservice(FilterActivity.this, strAvailableFor );

        }else{
            Intent i = new Intent(FilterActivity.this, NetworkNotAvailable.class);
            transitionflag = StaticClass.transitionflagBack;
            startActivity(i);
        }

        if(cd.isConnectingToInternet()){
            new FilterVehicleType_Webservice().vehicletypeWebservice(FilterActivity.this);

            new FilterVehicleBrand_Webservice().vehicleBrandWebservice(FilterActivity.this);

        }else{
            Intent i = new Intent(FilterActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }

        Setvalue();
    }


    private void Setvalue(){

        tvFromDate.setText(strFromDate);
//        tvPickUp.setText(strPickUptime);
        tvPassengerNo.setText(strPassengerText);
        //etBaggageAllowance.setText(strBaggageAllowance);
        tvBaggageNo.setText(strBaggageAllowance);

        if (!TextUtils.isEmpty(strPickUptime)){
            convert12(strPickUptime);
        }else {
            tvPickUp.setText(getResources().getString(R.string.tvPickupTime));
        }

        if(strOvertimeAvailableCheck.equalsIgnoreCase("Y")){
            ivCheck.setChecked(true);
        }
        else {
            ivCheck.setChecked(false);
        }
    }

    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvFilterHeader));

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);

        ivCheck = findViewById(R.id.ivCheck);

        rlBackLayout = findViewById(R.id.rlBackLayout);

        flSpnrFilterDuration = findViewById(R.id.flSpnrFilterDuration);
        spnrFilterDuration = findViewById(R.id.spnrFilterDuration);

        flSpnrFilterVehicleType = findViewById(R.id.flSpnrFilterVehicleType);
        spnrFilterVehicleType = findViewById(R.id.spnrFilterVehicleType);
        spnrFilterVehicleBrand = findViewById(R.id.spnrFilterVehicleBrand);

        llMinus = findViewById(R.id.llMinus);
        llPlus = findViewById(R.id.llPlus);
        tvPassengerNo = findViewById(R.id.tvPassengerNo);

        rlBaggageIncreaseDecrease = findViewById(R.id.rlBaggageIncreaseDecrease);
        llBaggageIncreaseDecrease = findViewById(R.id.llBaggageIncreaseDecrease);
        llBaggageMinus = findViewById(R.id.llBaggageMinus);
        llBaggagePlus = findViewById(R.id.llBaggagePlus);
        tvBaggageNo = findViewById(R.id.tvBaggageNo);
        tv_toolbar_clear= findViewById(R.id.tv_toolbar_clear);
        tv_toolbar_clear.setVisibility(View.VISIBLE);


        min_price= findViewById(R.id.min_price);
        max_price= findViewById(R.id.max_price);

        seekBar = findViewById(R.id.skbarPrice);

//        setSeekBar();


        tvFromDate = findViewById(R.id.tvFromDate);
       // tvToDate  = findViewById(R.id.tvToDate);
        //tvDuration = findViewById(R.id.tvDuration);
        tvPickUp = findViewById(R.id.tvPickUp);
        //tvTypeOfVehicle = findViewById(R.id.tvTypeOfVehicle);

        btnApply = findViewById(R.id.btnApply);

        mcalendar = Calendar.getInstance();
        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);
        monthName =  mcalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        bottomview.BottomView(FilterActivity.this, StaticClass.Menu_Explore);

    }

    private void setSeekBar(){

        seekBar.setRangeValues(Integer.parseInt(strMinValue), Integer.parseInt(strMaxValue));

        seekBar.setSelectedMinValue(Integer.parseInt(strSelectedMinValue));
        if (TextUtils.isEmpty(strSelectedMaxValue)) {
            seekBar.setSelectedMaxValue(Integer.parseInt(strMaxValue));
        }else {
            seekBar.setSelectedMaxValue(Integer.parseInt(strSelectedMaxValue));
        }

        min_price.setText(getResources().getString(R.string.Rs) + Integer.parseInt(strSelectedMinValue));

        if (TextUtils.isEmpty(strSelectedMaxValue)) {
            max_price.setText(getResources().getString(R.string.Rs) + Integer.parseInt(strMaxValue));
        }else{
            max_price.setText(getResources().getString(R.string.Rs) + Integer.parseInt(strSelectedMaxValue));
        }

        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                //Now you have the minValue and maxValue of your RangeSeekbar

                min_price.setText(getResources().getString(R.string.Rs)+minValue);
                max_price.setText(getResources().getString(R.string.Rs)+maxValue);

                strSelectedMinValue = String.valueOf(minValue);
                strSelectedMaxValue = String.valueOf(maxValue);

                Log.d("d", "Min value: "+strMinValue);
                Log.d("d", "Max value: "+strMaxValue);

            }
        });

        // Get noticed while dragging
        seekBar.setNotifyWhileDragging(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //StaticClass.BottomExplore = false;
        if(StaticClass.BottomExplore){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    public void convert12(String strTime)
    {

        String timeFinal = "";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(strTime);
            timeFinal = new SimpleDateFormat("hh:mm a").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        tvPickUp.setText(timeFinal);

    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        ibNavigateMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                Intent applyintent = new Intent();
                setResult(StaticClass.BackRequestCode,applyintent);
                finish();
            }
        });



       ivCheck.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               if(ivCheck.isChecked()){
                   strOvertimeAvailableCheck="Y";
               }
               else {
                   strOvertimeAvailableCheck="N";

               }

           }
       });

        llMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    intPassenger = Integer.parseInt(tvPassengerNo.getText().toString());
                    if (intPassenger > 0){
                        intPassenger = intPassenger - 1;
                        strPassengerText = String.valueOf(intPassenger);
                        tvPassengerNo.setText(strPassengerText);

                    }else{
                        new CustomToast(FilterActivity.this, getResources().getString(R.string.strPassengerNumberExceeding));
                    }

                }catch (NumberFormatException nfe){
                    nfe.printStackTrace();
                    System.out.println("Could not parse " + nfe);
                }


            }
        });

        llPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    intPassenger = Integer.parseInt(tvPassengerNo.getText().toString());
                    intPassenger = intPassenger+ 1;
                    strPassengerText = String.valueOf(intPassenger);
                    tvPassengerNo.setText(strPassengerText);

                } catch(NumberFormatException nfe) {
                    nfe.printStackTrace();
                    System.out.println("Could not parse " + nfe);
                }

            }
        });

        llBaggageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    intBaggageAllowance = Integer.parseInt(tvBaggageNo.getText().toString());
                    if (intBaggageAllowance > 0){
                        intBaggageAllowance = intBaggageAllowance - 1;
                        strBaggageAllowance = String.valueOf(intBaggageAllowance);
                        tvBaggageNo.setText(strBaggageAllowance);

                    }else{
                        new CustomToast(FilterActivity.this, getResources().getString(R.string.strBaggageAllowanceNumberExceeding));
                    }

                }catch (NumberFormatException nfe){
                    nfe.printStackTrace();
                    System.out.println("Could not parse: " + nfe);
                }
            }
        });

        llBaggagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    intBaggageAllowance = Integer.parseInt(tvBaggageNo.getText().toString());
                    intBaggageAllowance = intBaggageAllowance+ 1;
                    strBaggageAllowance = String.valueOf(intBaggageAllowance);
                    tvBaggageNo.setText(strBaggageAllowance);

                }catch (NumberFormatException nfe){
                    nfe.printStackTrace();
                    System.out.println("Could not parse " + nfe);
                }
            }
        });


        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // strBaggageAllowance = etBaggageAllowance.getText().toString();

                if (isClearClicked) {
                    StaticClass.isClearClicked = true;
                }else {
                    StaticClass.isClearClicked = false;
                }
                Intent applyintent = new Intent();
                applyintent.putExtra("MinValue", strSelectedMinValue);
                applyintent.putExtra("MaxValue", strSelectedMaxValue);
                applyintent.putExtra("FromDate", strFromDate );
                applyintent.putExtra("ToDate", strToDate );
                applyintent.putExtra("Duration", strBookingHourValue );
                applyintent.putExtra("PickUpTime", strPickUptime );
                applyintent.putExtra("VehicleType", strVehicleTypeId);
                applyintent.putExtra("VehicleTypeName", strVehicleTypeName);
                applyintent.putExtra("PassengerNo", strPassengerText);
                applyintent.putExtra("BaggageAllowance", strBaggageAllowance);
                applyintent.putExtra("OvertimeAvailable", strOvertimeAvailableCheck);
                applyintent.putExtra("strVehicleBrandId",strVehicleBrandId);
                applyintent.putExtra("strVehicleBrandName",strVehicleBrandName);
                applyintent.putExtra("MonthName",isClickedDateP?monthName:strMonthname);
                applyintent.putExtra("day", isClickedDateP?""+day:strDay);

                setResult(StaticClass.SearchFilterRequestCode, applyintent);
                finish();
            }
        });

        tv_toolbar_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isClearClicked = true;
                StaticClass.isClearClicked = true;
                strMinValue=Integer.toString(StaticClass.minPrice);
//                        strMaxValue=Integer.toString(StaticClass.maxPrice);
                        strSelectedMaxValue = strMaxValue;
                        strSelectedMinValue = strMinValue;
//                        strFromDate="";
                        strToDate="";
                        strBookingHourValue="";
                        strVehicleTypeId="";
                        strVehicleTypeName="";
                        strPassengerText="0";
                        strBaggageAllowance="0";
//                        strPickUptime="";
                        strOvertimeAvailableCheck="N";
                        strVehicleBrandId="";
                        strVehicleBrandName ="";

                Setvalue();
                setSeekBar();
                BookingHoursList();
                VehicleTypeList();
                vehicleBrandList();
            }
        });


        tvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateSelectFlag=false;
//                DoBDialog();
            }
        });

        tvPickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("d", "***tvStartTime***");
//                customTimePickerDialog = new CustomTimePickerDialog1(FilterActivity.this, 24,60,false);
//                customTimePickerDialog.show();
            }
        });
    }

    public void DoBDialog(){
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" +monthOfYear);

                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Date FromDate = sdf.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            tvFromDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            strFromDate = tvFromDate.getText().toString();
                            Log.d("d", "FromDate String 4: "+strFromDate);
                            day=dayOfMonth;
                            monthName =  StaticClass.MONTHS[monthOfYear];
                            isClickedDateP = true;

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(FilterActivity.this, listener, year, month, day);
       // dpDialog.getDatePicker().setMaxDate(mcalendar.getTimeInMillis());
        dpDialog.getDatePicker().setMinDate(mcalendar.getTimeInMillis());
        dpDialog.show();
    }

    @Override
    public void BookingHoursList(final ArrayList<BookingHoursModel> arr_listBookingHours) {
        arrlistBookingHours=new ArrayList<>();
        arrlistBookingHours=arr_listBookingHours;
        BookingHoursList();
    }

   private void  BookingHoursList(){
       int pos=-1;
       if (arrlistBookingHours != null){
           listOfBookingHour = new ArrayList<String>();
           if(arrlistBookingHours.size() > 0){
               for (int x=0; x<arrlistBookingHours.size(); x++){
                   listOfBookingHour.add(arrlistBookingHours.get(x).getLabel());
                   if(arrlistBookingHours.get(x).getValue().equals(strBookingHourValue)){
                       pos=x;
                   }
               }
               Log.d("d", "Array list vehicle booking hour size::"+arrlistBookingHours.size());
               arrayAdapterBookingHour = new ArrayAdapter<String>(context, R.layout.countryitem, listOfBookingHour);
               arrayAdapterBookingHour.setDropDownViewResource(R.layout.simpledropdownitem);
               spnrFilterDuration.setPrompt(getResources().getString(R.string.tvPlsSelectDuration));
               spnrFilterDuration.setAdapter(arrayAdapterBookingHour);

               spnrFilterDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                   @Override
                   public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

                       strBookingHourLabel = arrlistBookingHours.get(position).getLabel();
                       strBookingHourValue = arrlistBookingHours.get(position).getValue();

                   }

                   @Override
                   public void onNothingSelected(AdapterView<?> adapterView) {

                   }
               });

               if(pos!=-1){
                   spnrFilterDuration.setSelection(pos);
               }
           }
       }else{
           new CustomToast(this, getResources().getString(R.string.tvDurationNtFound));
       }
    }

    @Override
    public void VehicleTypeList(final ArrayList<VehicleTypeModel> arr_listVehicleType, String max_slider, String min_slider) {
        arrlistVehicleType=new ArrayList<>();
        arrlistVehicleType=arr_listVehicleType;

        if(!TextUtils.isEmpty(max_slider)){
            String arrMax_slider[] = max_slider.split("\\.");

            strMaxValue = arrMax_slider[0];
            setSeekBar();
        }

        VehicleTypeList();
    }

    private void VehicleTypeList(){
        int pos=-1;
        if (arrlistVehicleType != null){
            listOfVehicleType=new ArrayList<String>();
            if(arrlistVehicleType.size()>0){
                for (int x=0;x<arrlistVehicleType.size();x++){
                    listOfVehicleType.add(arrlistVehicleType.get(x).getType_name());
                    if(arrlistVehicleType.get(x).getType_name().equals(strVehicleTypeName)){
                        pos=x;
                    }
                }

                Log.d("d", "Array list vehicle type size::"+arrlistVehicleType.size());

                arrayAdapterVehicleType = new ArrayAdapter<String>(context, R.layout.countryitem, listOfVehicleType);
                arrayAdapterVehicleType.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrFilterVehicleType.setPrompt(getResources().getString(R.string.tvPlsSelectVehcileType));
                spnrFilterVehicleType.setAdapter(arrayAdapterVehicleType);

                //spnrVehicleType.setSelection(0);

                spnrFilterVehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        //  spCurrency.setTag(countryListModelArrayList.get(position));

                        strVehicleTypeId = arrlistVehicleType.get(position).getId();
                        Log.d("Vehicle type id: ", strVehicleTypeId);
                        strVehicleTypeName = arrlistVehicleType.get(position).getType_name();
                        Log.d("d", "Vehicle type name: " + strVehicleTypeName);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                spnrFilterVehicleType.setSelection(pos);
            }

        }else {
            new CustomToast(this, getResources().getString(R.string.tvVehicleTypeNotFound));
        }
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        Intent applyintent = new Intent();

        setResult(StaticClass.BackRequestCode,applyintent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(FilterActivity.this, transitionflag);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) FilterActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void VehicleBrandList(ArrayList<VehicleBrandModel> arrlist_VehicleBrand) {

        arrlistVehicleBrand = new ArrayList<>();
        arrlistVehicleBrand = arrlist_VehicleBrand;

        vehicleBrandList();
    }

    private void vehicleBrandList() {
        int pos=-1;
        if (arrlistVehicleBrand != null){
            listOfVehicleBrand=new ArrayList<String>();
            if(arrlistVehicleBrand.size()>0){
                for (int x=0;x<arrlistVehicleBrand.size();x++){
                    listOfVehicleBrand.add(arrlistVehicleBrand.get(x).getTitle());
                    if(arrlistVehicleBrand.get(x).getTitle().equals(strVehicleBrandName)){
                        pos=x;
                    }
                }

                Log.d("d", "Array list vehicle brand size::"+arrlistVehicleBrand.size());

                arrayAdapterVehicleBrand = new ArrayAdapter<String>(context, R.layout.countryitem, listOfVehicleBrand);
                arrayAdapterVehicleBrand.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrFilterVehicleBrand.setPrompt(getResources().getString(R.string.tvPlsSelectVehcileBrand));
                spnrFilterVehicleBrand.setAdapter(arrayAdapterVehicleBrand);

                spnrFilterVehicleBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        //  spCurrency.setTag(countryListModelArrayList.get(position));

                        strVehicleBrandId = arrlistVehicleBrand.get(position).getId();
                        Log.d("Vehicle type id: ", strVehicleBrandId);
                        strVehicleBrandName = arrlistVehicleBrand.get(position).getTitle();
                        Log.d("d", "Vehicle type name: " + strVehicleBrandName);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                spnrFilterVehicleBrand.setSelection(pos);
            }

        }else {
            new CustomToast(this, getResources().getString(R.string.tvVehicleBrandNotFound));
        }
    }

    @Override
    public void onButtonClick(TimePicker mTimePicker, int hour, int minute) {

        System.out.println("*****mTimePicker*****"+mTimePicker);
        System.out.println("*****hour*****"+hour);
        System.out.println("*****minute*****"+minute);

        Log.d("d", "***strPickUptime***"+strPickUptime);

        //here minute have 4 value : 0 for 00, 1 for 15, 2 for 30, 3 for 45

        if(minute == 0){
            minute = 0;
        }else if (minute == 1){
            minute = 15;
        }else if (minute == 2){
            minute = 30;
        }else if (minute == 3){
            minute = 45;
        }

        //strPickUptime to send in webservice

        String time24="";
        if (hour < 10 && minute == 0){
            strPickUptime = "0"+hour+":0"+minute;
        }else if (hour < 10){
            strPickUptime = "0"+hour+":"+minute;
        }else if (minute == 0){
            strPickUptime = hour+":0"+minute;
        }else{
            strPickUptime = hour+":"+minute;
        }

        if (hour == 0) {
            hour += 12;
            format = "AM";

        } else if (hour == 12) {
            format = "PM";

        } else if (hour > 12) {
            hour -= 12;
            format = "PM";

        } else {
            format = "AM";
        }

        strPickUptime = strPickUptime+" "+format;

        if (minute == 0){
            time24 = hour+":0"+minute+" "+format;
            if (hour < 10){
                time24 = "0"+hour+":0"+minute+" "+format;
            }
        }else if (hour < 10){
            time24 = "0"+hour+":"+minute+" "+format;
        }else {
            time24 = hour+":"+minute+" "+format;
        }
        tvPickUp.setText(time24);

        customTimePickerDialog.dismiss();
    }
}
