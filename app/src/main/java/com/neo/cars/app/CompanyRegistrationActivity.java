package com.neo.cars.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.media.ExifInterface;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.CurrentLatLong_Interface;
import com.neo.cars.app.LocationTracker.CurrentLatLong;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.CompanyRegistrationWebService;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public class CompanyRegistrationActivity extends RootActivity implements View.OnClickListener, CallBackButtonClick,
        CurrentLatLong_Interface {

    private static final String TAG = CompanyRegistrationActivity.class.getSimpleName();

    private View mView;
    private Context context;
    private ConnectionDetector cd;
    private CustomEditTextTitilliumWebRegular etNameOfCompany , etRegEmailCompany, etRegPhoneCompany, etRegMobileCompany, etCompanyGSTNumber,
            etCompanyPANNumber, etPrimaryContactPName, etPrimaryPhoneCompany, etSecondaryContactPName, etSecondaryPhoneCompany, etbankAccountNumber,
            etIfscCode;
   // private CustomEditTextTitilliumWebRegular  etRegPassword, etRegConfPassword;
    private CircularImageViewBorder ivCompanyProfilePic;
    private ImageView ivPrimaryContactId, ivPrimaryContactIdBack, ivSecondaryContactId, ivCompanyPANCard, ivCanceledCheque, ivContactIconMobileNo, ivContactIconOfficeNo, ivPCMobileNo, ivSCMobileNo,
            ivPrimaryContactIDUploaded, ivPrimaryContactIdImageBackUploaded, ivSecondaryContactIDUploaded,
            ivSecondaryContactIdBackUploaded, ivCompanyPANCardUploaded, ivCanceledChequeUploaded;

    private ImageView ivInfoComapnyNameReg, ivInfoComapnyAddressReg, ivInfoEmailIcon, ivInfoMobileNoCompReg, ivInfoOfficePhNoCompReg,
            ivInfoGSTNOReg, ivInfoPANNOReg, ivInfoPrimaryCPNIcon, ivInfoPrimaryContactIdReg, ivInfoPrimaryContactIdBack,
            ivInfoSecondaryContactIdReg, ivInfoSecondaryContactIdBack,ivSecondaryContactIdImageBack, ivInfoCompPANCardReg, ivInfoIFSCCodeReg, ivInfoCanceledChequeReg,
            ivInfoSecondaryCPIcon, ivInfobankAccountNumber;
    private CustomButtonTitilliumSemibold btnRegisterCompany;
    private TextView tvTermsAndCondition;

    private CustomTextviewTitilliumWebRegular tvPrimaryContactId, tvSecondaryContactId, tvSecondaryContactIdBack, tvCompanyPANCard, tvCanceledCheque,

    etRegisteredAddress; //etRegisteredAddress changed later from edit to text

    private String userChoosenTask="", imageFilePath="", strImage = "", strCompanyProfilePic = "", strPrimaryContactID = "",
            strSecondaryContactID, strPANCARD = "", strCancelledCheque = "", strSelectedCompanyProfilePic = "", strSelectedPrimaryContactID = "", strSelectedSecondaryContactID = "",
            strSelectedPANCARD = "", strSelectedCancelledCheque = "";

    private String strNameOfCompany = "", strRegisteredAddress = "", strRegEmailCompany = "", strRegPassCompany = "", strRegConfPassCompany = "", strRegPhoneCompany = "",
            strRegMobileCompany = "", strCompanyGSTNumber = "", strCompanyPANNumber = "", strPrimaryContactPName = "", strprimayPhoneCompany, strSecondaryContactPName, strSecondaryPhoneCompany, strbankAccountNumber, strIfscCode;

    public static final int REQUEST_IMAGE_CAPTURE = 1044;
    public static final int PICK_IMAGE_REQUEST = 905;

    private File file, fileCompanyProfilePic, filePrimaryContactID, filePrimaryContactIDBack, fileSecondaryContactID,
            fileSecondaryContactIDBack, filePANCARD, fileCancelledCheque;
    private int transitionflag = StaticClass.transitionflagNext;

    private ImageButton ibBackbutton;
    private CheckBox checkBox;
    private RelativeLayout checkForSameMNoll;
    private ProgressDialog dialog;
    private Uri imageUri;
    private Uri uriContact;
    private String strAddressFromLatLong = "";
    private String selectedNumber = "", strPhoneNo = "";
    private static final int REQUEST_CODE_PICK_CONTACTS = 114;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 569;
    private boolean isLocationEnableCancelled = false, isPermissionDenied  = false;
    private SharedPrefUserDetails prefs;
    private CheckBox cb_terms;
    private TextView tvByLoggingIntoText_privacypolicy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_registration);

        new AnalyticsClass(CompanyRegistrationActivity.this);

        prefs = new SharedPrefUserDetails(this);

        initialize();
        listener();

    }

    private void checkStuff(){
//        Log.d("check", ""+isLocationEnable() );

        if(!isLocationEnableCancelled) {
            if (isLocationEnable()) {

                if(!isPermissionDenied) {

                    checkLocationPermission();
                }
            } else {

                showAlertToEnable();
            }
        }
    }

    private void showAlertToEnable() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage("Please enable your device location.");
            dialog.setCancelable(false);
            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);


                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
//                    finish();
                    isLocationEnableCancelled = true;
                }
            });
            dialog.show();
    }

    private void checkLocationPermission() {

        if(NetWorkStatus.isNetworkAvailable(this)) {


                Dexter.withActivity(CompanyRegistrationActivity.this).withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        .withListener(new MultiplePermissionsListener() {

                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {

                                    Log.d("check", ""+isLocationEnable() );

//                                    isPermissionGranted(CompanyRegistrationActivity.this);

                                    new CurrentLatLong().currentlatlong(CompanyRegistrationActivity.this);
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
//                                    showAlert();
                                    showAlertLocation();
                                    isPermissionDenied = true;
                                    StaticClass.isLocationPermissionCanceled = true;
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */

                                token.continuePermissionRequest();
                                isPermissionDenied = true;

                            }
                        }).onSameThread().check();


        }else{
            startActivity(new Intent(this, NetworkNotAvailable.class));
        }
    }


    public void showAlertLocation(){

        if(!StaticClass.isLocationPermissionCanceled) {


            final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Permissions Required")
                    .setMessage("You have forcefully denied some of the required permissions " +
                            "for this action. Please open settings, go to permissions and allow them.")
                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }



    private void initialize() {

        context = CompanyRegistrationActivity.this;
        cd = new ConnectionDetector(this);

        etNameOfCompany = findViewById(R.id.etRegNameOfCompany);
        etRegisteredAddress = findViewById(R.id.etRegCompanyAddress);
        etRegEmailCompany = findViewById(R.id.etRegEmailCompany);
        //etRegPassword = findViewById(R.id.etRegPassword);
        //etRegConfPassword = findViewById(R.id.etRegConfPassword);
        etRegPhoneCompany = findViewById(R.id.etRegPhoneCompany);
        etRegMobileCompany = findViewById(R.id.etRegMobileCompany);
        etCompanyGSTNumber = findViewById(R.id.etCompanyGSTNumber);
        etCompanyPANNumber = findViewById(R.id.etCompanyPANNumber);
        etPrimaryContactPName = findViewById(R.id.etPrimaryContactPName);
        etPrimaryPhoneCompany = findViewById(R.id.etPrimaryPhoneCompany);
        etSecondaryContactPName = findViewById(R.id.etSecondaryContactPName);
        etSecondaryPhoneCompany = findViewById(R.id.etSecondaryPhoneCompany);
        etbankAccountNumber = findViewById(R.id.etbankAccountNumber);
        ivSecondaryContactIdImageBack = findViewById(R.id.ivSecondaryContactIdImageBack);
        etIfscCode = findViewById(R.id.etIfscCode);

        tvPrimaryContactId = findViewById(R.id.tvPrimaryContactId);
        tvSecondaryContactId = findViewById(R.id.tvSecondaryContactId);
        tvSecondaryContactIdBack = findViewById(R.id.tvSecondaryContactIdBack);
        tvCompanyPANCard = findViewById(R.id.tvCompanyPANCard);
        tvCanceledCheque = findViewById(R.id.tvCanceledCheque);
        tvTermsAndCondition = findViewById(R.id.tvTermsAndCondition);

        ivCompanyProfilePic = findViewById(R.id.ivCompanyProfilePic);

        ivPrimaryContactId = findViewById(R.id.ivPrimaryContactId);
        ivPrimaryContactIdBack = findViewById(R.id.ivPrimaryContactIdBack);
        ivSecondaryContactId = findViewById(R.id.ivSecondaryContactId);
        ivCompanyPANCard = findViewById(R.id.ivCompanyPANCard);
        ivCanceledCheque = findViewById(R.id.ivCanceledCheque);
        ivContactIconMobileNo = findViewById(R.id.ivContactIconMobileNo);
        ivContactIconOfficeNo = findViewById(R.id.ivContactIconOfficeNo);
        ivPCMobileNo = findViewById(R.id.ivPCMobileNo);
        ivSCMobileNo = findViewById(R.id.ivSCMobileNo);
        ivInfoComapnyNameReg = findViewById(R.id.ivInfoComapnyNameReg);
        ivInfoComapnyAddressReg = findViewById(R.id.ivInfoComapnyAddressReg);
        ivInfoEmailIcon = findViewById(R.id.ivInfoEmailIcon);
        ivInfoMobileNoCompReg = findViewById(R.id.ivInfoMobileNoCompReg);
        ivInfoOfficePhNoCompReg = findViewById(R.id.ivInfoOfficePhNoCompReg);
        ivInfoGSTNOReg = findViewById(R.id.ivInfoGSTNOReg);
        ivInfoPANNOReg = findViewById(R.id.ivInfoPANNOReg);
        ivInfoPrimaryCPNIcon = findViewById(R.id.ivInfoPrimaryCPNIcon);
        ivInfoPrimaryContactIdReg = findViewById(R.id.ivInfoPrimaryContactIdReg);
        ivInfoPrimaryContactIdBack = findViewById(R.id.ivInfoPrimaryContactIdBack);
        ivInfoSecondaryCPIcon = findViewById(R.id.ivInfoSecondaryCPIcon);
        ivInfoSecondaryContactIdReg = findViewById(R.id.ivInfoSecondaryContactIdReg);
        ivInfoSecondaryContactIdBack = findViewById(R.id.ivInfoSecondaryContactIdBack);
        ivInfoCompPANCardReg = findViewById(R.id.ivInfoCompPANCardReg);
        ivInfoIFSCCodeReg = findViewById(R.id.ivInfoIFSCCodeReg);
        ivInfoCanceledChequeReg = findViewById(R.id.ivInfoCanceledChequeReg);
        ivInfobankAccountNumber = findViewById(R.id.ivInfobankAccountNumber);
        ivPrimaryContactIDUploaded = findViewById(R.id.ivPrimaryContactIDUploaded);
        ivPrimaryContactIdImageBackUploaded = findViewById(R.id.ivPrimaryContactIdImageBack);
        ivSecondaryContactIDUploaded = findViewById(R.id.ivSecondaryContactIDUploaded);
        ivSecondaryContactIdBackUploaded = findViewById(R.id.ivSecondaryContactIdBack);
        ivCompanyPANCardUploaded = findViewById(R.id.ivCompanyPANCardUploaded);
        ivCanceledChequeUploaded = findViewById(R.id.ivCanceledChequeUploaded);

        btnRegisterCompany = findViewById(R.id.btnRegisterCompany);

        ibBackbutton = findViewById(R.id.ibBackbutton);
        checkBox = findViewById(R.id.checkForSameMNo);
        checkForSameMNoll = findViewById(R.id.checkForSameMNoll);

        cb_terms= findViewById(R.id.cb_terms);
        tvByLoggingIntoText_privacypolicy= findViewById(R.id.tvByLoggingIntoText_privacypolicy);
    }

    private void listener() {
        ivCompanyProfilePic.setOnClickListener(this);
        ivPrimaryContactId.setOnClickListener(this);
        ivPrimaryContactIdBack.setOnClickListener(this);
        ivSecondaryContactId.setOnClickListener(this);
        ivSecondaryContactIdBackUploaded.setOnClickListener(this);
        ivCompanyPANCard.setOnClickListener(this);
        ivCanceledCheque.setOnClickListener(this);

        ivInfoComapnyNameReg.setOnClickListener(this);
        ivInfoComapnyAddressReg.setOnClickListener(this);
        ivInfoEmailIcon.setOnClickListener(this);
        ivInfoMobileNoCompReg.setOnClickListener(this);
        ivInfoOfficePhNoCompReg.setOnClickListener(this);
        ivInfoGSTNOReg.setOnClickListener(this);
        ivInfoPANNOReg.setOnClickListener(this);
        ivInfoPrimaryCPNIcon.setOnClickListener(this);
        ivInfoPrimaryContactIdReg.setOnClickListener(this);
        ivInfoPrimaryContactIdBack.setOnClickListener(this);
        ivInfoSecondaryCPIcon.setOnClickListener(this);
        ivInfoSecondaryContactIdReg.setOnClickListener(this);
        ivInfoSecondaryContactIdBack.setOnClickListener(this);
        ivInfoCompPANCardReg.setOnClickListener(this);
        ivInfobankAccountNumber.setOnClickListener(this);
        ivInfoIFSCCodeReg.setOnClickListener(this);
        ivInfoCanceledChequeReg.setOnClickListener(this);

        btnRegisterCompany.setOnClickListener(this);
        ibBackbutton.setOnClickListener(this);
        tvTermsAndCondition.setOnClickListener(this);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked()) {

                    strRegMobileCompany = etRegMobileCompany.getText().toString().trim();
                    if (strRegMobileCompany != null && !strRegMobileCompany.equals("")) {
                        etPrimaryPhoneCompany.setText(strRegMobileCompany);
                    }else {
                        etRegMobileCompany.setError(null);
                    }

                }else {
                    etPrimaryPhoneCompany.setText("");
                }
            }
        });

        checkForSameMNoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    etPrimaryPhoneCompany.setText("");

                }else {
                    checkBox.setChecked(true);
                    strRegMobileCompany = etRegMobileCompany.getText().toString().trim();
                    if (strRegMobileCompany != null && !strRegMobileCompany.equals("")) {
                        etPrimaryPhoneCompany.setText(strRegMobileCompany);
                    }else {
                        etRegMobileCompany.setError(null);
                    }
                }
            }
        });

        etRegisteredAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (NetWorkStatus.isNetworkAvailable(CompanyRegistrationActivity.this)) {

                                        // Initialize Places.
                                        Places.initialize(getApplicationContext(), StaticClass.API_KEY);

                                        PlacesClient placesClient = Places.createClient(CompanyRegistrationActivity.this);

                                        try {

                                            // Set the fields to specify which types of place data to
                                            // return after the user has made a selection.
                                            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS);

                                            // Start the autocomplete intent.
                                            Intent intent = new Autocomplete.IntentBuilder(
                                                    AutocompleteActivityMode.FULLSCREEN, fields)
                                                    .setCountry("IN")
                                                    .build(CompanyRegistrationActivity.this);

                                            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                }
            }
        });

        tvByLoggingIntoText_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsIntent = new Intent(CompanyRegistrationActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.PrivacyPolicy);
                startActivity(termsIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkStuff();

//        if(openSettings) {
//
//            openSettings = false;
//            onRestart();
//        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tvTermsAndCondition:
                Intent termsIntent = new Intent(this, CompanyCmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);
                break;
            case R.id.ivCompanyProfilePic:

                strImage = "CompanyProfilePic";
                selectImage(R.string.upload_company_photo, strImage);

                break;
            case R.id.ivPrimaryContactId:
                strImage = "PrimaryContactId";
                selectImage(R.string.upload_primary_contactID, strImage);

                break;

            case R.id.ivPrimaryContactIdBack:
                strImage = "PrimaryContactIdBack";
                selectImage(R.string.upload_primary_contactID, strImage);

                break;
            case R.id.ivSecondaryContactId:
                strImage = "SecondaryContactId";
                selectImage(R.string.upload_secondary_contactID, strImage);

                break;

            case R.id.ivSecondaryContactIdBack:
                strImage = "SecondaryContactIdBack";
                selectImage(R.string.upload_secondary_contactID, strImage);

                break;
            case R.id.ivCompanyPANCard:
                strImage = "CompanyPANCard";
                selectImage(R.string.upload_company_pan_card, strImage);

                break;
            case R.id.ivCanceledCheque:
                strImage = "CanceledCheque";
                selectImage(R.string.upload_cancelled_cheque, strImage);
                break;

            case R.id.ivInfoComapnyNameReg:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoCompanyName));
                break;

            case R.id.ivInfoComapnyAddressReg:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoRegCompAddress));
                break;

            case R.id.ivInfoEmailIcon:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoEmail));
                break;

            case R.id.ivInfoMobileNoCompReg:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoContact));
                break;

            case R.id.ivInfoOfficePhNoCompReg:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoOfficePhone));
                break;

            case R.id.ivInfoGSTNOReg:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoGSTNo));
                break;


            case R.id.ivInfoPANNOReg:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoPANNo));
                break;

            case R.id.ivInfoPrimaryCPNIcon:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoPrimary));
                break;

            case R.id.ivInfoPrimaryContactIdReg:

                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoPrimaryIdUpload));
                break;

            case R.id.ivInfoPrimaryContactIdBack:

                new CustomToast(this, getResources().getString(R.string.infoPrimaryIdUploadBack));
                break;

            case R.id.ivInfoSecondaryCPIcon:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoSecondary));
                break;


            case R.id.ivInfoSecondaryContactIdReg:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoSecondaryIdUpload));
                break;

            case R.id.ivInfoSecondaryContactIdEPBack:
                new CustomToast(this, getResources().getString(R.string.infoSecondaryIdUploadBack));
                break;

            case R.id.ivInfoCompPANCardReg:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoPANCard));
                break;


            case R.id.ivInfobankAccountNumber:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoBank));
                break;

            case R.id.ivInfoIFSCCodeReg:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoIFSCCode));
                break;

            case R.id.ivInfoCanceledChequeReg:
                new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.infoCancelledCheque));
                break;

            case R.id.btnRegisterCompany:

                if(cb_terms.isChecked()) {
                    checkValidation();
                }else{
                    new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));
                }


                break;
            case R.id.ibBackbutton:
                transitionflag = StaticClass.transitionflagBack;
                Intent i = new Intent(CompanyRegistrationActivity.this, LoginStepOneActivity.class);
                startActivity(i);
                finish();
                break;
            default:
                break;
        }
    }



    private void checkValidation() {

        etNameOfCompany.setError(null);
        etRegisteredAddress.setError(null);
        etRegEmailCompany.setError(null);
        //etRegPassword.setError(null);
        //etRegConfPassword.setError(null);
        etRegPhoneCompany.setError(null);
        etRegMobileCompany.setError(null);
//        etCompanyGSTNumber.setError(null);
        etCompanyPANNumber.setError(null);
        etPrimaryContactPName.setError(null);
        etPrimaryPhoneCompany.setError(null);
        etSecondaryContactPName.setError(null);
        etSecondaryPhoneCompany.setError(null);
        etbankAccountNumber.setError(null);
        etIfscCode.setError(null);

        boolean cancel = false;
        View focusView = null;

        strNameOfCompany = etNameOfCompany.getText().toString().trim();
        strRegisteredAddress = etRegisteredAddress.getText().toString().trim();
        strRegEmailCompany = etRegEmailCompany.getText().toString().trim();
       // strRegPassCompany = etRegPassword.getText().toString().trim();
      //  strRegConfPassCompany = etRegConfPassword.getText().toString().trim();
        strRegPhoneCompany = etRegPhoneCompany.getText().toString().trim();
        strRegMobileCompany = etRegMobileCompany.getText().toString().trim();
        strCompanyGSTNumber = etCompanyGSTNumber.getText().toString().trim();
        strCompanyPANNumber = etCompanyPANNumber.getText().toString().trim();
        strPrimaryContactPName = etPrimaryContactPName.getText().toString().trim();
        strprimayPhoneCompany = etPrimaryPhoneCompany.getText().toString().trim();
        strSecondaryContactPName = etSecondaryContactPName.getText().toString().trim();
        strSecondaryPhoneCompany = etSecondaryPhoneCompany.getText().toString().trim();
        strbankAccountNumber = etbankAccountNumber.getText().toString().trim();
        strIfscCode = etIfscCode.getText().toString().trim();

        if(TextUtils.isEmpty(strNameOfCompany)) {
            etNameOfCompany.setError(getString(R.string.error_field_required));

        }else if (TextUtils.isEmpty(strRegisteredAddress)) {
            etRegisteredAddress.setError(getString(R.string.error_field_required));
            etRegisteredAddress.requestFocus();

        }else if(TextUtils.isEmpty(strRegEmailCompany)) {
            etRegEmailCompany.setError(getString(R.string.error_field_required));

        }
       /* else if (TextUtils.isEmpty(strRegPassCompany)) {
            etRegPassword.setError(getString(R.string.error_field_required));
            etRegPassword.requestFocus();

        }else if (strRegPassCompany.length() < 6){
            new CustomToast(CompanyRegistrationActivity.this, getString(R.string.minimum_length_six));
            etRegPassword.requestFocus();
        }
        else if(TextUtils.isEmpty(strRegConfPassCompany)) {
            etRegConfPassword.setError(getString(R.string.error_field_required));

        }*/else if (!strRegPassCompany.equals(strRegConfPassCompany)){
            new CustomToast(CompanyRegistrationActivity.this, getString(R.string.passwordnotmatched));

        }
        else if (TextUtils.isEmpty(strRegPhoneCompany)) {
            etRegPhoneCompany.setError(getString(R.string.error_field_required));
            etRegPhoneCompany.requestFocus();

        }else if (TextUtils.isEmpty(strRegMobileCompany)) {
            etRegMobileCompany.setError(getString(R.string.error_field_required));
            etRegMobileCompany.requestFocus();

        }else if (!(new Emailvalidation().phone_validation(strRegMobileCompany))) {
            new CustomToast(this, getResources().getString(R.string.txt_message_mobile_no));

        }
//      else if (TextUtils.isEmpty(strCompanyPANNumber)) {
//            etCompanyPANNumber.setError(getString(R.string.error_field_required));
//            etCompanyPANNumber.requestFocus();

//        }
        else if (TextUtils.isEmpty(strPrimaryContactPName)) {
            etPrimaryContactPName.setError(getString(R.string.error_field_required));
            etPrimaryContactPName.requestFocus();

        }else if(TextUtils.isEmpty(strprimayPhoneCompany)) {
            etPrimaryPhoneCompany.setError(getString(R.string.error_field_required));
            etPrimaryPhoneCompany.requestFocus();

        }else if (!(new Emailvalidation().phone_validation(strprimayPhoneCompany))) {
            new CustomToast(this, getResources().getString(R.string.txt_message_mobile_no));

        }else if (TextUtils.isEmpty(strSecondaryContactPName)) {
            etSecondaryContactPName.setError(getString(R.string.error_field_required));
            etSecondaryContactPName.requestFocus();

        }else if (TextUtils.isEmpty(strSecondaryPhoneCompany)) {
            etSecondaryPhoneCompany.setError(getString(R.string.error_field_required));
            etSecondaryPhoneCompany.requestFocus();

        }else if (!(new Emailvalidation().phone_validation(strSecondaryPhoneCompany))) {
            new CustomToast(this, getResources().getString(R.string.txt_message_mobile_no));

        }else if (strRegMobileCompany.equalsIgnoreCase(strSecondaryPhoneCompany)
                || strprimayPhoneCompany.equalsIgnoreCase(strSecondaryPhoneCompany)){

            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.phoneNoCheck));
        }
        else if(TextUtils.isEmpty(strbankAccountNumber)) {
            etbankAccountNumber.setError(getString(R.string.error_field_required));
            etbankAccountNumber.requestFocus();

        }else if (TextUtils.isEmpty(strIfscCode)) {
            etIfscCode.setError(getString(R.string.error_field_required));
            etIfscCode.requestFocus();

        }else if (strIfscCode.length() != 11){

            new CustomToast(CompanyRegistrationActivity.this, getString(R.string.minimum_length_11));
        }else if (fileCompanyProfilePic == null) {
            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.companyImgUploadMsg));

        }else if (filePrimaryContactID == null) {
            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.companyPCIdUploadMsg));

        }else if (filePrimaryContactIDBack == null) {
            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.companyPCIdUploadMsg));

        }
//        else if (fileSecondaryContactID == null) {
//            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.companySCIdUploadMsg));
//
//        }

        else if (filePANCARD == null) {
            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.companyPANUploadMsg));

        }else if (fileCancelledCheque == null) {
            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.companyCanChequeUploadMsg));
        }else {
            if(cd.isConnectingToInternet()){

                new CompanyRegistrationWebService().Registration(CompanyRegistrationActivity.this, strNameOfCompany, strRegisteredAddress, strRegEmailCompany,
                         strRegPhoneCompany, strRegMobileCompany, strCompanyGSTNumber, strCompanyPANNumber, strPrimaryContactPName,
                        strprimayPhoneCompany, strSecondaryContactPName, strSecondaryPhoneCompany, strbankAccountNumber, strIfscCode, fileCompanyProfilePic,
                        filePrimaryContactID, filePrimaryContactIDBack, fileSecondaryContactID, fileSecondaryContactIDBack,filePANCARD, fileCancelledCheque);

                /*new CompanyRegistrationWebService().Registration(CompanyRegistrationActivity.this, strNameOfCompany, strRegisteredAddress, strRegEmailCompany,
                        strRegPassCompany, strRegConfPassCompany, strRegPhoneCompany, strRegMobileCompany, strCompanyGSTNumber, strCompanyPANNumber, strPrimaryContactPName,
                        strprimayPhoneCompany, strSecondaryContactPName, strSecondaryPhoneCompany, strbankAccountNumber, strIfscCode, fileCompanyProfilePic,
                        filePrimaryContactID, filePrimaryContactIDBack, fileSecondaryContactID, fileSecondaryContactIDBack,filePANCARD, fileCancelledCheque);*/
            }else{
                Intent i = new Intent(CompanyRegistrationActivity.this, NetworkNotAvailable.class);
                startActivity(i);
            }
        }
    }


    public void selectImage(final int upload_company_photo, String filetype) {

        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(CompanyRegistrationActivity.this,
                CompanyRegistrationActivity.this,
                getResources().getString(upload_company_photo),
                CompanyRegistrationActivity.this.getResources().getString(R.string.Camera),
                CompanyRegistrationActivity.this.getResources().getString(R.string.Gallery));
    }


    @Override
    public void onButtonClick(String strButtonText) {
//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {
//                takePhoto();
//            }

            Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener()
                    {
                        @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if(report.areAllPermissionsGranted()){

                                takePhoto();
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                                showAlert();
                            }
                        }
                        @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                        {/* ... */

                            token.continuePermissionRequest();
                        }
                    }).check();


        } else if (strButtonText.equals(getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";
//            if (result) {
//                mCallPhotoGallary();
//            }

            Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener()
                    {
                        @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if(report.areAllPermissionsGranted()){

                                mCallPhotoGallary();
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                                showAlert();
                            }
                        }
                        @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                        {/* ... */

                            token.continuePermissionRequest();
                        }
                    }).check();
        }
    }

    private void takePhoto() {

        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

    }

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:

                    dialog = new ProgressDialog(context);
                    try {
                        dialog.setMessage("Image processing...");
                        dialog.show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {
                                    Bitmap fullImage = MediaStore.Images.Media.getBitmap(CompanyRegistrationActivity.this.getContentResolver(),imageUri);

                                    InputStream input = context.getContentResolver().openInputStream(imageUri);
                                    ExifInterface ei;
                                    if (Build.VERSION.SDK_INT > 23)
                                        ei = new ExifInterface(input);
                                    else
                                        ei = new ExifInterface(imageUri.getPath());

                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    switch (orientation) {
                                        case ExifInterface.ORIENTATION_ROTATE_90:
                                            fullImage = rotateImage(fullImage, 90);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_180:
                                            fullImage = rotateImage(fullImage, 180);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_270:
                                            fullImage = rotateImage(fullImage, 270);
                                            break;
                                        default:
                                            break;
                                    }

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                    byte[] byteArray = bytes.toByteArray();
                                    Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);
                                    file = ImageUtils.saveImage(scaledBitmap, context);

                                    if (file != null) {
                                        if (strImage == "CompanyProfilePic"){
                                            fileCompanyProfilePic = file;
                                            Log.d("d", "Camera Driver pic file:" + file);
                                            Picasso.get().load(fileCompanyProfilePic).into(ivCompanyProfilePic);

                                        }else if (strImage == "PrimaryContactId"){
                                            filePrimaryContactID = file;
                                            Log.d("d", "Camera PrimaryContactId file:" + filePrimaryContactID);
                                            Picasso.get().load(filePrimaryContactID).into(ivPrimaryContactIDUploaded);

                                        }else if (strImage == "PrimaryContactIdBack"){
                                            filePrimaryContactIDBack = file;
                                            Log.d("d", "Camera Driver pic file:" + filePrimaryContactIDBack);
                                            Picasso.get().load(filePrimaryContactIDBack).into(ivPrimaryContactIdImageBackUploaded);

                                        }

                                        else if (strImage == "SecondaryContactId"){
                                            fileSecondaryContactID = file;
                                            Log.d("d", "Camera SecondaryContactId file:" + fileSecondaryContactID);
                                            Picasso.get().load(fileSecondaryContactID).into(ivSecondaryContactIDUploaded);

                                        }else if (strImage == "SecondaryContactIdBack"){
                                            fileSecondaryContactIDBack = file;
                                            Log.d("d", "Camera Driver pic file:" + fileSecondaryContactIDBack);
                                            //Picasso.get().load(fileSecondaryContactIDBack).into(ivSecondaryContactIdBackUploaded);
                                            Picasso.get().load(fileSecondaryContactIDBack).into(ivSecondaryContactIdImageBack);


                                        }

                                        else if (strImage == "CompanyPANCard"){
                                            filePANCARD = file;
                                            Log.d("d", "Camera CompanyPANCard file:" + filePANCARD);
                                            Picasso.get().load(filePANCARD).into(ivCompanyPANCardUploaded);

                                        }else if (strImage == "CanceledCheque"){
                                            fileCancelledCheque = file;
                                            Log.d("d", "Camera CanceledCheque file:" + fileCancelledCheque);
                                            Picasso.get().load(fileCancelledCheque).into(ivCanceledChequeUploaded);
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                }
                                finally{
                                    dialog.dismiss();
                                }
                            }
                        }, 4000);  // Delay 4 sec to processing of image captured

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                    }

                    break;

                case PICK_IMAGE_REQUEST:

                    try {

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {

//                            String filePath = "";
//                            Uri imageUri = data.getData();

                                InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                                //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                                bufferedInputStream = new BufferedInputStream(inputStream);
                                bmp = BitmapFactory.decodeStream(bufferedInputStream);
                                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                byte[] byteArray = bytes.toByteArray();
                                Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);

                                file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }

                        if (strImage == "CompanyProfilePic"){
//                            strCompanyProfilePic = "file://" + photos;
//                            Log.d("d", "Driver pic path : " + strCompanyProfilePic);
//                            strSelectedCompanyProfilePic = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedCompanyProfilePic);
//                            fileCompanyProfilePic = new File(strSelectedCompanyProfilePic);
                            fileCompanyProfilePic = file;
                            Log.d("d", "Driver pic Image File::" + fileCompanyProfilePic);
                            Picasso.get().load(file).resize(200, 200).into(ivCompanyProfilePic);

                        }else if (strImage == "PrimaryContactId"){
//                            strPrimaryContactID = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedPrimaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedPrimaryContactID);
//                            filePrimaryContactID = new File(strSelectedPrimaryContactID);
                            filePrimaryContactID = file;
                            Log.d("d", "Driver pic Image File::" + filePrimaryContactID);
                            Picasso.get().load(file).resize(200, 200).into(ivPrimaryContactIDUploaded);

                        }else if (strImage == "PrimaryContactIdBack") {
//                            strPrimaryContactID = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedPrimaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedPrimaryContactID);
//                            filePrimaryContactIDBack = new File(strSelectedPrimaryContactID);
                            filePrimaryContactIDBack = file;
                            Log.d("d", "Driver pic Image File::" + filePrimaryContactIDBack);
                            Picasso.get().load(file).resize(200, 200).into(ivPrimaryContactIdImageBackUploaded);

                        }

                        else if (strImage == "SecondaryContactId"){
//                            strSecondaryContactID = "file://" + photos.get(0);
//                            strSelectedSecondaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedSecondaryContactID);
//                            fileSecondaryContactID = new File(strSelectedSecondaryContactID);
                            fileSecondaryContactID = file;
                            Log.d("d", "Driver pic Image File::" + fileSecondaryContactID);
                            Picasso.get().load(file).resize(200, 200).into(ivSecondaryContactIDUploaded);

                        }else if (strImage == "SecondaryContactIdBack") {
//                            strSecondaryContactID = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedSecondaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedSecondaryContactID);
//                            fileSecondaryContactIDBack = new File(strSelectedSecondaryContactID);
                            fileSecondaryContactIDBack = file;
                            Log.d("d", "Driver pic Image File::" + fileSecondaryContactIDBack);
                            //Picasso.get().load(file).resize(200, 200).into(ivSecondaryContactIdBackUploaded);
                            Picasso.get().load(file).resize(200, 200).into(ivSecondaryContactIdImageBack);

                        }

                        else if (strImage == "CompanyPANCard"){
//                            strPANCARD = "file://" + photos.get(0);
//                            strSelectedPANCARD = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedPANCARD);
//                            filePANCARD = new File(strSelectedPANCARD);
                            filePANCARD = file;
                            Log.d("d", "Driver pic Image File::" + filePANCARD);
                            Picasso.get().load(file).resize(200, 200).into(ivCompanyPANCardUploaded);

                        }else if (strImage == "CanceledCheque"){
//                            strCancelledCheque = "file://" + photos.get(0);
//                            strSelectedCancelledCheque = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedCancelledCheque);
//                            fileCancelledCheque = new File(strSelectedCancelledCheque);
                            fileCancelledCheque = file;
                            Log.d("d", "Driver pic Image File::" + fileCancelledCheque);
                            Picasso.get().load(file).resize(200, 200).into(ivCanceledChequeUploaded);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();
                    getContactNumber(data, context);

                    break;

                case PLACE_AUTOCOMPLETE_REQUEST_CODE:

                    if (resultCode == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i(TAG, "Place: " + place.getName() + ", " + place.getAddress());
                        etRegisteredAddress.setText(place.getAddress());

                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i(TAG, status.getStatusMessage());

                    } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }

                    break;
            }
        }
    }



    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e(TAG, "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    public void getContactNumber(Intent data, final Context context) {
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";

        List<String> allNumbers = new ArrayList<>();
        int contactIdColumnId, phoneColumnID, nameColumnID;
        try {
            Uri result = data.getData();
            String id = result.getLastPathSegment();
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);
                    Log.e("Phone", phoneNumber);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
                    allNumbers.size();
                    cursor.moveToNext();

                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (allNumbers.size() > 1 && !allNumbers.isEmpty()) {

                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        selectedNumber = items[item].toString().replaceAll("[^0-9\\+]", "");

                        Log.d("numberOnClick", selectedNumber);

                        if (selectedNumber != null && (new Emailvalidation()).phone_validation(selectedNumber)) {

                            if (strPhoneNo.equalsIgnoreCase("MobileNo")) {

                                etRegMobileCompany.setText(selectedNumber);
                                etRegMobileCompany.requestFocus();

                            }else if (strPhoneNo.equalsIgnoreCase("OfficeNo")){

                                etRegPhoneCompany.setText(selectedNumber);
                                etRegPhoneCompany.requestFocus();

                            }else if (strPhoneNo.equalsIgnoreCase("PCMobileNo")){

                                etPrimaryPhoneCompany.setText(selectedNumber);
                                etPrimaryPhoneCompany.requestFocus();

                            }else if (strPhoneNo.equalsIgnoreCase("SCMobileNo")){

                                etSecondaryPhoneCompany.setText(selectedNumber);
                                etSecondaryPhoneCompany.requestFocus();

                            }

                        } else {
                            new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                        }
                    }
                });
                AlertDialog alert = builder.create();

                alert.show();

            } else {

                selectedNumber = phoneNumber.replaceAll("[^0-9\\+]", "");

                Log.d("number", selectedNumber);

                if (selectedNumber != null && (new Emailvalidation()).phone_validation(selectedNumber)) {

                    if (strPhoneNo.equalsIgnoreCase("MobileNo")) {

                        etRegMobileCompany.setText(selectedNumber);
                        etRegMobileCompany.requestFocus();

                    }else if (strPhoneNo.equalsIgnoreCase("OfficeNo")){

                        etRegPhoneCompany.setText(selectedNumber);
                        etRegPhoneCompany.requestFocus();

                    }else if (strPhoneNo.equalsIgnoreCase("PCMobileNo")){

                        etPrimaryPhoneCompany.setText(selectedNumber);
                        etPrimaryPhoneCompany.requestFocus();

                    }else if (strPhoneNo.equalsIgnoreCase("SCMobileNo")){

                        etSecondaryPhoneCompany.setText(selectedNumber);
                        etSecondaryPhoneCompany.requestFocus();

                    }
                } else {
                    new CustomToast(CompanyRegistrationActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                }
            }
        }
    }

    @Override
    public void onLatLong(Double Latitude, Double Longitude) {

        System.out.println("Latitude****"+Latitude);
        System.out.println("Longitude****"+Longitude);
        strAddressFromLatLong = getCompleteAddressString(Latitude, Longitude);
        etRegisteredAddress.setText(strAddressFromLatLong!=null||!strAddressFromLatLong.equals("")?strAddressFromLatLong:"");
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyRegistrationActivity.this, transitionflag);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyRegistrationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
