package com.neo.cars.app.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.neo.cars.app.FilterOnBookActivity;
import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.Inbox;
import com.neo.cars.app.MyBookingActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SearchActivity;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Wishlist;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by parna on 27/2/18.
 */

public class BottomView {

    private View v;
    private Activity mcontext;
    private Context ab;

    private TextView tvProfile,tvExplore,tvMyBooking,tvMessages,tvWishlist;
    private String sSelectmenu= StaticClass.Menu_profile;
    private int transitionflag = StaticClass.transitionflagNext;
    boolean boolWishlist=false;
    private SharedPrefUserDetails sharedPref;
    private CustomTextviewTitilliumWebRegular tvBadge;
    public Timer timer;

    public View BottomView(Activity context,String Selectmenu){
        mcontext = context;
        ab = context;
        sSelectmenu=Selectmenu;
        sharedPref = new SharedPrefUserDetails(ab);
        Initialize();
        Listener();
        return v;
    }

    private void Initialize() {
        tvProfile= mcontext.findViewById(R.id.tvProfile);
        tvExplore= mcontext.findViewById(R.id.tvExplore);
        tvMyBooking= mcontext.findViewById(R.id.tvMyBooking);
        tvMessages= mcontext.findViewById(R.id.tvMessages);
        tvWishlist= mcontext.findViewById(R.id.tvWishlist);
        tvBadge = mcontext.findViewById(R.id.tvBadge);

        if(sharedPref.getBottomView().equals(StaticClass.Menu_profile)){
            tvProfile.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.footer_profile_icon_active_1, 0, 0);
        }else  if(sharedPref.getBottomView().equals(StaticClass.Menu_Explore)){
            tvExplore.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.taxi_icon_select, 0, 0);
        }else  if(sharedPref.getBottomView().equals(StaticClass.Menu_Messages)){
            tvMessages.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.inbox_select, 0, 0);
        }else  if(sharedPref.getBottomView().equals(StaticClass.Menu_Search)){
            tvMyBooking.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_booking_select_lower, 0, 0);
        }else if(sharedPref.getBottomView().equals(StaticClass.Menu_Wishlist)){
            tvWishlist.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.footer_wishlist_icon_active, 0, 0);
        }

        try {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {

                    mcontext.runOnUiThread(new Runnable(){

                        @Override
                        public void run() {

                            //System.out.println("*****mcontext.getClass().getName()*****"+mcontext.getClass().getName());
                            if(sharedPref.getNewNotificationStatus()) {
                                tvBadge.setVisibility(View.VISIBLE);
                            }else{
                                tvBadge.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            },0, 1*1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Listener(){

        tvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Log.d("BottomView*********", ""+mcontext.getClass().getName().equals(mcontext.getPackageName()));
                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".HomeProfileActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddVehicleDriverInfoActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".MoreEditDetailsActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditVehicleInfoFragment")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowPaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OvertimeBookingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OverTimePaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookACarActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchResultActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".FilterNewActivity")) ){

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvProfileListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();

                    }else{
                        tvProfileListener();
                    }
                }
            }
        });


        tvExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("tvExplorePackage", mcontext.getClass().getName());
//                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")){
                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddVehicleDriverInfoActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".MoreEditDetailsActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRiderRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowPaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OvertimeBookingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OverTimePaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".RequestOvertimeCheckActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookACarActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchResultActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".FilterNewActivity"))) {

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvExploreListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();

                    }else{
                        tvExploreListener();
                    }
                }else{

                }
            }
        });

        tvMyBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("BottomView*********", ""+mcontext.getClass().getName());
                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".MyBookingActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddVehicleDriverInfoActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".MoreEditDetailsActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRiderRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowPaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OvertimeBookingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OverTimePaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookACarActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchResultActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".FilterNewActivity"))) {

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvMyBookingListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();


                    }else{
                        tvMyBookingListener();
                    }
                }
            }
        });

        tvMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".Inbox")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddVehicleDriverInfoActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".MoreEditDetailsActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRiderRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowPaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OvertimeBookingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OverTimePaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookACarActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchResultActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".FilterNewActivity"))) {

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvMessagesListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();
                    }else{
                        tvMessagesListener();
                    }

                }
            }
        });

        tvWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".Wishlist")){
                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddVehicleDriverInfoActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".MoreEditDetailsActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRiderRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".AddRatingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookNowPaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OvertimeBookingActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".OverTimePaymentActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".BookACarActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchResultActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".FilterNewActivity"))) {

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvWishlistListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();

                    }else{
                        tvWishlistListener();
                    }
                }
            }
        });
    }

    private void tvProfileListener(){

        if(!sharedPref.getBottomView().equals(StaticClass.Menu_profile)){
            sharedPref.putBottomView(StaticClass.Menu_profile);
            new OnPauseSlider(mcontext, transitionflag);

            Intent mybookingIntent = new Intent(mcontext, HomeProfileActivity.class);
            mcontext.startActivity(mybookingIntent);
            mcontext.finish();

        }else {
            StaticClass.BottomProfile = true;
            mcontext.finish();

        }
    }

    private void tvExploreListener(){
        Log.d("sharedPrefBottomView", sharedPref.getBottomView());
        if(!sharedPref.getBottomView().equals(StaticClass.Menu_Explore)){
            sharedPref.putBottomView(StaticClass.Menu_Explore);
            new OnPauseSlider(mcontext, transitionflag);
//            Intent mybookingIntent = new Intent(mcontext, FilterOnBookActivity.class);
            Intent mybookingIntent = new Intent(mcontext, SearchActivity.class);
            mcontext.startActivity(mybookingIntent);
//            mcontext.finish();
        }else{

            new OnPauseSlider(mcontext, transitionflag);

//            if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".ExploreActivity")){
            if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".SearchResultActivity")){
                StaticClass.BottomExplore = true;
                mcontext.finish();
            }else{
//                Intent mybookingIntent = new Intent(mcontext, FilterOnBookActivity.class);
//                Intent mybookingIntent = new Intent(mcontext, SearchActivity.class);
//                mcontext.startActivity(mybookingIntent);
                StaticClass.BottomExplore = true;
                mcontext.finish();
            }
        }
    }

    private void tvMyBookingListener(){

        if(!sharedPref.getBottomView().equals(StaticClass.Menu_Search)){
            sharedPref.putBottomView(StaticClass.Menu_Search);
            new OnPauseSlider(mcontext, transitionflag);
            Intent mybookingIntent = new Intent(mcontext, MyBookingActivity.class);
            mcontext.startActivity(mybookingIntent);
//            mcontext.finish();
        }else{
            StaticClass.BottomSearch = true;
            mcontext.finish();
        }
    }

    private void tvMessagesListener(){
        if(!sharedPref.getBottomView().equals(StaticClass.Menu_Messages)){
            sharedPref.putBottomView(StaticClass.Menu_Messages);
            new OnPauseSlider(mcontext, transitionflag);

            Intent mybookingIntent = new Intent(mcontext, Inbox.class);
            mcontext.startActivity(mybookingIntent);
//            mcontext.finish();
        }else {
            StaticClass.BottomMessages = true;
            mcontext.finish();

        }
    }

    private void tvWishlistListener(){
        if(!sharedPref.getBottomView().equals(StaticClass.Menu_Wishlist)){
            sharedPref.putBottomView(StaticClass.Menu_Wishlist);
            new OnPauseSlider(mcontext, transitionflag);

            Intent mybookingIntent = new Intent(mcontext, Wishlist.class);
            mcontext.startActivity(mybookingIntent);
//            mcontext.finish();
        }else{
            StaticClass.BottomWishlist = true;
            mcontext.finish();
        }
    }
}