package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neo.cars.app.Adapter.ExploreCityListAdapter;
import com.neo.cars.app.Interface.VehicleSearch_Interface;
import com.neo.cars.app.SetGet.VehicleSearchModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.RecyclerItemClickListener;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.VehicleSearch_WebService;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by parna on 12/4/18.
 */

public class SearchCityByLocationActivity extends AppCompatActivity implements VehicleSearch_Interface {

    private Context context;
    private ImageView ivFilter, ivCancel;
    private CustomEditTextTitilliumWebRegular etSearchCity;
    private ConnectionDetector cd;
    private RecyclerView rcvCityList;
    private ExploreCityListAdapter exploreCityListAdapter;
    private LinearLayoutManager layoutManagerVertical;
    private ArrayList<VehicleSearchModel> arrTempCityList;
    private ArrayList<VehicleSearchModel> arrlistCity;

    //timer for editext change
    private Timer timer=new Timer();
    private final long DELAY = 1000; // milliseconds

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private int transitionflag = StaticClass.transitionflagNext;
    private RelativeLayout rlBackLayout;
    private Bundle bundle;
    private String strMinValue="", strMaxValue="", strFromDate="", strToDate="", strDuration="", strPickUpTime="", strVehicleType="",
            strVehicleTypeName="", strPassengerNo="", strBaggageAllowance="", strOvertimeAvailable="", strSateId = "";

    private String FilterString="";
    private boolean timerflag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchcitybylocation);

        new AnalyticsClass(SearchCityByLocationActivity.this);

        Initialize();
        Listener();

        //Fetch city list
        if(cd.isConnectingToInternet()){
            new VehicleSearch_WebService().vehicleSearchWebservice(SearchCityByLocationActivity.this);
        }else{
            new CustomToast(this, getResources().getString(R.string.error_no_network));
        }

        bundle = getIntent().getExtras();
        if (bundle != null){
            strMinValue = bundle.getString("MinValue");
            strMaxValue = bundle.getString("MaxValue");
            strFromDate = bundle.getString("FromDate");
            //  strToDate = bundle.getString("ToDate");
            strDuration = bundle.getString("Duration");
            strPickUpTime = bundle.getString("PickUpTime");
            strVehicleType = bundle.getString("VehicleType");
            strVehicleTypeName = bundle.getString("VehicleTypeName");
            strPassengerNo = bundle.getString("PaassengerNo");
            strBaggageAllowance = bundle.getString("BaggageAllowance");
            strOvertimeAvailable = bundle.getString("OvertimeAvailable");
        }


        etSearchCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {

                if(s.toString().trim().equals("")) {
                    ivCancel.setVisibility(View.GONE);
                }else{
                    ivCancel.setVisibility(View.VISIBLE);
                }

                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                try{

                                    // TODO: do what you need here (refresh list)
                                    // you will probably need to use runOnUiThread(Runnable action) for some specific actions
                                    arrTempCityList = new ArrayList<VehicleSearchModel>();

                                    if (arrlistCity != null){


                                        for (int i=0; i<arrlistCity.size(); i++){
                                            if (arrlistCity.get(i).getName().toLowerCase().startsWith(s.toString().toLowerCase())){
                                                Log.d("d", "arrlistCity name::"+arrlistCity.get(i).getName());
                                                arrTempCityList.add(arrlistCity.get(i));
                                            }
                                        }

                                        runOnUiThread(new Runnable(){

                                            @Override
                                            public void run() {
                                                SetAdapter();
                                            }
                                        });
                                    }
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        },
                        DELAY
                );

                timerflag=false;
                try {
                    timer.cancel();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }


                System.out.println("*****addTextChangedListener*****");
                try {
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if(timerflag){
                                try {
                                    timer.cancel();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                runOnUiThread(new Runnable(){

                                    @Override
                                    public void run() {
                                        FilterString=s.toString().trim();
                                        newArrayList();
                                    }
                                });
                            }else{
                                timerflag=true;
                            }
                        }
                    },0, 1*1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(final Editable s) {


            }
        });
    }

    private void  newArrayList(){
        arrTempCityList = new ArrayList<VehicleSearchModel>();

        if(FilterString.equals("")){
            ivCancel.setVisibility(View.GONE);
            arrTempCityList=arrlistCity;
        }else{
            ivCancel.setVisibility(View.VISIBLE);

            try {
                if (arrlistCity.size() > 0) {
                    for (int i = 0; i < arrlistCity.size(); i++) {
                        if (arrlistCity.get(i).getName().toLowerCase().startsWith(FilterString.toLowerCase())) {
                            arrTempCityList.add(arrlistCity.get(i));
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        SetAdapter();

    }

    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvSeacrhCity));


        rlBackLayout = findViewById(R.id.rlBackLayout);
        ivFilter = findViewById(R.id.ivFilter);
        ivCancel = findViewById(R.id.ivCancel);
        etSearchCity = findViewById(R.id.etSearchCity);
        rcvCityList = findViewById(R.id.rcvCityList);

        ivFilter.setVisibility(View.GONE);
        ivCancel.setVisibility(View.GONE);

    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                Intent applyintent = new Intent();
                setResult(StaticClass.BackRequestCode,applyintent);
                finish();
            }
        });

        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivCancel.setVisibility(View.GONE);
                etSearchCity.setText("");
            }
        });


        rcvCityList.addOnItemTouchListener(new RecyclerItemClickListener(SearchCityByLocationActivity.this,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                                System.out.println("CityId*********"+arrTempCityList.get(position).getId());
                                Intent intent=new Intent();
                                intent.putExtra("CityId",arrTempCityList.get(position).getId());
                                intent.putExtra("CityName", arrTempCityList.get(position).getName());
                                intent.putExtra("isState", arrTempCityList.get(position).getIsState());
                                setResult(StaticClass.SearchCityRequestCode,intent);
                                finish(); //finishing activity

                            }
                        })
        );
    }


    @Override
    public void vehicleSearch(ArrayList<VehicleSearchModel> arrlistVehicle) {
        arrlistCity=new ArrayList<>();
        arrTempCityList = new ArrayList<VehicleSearchModel>();

        arrlistCity = arrlistVehicle;
        arrTempCityList=arrlistVehicle;

        SetAdapter();
    }

//    @Override
//    public void CityList( ArrayList<CityListModel> arlistCity) {
//        arrlistCity=new ArrayList<>();
//        arrTempCityList = new ArrayList<CityListModel>();
//
//        arrlistCity = arlistCity;
//        arrTempCityList=arlistCity;
//
//        SetAdapter();
//    }


    private void SetAdapter(){
        exploreCityListAdapter = new ExploreCityListAdapter(SearchCityByLocationActivity.this, arrTempCityList);
        layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rcvCityList.setLayoutManager(layoutManagerVertical);
        rcvCityList.setItemAnimator(new DefaultItemAnimator());
        rcvCityList.setHasFixedSize(true);
        rcvCityList.setAdapter(exploreCityListAdapter);
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        Intent applyintent = new Intent();
        setResult(StaticClass.BackRequestCode,applyintent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(SearchCityByLocationActivity.this, transitionflag);
    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) SearchCityByLocationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
