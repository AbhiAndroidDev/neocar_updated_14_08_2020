package com.neo.cars.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

/**
 * Created by parna on 19/3/18.
 */

public class MoreVehicleListPager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    private int tabCount;
    private String vehicleId="",DriverId="";
    private Bundle bundle=null;

    public MoreVehicleListPager(FragmentManager fm, int tabCount,String svehicleId,String sDriverId) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        vehicleId=svehicleId;
        DriverId=sDriverId;
        Log.d("d", "vehicleId::"+vehicleId);
    }

    public MoreVehicleListPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                MoreVehicleInfoFragment tab1 = new MoreVehicleInfoFragment();
                bundle = new Bundle();
                bundle.putString("vehicleId", vehicleId);
                tab1.setArguments(bundle);
                return tab1;

            case 1:
                MoreDriverInfoFragment tab2 = new MoreDriverInfoFragment();
                bundle = new Bundle();
                bundle.putString("driverId", DriverId);
                tab2.setArguments(bundle);
                return tab2;

            default:
                return null;

        }

    }
    @Override
    public int getCount() {
        return tabCount;
    }
}
