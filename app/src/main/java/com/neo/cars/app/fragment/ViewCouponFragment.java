package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.neo.cars.app.Adapter.ViewCouponAdapter;
import com.neo.cars.app.Interface.UserVoucher_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserVoucherModel;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Webservice.UserVoucher_Webservice;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 8/3/18.
 */

public class ViewCouponFragment extends Fragment implements UserVoucher_Interface {

    private View mView;
    private RecyclerView rcvCoupon;
    private Context context;
    private LinearLayoutManager layoutManagerVertical;
    private ViewCouponAdapter viewCouponAdapter;
    private List<UserVoucherModel> manageCouponModelList = new ArrayList<>();
    private CustomTitilliumTextViewSemiBold tvNoCouponFound;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_view_coupon, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        Initialize();
        Listener();

        return mView;
    }

    private void Initialize(){
        context = getActivity();
        rcvCoupon = mView.findViewById(R.id.rcvCoupon);
        tvNoCouponFound = mView.findViewById(R.id.tvNoCouponFound);


        if (NetWorkStatus.isNetworkAvailable(context)) {
            new UserVoucher_Webservice().UserVoucher(getActivity(),ViewCouponFragment.this);
        } else {
            Intent i = new Intent(context, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    private void Listener(){
    }

    @Override
    public void OnUserVoucher(ArrayList<UserVoucherModel> arr_UserVoucherModel) {

        if(arr_UserVoucherModel.size() > 0){
            manageCouponModelList=arr_UserVoucherModel;
            viewCouponAdapter = new ViewCouponAdapter(manageCouponModelList);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvCoupon.setLayoutManager(layoutManagerVertical);
            rcvCoupon.setItemAnimator(new DefaultItemAnimator());
            rcvCoupon.setHasFixedSize(true);
            rcvCoupon.setAdapter(viewCouponAdapter);

        }else{
            rcvCoupon.setVisibility(View.GONE);
            tvNoCouponFound.setVisibility(View.VISIBLE);
        }
    }
}
