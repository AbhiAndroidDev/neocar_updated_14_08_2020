package com.neo.cars.app;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Adapter.InboxAdapter;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.DeleteNotification_Interface;
import com.neo.cars.app.Interface.InboxModalInterface;
import com.neo.cars.app.SetGet.InboxModal;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.RecyclerItemClickListener;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.DeleteNotification_Webservice;
import com.neo.cars.app.Webservice.ReadNotification_Webservice;
import com.neo.cars.app.Webservice.UserNotification_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;

public class Inbox extends AppCompatActivity implements InboxModalInterface,SwipeRefreshLayout.OnRefreshListener,CallBackButtonClick,
        DeleteNotification_Interface {

    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private CustomTitilliumTextViewSemiBold tvNoMsgFound;
    private RecyclerView rcv_Inbox;
    private LinearLayoutManager layoutManagerVertical;
    private InboxAdapter inboxAdapter;
    private RelativeLayout rlBackLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPrefUserDetails sharedPrefUser;
    private BottomView bottomview = new BottomView();
    private ArrayList<InboxModal> arr_InboxModal;
    private RelativeLayout iv_delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        sharedPrefUser = new SharedPrefUserDetails(Inbox.this);
        sharedPrefUser.setNewNotificationStatus(false);
        sharedPrefUser.putBottomView( StaticClass.Menu_Messages);

        new AnalyticsClass(Inbox.this);

        Initialize();
        Listener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Initialize() {
        swipeRefreshLayout = findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(Inbox.this);
        rcv_Inbox = findViewById(R.id.rcv_Inbox);
        tvNoMsgFound = findViewById(R.id.tvNoMsgFound);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.inbox));
        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlBackLayout.setVisibility(View.GONE);
        iv_delete= findViewById(R.id.iv_delete);
        bottomview.BottomView(Inbox.this, StaticClass.Menu_Messages);
    }

    private void Listener() {
        rcv_Inbox.addOnItemTouchListener(new RecyclerItemClickListener(Inbox.this,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        if(arr_InboxModal.get(position).getIs_read().equalsIgnoreCase("Y")) {
                            transitionflag = StaticClass.transitionflagNext;
                            Intent inboxdetails = new Intent(Inbox.this, InboxDetails.class);
                            inboxdetails.putExtra("messagetitle", arr_InboxModal.get(position).getTitle());
                            inboxdetails.putExtra("message", arr_InboxModal.get(position).getDescription());
                            inboxdetails.putExtra("Datetime", arr_InboxModal.get(position).getCreated_at());
                            inboxdetails.putExtra("notificationId", arr_InboxModal.get(position).getId());
                            inboxdetails.putExtra("notification_type", arr_InboxModal.get(position).getNotification_type());
                            startActivity(inboxdetails);
                        }else{
                            if (NetWorkStatus.isNetworkAvailable(Inbox.this)) {
                                new ReadNotification_Webservice().ReadNotification(Inbox.this,
                                        arr_InboxModal,position);
                            } else {
                                Intent i = new Intent(Inbox.this, NetworkNotAvailable.class);
                                startActivity(i);
                            }
                        }
                    }
                })
        );

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(Inbox.this,
                        Inbox.this,
                        Inbox.this.getResources().getString(R.string.Delete_All_msg),
                        Inbox.this.getResources().getString(R.string.yes),
                        Inbox.this.getResources().getString(R.string.no));
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(Inbox.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }

    @Override
    public void OnInboxModal(ArrayList<InboxModal> InboxModal) {
        arr_InboxModal=new ArrayList<>();
        arr_InboxModal=InboxModal;

        if(arr_InboxModal.size() > 0){
            inboxAdapter = new InboxAdapter(Inbox.this,arr_InboxModal);
            layoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rcv_Inbox.setLayoutManager(layoutManagerVertical);
            rcv_Inbox.setItemAnimator(new DefaultItemAnimator());
            rcv_Inbox.setHasFixedSize(true);
            rcv_Inbox.setAdapter(inboxAdapter);

            inboxAdapter.notifyDataSetChanged();

            iv_delete.setVisibility(View.VISIBLE);

        }else{
            rcv_Inbox.setVisibility(View.GONE);
            tvNoMsgFound.setVisibility(View.VISIBLE);
            iv_delete.setVisibility(View.GONE);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        StaticClass.BottomMessages = false;
        refreshList();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void  refreshList(){

        if (NetWorkStatus.isNetworkAvailable(Inbox.this)) {
            new UserNotification_Webservice().UserNotification(Inbox.this);
        } else {
            Intent i = new Intent(Inbox.this, NetworkNotAvailable.class);
            startActivity(i);
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onButtonClick(String strButtonText) {

        if (strButtonText.equals(Inbox.this.getResources().getString(R.string.yes))) {
            if (NetWorkStatus.isNetworkAvailable(Inbox.this)) {
                new DeleteNotification_Webservice().DeleteNotification(Inbox.this,"");
            } else {
              new CustomToast(Inbox.this,getResources().getString(R.string.no_internet_connection_try_later));
            }
        }
    }

    @Override
    public void onDeleteNotification() {
        refreshList();
    }
}