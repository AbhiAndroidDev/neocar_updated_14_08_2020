package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.TypeOfVehicleModel;

import java.util.ArrayList;

public interface TypeOfVehicleInterface {

    void TypeOfVehicle(ArrayList<TypeOfVehicleModel> arrlistVehicleType);
}
