package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.neo.cars.app.BookingInformationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.MyBookingListModel;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by parna on 8/3/18.
 */

public class UpcomingBookingAdapter extends RecyclerView.Adapter<UpcomingBookingAdapter.MyViewHolder> {

    private List<MyBookingListModel> myBookingModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircularImageViewBorder civUpcomingBookingPic;
        CustomTextviewTitilliumWebRegular tvLocation, tvCarOwnerName, tvDate, tvBookingRefId;
        RelativeLayout rlParent;


        public MyViewHolder(View view) {
            super(view);

            civUpcomingBookingPic= view.findViewById(R.id.civUpcomingBookingPic);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvCarOwnerName = view.findViewById(R.id.tvCarOwnerName);
            tvDate = view.findViewById(R.id.tvDate);
            tvBookingRefId = view.findViewById(R.id.tvBookingRefId);
            rlParent = view.findViewById(R.id.rlParent);

        }
    }

    public UpcomingBookingAdapter(Context context,List<MyBookingListModel> myBookingModelList) {
        this.context = context;
        this.myBookingModelList = myBookingModelList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_booking_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvLocation.setText(myBookingModelList.get(position).getPickup_location());
        holder.tvDate.setText(myBookingModelList.get(position).getBooking_date());
        holder.tvCarOwnerName.setText(myBookingModelList.get(position).getDuration()+" hours");
        holder.tvBookingRefId.setText(myBookingModelList.get(position).getBooking_id());
        Picasso.get().load(myBookingModelList.get(position).getVehicle_image()) /*.transform(new CropCircleTransformation())*/.into(holder.civUpcomingBookingPic);

        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent moreIntent = new Intent(context, BookingInformationActivity.class);
                moreIntent.putExtra("booking_id",   myBookingModelList.get(position).getBooking_id());
                moreIntent.putExtra("booking_Type", StaticClass.MyVehicleUpcoming);
                context.startActivity(moreIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return myBookingModelList.size();
        //return manageCouponModelList.size();
    }
}
