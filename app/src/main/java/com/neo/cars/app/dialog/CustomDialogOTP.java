package com.neo.cars.app.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.neo.cars.app.Interface.CallbackCancelButtonClick;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Webservice.MobileLogin_Webservice;
import com.neo.cars.app.Webservice.VerifyPCMobileOTPWebService;

/**
 * Created by parna on 17/7/18.
 */

public class CustomDialogOTP {

    private Context mContext;
    private Activity mActivity;
    private CallbackCancelButtonClick mClickCallBackButton;
    private String mStrMsg, mStrMobile,  mStrNegative, mStrPositive, mBookingFlag, mBookingType;
    private CallbackCancelButtonClick buttonClick=null;
    ConnectionDetector cd ;
    EditText etText;
    TextView tvMsg;
    Button btNegative;
    String user_Type="", user_id = "";
    String number_Type="";
    SharedPrefUserDetails sharedprefs;

    public CustomDialogOTP(Context context, Activity activity, String strMsg,  String strMobileNo, String userType, String numberType) {

        this.mContext = context;
        this.mActivity = activity;
        this.mStrMsg = strMsg;
        this.mStrMobile = strMobileNo;
        this.user_Type = userType;
        this.number_Type = numberType;

        sharedprefs = new SharedPrefUserDetails(mContext);

        // call bottom sheet dialog
        cd = new ConnectionDetector(context);
        callBottomSheetDialog();


    }


    private void callBottomSheetDialog() {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(mContext);
        View sheetView = mActivity.getLayoutInflater().inflate(R.layout.bottom_sheet_otp_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        tvMsg = sheetView.findViewById(R.id.tvMsg);
        btNegative = sheetView.findViewById(R.id.btNegative);
        etText = sheetView.findViewById(R.id.etText);

      /*  tvMsg.setText(mStrMsg);
        btNegative.setText(mStrNegative);*/

        btNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

               /* if (buttonClick == null) {
                    (( CallbackCancelButtonClick ) mActivity).onButtonClick(mStrNegative, mBookingFlag, mBookingType);

                }else {
                    buttonClick.onButtonClick(mStrNegative, mBookingFlag, mBookingType);

                }*/

                if ( !etText.getText().toString().trim().equals("") ){

                    if(cd.isConnectingToInternet()){
                        if (sharedprefs.getUserType().equalsIgnoreCase("U")) {
                            new MobileLogin_Webservice().MobileLogin(mActivity, mStrMobile, etText.getText().toString().trim(), user_Type);

                        }else if (sharedprefs.getUserType().equalsIgnoreCase("C")){

                            if (number_Type != null && !number_Type.equalsIgnoreCase("")) {

                                new VerifyPCMobileOTPWebService().VerifyPCMobileOTP(mActivity, mStrMobile, etText.getText().toString().trim(), number_Type);
                            }else {
                                new MobileLogin_Webservice().MobileLogin(mActivity, mStrMobile, etText.getText().toString().trim(), user_Type);

                            }
                        }
                    }else{
                        Intent i = new Intent(mActivity, NetworkNotAvailable.class);
                        mActivity.startActivity(i);
                    }
                }else{
                    new CustomToast(mActivity, "Please provide OTP.");
                }
            }
        });
    }
}
