package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.neo.cars.app.ImageFullActivity;
import com.neo.cars.app.Interface.MoreVehicleInfo_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleGalleryModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Webservice.MoreVehicleInfo_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by parna on 23/4/18.
 */

public class MoreVehicleInfoFragment extends Fragment implements MoreVehicleInfo_Interface {

    private View view;
    private Context context;
    private CustomTextviewTitilliumWebRegular tvLocation, tvTypeName, tvBrandName, tvVehicleMake, tvYrManufacture, tvTotalKm, tvMaxPassengers, tvMaxLuggage,
            tvSpecialInfo, tvPickLoc, tvLicensePlate, tvTaxToken, tvPUCPaper, tvRegionalTransportPermit, tvRegistrationCertificateFront,
            tvRegistrationCertificateBack ,tvFitnessCertificate, tvInsuranceCertificate;
    private ConnectionDetector cd;
    private ImageView ivPucSuccess, ivTaxSuccess, ivRegionalTransportSuccess, ivRegistrationCertificateSuccessFront, ivRegistrationCertificateSuccessBack,
            ivFitnessCertificateSuccess, ivInsuranceCertificateSuccess;
    private ArrayList<String>  listOfVehicleImage;
    private ViewPager view_pager;
    private RelativeLayout rlFirstRelativeLayout;
    private ScrollView scvCarDetails;
    private int[] layouts;
    private TextView[] dots;
    private LinearLayout dotsLayout;
    private Toolbar toolbar;
    private MyViewPagerAdapter myViewPagerAdapter;
    private ArrayList<VehicleGalleryModel> listOfVehicleGallery;
    private String strvehicleId="";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {

            view = inflater.inflate(R.layout.fragment_more_vehicle_info, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            strvehicleId = getArguments().getString("vehicleId");
            Log.d("d", "Get strvehicleId: "+strvehicleId);
        }

        Initialize(view);
        Listener();

        //fetch user vehicle list
        getVehicleInfo();

        return view;
    }

    public void getVehicleInfo(){
        if (NetWorkStatus.isNetworkAvailable(getActivity())) {
            new MoreVehicleInfo_Webservice().moreVehicleInfo(getActivity() , MoreVehicleInfoFragment.this,strvehicleId);
        } else {
            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    private void Initialize(View mView){

        context = getActivity();
        cd = new ConnectionDetector(getActivity());

        view_pager = mView.findViewById(R.id.view_pagers);
        scvCarDetails = mView.findViewById(R.id.scvCarDetails);
        rlFirstRelativeLayout = mView.findViewById(R.id.rlFirstRelativeLayout);
        dotsLayout = mView.findViewById(R.id.layoutDots);

        tvLocation = mView.findViewById(R.id.tvLocation);
        tvTypeName = mView.findViewById(R.id.tvTypeName);
        tvBrandName = mView.findViewById(R.id.tvBrandName);
        tvVehicleMake = mView.findViewById(R.id.tvVehicleMake);
        tvYrManufacture = mView.findViewById(R.id.tvYrManufacture);
        tvTotalKm = mView.findViewById(R.id.tvTotalKm);
        tvMaxPassengers = mView.findViewById(R.id.tvMaxPassengers);
        tvMaxLuggage = mView.findViewById(R.id.tvMaxLuggage);
        tvSpecialInfo = mView.findViewById(R.id.tvSpecialInfo);
        tvPickLoc = mView.findViewById(R.id.tvPickLoc);
        tvLicensePlate = mView.findViewById(R.id.tvLicensePlate);
        tvTaxToken = mView.findViewById(R.id.tvTaxToken);
        tvPUCPaper = mView.findViewById(R.id.tvPUCPaper);

        tvRegionalTransportPermit = mView.findViewById(R.id.tvRegionalTransportPermit);
        tvRegistrationCertificateFront = mView.findViewById(R.id.tvRegistrationCertificateFront);
        tvRegistrationCertificateBack = mView.findViewById(R.id.tvRegistrationCertificateBack);
        tvFitnessCertificate = mView.findViewById(R.id.tvFitnessCertificate);
        tvInsuranceCertificate = mView.findViewById(R.id.tvInsuranceCertificate);

        ivPucSuccess =  mView.findViewById(R.id.ivPucSuccess);
        ivTaxSuccess = mView.findViewById(R.id.ivTaxSuccess);

        ivRegionalTransportSuccess = mView.findViewById(R.id.ivRegionalTransportSuccess);
        ivRegistrationCertificateSuccessFront = mView.findViewById(R.id.ivRegistrationCertificateSuccessFront);
        ivRegistrationCertificateSuccessBack = mView.findViewById(R.id.ivRegistrationCertificateSuccessBack);
        ivFitnessCertificateSuccess = mView.findViewById(R.id.ivFitnessCertificateSuccess);
        ivInsuranceCertificateSuccess = mView.findViewById(R.id.ivInsuranceCertificateSuccess);

    }

    private void Listener(){

    }

    @Override
    public void onResume() {
        super.onResume();

        getVehicleInfo();
    }

    @Override
    public void MoreVehicleInfo(VehicleTypeModel vehicleTypeModel) {

        if (!TextUtils.isEmpty(vehicleTypeModel.getCity_name())){
            tvLocation.setText(vehicleTypeModel.getCity_name());
        }

        if (! vehicleTypeModel.getVehicle_type_name().equals("") && !vehicleTypeModel.getVehicle_type_name().equals("null")){
            tvTypeName.setText(vehicleTypeModel.getVehicle_type_name());
        }

        if (! vehicleTypeModel.getModel_name().equals("") && !vehicleTypeModel.getModel_name().equals("null")){
            tvBrandName.setText(vehicleTypeModel.getModel_name());
        }

        if (! vehicleTypeModel.getBrand_name().equals("") && !vehicleTypeModel.getBrand_name().equals("null")){
            tvVehicleMake.setText(vehicleTypeModel.getBrand_name());
        }

        if (! vehicleTypeModel.getYear().equals("") && !vehicleTypeModel.getYear().equals("null")){
            tvYrManufacture.setText(vehicleTypeModel.getYear());
        }

        if (! vehicleTypeModel.getKm_travelled().equals("") && !vehicleTypeModel.getKm_travelled().equals("null")){
            tvTotalKm.setText(vehicleTypeModel.getKm_travelled());
        }

        if(! vehicleTypeModel.getMax_passenger().equals("") && !vehicleTypeModel.getMax_passenger().equals("null")){
            tvMaxPassengers.setText(vehicleTypeModel.getMax_passenger());
        }

        if(!vehicleTypeModel.getMax_luggage().equals("") && !vehicleTypeModel.getMax_luggage().equals("null")){
            tvMaxLuggage.setText(vehicleTypeModel.getMax_luggage());
        }

        if(!vehicleTypeModel.getDescription().equals("") && ! vehicleTypeModel.getDescription().equals("null")){
            tvSpecialInfo.setText(vehicleTypeModel.getDescription());

        }else {
            tvSpecialInfo.setText("N/A");

        }
        if(vehicleTypeModel.getArr_PickUpLocationModel().size()>0){

            String Strpickup="";

            for(int i=0;i<vehicleTypeModel.getArr_PickUpLocationModel().size();i++){
                if(i==0){
                    Strpickup=vehicleTypeModel.getArr_PickUpLocationModel().get(i).getPickup_location();
                }else{
                    Strpickup=Strpickup+", "+vehicleTypeModel.getArr_PickUpLocationModel().get(i).getPickup_location();
                }
            }
            tvPickLoc.setText( Strpickup);

        }else {
            tvPickLoc.setText(getResources().getString(R.string.tvAirport) +", " + getResources().getString(R.string.tvRailwaystation));

        }

        if(! vehicleTypeModel.getLicense_plate_no().equals("") && ! vehicleTypeModel.getLicense_plate_no().equals("null")){
            tvLicensePlate.setText(vehicleTypeModel.getLicense_plate_no());
        }

        if(!TextUtils.isEmpty(vehicleTypeModel.getTax_token_image())){

            String[] parts = vehicleTypeModel.getTax_token_image().split("/");
            tvTaxToken.setText(parts[parts.length-1]);
            ivTaxSuccess.setVisibility(View.VISIBLE);
        }else{
            ivTaxSuccess.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(vehicleTypeModel.getPcu_paper_image())){

            String[] parts = vehicleTypeModel.getPcu_paper_image().split("/");
            tvPUCPaper.setText(parts[parts.length-1]);
            ivPucSuccess.setVisibility(View.VISIBLE);
        }else{
            ivPucSuccess.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(vehicleTypeModel.getValid_permit())){
            String[] parts = vehicleTypeModel.getValid_permit().split("/");
            tvRegionalTransportPermit.setText(parts[parts.length-1]);
            ivRegionalTransportSuccess.setVisibility(View.VISIBLE);
        }else{
            ivRegionalTransportSuccess.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getRegistration_certificate())){
            String[] parts = vehicleTypeModel.getRegistration_certificate().split("/");
            tvRegistrationCertificateFront.setText(parts[parts.length-1]);
            ivRegistrationCertificateSuccessFront.setVisibility(View.VISIBLE);
        }else{
            ivRegistrationCertificateSuccessFront.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getRegistration_certificate_back())){
            String[] parts = vehicleTypeModel.getRegistration_certificate_back().split("/");
            tvRegistrationCertificateBack.setText(parts[parts.length-1]);
            ivRegistrationCertificateSuccessBack.setVisibility(View.VISIBLE);
        }else{
            ivRegistrationCertificateSuccessBack.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getFitness_certificate())){
            String[] parts = vehicleTypeModel.getFitness_certificate().split("/");
            tvFitnessCertificate.setText(parts[parts.length-1]);
            ivFitnessCertificateSuccess.setVisibility(View.VISIBLE);
        }else{
            ivFitnessCertificateSuccess.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(vehicleTypeModel.getInsurance_certificate())){
            String[] parts = vehicleTypeModel.getInsurance_certificate().split("/");
            tvInsuranceCertificate.setText(parts[parts.length-1]);
            ivInsuranceCertificateSuccess.setVisibility(View.VISIBLE);
        }else{
            ivInsuranceCertificateSuccess.setVisibility(View.GONE);
        }

        listOfVehicleGallery = vehicleTypeModel.getArr_VehicleGalleryModel();
        listOfVehicleImage = new ArrayList<String>();
        for (int i=0;i<listOfVehicleGallery.size();i++) {
            listOfVehicleImage.add(listOfVehicleGallery.get(i).getImage_file());
            Log.d("d", "Vehicle arraylist image:: " + listOfVehicleImage.get(i));
        }

        Log.d("d", "Vehicle arraylist size: "+listOfVehicleImage.size());

        //addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        view_pager.setAdapter(myViewPagerAdapter);
        view_pager.addOnPageChangeListener(viewPagerPageChangeListener);

    }

    private void addBottomDots(int currentPage) {
        // dots = new TextView[layouts.length];
        try {
            dots = new TextView[listOfVehicleImage.size()];

            int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
            int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

            dotsLayout.removeAllViews();
            try {
                for (int i = 0; i < dots.length; i++) {
                    dots[i] = new TextView(getActivity());
                    dots[i].setText(Html.fromHtml("&#8226;"));
                    dots[i].setTextSize(35);
                    dots[i].setTextColor(colorsInactive[currentPage]);
                    dotsLayout.addView(dots[i]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(colorsActive[currentPage]);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {


        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        private ImageView ivVehicleGallery;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //View view = layoutInflater.inflate(layouts[position], container, false);
            View view = layoutInflater.inflate(R.layout.view_pager_vehicle_gallery, container, false);
            final ImageView ivVehicleGallery = view.findViewById(R.id.ivVehicleGallery);
            ivVehicleGallery.setScaleType(ImageView.ScaleType.CENTER_CROP);


//            Picasso.get().load(listOfVehicleImage.get(position)).into(ivVehicleGallery);

            Glide.with(getActivity())
                    .load(listOfVehicleImage.get(position))
                    .fitCenter()
                    .into(ivVehicleGallery);

            ivVehicleGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), ImageFullActivity.class);
                    i.putExtra("imgUrl",listOfVehicleImage.get(position));
                    startActivity(i);
                }
            });

            //Log.d("d", "Vehicle gallery: "+listOfVehicleImage);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return listOfVehicleImage.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
