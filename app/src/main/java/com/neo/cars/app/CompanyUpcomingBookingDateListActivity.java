package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.neo.cars.app.Adapter.CompanyUpcomingBookingAdapter;
import com.neo.cars.app.Interface.CompanyBookingList_Interface;
import com.neo.cars.app.SetGet.CompanyBookingListModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.Booking_list_on_calendar_date_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

public class CompanyUpcomingBookingDateListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,CompanyBookingList_Interface {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private Context context;
    private Activity activity;
    private ConnectionDetector cd;
    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    private RelativeLayout rlBackLayout;

    private RecyclerView rcvMyBooking;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectionPosition=0;
    private int transitionflag = StaticClass.transitionflagNext;
    private String strDate="";
    private LinearLayoutManager layoutManagerVertical;
    private CompanyUpcomingBookingAdapter upcomingBookingAdapter;
    private List<CompanyBookingListModel> myBookingModelList = new ArrayList<>();
    private CustomTitilliumTextViewSemiBold tvNoRecordsFound;
    SharedPrefUserDetails sharedPrefUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_upcoming_booking_date_list);

        new AnalyticsClass(CompanyUpcomingBookingDateListActivity.this);

        strDate=getIntent().getExtras().getString("strDate");

        initialize();
        listener();
    }

    private void initialize() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        sharedPrefUser = new SharedPrefUserDetails(CompanyUpcomingBookingDateListActivity.this);
        sharedPrefUser.putBottomViewCompany(StaticClass.Menu_MyBookings_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyUpcomingBookingDateListActivity.this, StaticClass.Menu_MyBookings_company);

        rlBackLayout = findViewById(R.id.rlBackLayout);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvMyVehicleBooking));
        context = CompanyUpcomingBookingDateListActivity.this;
        activity = CompanyUpcomingBookingDateListActivity.this;
        cd = new ConnectionDetector(CompanyUpcomingBookingDateListActivity.this);

        swipeRefreshLayout = findViewById(R.id.refreshLayoutC);
        swipeRefreshLayout.setOnRefreshListener(this);

        rcvMyBooking = findViewById(R.id.rcvMyBooking);
        tvNoRecordsFound = findViewById(R.id.tvNoRecordsFound);

        bottomViewCompany.BottomViewCompany(CompanyUpcomingBookingDateListActivity.this,StaticClass.Menu_profile_company);


    }
    private void listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        selectionPosition=0;
        refreshList();
    }

    private void  refreshList(){
        //fetch user vehicle list
        if (NetWorkStatus.isNetworkAvailable(activity)) {
            new Booking_list_on_calendar_date_Webservice().CompanyBookingList(activity, strDate);

        } else {
            StaticClass.MyVehicleAddUpdate=true;
            Intent i = new Intent(activity, NetworkNotAvailable.class);
            startActivity(i);
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshList();
//        StaticClass.BottomMyVehicleCompany = false;

//        sharedPref.putBottomViewCompany(StaticClass.Menu_MyVehicles_company);
//        bottomViewCompany = new BottomViewCompany();
//        bottomViewCompany.BottomViewCompany(CompanyUpcomingBookingDateListActivity.this, StaticClass.Menu_MyVehicles_company);

    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyUpcomingBookingDateListActivity.this, transitionflag);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyUpcomingBookingDateListActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void companyBookingList(ArrayList<CompanyBookingListModel> bookingList) {
        System.out.println("****UpcomingBookingFragment***"+bookingList.size());

        if(bookingList.size() > 0){
            upcomingBookingAdapter = new CompanyUpcomingBookingAdapter(context,bookingList);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvMyBooking.setLayoutManager(layoutManagerVertical);
            rcvMyBooking.setItemAnimator(new DefaultItemAnimator());
            rcvMyBooking.setHasFixedSize(true);
            rcvMyBooking.setAdapter(upcomingBookingAdapter);

        }else {
            rcvMyBooking.setVisibility(View.GONE);
            tvNoRecordsFound.setVisibility(View.VISIBLE);
        }

    }
}
