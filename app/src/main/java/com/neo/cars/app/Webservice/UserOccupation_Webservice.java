package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.OccupationInterface;
import com.neo.cars.app.Interface.StateListInterface;
import com.neo.cars.app.Interface.VehicleMake_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.OccupationModel;
import com.neo.cars.app.SetGet.StateListModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 12/6/18.
 */

public class UserOccupation_Webservice {


    Activity mcontext;
    private String Status = "0", Msg = "";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private OccupationModel occupationModel;
    JSONObject jsonObjectOccupation;

    ArrayList<OccupationModel> arrlistOccupation;


    public void userOccupationType (Activity context ){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistOccupation = new ArrayList<>();

        gson = new Gson();
        UserLoginDetails = new UserLoginDetailsModel();
        occupationModel = new OccupationModel();

        showProgressDialog();

        StringRequest userOccupationRequest  = new StringRequest(Request.Method.POST,  Urlstring.user_occupation,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };
        userOccupationRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(userOccupationRequest);

    }


    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{

            jobj_main = new JSONObject(response);

            Msg = jobj_main.optJSONObject("occupation_type").optString("message");
            Status = jobj_main.optJSONObject("occupation_type").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("occupation_type").optJSONArray("details");
            for (int i=0; i<jsonArrayDetails.length(); i++){
                jsonObjectOccupation = jsonArrayDetails.optJSONObject(i);

                OccupationModel occupationModel = new OccupationModel();
                occupationModel.setId(jsonObjectOccupation.optString("id"));
                occupationModel.setOccupation(jsonObjectOccupation.optString("occupation"));
                occupationModel.setStatus(jsonObjectOccupation.optString("status"));

                arrlistOccupation.add(occupationModel);
            }
            Log.d("Occupationmodel size:", "" + arrlistOccupation.size());
            Log.e("Occupationmodel", "" + arrlistOccupation);


        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {
            ((OccupationInterface)mcontext).OccupationList(arrlistOccupation);

        }

    }

}
