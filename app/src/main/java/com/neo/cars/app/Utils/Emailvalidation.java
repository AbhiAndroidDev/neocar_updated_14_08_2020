package com.neo.cars.app.Utils;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Emailvalidation {

    private Matcher Licencematcher;

    public boolean email_validation(String EmailString) {

		/*Pattern pattern;
		String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		Emailmatcher = pattern.matcher(EmailString);
		return Emailmatcher.matches()*/;

        if (EmailString == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(EmailString).matches();
        }
    }

    public boolean phone_validation(String phone) {
//        String PHONE_PATTERN = "^[+]?[0-9]{8,14}$";
        String PHONE_PATTERN = "^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$";
        return phone.matches(PHONE_PATTERN);
    }

    public boolean url_validation(String urlString){
        if(urlString == null){
            return false;
        }else{
            return Patterns.WEB_URL.matcher(urlString).matches();
        }
    }


    public boolean lincescePlate(String lic) {
        Pattern pattern;
//		String EMAIL_PATTERN = "[A-Z]{2}\\s[0-9|\\s]{1,2}\\s[A-Z|\\s]{1,2}\\s[0-9]{1,4}";
		String EMAIL_PATTERN =  "^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$";
		pattern = Pattern.compile(EMAIL_PATTERN);
        Licencematcher = pattern.matcher(lic);
		return Licencematcher.matches();
    }

}