package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Webservice.MobileEditWebService;

public class EditMobileActivity extends AppCompatActivity {

    private Button btnSubmit, btnCancel;
    private TextView tvPreMobNo;
    private EditText etMobileNo;
    private String mobileNo = "";
    private ConnectionDetector cd;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails shprefs;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mobile);

        new AnalyticsClass(EditMobileActivity.this);

        initialize();
        listener();
    }

    private void initialize() {

        cd = new ConnectionDetector(this);
        shprefs = new SharedPrefUserDetails(this);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        shprefs = new SharedPrefUserDetails(this);
        String struserdetails = shprefs.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnCancel = findViewById(R.id.btnCancel);
        tvPreMobNo = findViewById(R.id.tvPreMobNo);
        etMobileNo = findViewById(R.id.etMobileNo);

        if (!TextUtils.isEmpty(shprefs.getMobile())) {

            tvPreMobNo.setText(shprefs.getMobile());
        }else {
            tvPreMobNo.setText(UserLoginDetails.getMobile());
        }

        cd = new ConnectionDetector(this);
    }

    private void listener(){
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void checkValidation() {

        mobileNo = etMobileNo.getText().toString();
        if (!TextUtils.isEmpty(mobileNo)){

            if (!(new Emailvalidation().phone_validation(mobileNo))) {
                etMobileNo.setError(getString(R.string.error_telphone_format));
                etMobileNo.requestFocus();

            }else if (cd.isConnectingToInternet()) {
                (new MobileEditWebService()).mobileEditWebService(this, mobileNo);

            }else {
                startActivity(new Intent(this, NetworkNotAvailable.class));
            }
        }else {
            new CustomToast(this, getResources().getString(R.string.enterMobileNo));
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) EditMobileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
