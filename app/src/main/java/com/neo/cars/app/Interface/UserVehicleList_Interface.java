package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleTypeModel;

import java.util.ArrayList;

/**
 * Created by parna on 4/4/18.
 */

public interface UserVehicleList_Interface {

    public void UserVehicleListInterface(ArrayList<VehicleTypeModel> arrlistVehicle, String strCanAddVehicle);

}
