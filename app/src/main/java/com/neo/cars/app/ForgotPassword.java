package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.ForgetPwd_webService;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

public class ForgotPassword extends AppCompatActivity {


    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;

    private EditText etForgetPwd;
    private Button btnUpdatePasword;
    private int transitionflag=1;
    private int emailFlag=0;
    private String EMailID="",Mobile="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        new AnalyticsClass(ForgotPassword.this);

        Initialize();
        Listener();
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.ForgetPwd));
        rlBackLayout = findViewById(R.id.rlBackLayout);

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);

        etForgetPwd=findViewById(R.id.etForgetPwd);
        btnUpdatePasword=findViewById(R.id.btnUpdatePasword);
    }

    private void Listener(){

        btnUpdatePasword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=etForgetPwd.getText().toString().trim();
                if (!(email.length()==0)){
                    if (new Emailvalidation().email_validation(email)) {
                        emailFlag=1;
                        EMailID=email;
                        Mobile="";
                        forgotPassword();

                    }else{
                        etForgetPwd.setError(getString(R.string.EMail_Mob_not_valid));
                        etForgetPwd.requestFocus();
                    }
                }else {
                    etForgetPwd.setError(getString(R.string.error_field_required));
                    etForgetPwd.requestFocus();

                }
            }
        });


        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    private void forgotPassword(){

        if (NetWorkStatus.isNetworkAvailable(ForgotPassword.this)){
            new ForgetPwd_webService().forgetPwd_webServide(ForgotPassword.this,EMailID);
        }else {
            startActivity(new Intent(ForgotPassword.this, NetworkNotAvailable.class));
        }
    }

    @Override
    public void onBackPressed() {
        transitionflag= StaticClass.transitionflagBack;
        super.onBackPressed();
    }

    @Override
    public void onPause() {
        super.onPause();
        new OnPauseSlider(ForgotPassword.this,transitionflag);
    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) ForgotPassword.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }
}
