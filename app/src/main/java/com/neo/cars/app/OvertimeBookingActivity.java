package com.neo.cars.app;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.neo.cars.app.Interface.CallBackCustomTimePicker;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.AddStopOverAdapterBooking;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.BeforeOvertimeBooking_webservice;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.CustomTimePickerDialog1;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomButtonTitilliumWebRegular;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by parna on 19/3/18.
 */

public class OvertimeBookingActivity extends AppCompatActivity implements CallBackCustomTimePicker {

    private static final String TAG = OvertimeBookingActivity.class.getSimpleName();
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvDuration, tvNoOfPassengers, tvExpectedtime;
    private CustomEditTextTitilliumWebRegular tvExpectedTravelRoutes, tvReasonForExtension, tvAdditionalInfo,
            /*tvName, tvMobNo, */ tvBaggage;
    private LinearLayout llPassengerLayout, lladdmore_name_mobno;
    private CustomButtonTitilliumWebRegular btnAddMMore;
    private CustomButtonTitilliumSemibold btnOvertimeBook;
    private Bundle bundle;
    private String strDuration="", strBookingId="", strExpectedRoute="", strExtensionReason="",
            strExpectedTime="", strAddlInfo="", strBaggage="", strName="", strMobNo="", strMaxPassengers = "";
    private ConnectionDetector cd;
    private Context context;
    private String strLabel="";
    private BottomView bottomview = new BottomView();
    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;

    private ArrayList<String> Al_NameBooking=new ArrayList<String>();
    private ArrayList<String> Al_MobileNoBooking=new ArrayList<String>();

    private Uri uriContact;
    private String contactID;
    private AddStopOverAdapterBooking addStopOverAdapterBooking;
    private CustomTimePickerDialog1 customTimePickerDialog;
    private int selectedHour;
    private  String currentDate = "", currentHour = "", time24="", format = "", strStartDate = "";
    private static final int REQUEST_CODE_PICK_CONTACTS = 105;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overtime_booking);

        bundle = getIntent().getExtras();
        if(bundle != null){
            strDuration = bundle.getString("Duration");
            strBookingId = bundle.getString("BookingId");
            strLabel=bundle.getString("DurationLabel");
            strMaxPassengers=bundle.getString("strMaxPassengers");

            Log.d("d", "Duration::"+strDuration);
            Log.d("d", "BookingId::"+strBookingId);
            Log.d("d", "strMaxPassengers::"+strMaxPassengers);
        }

        new AnalyticsClass(OvertimeBookingActivity.this);


        Initialize();
        Listener();

        if (!strDuration.equals("") && !strDuration.equals("null")){
            tvDuration.setText(strLabel);
        }

        StaticClass.setAl_NameBooking(Al_NameBooking);
        StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);

    }

    private void Initialize(){
        context  = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvMyBookingsHeader));

        tvDuration = findViewById(R.id.tvDuration);
        tvExpectedTravelRoutes = findViewById(R.id.tvExpectedTravelRoutes);
        tvExpectedTravelRoutes = findViewById(R.id.tvExpectedTravelRoutes);
        tvReasonForExtension = findViewById(R.id.tvReasonForExtension);
        tvExpectedtime = findViewById(R.id.tvExpectedtime);
        tvAdditionalInfo = findViewById(R.id.tvAdditionalInfo);
        llPassengerLayout =  findViewById(R.id.llPassengerLayout);
        tvNoOfPassengers = findViewById(R.id.tvNoOfPassengers);
//        tvName = findViewById(R.id.tvName);
//        tvMobNo = findViewById(R.id.tvMobNo);
        tvBaggage = findViewById(R.id.tvBaggage);
        btnOvertimeBook = findViewById(R.id.btnOvertimeBook);

        btnAddMMore=findViewById(R.id.btnAddMMore);

        lladdmore_name_mobno=findViewById(R.id.lladdmore_name_mobno);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        tvNoOfPassengers.setText("(maximum "+strMaxPassengers+")");

        bottomview.BottomView(OvertimeBookingActivity.this, StaticClass.Menu_profile);
        StaticClass.isBookNow = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {

        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(OvertimeBookingActivity.this,
                OvertimeBookingActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                OvertimeBookingActivity.this.getResources().getString(R.string.yes),
                OvertimeBookingActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(OvertimeBookingActivity.this,
                        OvertimeBookingActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        OvertimeBookingActivity.this.getResources().getString(R.string.yes),
                        OvertimeBookingActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        btnOvertimeBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strBaggage = tvBaggage.getText().toString();

                boolean isMobileValid = true;

                if(strDuration.equals("") && strDuration.equals("null")){
                    new CustomToast(OvertimeBookingActivity.this, getResources().getString(R.string.error_field_required));

                }else if (TextUtils.isEmpty(time24)){
                    new CustomToast(OvertimeBookingActivity.this, getResources().getString(R.string.select_expected_time));
                }
                else
                    {
                        strExpectedRoute = tvExpectedTravelRoutes.getText().toString();
                        strExtensionReason = tvReasonForExtension.getText().toString();
                        strAddlInfo = tvAdditionalInfo.getText().toString();

                        /********* add more data  pass *******/
                        ArrayList<String> arrNamebooking = new ArrayList<String>();
                        ArrayList<String> arrMobileNoBooking = new ArrayList<String>();

                        if (StaticClass.getAl_NameBooking() != null) {
                            arrNamebooking = StaticClass.getAl_NameBooking();
                        }

                        if (StaticClass.getAl_MobileNoBooking() != null) {
                            arrMobileNoBooking = StaticClass.getAl_MobileNoBooking();
                        }

                        JSONArray jsonArrayNameMobNo = new JSONArray();

                        try {

                            for (int i=0;i< arrNamebooking.size();i++){

                                JSONObject Jobject1=new JSONObject();

                                if(!arrNamebooking.get(i).trim().equalsIgnoreCase("")){
                                    Jobject1.put("name", arrNamebooking.get(i).trim());
                                }

                                if(!arrMobileNoBooking.get(i).trim().equalsIgnoreCase("")){
                                    Jobject1.put("contact_no", arrMobileNoBooking.get(i).trim());
                                }
                                jsonArrayNameMobNo.put(Jobject1);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("jsonArrayNameMobNo: "+jsonArrayNameMobNo);


                        for(int i = 0; i < arrMobileNoBooking.size(); i++){

                            if(!arrMobileNoBooking.get(i).trim().equalsIgnoreCase("")) {
                                String mobileNo = arrMobileNoBooking.get(i).trim();

                                if (!new Emailvalidation().phone_validation(mobileNo)) {
                                    new CustomToast(OvertimeBookingActivity.this, "Please enter valid phone number");
                                    isMobileValid = false;
                                } else {

                                }
                            }
                        }

                        if(cd.isConnectingToInternet()){

                            if (isMobileValid) {
                                if (StaticClass.isBookNow) {
                                    StaticClass.isBookNow = false;
                                    new BeforeOvertimeBooking_webservice().processOvertimeBooking(OvertimeBookingActivity.this, strBookingId, strDuration, strExpectedRoute, strExtensionReason,
                                            strExpectedTime, strAddlInfo, strBaggage, jsonArrayNameMobNo.toString());
                                }
                            }
                        }else{
                            Intent i = new Intent(OvertimeBookingActivity.this, NetworkNotAvailable.class);
                            startActivity(i);
                        }
                    }
            }
        });

        tvExpectedtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customTimePickerDialog = new CustomTimePickerDialog1(OvertimeBookingActivity.this, 24,60,false);
                customTimePickerDialog.show();
            }
        });

        btnAddMMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    AddStopOverMethod();
            }
        });

    }
    private void AddStopOverMethod(){

        try {
            Log.d("Al_NameBookingsize", ""+StaticClass.getAl_NameBooking().size());
            if((StaticClass.getAl_NameBooking().size()<Integer.parseInt(strMaxPassengers))){

                if(Al_NameBooking.size()>0 || Al_MobileNoBooking.size() > 0){
                    boolean isVerified=true;
                    Log.d("Al_NameBookingSize", ""+Al_NameBooking.size());

                    for (int i=0;i<Al_NameBooking.size();i++){

                        if(Al_NameBooking.get(i).equals("") && Al_MobileNoBooking.get(i).equals("")){
                            isVerified=false;

                        }else if(!Al_MobileNoBooking.get(i).equals("")){

                            if(!new Emailvalidation().phone_validation(Al_MobileNoBooking.get(i).trim()) ){
                                isVerified=false;
                            }
                        }
                    }
                    if(isVerified){
                        Al_MobileNoBooking.add("");
                        Al_NameBooking.add("");
                        StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                        StaticClass.setAl_NameBooking(Al_NameBooking);

                        addStopOverAdapterBooking =  new AddStopOverAdapterBooking(OvertimeBookingActivity.this, lladdmore_name_mobno);
                    }else {
                        new CustomToast(OvertimeBookingActivity.this, MessageText.Enter_name_mobno);
                    }
                }else {
                    Al_MobileNoBooking.add("");
                    Al_NameBooking.add("");
                    StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                    StaticClass.setAl_NameBooking(Al_NameBooking);

                    addStopOverAdapterBooking =  new AddStopOverAdapterBooking(OvertimeBookingActivity.this, lladdmore_name_mobno);
                }

            }else {
                new CustomToast(OvertimeBookingActivity.this, getResources().getString(R.string.tvMaxAdditionalPassenger));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            System.out.println("requestCode: "+requestCode);
            System.out.println("data: "+data);
            switch (requestCode) {
                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();
//                    retrieveContactNumber();

                    getContactNumber(data, context);
                    break;

                default:
                    break;
            }
        }
    }

    private void retrieveContactNumber() {

        String contactNumber = null;
        // getting contacts ID
        Cursor cursorID = getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);
        if (cursorID.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }
        cursorID.close();
        Log.d(TAG, "Contact ID: " + contactID);

        // Using the contact ID now we will get contact phone number
        Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                new String[]{contactID},
                null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursorPhone.close();
        Log.d(TAG, "Contact Phone Number: " + contactNumber);
        if (contactNumber != null){
            contactNumber = contactNumber.replaceAll("[^0-9\\+]", "");
            Al_MobileNoBooking.set(StaticClass.book_now_contact_pos, contactNumber);
            StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
            StaticClass.setAl_NameBooking(Al_NameBooking);

        }else {
            Al_MobileNoBooking.set(StaticClass.book_now_contact_pos,"");
            Al_NameBooking.set(StaticClass.book_now_contact_pos,"");
            StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
            StaticClass.setAl_NameBooking(Al_NameBooking);
        }

        addStopOverAdapterBooking =  new AddStopOverAdapterBooking(OvertimeBookingActivity.this, lladdmore_name_mobno);

    }




    public void getContactNumber(Intent data, final Context context) {
        Cursor cursorPhone = null;
        String contactNumber = null;

        try {
            Uri result = data.getData();
//            Log.e(TAG, result.toString());
            String id = result.getLastPathSegment();
            cursorPhone = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);

            if (cursorPhone.moveToFirst()) {

                assert cursorPhone != null;
                contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            } else {
                // no results actions
            }

            Log.d(TAG, "Contact Phone Number: " + contactNumber);
            contactNumber = contactNumber.replaceAll("[^0-9\\+]", "");
            if (contactNumber != null && (new Emailvalidation()).phone_validation(contactNumber)){

                Al_MobileNoBooking.set(StaticClass.book_now_contact_pos,contactNumber);
                StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                StaticClass.setAl_NameBooking(Al_NameBooking);

            }else {

                new CustomToast(OvertimeBookingActivity.this, getResources().getString(R.string.txt_message_mobile_no));

                Al_MobileNoBooking.set(StaticClass.book_now_contact_pos,"");
                Al_NameBooking.set(StaticClass.book_now_contact_pos,"");
                StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                StaticClass.setAl_NameBooking(Al_NameBooking);
            }

            addStopOverAdapterBooking =  new AddStopOverAdapterBooking(OvertimeBookingActivity.this, lladdmore_name_mobno);

        } catch (Exception e) {
            // error actions
        } finally {
            if (cursorPhone != null) {
                cursorPhone.close();
            }
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.OverTimeBookingFlag) {
            StaticClass.OverTimeBookingFlag=false;
            finish();
        }

       // StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) OvertimeBookingActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void onButtonClick(TimePicker mTimePicker, int hour, int minute) {
        System.out.println("*****mTimePicker*****"+mTimePicker);
        System.out.println("*****hour*****"+hour);
        System.out.println("*****minute*****"+minute);

        selectedHour = hour;

        Log.d("d", "***strPickUptime***"+strExpectedTime);

        //here minute have 4 value : 0 for 00, 1 for 15, 2 for 30, 3 for 45

        if(minute == 0){
            minute = 0;
        }else if (minute == 1){
            minute = 15;
        }else if (minute == 2){
            minute = 30;
        }else if (minute == 3){
            minute = 45;
        }

        //strPickUptime to send in webservice

        time24="";
        if (hour < 10 && minute == 0){
            strExpectedTime = "0"+hour+":0"+minute;
        }else if (hour < 10){
            strExpectedTime = "0"+hour+":"+minute;
        }else if (minute == 0){
            strExpectedTime = hour+":0"+minute;
        }else{
            strExpectedTime = hour+":"+minute;
        }

        if (hour == 0) {
            hour += 12;
            format = "AM";

        } else if (hour == 12) {
            format = "PM";

        } else if (hour > 12) {
            hour -= 12;
            format = "PM";

        } else {
            format = "AM";
        }


        if (minute == 0){
            time24 = hour+":0"+minute+" "+format;
            if (hour < 10){
                time24 = "0"+hour+":0"+minute+" "+format;
            }
        }else if (hour < 10){
            time24 = "0"+hour+":"+minute+" "+format;
        }else {
            time24 = hour+":"+minute+" "+format;
        }

        tvExpectedtime.setText(time24);


        customTimePickerDialog.dismiss();
    }
}
