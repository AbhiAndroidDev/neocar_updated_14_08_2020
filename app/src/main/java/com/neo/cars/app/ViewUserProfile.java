package com.neo.cars.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.gson.Gson;
import com.neo.cars.app.Interface.OtpVerification_Interface;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.MyProfile_Webservice;
import com.neo.cars.app.dialog.CustomDialogOTP;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

/**
 * Created by parna on 11/6/18.
 */

public class ViewUserProfile extends AppCompatActivity implements Profile_Interface, OtpVerification_Interface {

    private ScrollView scrollview;
    private RelativeLayout rlParent, rlPicName, rlUserDetails, rlBackLayout, rlAddLayout;
    private CircularImageViewBorder civUserProfilePic;
    private CustomTextviewTitilliumWebRegular tvName, tvMobNo, tvDob, tvGender, tvNationality, tvEmail, tvState, tvCity, tvAddress, tvOccupation,
            tvCompany, tvFatherName, tvEmergencyContactName, tvEmergencyContactNo, tv_toolbar_title, rightTopBarText;

    private CustomTextviewTitilliumBold tvSecondaryDoc, tvSecondaryDocBack;
    private ImageView ivAadhaarCard, ivAadhaarCardBack, ivSecondaryDoc, ivSecondaryDocBack, ivPhnoVerify, ivEmailVerify;
    private Context context;
    private ConnectionDetector cd;
    private UserLoginDetailsModel muserLoginDetailsModel;
    private String strProfileEditable = "", strProfileEditableMessage = "", strMobileNo="";
    private Toolbar toolbar;
    private int transitionflag = StaticClass.transitionflagNext;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails sharedPref;
    private ImageButton ibEditMenu;
    private BottomView bottomview = new BottomView();
    private boolean phNoVerified=false;  //true for mob no verified and false for not verified mob no
    private boolean emailVerified = false; // true for email verified and false for not verified email


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewuserprofile);

        gson = new Gson();
        UserLoginDetails = new UserLoginDetailsModel();

        sharedPref = new SharedPrefUserDetails(ViewUserProfile.this);

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        new AnalyticsClass(ViewUserProfile.this);

        Initialize();

        //Fetch my profile details
        if (cd.isConnectingToInternet()) {
            new MyProfile_Webservice().MyProfile(ViewUserProfile.this);

        } else {
            transitionflag = StaticClass.transitionflagBack;
            startActivity(new Intent(ViewUserProfile.this, NetworkNotAvailable.class));

        }

        Listener();
    }

    private void Initialize() {
        context = this;
        cd = new ConnectionDetector(this);

        scrollview = findViewById(R.id.scrollview);
        rlParent = findViewById(R.id.rlParent);
        rlPicName = findViewById(R.id.rlPicName);
        rlUserDetails = findViewById(R.id.rlUserDetails);
        civUserProfilePic = findViewById(R.id.civUserProfilePic);
        tvName = findViewById(R.id.tvName);
        tvMobNo = findViewById(R.id.tvMobNo);
        tvDob = findViewById(R.id.tvDob);
        tvGender = findViewById(R.id.tvGender);
        tvNationality = findViewById(R.id.tvNationality);
        tvEmail = findViewById(R.id.tvEmail);
        tvState = findViewById(R.id.tvState);
        tvCity = findViewById(R.id.tvCity);
        tvAddress = findViewById(R.id.tvAddress);
        tvOccupation = findViewById(R.id.tvOccupation);
        tvCompany = findViewById(R.id.tvCompany);
        tvFatherName = findViewById(R.id.tvFatherName);
        tvEmergencyContactName = findViewById(R.id.tvEmergencyContactName);
        tvEmergencyContactNo = findViewById(R.id.tvEmergencyContactNo);
        tvSecondaryDoc = findViewById(R.id.tvSecondaryDoc);
        tvSecondaryDocBack = findViewById(R.id.tvSecondaryDocBack);
        ivAadhaarCard = findViewById(R.id.ivAadhaarCard);
        ivAadhaarCardBack = findViewById(R.id.ivAadhaarCardBack);
        ivSecondaryDoc = findViewById(R.id.ivSecondaryDoc);
        ivSecondaryDocBack = findViewById(R.id.ivSecondaryDocBack);
        ivPhnoVerify = findViewById(R.id.ivPhnoVerify);
        ivEmailVerify = findViewById(R.id.ivEmailVerify);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvProfileHeader));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);
        rightTopBarText=findViewById(R.id.rightTopBarText);
        rightTopBarText.setVisibility(View.GONE);
        ibEditMenu = findViewById(R.id.ibEditMenu);
        ibEditMenu.setVisibility(View.VISIBLE);

        bottomview.BottomView(ViewUserProfile.this,StaticClass.Menu_profile);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener() {

        civUserProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ViewUserProfile.this, ImageFullActivity.class);
                i.putExtra("imgUrl", UserLoginDetails.getProfile_pic());
                startActivity(i);

            }
        });

        ibEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 if("N".equals(strProfileEditable)) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(ViewUserProfile.this);
                     builder.setMessage(strProfileEditableMessage)
                             .setCancelable(false)
                             .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {

                                 }
                             });
                     AlertDialog alert = builder.create();
                     alert.show();
                 }else{
                     transitionflag = StaticClass.transitionflagNext;
                     Intent editprofileIntent = new Intent(ViewUserProfile.this, EditProfileActivity.class);
                     startActivity(editprofileIntent);
                     finish();
                 }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });


        ivPhnoVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(!phNoVerified){
//                    if(cd.isConnectingToInternet()){
//                        new GetLoginOtp_Webservice().GetLoginOtp(ViewUserProfile.this, strMobileNo);
//                        // new EditProfileOTP_webservice().EditProfileOTP(ViewUserProfile.this, strMobileNo);
//
//                    }else{
//                        startActivity(new Intent(ViewUserProfile.this, NetworkNotAvailable.class));
//
//                    }
//                }
            }
        });

//        ivEmailVerify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!emailVerified){
//                    new CustomToast(ViewUserProfile.this, getResources().getString(R.string.strEmailVerificationCheck));
//                }
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (cd.isConnectingToInternet()) {
            new MyProfile_Webservice().MyProfile(ViewUserProfile.this);

        } else {
            transitionflag = StaticClass.transitionflagBack;
            startActivity(new Intent(ViewUserProfile.this, NetworkNotAvailable.class));

        }

        if(StaticClass.BottomProfile){
            finish();
        }

        if(StaticClass.EditProfileIsSave){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public void MyProfile(UserLoginDetailsModel userLoginDetailsModel) {

        muserLoginDetailsModel = new UserLoginDetailsModel();
        muserLoginDetailsModel = userLoginDetailsModel;

        if (!userLoginDetailsModel.getProfile_name().equals("") && !userLoginDetailsModel.getProfile_name().equals("null")) {
            tvName.setText(userLoginDetailsModel.getProfile_name());
        }else{
            tvName.setText("N/A");
        }

        if (!userLoginDetailsModel.getMobile().equals("") && !userLoginDetailsModel.getMobile().equals("null")) {
            strMobileNo = userLoginDetailsModel.getMobile();
            tvMobNo.setText(userLoginDetailsModel.getMobile());
        }else{
            tvMobNo.setText("N/A");
        }

//        if(!userLoginDetailsModel.getMobile_verified().equals("") && !userLoginDetailsModel.getMobile_verified().equals("null")){
//            if("Y".equalsIgnoreCase(userLoginDetailsModel.getMobile_verified())){
//                phNoVerified = true;
//                ivPhnoVerify.setVisibility(View.VISIBLE);
//                ivPhnoVerify.setImageResource(R.drawable.verified_icon);
//
//            }else if ("N".equalsIgnoreCase(userLoginDetailsModel.getMobile_verified())){
//                phNoVerified = false;
//                ivPhnoVerify.setVisibility(View.VISIBLE);
//                ivPhnoVerify.setImageResource(R.drawable.not_verified_icon);
//
//            }
//        }

        if (!userLoginDetailsModel.getEmail_verified().equals("") && !userLoginDetailsModel.getEmail_verified().equals("null")){

//            if("Y".equalsIgnoreCase(userLoginDetailsModel.getEmail_verified())){
//                emailVerified = true;
//                ivEmailVerify.setVisibility(View.VISIBLE);
//                ivEmailVerify.setImageResource(R.drawable.verified_icon);
//
//            }else if ("N".equalsIgnoreCase(userLoginDetailsModel.getEmail_verified())){
//                emailVerified = false;
//                ivEmailVerify.setVisibility(View.VISIBLE);
//                ivEmailVerify.setImageResource(R.drawable.not_verified_icon);
//
//            }
        }

        if (!userLoginDetailsModel.getDob().equals("") && !userLoginDetailsModel.getDob().equals("null")) {
            tvDob.setText(userLoginDetailsModel.getDob());
        }else{
            tvDob.setText("N/A");
        }

        if (!userLoginDetailsModel.getSex().equals("") && !userLoginDetailsModel.getSex().equals("null")) {
            if ("F".equals(userLoginDetailsModel.getSex())) {
                //spnrGender.setSelection(1);
                tvGender.setText("Female");

            } else if ("M".equals(userLoginDetailsModel.getSex())) {
                //spnrGender.setSelection(0);
                tvGender.setText("Male");
            }
        }else{
            tvGender.setText("N/A");
        }


        if (!userLoginDetailsModel.getNationality().equals("") && !userLoginDetailsModel.getNationality().equals("null")) {
            tvNationality.setText(userLoginDetailsModel.getNationality());
        }else{
            tvNationality.setText("N/A");
        }

        if (!userLoginDetailsModel.getEmail().equals("") && !userLoginDetailsModel.getEmail().equals("null")) {
            tvEmail.setText(userLoginDetailsModel.getEmail());
        }else{
            tvEmail.setText("N/A");
        }

        if (!userLoginDetailsModel.getState_name().equals("") && !userLoginDetailsModel.getState_name().equals("null")) {
            tvState.setText(userLoginDetailsModel.getState_name());
        }else{
            tvState.setText("N/A");
        }

        if (!userLoginDetailsModel.getCity_name().equals("") && !userLoginDetailsModel.getCity_name().equals("null")) {
            tvCity.setText(userLoginDetailsModel.getCity_name());
        }else{
            tvCity.setText("N/A");
        }

        if (!userLoginDetailsModel.getAddress().equals("") && !userLoginDetailsModel.getAddress().equals("null")) {
            tvAddress.setText(userLoginDetailsModel.getAddress());
        }else{
            tvAddress.setText("N/A");
        }

        if (!userLoginDetailsModel.getOccupation_info().equals("") && !userLoginDetailsModel.getOccupation_info().equals("null")) {
            tvOccupation.setText(userLoginDetailsModel.getOccupation_info());
        }else if(!userLoginDetailsModel.getOther_occupation().equals("") && !userLoginDetailsModel.getOther_occupation().equals("null")){
            tvOccupation.setText(userLoginDetailsModel.getOther_occupation());
        }else{
            tvOccupation.setText("N/A");
        }

        if (!userLoginDetailsModel.getCompany().equals("") && !userLoginDetailsModel.getCompany().equals("null")) {
            tvCompany.setText(userLoginDetailsModel.getCompany());
        }else{
            tvCompany.setText("N/A");
        }

        if (!userLoginDetailsModel.getFather_name().equals("") && !userLoginDetailsModel.getFather_name().equals("null")) {
            tvFatherName.setText(userLoginDetailsModel.getFather_name());
        }else{
            tvFatherName.setText("N/A");
        }

        if (!userLoginDetailsModel.getEmergency_contact_name().equals("") && !userLoginDetailsModel.getEmergency_contact_name().equals("null")) {
            tvEmergencyContactName.setText(userLoginDetailsModel.getEmergency_contact_name());
        }else{
            tvEmergencyContactName.setText("N/A");
        }

        if (!userLoginDetailsModel.getEmergency_contact_number().equals("") && !userLoginDetailsModel.getEmergency_contact_number().equals("null")) {
            tvEmergencyContactNo.setText(userLoginDetailsModel.getEmergency_contact_number());
        }else{
            tvEmergencyContactNo.setText("N/A");
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getLast_document_name())){
            tvSecondaryDoc.setText(userLoginDetailsModel.getLast_document_name()+"(Front)");
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getLast_document_name())){
            tvSecondaryDocBack.setText(userLoginDetailsModel.getLast_document_name()+"(Back)");
        }

        System.out.println("getAadhar_document_url**********" + userLoginDetailsModel.getAadhar_document_url());
        System.out.println("getLast_document_url**********" + userLoginDetailsModel.getLast_document_url());

        if (!TextUtils.isEmpty(userLoginDetailsModel.getAadhar_document_url())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getAadhar_document_url()).into(ivAadhaarCard);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getAadhar_document_back_url())){
            try{

                Picasso.get().load(userLoginDetailsModel.getAadhar_document_back_url()).into(ivAadhaarCardBack);

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getLast_document_url())) {
            try {
                Log.d("ivSecondaryDoc**", userLoginDetailsModel.getLast_document_url());
                Picasso.get().load(userLoginDetailsModel.getLast_document_url()).into(ivSecondaryDoc);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getLast_document_back_url())) {
            try {
                Log.d("ivSecondaryDoc**", userLoginDetailsModel.getLast_document_back_url());
                Picasso.get().load(userLoginDetailsModel.getLast_document_back_url()).into(ivSecondaryDocBack);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(!userLoginDetailsModel.getProfile_pic().equals("") && !userLoginDetailsModel.getProfile_pic().equals("null")){
            try {
                Picasso.get().load(UserLoginDetails.getProfile_pic()).into(civUserProfilePic);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (! userLoginDetailsModel.getIs_profile_editable().equals("") && !userLoginDetailsModel.getIs_profile_editable().equals("null")) {
            strProfileEditable = userLoginDetailsModel.getIs_profile_editable();
            Log.d("d", "is Profile editable: " + strProfileEditable);
            if("N".equals(strProfileEditable)) {
                strProfileEditableMessage = userLoginDetailsModel.getProfile_noneditable_message();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(ViewUserProfile.this, transitionflag);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;

    }

    @Override
    public void OtpVerification(String status, String st_mobile, String otp, String userType) {
            new CustomDialogOTP(context,ViewUserProfile.this, "" , st_mobile, userType, "");
    }
}
