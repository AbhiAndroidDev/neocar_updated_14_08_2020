package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.neo.cars.app.Adapter.RatingReviewAdapter;
import com.neo.cars.app.Interface.RatingReviewList_Interface;
import com.neo.cars.app.SetGet.RatingReviewAllListModel;
import com.neo.cars.app.SetGet.RatingReviewListModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.RiderRatingReviewListWebService;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;

public class RiderRatingDetailsActivity extends AppCompatActivity implements RatingReviewList_Interface, SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvRatingReviewTotal;
    private RelativeLayout rlBackLayout;
    private RecyclerView rcvRatingReview;
    private RatingReviewAdapter reviewAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomTextviewTitilliumBold tvRatingValue, tvNoRatingsFound;
    private int transitionflag = StaticClass.transitionflagNext;
    private ImageView ivInfo;
    private ArrayList<RatingReviewAllListModel> reviewListModels;
    private LinearLayoutManager layoutManagerVertical;
    private RatingBar ratingBarDetails;
    private String user_id;
    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_rating_details);

        new AnalyticsClass(RiderRatingDetailsActivity.this);
        initialize();
        listener();
        refreshList();
    }

    private void initialize() {
        context = RiderRatingDetailsActivity.this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        swipeRefreshLayout = findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(RiderRatingDetailsActivity.this);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvRatings));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvRatingValue = findViewById(R.id.tvRatingValue);
        tvNoRatingsFound = findViewById(R.id.tvNoRatingsFound);
        tvRatingReviewTotal = findViewById(R.id.tvRatingReviewTotal);
        ivInfo = findViewById(R.id.ivInfo);
        rcvRatingReview = findViewById(R.id.rcvRatingReview);
        ratingBarDetails = findViewById(R.id.ratingBarDetails);
        ratingBarDetails.setIsIndicator(true);
        bottomview.BottomView(RiderRatingDetailsActivity.this, StaticClass.Menu_profile);
    }

    private void listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CustomToast(RiderRatingDetailsActivity.this, MessageText.tvRatingInfo);

            }
        });

    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void  refreshList(){
        if (NetWorkStatus.isNetworkAvailable(context)) {
            new RiderRatingReviewListWebService().RiderRatingReviewList(RiderRatingDetailsActivity.this);

        } else {
            StaticClass.MyBookingListFlag=true;
            Intent i = new Intent(context, NetworkNotAvailable.class);
            startActivity(i);
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onRatingReviewList(RatingReviewListModel ratingReviewList) {

        this.reviewListModels = ratingReviewList.getRatingReviewAllListModels();

        tvRatingValue.setText(ratingReviewList.getAverage_rating());

        ratingBarDetails.setRating(Float.parseFloat(ratingReviewList.getAverage_rating()));

        tvRatingReviewTotal.setText(ratingReviewList.getCount_rating()+" Ratings "+"& "+ratingReviewList.getCount_review()+" Reviews");

        if (reviewListModels.size() > 0){

            reviewAdapter = new RatingReviewAdapter(context, reviewListModels);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvRatingReview.setLayoutManager(layoutManagerVertical);
            rcvRatingReview.setItemAnimator(new DefaultItemAnimator());
            rcvRatingReview.setHasFixedSize(true);
            rcvRatingReview.setAdapter(reviewAdapter);
        }else {
            rcvRatingReview.setVisibility(View.GONE);
            tvNoRatingsFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.BottomProfile){
            finish();
        }

        if( StaticClass.MyBookingListFlag){
            finish();

        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(RiderRatingDetailsActivity.this, transitionflag);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }
}
