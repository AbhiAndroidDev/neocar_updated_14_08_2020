package com.neo.cars.app.SetGet;

import java.util.ArrayList;

/**
 * Created by parna on 12/9/18.
 */

public class RatingReviewListModel {

    String average_rating;
    String count_rating;
    String count_review;

    ArrayList<RatingReviewAllListModel> ratingReviewAllListModels;

    public ArrayList<RatingReviewAllListModel> getRatingReviewAllListModels() {
        return ratingReviewAllListModels;
    }

    public void setRatingReviewAllListModels(ArrayList<RatingReviewAllListModel> ratingReviewAllListModels) {
        this.ratingReviewAllListModels = ratingReviewAllListModels;
    }

    public String getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(String average_rating) {
        this.average_rating = average_rating;
    }

    public String getCount_rating() {
        return count_rating;
    }

    public void setCount_rating(String count_rating) {
        this.count_rating = count_rating;
    }

    public String getCount_review() {
        return count_review;
    }

    public void setCount_review(String count_review) {
        this.count_review = count_review;
    }

}
