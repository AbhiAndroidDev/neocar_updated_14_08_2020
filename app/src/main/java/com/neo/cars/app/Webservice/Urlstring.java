package com.neo.cars.app.Webservice;


public class Urlstring {

//    public static final String Api_serverkey_previous = "AIzaSyBQWoQ334aT5DWJUrzywo1B5B0V6cATZjU";
//
//    public static final String Api_serverkey = "AIzaSyDLOK9YB0bQYKoGQgv3XikbX6u6OgjVabA";

    public static String autocompletesearchlocation = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    public static String geocode = "https://maps.googleapis.com/maps/api/geocode/json?";
//    public static String directions = "http://maps.googleapis.com/maps/api/directions/json?";

//    public static String Basepathmain = "http://neocars.testyourprojects.biz/"; // internal development
//    public static String Basepathmain = "http://neocarslive.testyourprojects.biz/";  //build for client
     public static String Basepathmain = "http://staging.neocars.in/public/";  //build for client new added
   // public static String Basepathmain = "https://www.neocars.in/mobile-app/public/";  // base url for Play store


    public static String Basepath = Basepathmain+"api/";

    public static String get_api_access_token = Basepathmain + "apiAuth/get_api_access_token";
    public static String email_login = Basepath + "email_login";
    public static String get_login_otp = Basepath + "get_login_otp";
    public static String mobile_update = Basepath + "mobile_update";
    public static String email_update = Basepath + "email_update";
    public static String get_com_primary_seconday_otp = Basepath + "get_com_primary_seconday_otp";
    public static String mobile_login = Basepath + "mobile_login";
    public static String welcome_email = Basepath + "welcome_email";
    public static String verify_primary_secondary_otp = Basepath + "verify_primary_secondary_otp";
    public static String registration = Basepath + "registration";
    public static String social_login = Basepath + "social_login";
    public static String my_profile = Basepath + "my_profile";
    public static String update_password = Basepath + "reset_password";
    public static String edit_profile = Basepath + "edit_profile";
    public static String edit_company_profile = Basepath + "edit_company_profile";
    public static String state_list = Basepath +  "state_list";
    public static String city_list = Basepath + "city_list";
    public static String vehicle_unavailable_date_list = Basepath + "vehicle_unavailable_date_list";
    public static String upcoming_booking_date_list = Basepath + "upcoming_booking_date_list";
    public static String driver_unavailable_date_list = Basepath + "driver_unavailable_date_list";
    public static String vehicle_unavailable_details = Basepath + "vehicle_unavailable_details";
    public static String driver_unavailable_details = Basepath + "driver_unavailable_details";
    public static String vehicle_unavailable_delete = Basepath + "vehicle_unavailable_delete";
    public static String driver_unavailable_delete = Basepath + "driver_unavailable_delete";
    public static String add_com_vehicle_unavailable = Basepath + "add_com_vehicle_unavailable";
    public static String add_edit_driver_unavailable = Basepath + "add_edit_driver_unavailable";
    public static String state_city_list = Basepath + "state_city_list";
    public static String user_notification  = Basepath + "user_notification";
    public static String document_list = Basepath + "document_list";
    public static String slug_url = Basepathmain + "pages";
    public static String wishlist_listing = Basepath + "wishlist_listing";
    public static String vehicle_type = Basepath + "vehicle_type";
    public static String vehicle_brand = Basepath + "vehicle_make";
    public static String vehicle_brands_filter = Basepath + "vehicle_brands_filter";
    public static String user_vehicle_list = Basepath + "user_vehicle_list";
    public static String company_driver_list = Basepath + "company_driver_list";
    public static String vehicle_driver_association = Basepath + "vehicle_driver_association";
    public static String vehicle_make = Basepath + "vehicle_make";
    public static String vehicle_model = Basepath + "vehicle_model";
    public static String vehicle_add = Basepath + "vehicle_add";
    public static String driver_add_update = Basepath + "driver_add_update";
    public static String company_driver_add_update = Basepath + "company_driver_add_update";
    public static String vehicle_list = Basepath + "vehicle_list";
    public static String vehicle_details = Basepath + "vehicle_details";
    public static String user_delete = Basepath + "user_soft_delete";
    public static String log_out = Basepath + "log_out";
    public static String driver_details = Basepath + "driver_details";
    public static String vehicle_edit = Basepath + "vehicle_edit";
    public static String my_vehicle_booking = Basepath + "my_vehicle_booking";
    public static String carseeker_booking_list = Basepath + "carseeker_booking_list";
    public static String company_booking_list = Basepath + "company_booking_list";
    public static String booking_details = Basepath + "booking_details";
    public static String company_booking_details = Basepath + "company_booking_details";
    public static String booking_hours = Basepath + "booking_hours";
    public static String vehicle_booking_availability = Basepath + "vehicle_booking_availibility";
    public static String hire_purpose = Basepath + "booking_purpose";
    public static String process_booking = Basepath + "process_booking";
    public static String wishlist_add = Basepath + "wishlist_add";
    public static String user_voucher  = Basepath + "user_voucher";
    public static String cancel_booking_carseeker = Basepath + "cancel_booking_carseeker";
    public static String overtime_availability = Basepath + "overtime_avalibility";
    public static String before_overtime_booking = Basepath + "before_overtime_booking";
    public static String process_overtime_booking = Basepath + "process_overtime_booking";
    public static String wishlist_delete = Basepath + "wishlist_delete";
    public static String cancel_booking_carowner = Basepath + "cancel_booking_carowner";
    public static String stop_booking = Basepath + "stop_booking";
    public static String before_process_booking = Basepath + "before_process_booking";
    public static String payment_information  = Basepath + "payment_information";
    public static String payout_information  = Basepath + "payout_information";
    public static String company_payout_information  = Basepath + "company_payout_information";
    public static String user_voucher_redeem = Basepath + "user_voucher_redeem";
    public static String preocess_payment = Basepath + "preocess_payment";
    public static String payout_information_details = Basepath + "payout_information_details";
    public static String company_payout_info_details = Basepath + "company_payout_info_details";
    public static String toggle_notification = Basepath + "toggle_notification";
    public static String user_occupation = Basepath + "occupation_type";
    public static String owner_deactive_vehicle = Basepath + "owner_diactive_vehicle";
    public static String vehicle_soft_delete = Basepath + "vehicle_soft_delete";
    public static String read_notification = Basepath + "read_notification";
    public static String delete_notification = Basepath + "delete_notification";
    public static String confirm_booking = Basepath + "confirm_booking";
    public static String forgot_password = Basepath + "forgot_password";
    public static String add_review_rating = Basepath + "add_review_rating";
    public static String owner_add_review = Basepath + "owner_add_review";
    public static String is_user_mobile_email_verified = Basepath + "is_user_mobile_email_verified";
    public static String is_user_verified_for_booking = Basepath + "is_user_verified_for_booking";
    public static String vehicle_review_rating_list = Basepath + "vehicle_review_rating_list";
    public static String rider_review_rating_list = Basepath + "rider_review_rating_list";
    public static String companyregistration = Basepath + "company_registration";
    public static String view_company_profile = Basepath + "view_company_profile";
    public static String booking_list_on_calendar_date = Basepath + "booking_list_on_calendar_date";
    public static String booking_availability_checking = Basepath + "booking_availability_checking";
    public static String vehicle_image_delete = Basepath + "vehicle_image_delete";
    public static String vehicle_types_filter = Basepath + "vehicle_types_filter";
    public static String searched_vehicle_list = Basepath + "searched_vehicle_list";
    public static String before_redirect_to_paytm = Basepath + "before_redirect_to_paytm";
    public static String overtime_before_redirect_to_paytm = Basepath + "overtime_before_redirect_to_paytm";
}
