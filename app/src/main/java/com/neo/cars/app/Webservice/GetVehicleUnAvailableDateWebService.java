package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.VehicleUnAvailability_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleUnAvailabilityModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GetVehicleUnAvailableDateWebService {
    private Context mcontext;
    private Activity activity;
    private String Status = "0", Msg = "", strUserDeleted="", yearMonth = "";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private VehicleUnAvailabilityModel vehicleUnAvailabilityModel;
    private ArrayList<VehicleUnAvailabilityModel> arrListVehicleUnAvailabilityModel;
    private VehicleUnAvailability_Interface vehicleUnAvailability_interface;


    public void getUnAvailableDate(Context context, final Activity activity, final String strvehicleId, final String yearMonth, VehicleUnAvailability_Interface vehicleUnAvailability_interface ){

        this.mcontext = context;
        this.activity = activity;
        sharedPref = new SharedPrefUserDetails(mcontext);
        this.yearMonth = yearMonth;
        this.vehicleUnAvailability_interface = vehicleUnAvailability_interface;
        showProgressDialog();

        StringRequest citylistRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_unavailable_date_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(activity, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", sharedPref.getUserid());
                params.put("vehicle_id", strvehicleId);
                params.put("year_month", yearMonth);

                new PrintClass("city params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        citylistRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(citylistRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{

            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_unavailable_date_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_unavailable_date_list").optString("message");
            Status = jobj_main.optJSONObject("vehicle_unavailable_date_list").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("vehicle_unavailable_date_list").optJSONArray("details");

            arrListVehicleUnAvailabilityModel = new ArrayList<>();

            for (int i=0; i<jsonArrayDetails.length(); i++){
                vehicleUnAvailabilityModel = new VehicleUnAvailabilityModel();
                JSONObject jObject = jsonArrayDetails.optJSONObject(i);
                String unavailableDate = jObject.optString("unavailable_date");
                vehicleUnAvailabilityModel.setUnavailable_date(unavailableDate);
                arrListVehicleUnAvailabilityModel.add(vehicleUnAvailabilityModel);
            }


        }catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        if (Status.equals(StaticClass.SuccessResult)) {
            vehicleUnAvailability_interface.unAvailableDate(arrListVehicleUnAvailabilityModel);

        }

    }
}
