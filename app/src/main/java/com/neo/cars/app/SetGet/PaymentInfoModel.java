package com.neo.cars.app.SetGet;

/**
 * Created by joydeep on 18/5/18.
 */

public class PaymentInfoModel {

    String booking_id,user_id,booking_date,
            pickup_location,drop_location,
            total_amount, amount, refund;

    String net_payable_amount,discount_amount, payment_status, settled_datetime, booking_ref_id, trip_status;

    public String getBooking_ref_id() {
        return booking_ref_id;
    }

    public void setBooking_ref_id(String booking_ref_id) {
        this.booking_ref_id = booking_ref_id;
    }

    public String getTrip_status() {
        return trip_status;
    }

    public void setTrip_status(String trip_status) {
        this.trip_status = trip_status;
    }

    public String getSettled_datetime() {
        return settled_datetime;
    }

    public void setSettled_datetime(String settled_datetime) {
        this.settled_datetime = settled_datetime;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getNet_payable_amount() {
        return net_payable_amount;
    }

    public void setNet_payable_amount(String net_payable_amount) {
        this.net_payable_amount = net_payable_amount;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRefund() {
        return refund;
    }

    public void setRefund(String refund) {
        this.refund = refund;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }


}
