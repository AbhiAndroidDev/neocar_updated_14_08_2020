package com.neo.cars.app.LocationTracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;

import com.neo.cars.app.Interface.CurrentLatLong_Interface;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.NetWorkStatus;


public class CurrentLatLong {
	private GPSTracker gpsTracker;
	public Double currentLat=0.0,currentLong=0.0;
	Activity mcontext;
	int status=0;
    private SharedPrefUserDetails sharedPrefUserDetails;

	public int currentlatlong(Activity context){

		mcontext=context;
        sharedPrefUserDetails = new SharedPrefUserDetails(context);

		if(NetWorkStatus.isNetworkAvailable(mcontext)){
			Navigation();

		}else{
			AlertDialog.Builder alert = new AlertDialog.Builder(mcontext);
			alert.setIcon(android.R.drawable.ic_dialog_alert);
			alert.setTitle("");
			alert.setMessage(MessageText.Network_not_availabl);
			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mcontext.finish();					
				}
			});
			alert.show();
		}
		return status;

	}


	private void Navigation(){
		AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
		if(hasGpsOnDevice()){
			if(isGpsEnabledOnDevice()){
				try {
//					gpsTracker = new GPSTracker(mcontext);
//					currentLat=gpsTracker.getLocation().getLatitude();
//					currentLong=gpsTracker.getLocation().getLongitude();

                    currentLat = Double.parseDouble(sharedPrefUserDetails.getCurrentLat());
                    currentLong = Double.parseDouble(sharedPrefUserDetails.getCurrentLong());

					//gpsTracker.getLocation().bearingTo(gpsTracker.getLocation());

					((CurrentLatLong_Interface)mcontext).onLatLong(currentLat,currentLong);
					status=1;
					Log.d("location status", status + "");
				} catch (Exception e) {					
					//Toast.makeText(DirectionAndParking.this, "GPs are not enable", Toast.LENGTH_LONG).show();
				}	
			}else{
//				showCustomDialog();
			}
		}else{
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setTitle("");
			builder.setMessage("This device does not have GPS feature");
			builder.setPositiveButton("Ok", null);
			builder.show();
		}			
	}

	public boolean hasGpsOnDevice(){
		PackageManager pm = mcontext.getPackageManager();
		boolean hasGps = pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
		return hasGps;
	}

	public boolean isGpsEnabledOnDevice(){
		@SuppressWarnings("static-access")
		LocationManager mlocManager = (LocationManager)mcontext.getSystemService(mcontext.LOCATION_SERVICE);
		boolean isProviderEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


		return isProviderEnabled;
	}

	public void showCustomDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle("");
		builder.setMessage("Please enable your device location.");
		builder.setCancelable(false);
		builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mcontext.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
				//turnGPSOn();

			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface paramDialogInterface, int paramInt) {
				// TODO Auto-generated method stub
//                    finish();

			}
		});
		//builder.setNegativeButton("No", null);
		builder.show();
	}

	private void turnGPSOn(){
		/*String provider = Settings.Secure.getString(mcontext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		System.out.println("********provider*********"+provider);

		if(!provider.contains("gps")){ //if gps is disabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			mcontext.sendBroadcast(poke);
		}*/

		Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
		intent.putExtra("enabled", true);
		mcontext.sendBroadcast(intent);
	}
}
