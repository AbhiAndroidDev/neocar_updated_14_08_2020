package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.HirePurposeModel;
import java.util.ArrayList;

/**
 * Created by parna on 10/5/18.
 */

public interface HirePurpose_Interface {

    void HirePurposeList(ArrayList<HirePurposeModel> arrlistHirePurpose);
}
