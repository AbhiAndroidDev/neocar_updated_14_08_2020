package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.neo.cars.app.Interface.PushNotificationInterface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CommonUtility;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.PushNotificationWebservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

/**
 * Created by parna on 31/5/18.
 */

public class PushNotificationActivity extends AppCompatActivity implements PushNotificationInterface {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;
    private Context context;
    private ConnectionDetector cd;
    private String notificationstatus="";
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails sharedPref;
    private CheckBox chkNotification;
    private String userid="";
    private BottomView bottomview = new BottomView();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pushnotificarion);


        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        cd = new ConnectionDetector(this);

        sharedPref = new SharedPrefUserDetails(PushNotificationActivity.this);

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        userid=UserLoginDetails.getId();
        Log.e("userid",userid);

        if(cd.isConnectingToInternet()){
            new PushNotificationWebservice().PushNotification(PushNotificationActivity.this,userid,notificationstatus);


        }else{
            Intent i = new Intent(PushNotificationActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.notificationsettings));

        new AnalyticsClass(PushNotificationActivity.this);

        initialize();
        listener();

        String strtimeformat=new CommonUtility(PushNotificationActivity.this).timeformat("4:05:45");
        Log.e("Stringtimeformat",strtimeformat);

        bottomview.BottomView(PushNotificationActivity.this, StaticClass.Menu_profile);

    }
    private void initialize(){
        rlBackLayout = findViewById(R.id.rlBackLayout);
        chkNotification = findViewById(R.id.chkNotification);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
        chkNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(chkNotification.isChecked()){
                    notificationstatus="Y";
                }
                else {
                    notificationstatus="N";

                }

                if(cd.isConnectingToInternet()){
                    new PushNotificationWebservice().PushNotification(PushNotificationActivity.this,userid,notificationstatus);


                }else{
                    Intent i = new Intent(PushNotificationActivity.this, NetworkNotAvailable.class);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onpushNotificationInterface(String strNotificationStatus) {

        if (strNotificationStatus.equals("Y")){
            chkNotification.setChecked(true);

        }
        else {
            chkNotification.setChecked(false);

        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) PushNotificationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if (StaticClass.isLoginFalg) {
//            transitionflag = StaticClass.transitionflagBack;
//            finish();
//
//        }
    }
}
