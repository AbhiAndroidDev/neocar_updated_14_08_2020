package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neo.cars.app.CompanyDriverDetailsActivity;
import com.neo.cars.app.Interface.CompanyDriverList_interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CompanyDriverListModel;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by parna on 9/11/18.
 */

public class CompanyDriverListAdapter extends RecyclerView.Adapter<CompanyDriverListAdapter.MyViewHolder> {

    private ArrayList<CompanyDriverListModel> companyDriverList;
    private Context context;
    private int transitionflag = StaticClass.transitionflagNext;
    private CompanyDriverList_interface driverListAdapterInterface;

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView civDriverPic, ivVehicleStatus;
        CustomTextviewTitilliumWebRegular tvDriverName, tvDriverAge, tvDriverPhoneNo;
        RelativeLayout rlParent;

        public MyViewHolder(View view) {
            super(view);
            civDriverPic = view.findViewById(R.id.civDriverPic);
            ivVehicleStatus = view.findViewById(R.id.ivVehicleStatus);
            tvDriverName = view.findViewById(R.id.tvDriverName);
            tvDriverAge = view.findViewById(R.id.tvDriverAge);
            tvDriverPhoneNo = view.findViewById(R.id.tvDriverPhoneNo);
            rlParent = view.findViewById(R.id.rlParent);

        }
    }

    public CompanyDriverListAdapter (Context context, CompanyDriverList_interface driverListInterface, ArrayList<CompanyDriverListModel> myDriverlist){
        this.context = context;
        driverListAdapterInterface=driverListInterface;
        companyDriverList = myDriverlist;
    }

    @Override
    public CompanyDriverListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.company_driver_list_item, parent, false);
        return new CompanyDriverListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompanyDriverListAdapter.MyViewHolder holder, final  int position) {

        holder.tvDriverName.setText(companyDriverList.get(position).getName());
        holder.tvDriverAge.setText(companyDriverList.get(position).getAge());
        holder.tvDriverPhoneNo.setText(companyDriverList.get(position).getContactno());

        Log.d("d", "Driver id:::" + companyDriverList.get(position).getDriver_id());


        if (!companyDriverList.get(position).getStatus().equalsIgnoreCase("A")){
            holder.ivVehicleStatus.setVisibility(View.VISIBLE);
        }

        Picasso.get().load(companyDriverList.get(position).getRecent_photograph()).transform(new CropCircleTransformation()).into(holder.civDriverPic);
        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag= StaticClass.transitionflagNext;
                Intent moreIntent = new Intent(context, CompanyDriverDetailsActivity.class);
                moreIntent.putExtra("driverId", companyDriverList.get(position).getDriver_id());
//                moreIntent.putExtra("VehicleStatus", companyDriverList.get(position).getVehicle_status());
                context.startActivity(moreIntent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return companyDriverList.size();
    }
}
