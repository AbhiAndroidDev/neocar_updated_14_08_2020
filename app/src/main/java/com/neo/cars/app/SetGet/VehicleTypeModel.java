package com.neo.cars.app.SetGet;

import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;

/**
 * Created by parna on 29/3/18.
 */

public class VehicleTypeModel {

    String id;
    String type_name;
    String sitting_capacity;
    String luggae_capacity;
    String code;
    String title;
    String is_active;
    String make_id;
    String vehicle_id;
    String brand;
    String model;
    String year;
    String km_travelled;
    String special_info;
    String ac_available;
    String state;
    String city;
    String hourly_rate;
    String max_luggage;
    String max_passenger;
    String license_plate_no;
    String image_url;
    String driver_id;
    String driver_name;
    String driver_contact_no;
    String driver_email;
    String driver_address;
    String vehicle_type_id;
    String make;
    String state_id;
    String city_id;
    String description;
    String hourly_overtime_rate;
    String overtime_available;
    String toll_parking_charge;
    String night_charge_available;
    String night_rate;
    String user_id;
    String vehicle_reg_number;
    String license_plate_image;
    String tax_token_image;
    String pcu_paper_image;
    String valid_permit;
    String registration_certificate;
    String registration_certificate_back;
    String fitness_certificate;
    String insurance_certificate;
    String brand_name;
    String model_name;
    String vehicle_type_name;
    String max_addl_passenger;
    String booking_id;
    String booking_date;
    String start_time;
    String end_time;
    String total_passenger;
    String duration;
    String pickup_location;
    String drop_location;
    String total_amount;
    String night_charge;
    String net_payable_amount;
    String booking_purpose_id;
    String pickup_location_id;
    String droplocation_id;
    String addl_request;
    String addl_info;
    String expected_route;
    String booking_purpose;
    String city_name;
    String state_name;
    String vehicle_status;
    String is_active_by_owner;
    String is_in_wishlist;
    String wishlist_id;
    String average_rating;
    String count_review;
    String other_pickup_location;
    String vehicle_city_name;
    String neo_rating;

    //ac parameters
    String ac_ac_facility;
    String ac_hourly_min_rate;
    String ac_hourly_max_rate;
    String ac_hourly_overtime_min_rate;
    String ac_hourly_overtime_max_rate;
    String ac_min_night_rate;
    String ac_max_night_rate;


    //non-ac parameters
    String nonac_ac_facility;
    String nonac_hourly_min_rate;
    String nonac_hourly_max_rate;
    String nonac_hourly_overtime_min_rate;
    String nonac_hourly_overtime_max_rate;
    String nonac_min_night_rate;
    String nonac_max_night_rate;


    ArrayList<PickUpLocationModel> arr_PickUpLocationModel;
    ArrayList<VehicleGalleryModel> arr_VehicleGalleryModel;
    ArrayList<PassengerModel> arr_PassengerModel;


    public String getRegistration_certificate_back() {
        return registration_certificate_back;
    }

    public void setRegistration_certificate_back(String registration_certificate_back) {
        this.registration_certificate_back = registration_certificate_back;
    }

    public String getNet_payable_amount() {
        return net_payable_amount;
    }

    public void setNet_payable_amount(String net_payable_amount) {
        this.net_payable_amount = net_payable_amount;
    }

    public String getNight_charge() {
        return night_charge;
    }

    public void setNight_charge(String night_charge) {
        this.night_charge = night_charge;
    }

    public String getToll_parking_charge() {
        return toll_parking_charge;
    }

    public void setToll_parking_charge(String toll_parking_charge) {
        this.toll_parking_charge = toll_parking_charge;
    }

    public String getNight_charge_available() {
        return night_charge_available;
    }

    public void setNight_charge_available(String night_charge_available) {
        this.night_charge_available = night_charge_available;
    }

    public String getNight_rate() {
        return night_rate;
    }

    public void setNight_rate(String night_rate) {
        this.night_rate = night_rate;
    }

    public String getValid_permit() {
        return valid_permit;
    }

    public void setValid_permit(String valid_permit) {
        this.valid_permit = valid_permit;
    }

    public String getRegistration_certificate() {
        return registration_certificate;
    }

    public void setRegistration_certificate(String registration_certificate) {
        this.registration_certificate = registration_certificate;
    }

    public String getFitness_certificate() {
        return fitness_certificate;
    }

    public void setFitness_certificate(String fitness_certificate) {
        this.fitness_certificate = fitness_certificate;
    }

    public String getInsurance_certificate() {
        return insurance_certificate;
    }

    public void setInsurance_certificate(String insurance_certificate) {
        this.insurance_certificate = insurance_certificate;
    }

    public String getAc_min_night_rate() {
        return ac_min_night_rate;
    }

    public void setAc_min_night_rate(String ac_min_night_rate) {
        this.ac_min_night_rate = ac_min_night_rate;
    }

    public String getAc_max_night_rate() {
        return ac_max_night_rate;
    }

    public void setAc_max_night_rate(String ac_max_night_rate) {
        this.ac_max_night_rate = ac_max_night_rate;
    }

    public String getNonac_min_night_rate() {
        return nonac_min_night_rate;
    }

    public void setNonac_min_night_rate(String nonac_min_night_rate) {
        this.nonac_min_night_rate = nonac_min_night_rate;
    }

    public String getNonac_max_night_rate() {
        return nonac_max_night_rate;
    }

    public void setNonac_max_night_rate(String nonac_max_night_rate) {
        this.nonac_max_night_rate = nonac_max_night_rate;
    }

    public String getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(String average_rating) {
        this.average_rating = average_rating;
    }

    public String getCount_review() {
        return count_review;
    }

    public void setCount_review(String count_review) {
        this.count_review = count_review;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getSitting_capacity() {
        return sitting_capacity;
    }

    public void setSitting_capacity(String sitting_capacity) {
        this.sitting_capacity = sitting_capacity;
    }

    public String getLuggae_capacity() {
        return luggae_capacity;
    }

    public void setLuggae_capacity(String luggae_capacity) {
        this.luggae_capacity = luggae_capacity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMake_id() {
        return make_id;
    }

    public void setMake_id(String make_id) {
        this.make_id = make_id;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }


    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getKm_travelled() {
        return km_travelled;
    }

    public void setKm_travelled(String km_travelled) {
        this.km_travelled = km_travelled;
    }

    public String getSpecial_info() {
        return special_info;
    }

    public void setSpecial_info(String special_info) {
        this.special_info = special_info;
    }

    public String getAc_available() {
        return ac_available;
    }

    public void setAc_available(String ac_available) {
        this.ac_available = ac_available;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHourly_rate() {
        return hourly_rate;
    }

    public void setHourly_rate(String hourly_rate) {
        this.hourly_rate = hourly_rate;
    }

    public String getMax_luggage() {
        return max_luggage;
    }

    public void setMax_luggage(String max_luggage) {
        this.max_luggage = max_luggage;
    }

    public String getMax_passenger() {
        return max_passenger;
    }

    public void setMax_passenger(String max_passenger) {
        this.max_passenger = max_passenger;
    }

    public String getLicense_plate_no() {
        return license_plate_no;
    }

    public void setLicense_plate_no(String license_plate_no) {
        this.license_plate_no = license_plate_no;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_contact_no() {
        return driver_contact_no;
    }

    public void setDriver_contact_no(String driver_contact_no) {
        this.driver_contact_no = driver_contact_no;
    }

    public String getDriver_email() {
        return driver_email;
    }

    public void setDriver_email(String driver_email) {
        this.driver_email = driver_email;
    }

    public String getDriver_address() {
        return driver_address;
    }

    public void setDriver_address(String driver_address) {
        this.driver_address = driver_address;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHourly_overtime_rate() {
        return hourly_overtime_rate;
    }

    public void setHourly_overtime_rate(String hourly_overtime_rate) {
        this.hourly_overtime_rate = hourly_overtime_rate;
    }

    public String getOvertime_available() {
        return overtime_available;
    }

    public void setOvertime_available(String overtime_available) {
        this.overtime_available = overtime_available;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVehicle_reg_number() {
        return vehicle_reg_number;
    }

    public void setVehicle_reg_number(String vehicle_reg_number) {
        this.vehicle_reg_number = vehicle_reg_number;
    }

    public String getLicense_plate_image() {
        return license_plate_image;
    }

    public void setLicense_plate_image(String license_plate_image) {
        this.license_plate_image = license_plate_image;
    }

    public String getTax_token_image() {
        return tax_token_image;
    }

    public void setTax_token_image(String tax_token_image) {
        this.tax_token_image = tax_token_image;
    }

    public String getPcu_paper_image() {
        return pcu_paper_image;
    }

    public void setPcu_paper_image(String pcu_paper_image) {
        this.pcu_paper_image = pcu_paper_image;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getVehicle_type_name() {
        return vehicle_type_name;
    }

    public void setVehicle_type_name(String vehicle_type_name) {
        this.vehicle_type_name = vehicle_type_name;
    }

    public ArrayList<PickUpLocationModel> getArr_PickUpLocationModel() {
        return arr_PickUpLocationModel;
    }

    public void setArr_PickUpLocationModel(ArrayList<PickUpLocationModel> arr_PickUpLocationModel) {
        this.arr_PickUpLocationModel = arr_PickUpLocationModel;
    }

    public ArrayList<VehicleGalleryModel> getArr_VehicleGalleryModel() {
        return arr_VehicleGalleryModel;
    }

    public void setArr_VehicleGalleryModel(ArrayList<VehicleGalleryModel> arr_VehicleGalleryModel) {
        this.arr_VehicleGalleryModel = arr_VehicleGalleryModel;
    }

    public String getMax_addl_passenger() {
        return max_addl_passenger;
    }

    public void setMax_addl_passenger(String max_addl_passenger) {
        this.max_addl_passenger = max_addl_passenger;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTotal_passenger() {
        return total_passenger;
    }

    public void setTotal_passenger(String total_passenger) {
        this.total_passenger = total_passenger;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public ArrayList<PassengerModel> getArr_PassengerModel() {
        return arr_PassengerModel;
    }

    public void setArr_PassengerModel(ArrayList<PassengerModel> arr_PassengerModel) {
        this.arr_PassengerModel = arr_PassengerModel;
    }

    public String getBooking_purpose_id() {
        return booking_purpose_id;
    }

    public void setBooking_purpose_id(String booking_purpose_id) {
        this.booking_purpose_id = booking_purpose_id;
    }

    public String getPickup_location_id() {
        return pickup_location_id;
    }

    public void setPickup_location_id(String pickup_location_id) {
        this.pickup_location_id = pickup_location_id;
    }

    public String getDroplocation_id() {
        return droplocation_id;
    }

    public void setDroplocation_id(String droplocation_id) {
        this.droplocation_id = droplocation_id;
    }

    public String getAddl_request() {
        return addl_request;
    }

    public void setAddl_request(String addl_request) {
        this.addl_request = addl_request;
    }

    public String getAddl_info() {
        return addl_info;
    }

    public void setAddl_info(String addl_info) {
        this.addl_info = addl_info;
    }

    public String getExpected_route() {
        return expected_route;
    }

    public void setExpected_route(String expected_route) {
        this.expected_route = expected_route;
    }

    public String getBooking_purpose() {
        return booking_purpose;
    }

    public void setBooking_purpose(String booking_purpose) {
        this.booking_purpose = booking_purpose;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getVehicle_status() {
        return vehicle_status;
    }

    public void setVehicle_status(String vehicle_status) {
        this.vehicle_status = vehicle_status;
    }

    public String getIs_active_by_owner() {
        return is_active_by_owner;
    }

    public void setIs_active_by_owner(String is_active_by_owner) {
        this.is_active_by_owner = is_active_by_owner;
    }

    public String getIs_in_wishlist() {
        return is_in_wishlist;
    }

    public void setIs_in_wishlist(String is_in_wishlist) {
        this.is_in_wishlist = is_in_wishlist;
    }

    public String getWishlist_id() {
        return wishlist_id;
    }

    public void setWishlist_id(String wishlist_id) {
        this.wishlist_id = wishlist_id;
    }

    public String getAc_ac_facility() {
        return ac_ac_facility;
    }

    public void setAc_ac_facility(String ac_ac_facility) {
        this.ac_ac_facility = ac_ac_facility;
    }

    public String getAc_hourly_min_rate() {
        return ac_hourly_min_rate;
    }

    public void setAc_hourly_min_rate(String ac_hourly_min_rate) {
        this.ac_hourly_min_rate = ac_hourly_min_rate;
    }

    public String getAc_hourly_max_rate() {
        return ac_hourly_max_rate;
    }

    public void setAc_hourly_max_rate(String ac_hourly_max_rate) {
        this.ac_hourly_max_rate = ac_hourly_max_rate;
    }

    public String getAc_hourly_overtime_min_rate() {
        return ac_hourly_overtime_min_rate;
    }

    public void setAc_hourly_overtime_min_rate(String ac_hourly_overtime_min_rate) {
        this.ac_hourly_overtime_min_rate = ac_hourly_overtime_min_rate;
    }

    public String getAc_hourly_overtime_max_rate() {
        return ac_hourly_overtime_max_rate;
    }

    public void setAc_hourly_overtime_max_rate(String ac_hourly_overtime_max_rate) {
        this.ac_hourly_overtime_max_rate = ac_hourly_overtime_max_rate;
    }

    public String getNonac_ac_facility() {
        return nonac_ac_facility;
    }

    public void setNonac_ac_facility(String nonac_ac_facility) {
        this.nonac_ac_facility = nonac_ac_facility;
    }

    public String getNonac_hourly_min_rate() {
        return nonac_hourly_min_rate;
    }

    public void setNonac_hourly_min_rate(String nonac_hourly_min_rate) {
        this.nonac_hourly_min_rate = nonac_hourly_min_rate;
    }

    public String getNonac_hourly_max_rate() {
        return nonac_hourly_max_rate;
    }

    public void setNonac_hourly_max_rate(String nonac_hourly_max_rate) {
        this.nonac_hourly_max_rate = nonac_hourly_max_rate;
    }

    public String getNonac_hourly_overtime_min_rate() {
        return nonac_hourly_overtime_min_rate;
    }

    public void setNonac_hourly_overtime_min_rate(String nonac_hourly_overtime_min_rate) {
        this.nonac_hourly_overtime_min_rate = nonac_hourly_overtime_min_rate;
    }

    public String getNonac_hourly_overtime_max_rate() {
        return nonac_hourly_overtime_max_rate;
    }

    public void setNonac_hourly_overtime_max_rate(String nonac_hourly_overtime_max_rate) {
        this.nonac_hourly_overtime_max_rate = nonac_hourly_overtime_max_rate;
    }


    public String getOther_pickup_location() {
        return other_pickup_location;
    }

    public void setOther_pickup_location(String other_pickup_location) {
        this.other_pickup_location = other_pickup_location;
    }

    public String getVehicle_city_name() {
        return vehicle_city_name;
    }

    public void setVehicle_city_name(String vehicle_city_name) {
        this.vehicle_city_name = vehicle_city_name;
    }

    public String getLuggage_capacity() {
        return luggae_capacity;
    }

    public void setLuggage_capacity(String luggae_capacity) {
        this.luggae_capacity = luggae_capacity;
    }


    public String getNeo_rating() {
        return neo_rating;
    }

    public void setNeo_rating(String neo_rating) {
        this.neo_rating = neo_rating;
    }

}
