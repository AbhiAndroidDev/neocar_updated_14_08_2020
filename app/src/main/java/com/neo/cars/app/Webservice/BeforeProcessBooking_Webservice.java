package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.BookNowPaymentActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.PassengerModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialogText;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by parna on 18/5/18.
 */

public class BeforeProcessBooking_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialogText pdCusomeDialog;
    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    private UserLoginDetailsModel UserLoginDetails;
    private JSONObject details;
    private JSONObject jsonObjDetails;
    private PassengerModel passengerModel;
    private ArrayList<PassengerModel> arrlistAddlPassenger;

    private String strBookingId="", strUserId="", strdriverId="", strVehicleId="", strBookingDate="", strstartTime="", strEndTime="",
            strTotalPassenger="", strDuration="", strPickUpLoc="", strDropLoc="", strTotalAmount="", strNightCharge = "", strNetPayableAmount = "",
            strParkingCharge = "", strBookingPurposeId="", strPickuplocationId="", strDropLocationId="", strAddlReq="", strAddlInfo="", strExpectedRoute="", strBookingPurpose="";

    private String strBookingStartTime="";
    private int transitionflag = StaticClass.transitionflagNext;
    private String addlPassengerCounter = "";

    private JSONArray jsonArrayAddlPassenger;


    public void beforeProcessBooking (Activity context, final String strVehicleId, final String strDriverId,
                                final String strBookingDate, final String strStarttime, final  String strPickUpLocation,
                                final String strDropLocation, final String strDuration, final String strBookingpurpose, final String strBookingPurposeId,
                                final String strPickupLocationId, final String strDropLocationId, final String strAddlPassenger,
                                final String strAddlRequest, final String strAddlInfo, final String strExpectedRoute, final String strLocation, final String _addlPassengerCounter){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        this.addlPassengerCounter = _addlPassengerCounter;

        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        vehicleTypeModel = new VehicleTypeModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest stringProcessBookingRequest = new StringRequest(Request.Method.POST, Urlstring.before_process_booking,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response Booking hours", response);
                        Log.d("d", "** API strLocation **"+strLocation);
                        Apiparsedata(response, strLocation);
                        StaticClass.isBookNow=true;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            StaticClass.isBookNow=true;
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", UserLoginDetails.getId());
                params.put("vehicle_id", strVehicleId);
                params.put("driver_id", strDriverId);
                params.put("booking_date", strBookingDate);
                params.put("start_time", strStarttime);
                params.put("pickup_location", strPickUpLocation);
                params.put("drop_location", strDropLocation);
                params.put("duration", strDuration);
                params.put("booking_purpose_id", strBookingPurposeId);

                if(strPickupLocationId != null){
                    params.put("pickup_location_id", strPickupLocationId);
                }

                if(strDropLocationId != null){
                    params.put("drop_location_id", strDropLocationId);
                }

                if(strAddlPassenger != null){
                    params.put("addl_passenger", strAddlPassenger);
                }

                if(strAddlRequest != null){
                    params.put("addl_request", strAddlRequest);
                }

                if(strAddlInfo != null){
                    params.put("addl_info", strAddlInfo);
                }

                if(strExpectedRoute != null){
                    params.put("expected_route", strExpectedRoute);
                }


                new PrintClass("process booking params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        stringProcessBookingRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(stringProcessBookingRequest);


    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialogText(mcontext,mcontext.getResources().getString(R.string.Loading_PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response, String strLocation){
        JSONObject jobj_main = null;
        jsonArrayAddlPassenger = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("before_process_booking").optString("user_deleted");
            Msg = jobj_main.optJSONObject("before_process_booking").optString("message");
            Status = jobj_main.optJSONObject("before_process_booking").optString("status");

            jsonObjDetails = jobj_main.optJSONObject("before_process_booking").optJSONObject("details");
            if(Status.equals(StaticClass.SuccessResult)){

                vehicleTypeModel.setUser_id(jsonObjDetails.optString("user_id"));
                vehicleTypeModel.setDriver_id(jsonObjDetails.optString("driver_id"));
                vehicleTypeModel.setVehicle_id(jsonObjDetails.optString("vehicle_id"));
                vehicleTypeModel.setBooking_date(jsonObjDetails.optString("booking_date"));
                vehicleTypeModel.setStart_time(jsonObjDetails.optString("start_time"));
                vehicleTypeModel.setEnd_time(jsonObjDetails.optString("end_time"));
                vehicleTypeModel.setPickup_location(jsonObjDetails.optString("pickup_location"));
                vehicleTypeModel.setDrop_location(jsonObjDetails.optString("drop_location"));
                vehicleTypeModel.setDuration(jsonObjDetails.optString("duration"));
                vehicleTypeModel.setBooking_purpose_id(jsonObjDetails.optString("booking_purpose_id"));;
                vehicleTypeModel.setPickup_location_id(jsonObjDetails.optString("pickup_location_id"));
                vehicleTypeModel.setDroplocation_id(jsonObjDetails.optString("drop_location_id"));
                vehicleTypeModel.setAddl_request(jsonObjDetails.optString("addl_request"));
                vehicleTypeModel.setAddl_info(jsonObjDetails.optString("addl_info"));
                vehicleTypeModel.setExpected_route(jsonObjDetails.optString("expected_route"));
                vehicleTypeModel.setTotal_amount(jsonObjDetails.optString("total_amount"));
                vehicleTypeModel.setNight_charge(jsonObjDetails.optString("night_charge"));
                vehicleTypeModel.setNet_payable_amount(jsonObjDetails.optString("net_payable_amount"));
                vehicleTypeModel.setBooking_purpose(jsonObjDetails.optString("booking_purpose"));


                strUserId = jsonObjDetails.optString("user_id");
                strdriverId = jsonObjDetails.optString("driver_id");
                strVehicleId = jsonObjDetails.optString("vehicle_id");
                strBookingDate = jsonObjDetails.optString("booking_date");
                strstartTime = jsonObjDetails.optString("start_time");
                Log.d("d", "***strStartTime***"+strstartTime);


                strEndTime = jsonObjDetails.optString("end_time");
                strPickUpLoc = jsonObjDetails.optString("pickup_location");
                strDropLoc = jsonObjDetails.optString("drop_location");
                strDuration = jsonObjDetails.optString("duration");
                strBookingPurposeId = jsonObjDetails.optString("booking_purpose_id");
                strPickuplocationId = jsonObjDetails.optString("pickup_location_id");
                strDropLocationId = jsonObjDetails.optString("drop_location_id");
                strAddlReq = jsonObjDetails.optString("addl_request");
                strAddlInfo = jsonObjDetails.optString("addl_info");
                strExpectedRoute = jsonObjDetails.optString("expected_route");
                strTotalAmount = jsonObjDetails.optString("total_amount");
                strNightCharge = jsonObjDetails.optString("night_charge");
                strNetPayableAmount = jsonObjDetails.optString("net_payable_amount");
                strParkingCharge = jsonObjDetails.optString("parking_charge");
                strBookingPurpose = jsonObjDetails.optString("booking_purpose");

//                strBookingStartTime = strstartTime;

                try {

                    SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm", Locale.US);
                    SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a", Locale.US);
                    Date date = parseFormat.parse(strstartTime);
                    strBookingStartTime = displayFormat.format(date);
                    Log.d("d", "***strBookingStartTime***" + strBookingStartTime);
                }catch (Exception e){
                    e.printStackTrace();
                }


                jsonArrayAddlPassenger = jsonObjDetails.optJSONArray("addl_passenger");
                if(jsonArrayAddlPassenger != null){
                    arrlistAddlPassenger = new ArrayList<>();
                    for (int i=0; i<jsonArrayAddlPassenger.length(); i++){
                        JSONObject jsonObjectAddlPassenger = jsonArrayAddlPassenger.getJSONObject(i);
                        passengerModel = new PassengerModel();
                        passengerModel.setPassenger_name(jsonObjectAddlPassenger.optString("name"));
                        passengerModel.setPassenger_contact(jsonObjectAddlPassenger.optString("contact_no"));

                        arrlistAddlPassenger.add(passengerModel);
                    }
                    vehicleTypeModel.setArr_PassengerModel(arrlistAddlPassenger);
                }

            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("before_process_booking").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {

            if (Status.equals(StaticClass.SuccessResult)) {
                Intent intent = new Intent(mcontext, BookNowPaymentActivity.class);

                Log.d("d", "strLocation::"+strLocation);
                Log.e("driver_id", strdriverId);

                intent.putExtra("UserId", strUserId);
                intent.putExtra("DriverId", strdriverId);
                intent.putExtra("VehicleId", strVehicleId);
                intent.putExtra("BookingDate", strBookingDate);
                intent.putExtra("start_time", strstartTime);
                intent.putExtra("StartTimeToSend", strBookingStartTime );
                intent.putExtra("EndTime", strEndTime);
                intent.putExtra("PickupLoc", strPickUpLoc);
                intent.putExtra("DropLoc", strDropLoc);
                intent.putExtra("Duration", strDuration);
                intent.putExtra("BookingPurpose", strBookingPurpose);
                intent.putExtra("BookingPurposeId", strBookingPurposeId);
                intent.putExtra("PickUpLocId", strPickuplocationId);
                intent.putExtra("DropLocId", strDropLocationId);
                intent.putExtra("AddlReq", strAddlReq);
                intent.putExtra("AddlInfo", strAddlInfo);
                intent.putExtra("ExpectedRoute", strExpectedRoute);
                intent.putExtra("TotalAmount", strTotalAmount);
                intent.putExtra("NightCharge", strNightCharge);
                intent.putExtra("NetPayableAmount", strNetPayableAmount);
                intent.putExtra("parking_charge", strParkingCharge);
                intent.putExtra("Location", strLocation);
                intent.putExtra("addlPassengerCounter", addlPassengerCounter);
                intent.putParcelableArrayListExtra("PassengerList", arrlistAddlPassenger);

                if(jsonArrayAddlPassenger != null) {
                    intent.putExtra("addl_passenger", jsonArrayAddlPassenger.toString());
                }

                mcontext.startActivity(intent);
                // mcontext.finish();


            } else {
//                new CustomToast(mcontext, Msg);
            }


        }
    }


}
