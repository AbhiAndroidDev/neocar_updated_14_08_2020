package com.neo.cars.app.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.neo.cars.app.CompanyInboxActivity;
import com.neo.cars.app.Inbox;
import com.neo.cars.app.R;
import com.neo.cars.app.SharedPreference.ShareFcmDetails;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by kamail on 22/9/16.
 */

/**
 * Modified by Mithilesh on 06/02/19.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

//    private static final String CHANNEL_ID = "10001";
    private static final String CHANNEL_ID = "notification_channel";

    //String type_text, title=getApplicationContext().getString(R.string.app_name),content="Test Notification";
    String Title, Message,type,user_id;
    private SharedPrefUserDetails sharedPref;
    private ShareFcmDetails sharedPrefDetails;


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        Log.d(TAG, "Refreshed token: " + token);
        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token) {
        sharedPrefDetails = new ShareFcmDetails(this);

        if (sharedPrefDetails.getFcmId().equals("")) {
            sharedPrefDetails.setFcmId(token);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
        Log.e(TAG, "RemoteMessage: " + remoteMessage.getData().get("message"));

        //  {"title":"test342","description":"test342","type":"admin_send","user_id":97}

        try {
            JSONObject jo=new JSONObject(remoteMessage.getData().get("message"));

            Message=jo.optString("description");
            Title=jo.optString("title");
            type=jo.optString("type");
            user_id=jo.optString("user_id");


            Log.d("d", "**Message**"+Message);
            //  Log.d("d", "**Title**"+Title);

            if(Message != null){
                sendNotification(Title, Message);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void sendNotification(String Title,  String Message) {

        sharedPref = new SharedPrefUserDetails(this);
//        sharedPref.setNewNotificationStatus(true);

        Intent intent = null;
        PendingIntent pendingIntent = null;

        Log.d("userType***", sharedPref.getUserType());
        Log.d("Message***", Message);

        if (sharedPref.getUserType().equalsIgnoreCase("U")) {

            sharedPref.setNewNotificationStatus(true);
            intent = new Intent(this, Inbox.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        }else if (sharedPref.getUserType().equalsIgnoreCase("C")){

            sharedPref.setNewNotificationStatusCompany(true);
            intent = new Intent(this, CompanyInboxActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        if(intent != null) {
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence name = getString(R.string.channel_name);
//            String description = getString(R.string.channel_description);
//            int importance = NotificationManager.IMPORTANCE_DEFAULT;
//            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
//            channel.setDescription(description);
//            // Register the channel with the system; you can't change the importance
//            // or other notification behaviors after this
//            NotificationManager notificationManager = getSystemService(NotificationManager.class);
//            notificationManager.createNotificationChannel(channel);
//
//        }else {

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.icon)
                    .setContentTitle(Title)
                    .setContentText(Message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setShowWhen(true)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificationBuilder.build());
//        }
        }

    }
    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationManager
                mNotificationManager =
                (NotificationManager)
                        getSystemService(Context.NOTIFICATION_SERVICE);
// The id of the channel.
        String id = CHANNEL_ID;
// The user-visible name of the channel.
        CharSequence name = "NeoCar notification";
// The user-visible description of the channel.
        String description = "Notification_channel_NeoCar";
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
// Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mChannel.setLockscreenVisibility(android.app.Notification.VISIBILITY_PUBLIC);
        mNotificationManager.createNotificationChannel(mChannel);
    }
}
