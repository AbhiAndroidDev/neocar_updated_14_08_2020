package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.CityListModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 10/5/18.
 */

public interface ExploreViewMore_Interface {
    void onExploreViewMore(String CityId,String CityName);
}
