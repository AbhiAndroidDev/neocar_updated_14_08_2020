package com.neo.cars.app.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

/**
 * Created by parna on 2/3/18.
 */

public class TextInputTitilliumWebRegular extends TextInputLayout{
    public TextInputTitilliumWebRegular(Context context) {
        super(context);
    }

    public TextInputTitilliumWebRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextInputTitilliumWebRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if(!isInEditMode()){
            //------ Font------//
            Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/TitilliumWeb-Regular.ttf");
            //Typeface typeface2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/SaintAndrewdesKiwis.ttf");

            setTypeface(typeface1);
        }

    }

}
