package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SetGet.WishlistModel;

/**
 * Created by parna on 11/5/18.
 */

public interface WishlistAdd_Interface {

    public void wishlistAddInterface(WishlistModel wishlistdetails);
}
