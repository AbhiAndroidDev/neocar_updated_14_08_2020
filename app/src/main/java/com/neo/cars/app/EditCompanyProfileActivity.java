package com.neo.cars.app;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.EditProfileSave_Interface;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.CompanyEditProfileWebService;
import com.neo.cars.app.Webservice.CompanyProfile_WebService;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

//import me.iwf.photopicker.PhotoPicker;

public class EditCompanyProfileActivity extends RootActivity implements View.OnClickListener, CallBackButtonClick, Profile_Interface, EditProfileSave_Interface {

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private View mView;
    private Context context;
    private Activity activity;
    private ConnectionDetector cd;
    private ScrollView scrollview;
    private CircularImageViewBorder civCompanyProfilePic;
    private CustomEditTextTitilliumWebRegular etCompanyName, etCompanyPhoneNo, etGSTNumberProfile, etPANNumberProfile,
            etPCPName, etPCMNumber, etSCPName, etSCMNumber, etbankAccountNumberProfile, etIfscCodeProfile;

    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvPrimaryContactId, tvSecondaryContactId, tvCompanyPANCard, tvCanceledCheque,

    etRegisteredAddress, etCompanyEmail, etCompanyMobNo;  //Added later from edit to text

    private CustomButtonTitilliumSemibold btnProfileSave;
    private CustomTitilliumTextViewSemiBold tvChangePassword;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;
    private ImageView ivPrimaryContactId, ivPrimaryContactIdBack, ivSecondaryContactId, ivSecondaryContactIdBack, ivCompanyPANCard, ivCanceledCheque, ivInfoCompanyNameEP, ivInfoCompanyAddressEP, ivInfoCompanyEmailEP,
            ivInfoMobileNoEP, ivInfoOfficePhNoEp, ivInfoGSTNoEP, ivInfoPANNoEP, ivInfoPCPNameIcon, ivInfoPrimaryContactIdEP, ivInfoPrimaryContactIdEPBack,
            ivInfoSCPNameIcon, ivInfoSecondaryContactIdEP, ivInfoSecondaryContactIdEPBack, ivInfoCompanyPANCardEP, ivInfobankAccountNumber, ivInfoIFSCCodeEP, ivPrimaryContactIdImage,
            ivPrimaryContactIdImageBack, ivSecondaryContactIdImage, ivSecondaryContactIdImageBack,
            ivInfoCanceledCheque, ivPANCardImage, ivcanceledChequeImage;

    private ImageView ivMobileEP, ivOfficePhoneEP, ivPCMobileEP, ivSCMobileEP;

    private String userChoosenTask="", imageFilePath="", mSelectedGalleryImagePath="", mGalleryImagePath="", strImage = "", strCompanyProfilePic = "",
            strPrimaryContactID = "", strPrimaryContactIDBack = "",
            strSecondaryContactID = "", strSecondaryContactIDBack = "", strPANCARD = "", strCancelledCheque = "", strSelectedCompanyProfilePic = "", strSelectedPrimaryContactID = "", strSelectedSecondaryContactID = "",
            strSelectedPANCARD = "", strSelectedCancelledCheque = "";

    private String strNameOfCompany = "", strRegisteredAddress = "", strEmailCompany = "", strPhoneCompany = "",
            strMobileCompany = "", strCompanyGSTNumber = "", strCompanyPANNumber = "", strPrimaryContactPName = "", strprimayMobileCompany, strSecondaryContactPName,
            strSecondaryMobileCompany, strbankAccountNumber, strIfscCode;

    private String strFileType="", selectedNumber = "", strPhoneNo = "";
    private String strNameHeader="", strEmailHeader="", strMobileHeader="", strRegAddressHeader="", strphoneHeader;
    private static final int REQUEST_IMAGE_CAPTURE = 1003;
    private static final int PICK_IMAGE_REQUEST = 906;
    private File file, fileCompanyProfilePic, filePrimaryContactID, filePrimaryContactIDBack, fileSecondaryContactID,
            fileSecondaryContactIDBack, filePANCARD, fileCancelledCheque;
    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private SharedPrefUserDetails sharedPref;
    private ProgressDialog dialog;
    private Uri imageUri;
    private Uri uriContact;
    private static final int REQUEST_CODE_PICK_CONTACTS = 124;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 648;
    private UserLoginDetailsModel UserLoginDetails;
    private Gson gson;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    private CheckBox cb_terms;
    private TextView tvByLoggingIntoText_privacypolicy;
    private TextView tvTermsAndCondition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_company_profile);

        // Initialize Places.
//        Places.initialize(getApplicationContext(), StaticClass.API_KEY);
//
//        PlacesClient placesClient = Places.createClient(this);

        new AnalyticsClass(EditCompanyProfileActivity.this);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        sharedPref = new SharedPrefUserDetails(EditCompanyProfileActivity.this);

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        initialize();
        listener();

        if (cd.isConnectingToInternet()){
            new CompanyProfile_WebService().companyProfile(activity);

        }else{
            startActivity(new Intent(this, NetworkNotAvailable.class));
            transitionflag = StaticClass.transitionflagBack;
        }
    }

    private void initialize() {
        context = EditCompanyProfileActivity.this;
        activity = EditCompanyProfileActivity.this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        sharedPref = new SharedPrefUserDetails(this);
        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvProfileHeader));
        ibNavigateMenu =  findViewById(R.id.ibNavigateMenu);
        rlBackLayout =  findViewById(R.id.rlBackLayout);
        etCompanyName = findViewById(R.id.etCompanyName);
        etRegisteredAddress = findViewById(R.id.etRegisteredAddress);
        etCompanyEmail = findViewById(R.id.etCompanyEmail);
        etCompanyPhoneNo = findViewById(R.id.etCompanyPhoneNo);
        etCompanyMobNo = findViewById(R.id.etCompanyMobNo);
        etGSTNumberProfile = findViewById(R.id.etGSTNumberProfile);
        etPANNumberProfile = findViewById(R.id.etPANNumberProfile);
        etPCPName = findViewById(R.id.etPCPName);
        etPCMNumber = findViewById(R.id.etPCMNumber);
        etSCPName = findViewById(R.id.etSCPName);
        etSCMNumber = findViewById(R.id.etSCMNumber);
        etbankAccountNumberProfile = findViewById(R.id.etbankAccountNumberProfile);
        etIfscCodeProfile = findViewById(R.id.etIfscCodeProfile);

        tvPrimaryContactId =  findViewById(R.id.tvPrimaryContactId);
        tvSecondaryContactId =  findViewById(R.id.tvSecondaryContactId);
        tvCompanyPANCard =  findViewById(R.id.tvCompanyPANCard);
        tvCanceledCheque =  findViewById(R.id.tvCanceledCheque);

        civCompanyProfilePic = findViewById(R.id.civCompanyProfilePic);
        ivPrimaryContactId = findViewById(R.id.ivPrimaryContactId);
        ivPrimaryContactIdBack = findViewById(R.id.ivPrimaryContactIdBack);
        ivPrimaryContactIdImage = findViewById(R.id.ivPrimaryContactIdImage);
        ivPrimaryContactIdImageBack = findViewById(R.id.ivPrimaryContactIdImageBack);
        ivSecondaryContactIdImage = findViewById(R.id.ivSecondaryContactIdImage);
        ivSecondaryContactIdImageBack = findViewById(R.id.ivSecondaryContactIdImageBack);
        ivPANCardImage = findViewById(R.id.ivPANCardImage);
        ivcanceledChequeImage = findViewById(R.id.ivcanceledChequeImage);
        ivSecondaryContactId = findViewById(R.id.ivSecondaryContactId);
        ivSecondaryContactIdBack = findViewById(R.id.ivSecondaryContactIdBack);
        ivCompanyPANCard = findViewById(R.id.ivCompanyPANCard);
        ivCanceledCheque = findViewById(R.id.ivCanceledCheque);

        ivInfoCompanyNameEP = findViewById(R.id.ivInfoCompanyNameEP);
        ivInfoCompanyAddressEP = findViewById(R.id.ivInfoCompanyAddressEP);
        ivInfoCompanyEmailEP = findViewById(R.id.ivInfoCompanyEmailEP);
        ivInfoMobileNoEP = findViewById(R.id.ivInfoMobileNoEP);
        ivInfoOfficePhNoEp = findViewById(R.id.ivInfoOfficePhNoEp);
        ivInfoGSTNoEP = findViewById(R.id.ivInfoGSTNoEP);
        ivInfoPANNoEP = findViewById(R.id.ivInfoPANNoEP);
        ivInfoPCPNameIcon = findViewById(R.id.ivInfoPCPNameIcon);
        ivInfoPrimaryContactIdEP = findViewById(R.id.ivInfoPrimaryContactIdEP);
        ivInfoPrimaryContactIdEPBack = findViewById(R.id.ivInfoPrimaryContactIdEPBack);
        ivInfoSCPNameIcon = findViewById(R.id.ivInfoSCPNameIcon);
        ivInfoSecondaryContactIdEP = findViewById(R.id.ivInfoSecondaryContactIdEP);
        ivInfoSecondaryContactIdEPBack = findViewById(R.id.ivInfoSecondaryContactIdEPBack);
        ivInfoCompanyPANCardEP = findViewById(R.id.ivInfoCompanyPANCardEP);
        ivInfobankAccountNumber = findViewById(R.id.ivInfobankAccountNumber);
        ivInfoIFSCCodeEP = findViewById(R.id.ivInfoIFSCCodeEP);
        ivInfoCanceledCheque = findViewById(R.id.ivInfoCanceledCheque);

        ivMobileEP = findViewById(R.id.ivMobileEP);
        ivOfficePhoneEP = findViewById(R.id.ivOfficePhoneEP);
        ivPCMobileEP = findViewById(R.id.ivPCMobileEP);
        ivSCMobileEP = findViewById(R.id.ivSCMobileEP);

        btnProfileSave = findViewById(R.id.btnProfileSave);
        tvChangePassword = findViewById(R.id.tvChangePassword);

        etCompanyMobNo.setText(UserLoginDetails.getMobile());
        etCompanyEmail.setText(UserLoginDetails.getEmail());


        tvTermsAndCondition = findViewById(R.id.tvByLoggingIntoText_termsservices);
        cb_terms= findViewById(R.id.cb_terms);
        tvByLoggingIntoText_privacypolicy= findViewById(R.id.tvByLoggingIntoText_privacypolicy);
    }

    private void listener() {

        civCompanyProfilePic.setOnClickListener(this);
        ivPrimaryContactId.setOnClickListener(this);
        ivPrimaryContactIdBack.setOnClickListener(this);
        ivSecondaryContactId.setOnClickListener(this);
        ivSecondaryContactIdBack.setOnClickListener(this);
        ivCompanyPANCard.setOnClickListener(this);
        ivCanceledCheque.setOnClickListener(this);

        etRegisteredAddress.setOnClickListener(this);

        ivInfoCompanyNameEP.setOnClickListener(this);
        ivInfoCompanyAddressEP.setOnClickListener(this);
        ivInfoCompanyEmailEP.setOnClickListener(this);
        ivInfoMobileNoEP.setOnClickListener(this);
        ivInfoOfficePhNoEp.setOnClickListener(this);
        ivInfoGSTNoEP.setOnClickListener(this);
        ivInfoPANNoEP.setOnClickListener(this);
        ivInfoPCPNameIcon.setOnClickListener(this);
        ivInfoPrimaryContactIdEP.setOnClickListener(this);
        ivInfoPrimaryContactIdEPBack.setOnClickListener(this);
        ivInfoSCPNameIcon.setOnClickListener(this);
        ivInfoSecondaryContactIdEP.setOnClickListener(this);
        ivInfoSecondaryContactIdEPBack.setOnClickListener(this);
        ivInfoCompanyPANCardEP.setOnClickListener(this);
        ivInfobankAccountNumber.setOnClickListener(this);
        ivInfoIFSCCodeEP.setOnClickListener(this);
        ivInfoCanceledCheque.setOnClickListener(this);

        tvChangePassword.setOnClickListener(this);
        btnProfileSave.setOnClickListener(this);
        rlBackLayout.setOnClickListener(this);
        tvTermsAndCondition.setOnClickListener(this);
        tvByLoggingIntoText_privacypolicy.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.tvByLoggingIntoText_termsservices:
                Intent termsIntent = new Intent(EditCompanyProfileActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);
                break;

            case R.id.tvByLoggingIntoText_privacypolicy:
                Intent termsIntent1 = new Intent(EditCompanyProfileActivity.this, CmsPageActivity.class);
                termsIntent1.putExtra("cms", StaticClass.PrivacyPolicy);
                startActivity(termsIntent1);
                break;


            case R.id.civCompanyProfilePic:
                strImage = "CompanyProfilePic";
                selectImage(R.string.upload_company_photo, strImage);
                break;

            case R.id.ivPrimaryContactId:
                strImage = "PrimaryContactId";
                selectImage(R.string.upload_primary_contactID, strImage);
                break;

            case R.id.ivPrimaryContactIdBack:
                strImage = "PrimaryContactIdBack";
                selectImage(R.string.upload_primary_contactID, strImage);
                break;

            case R.id.ivSecondaryContactId:
                strImage = "SecondaryContactId";
                selectImage(R.string.upload_secondary_contactID, strImage);
                break;

            case R.id.ivSecondaryContactIdBack:
                strImage = "SecondaryContactIdBack";
                selectImage(R.string.upload_secondary_contactID, strImage);
                break;

            case R.id.ivCompanyPANCard:
                strImage = "CompanyPANCard";
                selectImage(R.string.upload_company_pan_card, strImage);
                break;

            case R.id.ivCanceledCheque:
                strImage = "CanceledCheque";
                selectImage(R.string.upload_cancelled_cheque, strImage);
                break;


            case R.id.etRegisteredAddress:

                                    if (NetWorkStatus.isNetworkAvailable(EditCompanyProfileActivity.this)) {

                                        Places.initialize(getApplicationContext(), StaticClass.API_KEY);

                                        PlacesClient placesClient = Places.createClient(EditCompanyProfileActivity.this);

                                        try {

                                            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS);

                                            // Start the autocomplete intent.
                                            Intent intent = new Autocomplete.IntentBuilder(
                                                    AutocompleteActivityMode.FULLSCREEN, fields)
                                                    .setCountry("IN")
                                                    .build(EditCompanyProfileActivity.this);

                                            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                    }else{
                                        startActivity(new Intent(EditCompanyProfileActivity.this, NetworkNotAvailable.class));
                                    }

                break;

            case R.id.ivInfoCompanyNameEP:
                new CustomToast(activity, getResources().getString(R.string.infoCompanyName));
                break;

            case R.id.ivInfoCompanyAddressEP:
                new CustomToast(activity, getResources().getString(R.string.infoRegCompAddress));
                break;

            case R.id.ivInfoCompanyEmailEP:
                new CustomToast(activity, getResources().getString(R.string.infoEmail));
                break;

            case R.id.ivInfoMobileNoEP:
                new CustomToast(activity, getResources().getString(R.string.infoContact));
                break;

            case R.id.ivInfoOfficePhNoEp:
                new CustomToast(activity, getResources().getString(R.string.infoOfficePhone));
                break;

            case R.id.ivInfoGSTNoEP:
                new CustomToast(activity, getResources().getString(R.string.infoGSTNo));
                break;

            case R.id.ivInfoPANNoEP:
                new CustomToast(activity, getResources().getString(R.string.infoPANNo));
                break;

            case R.id.ivInfoPCPNameIcon:
                new CustomToast(activity, getResources().getString(R.string.infoPrimary));
                break;

            case R.id.ivInfoPrimaryContactIdEP:

                new CustomToast(activity, getResources().getString(R.string.infoPrimaryIdUpload));
                break;

            case R.id.ivInfoPrimaryContactIdEPBack:

                new CustomToast(activity, getResources().getString(R.string.infoPrimaryIdUploadBack));
                break;

            case R.id.ivInfoSCPNameIcon:
                new CustomToast(activity, getResources().getString(R.string.infoSecondary));
                break;


            case R.id.ivInfoSecondaryContactIdEP:
                new CustomToast(activity, getResources().getString(R.string.infoSecondaryIdUpload));
                break;

            case R.id.ivInfoSecondaryContactIdEPBack:
                new CustomToast(activity, getResources().getString(R.string.infoSecondaryIdUploadBack));
                break;

            case R.id.ivInfoCompanyPANCardEP:
                new CustomToast(activity, getResources().getString(R.string.infoPANCard));
                break;


            case R.id.ivInfobankAccountNumber:
                new CustomToast(activity, getResources().getString(R.string.infoBank));
                break;

            case R.id.ivInfoIFSCCodeEP:
                new CustomToast(activity, getResources().getString(R.string.infoIFSCCode));
                break;

            case R.id.ivInfoCanceledCheque:
                new CustomToast(activity, getResources().getString(R.string.infoCancelledCheque));
                break;



            case R.id.rlBackLayout:
                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                        activity.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        activity.getResources().getString(R.string.yes),
                        activity.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
                break;

            case R.id.btnProfileSave:

                if(cb_terms.isChecked()) {
                    checkValidation();
                }else{
                    new CustomToast(activity, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));
                }


                break;
            case R.id.tvChangePassword:
                transitionflag = StaticClass.transitionflagNext;
                Intent updatepasswordactivity = new Intent(activity, UpdatePasswordActivity.class);
                startActivity(updatepasswordactivity);
                finish();
                break;

            default:
                break;
        }

    }

    private void checkValidation() {
        etCompanyName.setError(null);
        etRegisteredAddress.setError(null);
        etCompanyPhoneNo.setError(null);
//        etGSTNumberProfile.setError(null);
        etPANNumberProfile.setError(null);
        etPCPName.setError(null);
        etPCMNumber.setError(null);
        etSCPName.setError(null);
        etSCMNumber.setError(null);
        etbankAccountNumberProfile.setError(null);
        etIfscCodeProfile.setError(null);

        boolean cancel = false;
        View focusView = null;

        strNameOfCompany = etCompanyName.getText().toString().trim();
        strRegisteredAddress = etRegisteredAddress.getText().toString().trim();
        strEmailCompany = etCompanyEmail.getText().toString().trim();
        strPhoneCompany = etCompanyPhoneNo.getText().toString().trim();
        strMobileCompany = etCompanyMobNo.getText().toString().trim();
        strCompanyGSTNumber = etGSTNumberProfile.getText().toString().trim();
        strCompanyPANNumber = etPANNumberProfile.getText().toString().trim();
        strPrimaryContactPName = etPCPName.getText().toString().trim();
        strprimayMobileCompany = etPCMNumber.getText().toString().trim();
        strSecondaryContactPName = etSCPName.getText().toString().trim();
        strSecondaryMobileCompany = etSCMNumber.getText().toString().trim();
        strbankAccountNumber = etbankAccountNumberProfile.getText().toString().trim();
        strIfscCode = etIfscCodeProfile.getText().toString().trim();

        if(TextUtils.isEmpty(strNameOfCompany)) {
            etCompanyName.setError(getString(R.string.error_field_required));

        }else if (TextUtils.isEmpty(strRegisteredAddress)) {
            etRegisteredAddress.setError(getString(R.string.error_field_required));
            etRegisteredAddress.requestFocus();

        }
//        else if(TextUtils.isEmpty(strEmailCompany)) {
//            etCompanyEmail.setError(getString(R.string.error_field_required));
//
//        }
        else if (TextUtils.isEmpty(strPhoneCompany)) {
            etCompanyPhoneNo.setError(getString(R.string.error_field_required));
            etCompanyPhoneNo.requestFocus();

        }
//        else if (TextUtils.isEmpty(strMobileCompany)) {
//            etCompanyMobNo.setError(getString(R.string.error_field_required));
//            etCompanyMobNo.requestFocus();
//
//        }
//        else if(TextUtils.isEmpty(strCompanyGSTNumber)) {
//            etGSTNumberProfile.setError(getString(R.string.error_field_required));
//            etGSTNumberProfile.requestFocus();
//
//        }


        else if (strCompanyGSTNumber.length() > 0 && strCompanyGSTNumber.length() != 15) {
            new CustomToast(EditCompanyProfileActivity.this, getString(R.string.gstnumberlength));
            etGSTNumberProfile.requestFocus();

        }
        else if (TextUtils.isEmpty(strCompanyPANNumber)) {
            etPANNumberProfile.setError(getString(R.string.error_field_required));
            etPANNumberProfile.requestFocus();

        }else if (strCompanyPANNumber.length() != 10){
            new CustomToast(EditCompanyProfileActivity.this, getString(R.string.pannumberlength));

        }else if (TextUtils.isEmpty(strPrimaryContactPName)) {
            etPCPName.setError(getString(R.string.error_field_required));
            etPCPName.requestFocus();

        }else if(TextUtils.isEmpty(strprimayMobileCompany)) {
            etPCMNumber.setError(getString(R.string.error_field_required));
            etPCMNumber.requestFocus();

        }

        else if (strPrimaryContactID == null) {
            new CustomToast(this, getResources().getString(R.string.companyPCIdUploadMsg));

        }else if (strPrimaryContactIDBack == null) {
            new CustomToast(this, getResources().getString(R.string.companyPCIdUploadMsg));

        }

        else if (!(new Emailvalidation().phone_validation(strprimayMobileCompany))) {
            new CustomToast(this, getResources().getString(R.string.txt_message_mobile_no));

        }
        else if (TextUtils.isEmpty(strSecondaryContactPName)) {
            etSCPName.setError(getString(R.string.error_field_required));
            etSCPName.requestFocus();

        }else if (TextUtils.isEmpty(strSecondaryMobileCompany)) {
            etSCMNumber.setError(getString(R.string.error_field_required));
            etSCMNumber.requestFocus();

        }else if (!(new Emailvalidation().phone_validation(strSecondaryMobileCompany))) {
            new CustomToast(this, getResources().getString(R.string.txt_message_mobile_no));

        }else if (strMobileCompany.equalsIgnoreCase(strSecondaryMobileCompany)
                || strprimayMobileCompany.equalsIgnoreCase(strSecondaryMobileCompany)){

            new CustomToast(activity, getResources().getString(R.string.phoneNoCheck));
        }
        else if(TextUtils.isEmpty(strbankAccountNumber)) {
            etbankAccountNumberProfile.setError(getString(R.string.error_field_required));
            etbankAccountNumberProfile.requestFocus();

        }else if (TextUtils.isEmpty(strIfscCode)) {
            etIfscCodeProfile.setError(getString(R.string.error_field_required));
            etIfscCodeProfile.requestFocus();

        }else if (strIfscCode.length() != 11){
            new CustomToast(EditCompanyProfileActivity.this, getString(R.string.minimum_length_11));
            etIfscCodeProfile.requestFocus();
        }
        else {
            if(cd.isConnectingToInternet()){

                new CompanyEditProfileWebService().companyEditProfile(activity, strNameOfCompany, strEmailCompany, strMobileCompany, fileCompanyProfilePic,
                        "A", "strDeviceid", strRegisteredAddress, strPhoneCompany, strCompanyGSTNumber, strCompanyPANNumber,
                        strPrimaryContactPName, strprimayMobileCompany, filePrimaryContactID, filePrimaryContactIDBack, strSecondaryContactPName,
                        strSecondaryMobileCompany, fileSecondaryContactID, fileSecondaryContactIDBack,
                        filePANCARD, strbankAccountNumber, strIfscCode, fileCancelledCheque, sharedPref.getUserid());
            }else{
                startActivity(new Intent(activity, NetworkNotAvailable.class));
            }
        }
    }


    public void selectImage(final int message, String filetype) {

        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(context,
                activity,
                EditCompanyProfileActivity.this,
                getResources().getString(message),
                EditCompanyProfileActivity.this.getResources().getString(R.string.Camera),
                EditCompanyProfileActivity.this.getResources().getString(R.string.Gallery));
    }

    private void takePhoto() {

        // set a image file path
        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:
//                    new ImageCompression().execute(imageFilePath);

                    dialog = new ProgressDialog(context);
                    try {

                        dialog.setMessage("Image processing...");
                        dialog.show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {
                                    Bitmap fullImage = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);

                                    InputStream input = context.getContentResolver().openInputStream(imageUri);
                                    ExifInterface ei;
                                    if (Build.VERSION.SDK_INT > 23)
                                        ei = new ExifInterface(input);
                                    else
                                        ei = new ExifInterface(imageUri.getPath());

                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    switch (orientation) {
                                        case ExifInterface.ORIENTATION_ROTATE_90:
                                            fullImage = rotateImage(fullImage, 90);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_180:
                                            fullImage = rotateImage(fullImage, 180);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_270:
                                            fullImage = rotateImage(fullImage, 270);
                                            break;
                                        default:
                                            break;
                                    }


                                    try {
                                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                        fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                        byte[] byteArray = bytes.toByteArray();
                                        Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                                        file = ImageUtils.saveImage(scaledBitmap, context);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    if (file != null) {
                                        if (strImage == "CompanyProfilePic"){
                                            fileCompanyProfilePic = file;
                                            Log.d("d", "Camera Driver pic file:" + fileCompanyProfilePic);
                                            Picasso.get().load(fileCompanyProfilePic).resize(200, 200).into(civCompanyProfilePic);

                                        }else if (strImage == "PrimaryContactId"){
                                            filePrimaryContactID = file;
                                            strPrimaryContactID = file.getName();
                                            Log.d("d", "Camera Driver pic file:" + filePrimaryContactID);
                                            Picasso.get().load(filePrimaryContactID).resize(200, 200).into(ivPrimaryContactIdImage);

                                        }else if (strImage == "PrimaryContactIdBack"){
                                            strPrimaryContactIDBack = file.getName();
                                            filePrimaryContactIDBack = file;
                                            Log.d("d", "Camera Driver pic file:" + filePrimaryContactIDBack);
                                            Picasso.get().load(filePrimaryContactIDBack).resize(200, 200).into(ivPrimaryContactIdImageBack);

                                        }else if (strImage == "SecondaryContactId"){
                                            strSecondaryContactID = file.getName();
                                            fileSecondaryContactID = file;
                                            Log.d("d", "Camera Driver pic file:" + fileSecondaryContactID);
                                            Picasso.get().load(fileSecondaryContactID).resize(200, 200).into(ivSecondaryContactIdImage);

                                        }else if (strImage == "SecondaryContactIdBack"){
                                            strSecondaryContactIDBack = file.getName();
                                            fileSecondaryContactIDBack = file;
                                            Log.d("d", "Camera Driver pic file:" + fileSecondaryContactIDBack);
                                            Picasso.get().load(fileSecondaryContactIDBack).resize(200, 200).into(ivSecondaryContactIdImageBack);

                                        }else if (strImage == "CompanyPANCard"){
                                            filePANCARD = file;
                                            Log.d("d", "Camera Driver pic file:" + filePANCARD);
                                            Picasso.get().load(filePANCARD).resize(200, 200).into(ivPANCardImage);

                                        }else if (strImage == "CanceledCheque"){
                                            fileCancelledCheque = file;
                                            Log.d("d", "Camera Driver pic file:" + fileCancelledCheque);
                                            Picasso.get().load(fileCancelledCheque).resize(200, 200).into(ivcanceledChequeImage);
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                }
                                finally{
                                    dialog.dismiss();
                                }
                            }
                        }, 4000);  // 4 sec to allow processing of image captured


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case PICK_IMAGE_REQUEST:

                    try {
//                        ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {

                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }


                        if (strImage == "CompanyProfilePic") {
//                            strCompanyProfilePic = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strCompanyProfilePic);
//                            strSelectedCompanyProfilePic = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedCompanyProfilePic);
                            fileCompanyProfilePic = file;
//                            fileCompanyProfilePic = file;
                            Log.d("d", "Driver pic Image File::" + fileCompanyProfilePic);
                            Picasso.get().load(file).resize(200, 200).into(civCompanyProfilePic);

                        } else if (strImage == "PrimaryContactId") {
//                            strPrimaryContactID = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedPrimaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedPrimaryContactID);
                            filePrimaryContactID = file;

//                            Long length = filePrimaryContactID.length();
//                            Log.d("length**", ""+length);
//                            filePrimaryContactID = file;
                            Log.d("d", "Driver pic Image File::" + filePrimaryContactID);
                            Picasso.get().load(file).resize(200, 200).into(ivPrimaryContactIdImage);

                        }else if (strImage == "PrimaryContactIdBack") {
//                            strPrimaryContactIDBack = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strPrimaryContactIDBack);
//                            strSelectedPrimaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedPrimaryContactID);
                            filePrimaryContactIDBack = file;
//                            filePrimaryContactIDBack = file;
                            Log.d("d", "Driver pic Image File::" + filePrimaryContactID);
                            Picasso.get().load(file).resize(200, 200).into(ivPrimaryContactIdImageBack);

                        } else if (strImage == "SecondaryContactId") {
//                            strSecondaryContactID = "file://" + photos.get(0);
////                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedSecondaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedSecondaryContactID);
                            fileSecondaryContactID = file;
//                            fileSecondaryContactID = file;
                            Log.d("d", "Driver pic Image File::" + fileSecondaryContactID);
                            Picasso.get().load(file).resize(200, 200).into(ivSecondaryContactIdImage);

                        }else if (strImage == "SecondaryContactIdBack") {
//                            strSecondaryContactIDBack = "file://" + photos.get(0);
////                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedSecondaryContactID = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedSecondaryContactID);
                            fileSecondaryContactIDBack = file;
//                            fileSecondaryContactIDBack = file;
                            Log.d("d", "Driver pic Image File::" + fileSecondaryContactIDBack);
                            Picasso.get().load(file).resize(200, 200).into(ivSecondaryContactIdImageBack);

                        } else if (strImage == "CompanyPANCard") {
//                            strPANCARD = "file://" + photos.get(0);
////                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedPANCARD = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedPANCARD);
                            filePANCARD = file;
//                            filePANCARD = file;
                            Log.d("d", "Driver pic Image File::" + filePANCARD);
                            Picasso.get().load(file).resize(200, 200).into(ivPANCardImage);

                        } else if (strImage == "CanceledCheque") {
//                            strCancelledCheque = "file://" + photos.get(0);
////                            Log.d("d", "Driver pic path : " + strPrimaryContactID);
//                            strSelectedCancelledCheque = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedCancelledCheque);
                            fileCancelledCheque = file;
//                            fileCancelledCheque = file;
                            Log.d("d", "Driver pic Image File::" + fileCancelledCheque);
                            Picasso.get().load(file).resize(200, 200).into(ivcanceledChequeImage);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();
                    getContactNumber(data, context);

                    break;

                case PLACE_AUTOCOMPLETE_REQUEST_CODE:

                    if (resultCode == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i(TAG, "Place: " + place.getName() + ", " + place.getAddress());
                        etRegisteredAddress.setText(place.getAddress());

                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i(TAG, status.getStatusMessage());

                    } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }

                    break;
            }
        }
    }


    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }
        catch(Exception e){
            e.getMessage();
            return null;
        }
    }

    public File BitmapTOFile(Bitmap fullImage){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        byte[] byteArray = bytes.toByteArray();
        Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);
        file = ImageUtils.saveImage(scaledBitmap, context);

        return file;
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void getContactNumber(Intent data, final Context context) {
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";
//        etRegMob.setText("");

        List<String> allNumbers = new ArrayList<>();
        int contactIdColumnId, phoneColumnID, nameColumnID;
        try {
            Uri result = data.getData();
//            Log.e(TAG, result.toString());
            String id = result.getLastPathSegment();
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);
                    Log.e("Phone", phoneNumber);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
                    allNumbers.size();
                    cursor.moveToNext();

                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (allNumbers.size() > 1 && !allNumbers.isEmpty()) {

                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        selectedNumber = items[item].toString().replaceAll("[^0-9\\+]", "");

                        Log.d("numberOnClick", selectedNumber);

                        if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {

                            if (strPhoneNo.equalsIgnoreCase("MobileNo")) {

//                                etCompanyMobNo.setText(selectedNumber);
//                                etCompanyMobNo.requestFocus();

                            }else if (strPhoneNo.equalsIgnoreCase("OfficeNo")){

                                etCompanyPhoneNo.setText(selectedNumber);
                                etCompanyPhoneNo.requestFocus();

                            }else if (strPhoneNo.equalsIgnoreCase("PCMobileNo")){

                                etPCMNumber.setText(selectedNumber);
                                etPCMNumber.requestFocus();

                            }else if (strPhoneNo.equalsIgnoreCase("SCMobileNo")){

                                etSCMNumber.setText(selectedNumber);
                                etSCMNumber.requestFocus();

                            }

                        } else {
                            new CustomToast(activity, getResources().getString(R.string.txt_message_mobile_no));
                        }
                    }
                });
                AlertDialog alert = builder.create();

                alert.show();

            } else {

                selectedNumber = phoneNumber.replaceAll("[^0-9\\+]", "");

                Log.d("number", selectedNumber);

                if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {

                    if (strPhoneNo.equalsIgnoreCase("MobileNo")) {

//                        etCompanyMobNo.setText(selectedNumber);
//                        etCompanyMobNo.requestFocus();

                    }else if (strPhoneNo.equalsIgnoreCase("OfficeNo")){

                        etCompanyPhoneNo.setText(selectedNumber);
                        etCompanyPhoneNo.requestFocus();

                    }else if (strPhoneNo.equalsIgnoreCase("PCMobileNo")){

                        etPCMNumber.setText(selectedNumber);
                        etPCMNumber.requestFocus();

                    }else if (strPhoneNo.equalsIgnoreCase("SCMobileNo")){

                        etSCMNumber.setText(selectedNumber);
                        etSCMNumber.requestFocus();

                    }
                } else {
                    new CustomToast(activity, getResources().getString(R.string.txt_message_mobile_no));
                }
            }
        }
    }

    @Override
    public void MyProfile(UserLoginDetailsModel userLoginDetailsModel) {

        UserLoginDetailsModel muserLoginDetailsModel = new UserLoginDetailsModel();
        muserLoginDetailsModel = userLoginDetailsModel;
        if(!"".equals(userLoginDetailsModel.getCompany_name()) && !"null".equals(userLoginDetailsModel.getCompany_name())){
            etCompanyName.setText(userLoginDetailsModel.getCompany_name());
        }
        if(!"".equals(userLoginDetailsModel.getCompany_address()) && !"null".equals(userLoginDetailsModel.getCompany_address())){
            etRegisteredAddress.setText(userLoginDetailsModel.getCompany_address());
        }

        if (!userLoginDetailsModel.getEmail().equals("") && !userLoginDetailsModel.getEmail().equals("null")) {
            etCompanyEmail.setText(userLoginDetailsModel.getEmail());
        }else{
            etCompanyEmail.setText("N/A");
        }

        if (!userLoginDetailsModel.getCompany_phone_number().equals("") && !userLoginDetailsModel.getCompany_phone_number().equals("null")) {
            etCompanyPhoneNo.setText(userLoginDetailsModel.getCompany_phone_number());
        }

//        if (!userLoginDetailsModel.getCompany_mobile_number().equals("") && !userLoginDetailsModel.getCompany_mobile_number().equals("null")) {
//            etCompanyMobNo.setText(userLoginDetailsModel.getCompany_mobile_number());
//        }

        if (!userLoginDetailsModel.getCompany_gst_number().equals("") && !userLoginDetailsModel.getCompany_gst_number().equals("null")) {
            etGSTNumberProfile.setText(userLoginDetailsModel.getCompany_gst_number());
        }

        if (!userLoginDetailsModel.getCompany_pan_number().equals("") && !userLoginDetailsModel.getCompany_pan_number().equals("null")) {
            etPANNumberProfile.setText(userLoginDetailsModel.getCompany_pan_number());
        }

        if (!userLoginDetailsModel.getPrimary_contact_name().equals("") && !userLoginDetailsModel.getPrimary_contact_name().equals("null")) {
            etPCPName.setText(userLoginDetailsModel.getPrimary_contact_name());
        }

        if (!userLoginDetailsModel.getPrimary_contact_mobile().equals("") && !userLoginDetailsModel.getPrimary_contact_mobile().equals("null")) {
            etPCMNumber.setText(userLoginDetailsModel.getPrimary_contact_mobile());
        }

        if (!userLoginDetailsModel.getSecondary_contact_name().equals("") && !userLoginDetailsModel.getSecondary_contact_name().equals("null")) {
            etSCPName.setText(userLoginDetailsModel.getSecondary_contact_name());
        }

        if (!userLoginDetailsModel.getSecondary_contact_mobile().equals("") && !userLoginDetailsModel.getSecondary_contact_mobile().equals("null")) {
            etSCMNumber.setText(userLoginDetailsModel.getSecondary_contact_mobile());
        }

        if (!userLoginDetailsModel.getBank_account_number().equals("") && !userLoginDetailsModel.getBank_account_number().equals("null")) {
            etbankAccountNumberProfile.setText(userLoginDetailsModel.getBank_account_number());
        }

        if (!userLoginDetailsModel.getIfsc_code().equals("") && !userLoginDetailsModel.getIfsc_code().equals("null")) {
            etIfscCodeProfile.setText(userLoginDetailsModel.getIfsc_code());
        }


        if (!userLoginDetailsModel.getPrimary_contact_id().equals("") && !userLoginDetailsModel.getPrimary_contact_id().equals("null")) {
            try {
                Picasso.get().load(userLoginDetailsModel.getPrimary_contact_id()).resize(200, 200).into(ivPrimaryContactIdImage);

                strPrimaryContactID = userLoginDetailsModel.getPrimary_contact_id();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getPrimary_id_back())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getPrimary_id_back()).resize(200, 200).into(ivPrimaryContactIdImageBack);

                strPrimaryContactIDBack = userLoginDetailsModel.getPrimary_id_back();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getSecondary_contact_id())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getSecondary_contact_id()).resize(200, 200).into(ivSecondaryContactIdImage);

                strSecondaryContactID = userLoginDetailsModel.getSecondary_contact_id();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (!TextUtils.isEmpty(userLoginDetailsModel.getSecondary_id_back() )) {
            try {
                Picasso.get().load(userLoginDetailsModel.getSecondary_id_back()).resize(200, 200).into(ivSecondaryContactIdImageBack);

                strSecondaryContactIDBack = userLoginDetailsModel.getSecondary_id_back();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (!TextUtils.isEmpty(userLoginDetailsModel.getCompany_pan_card())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getCompany_pan_card()).resize(200, 200).into(ivPANCardImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(userLoginDetailsModel.getCancel_check())) {
            try {
                Picasso.get().load(userLoginDetailsModel.getCancel_check()).resize(200, 200).into(ivcanceledChequeImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(!userLoginDetailsModel.getProfile_pic().equals("") && !userLoginDetailsModel.getProfile_pic().equals("null")){
            try {
                Picasso.get().load(userLoginDetailsModel.getProfile_pic()).into(civCompanyProfilePic);
//                Picasso.with(HomeProfileActivity.this).load(UserLoginDetails.getProfile_pic()).into(civProfilePic);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void editProfileSaveInterface(String msg) {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                msg ,
                context.getResources().getString(R.string.btn_ok),
                "");

        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageUtils.deleteImageGallery();
                finish();

            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void onButtonClick(String strButtonText) {
//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {
//                takePhoto();
//            }

            Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener()
                    {
                        @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if(report.areAllPermissionsGranted()){

                                takePhoto();
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                                showAlert();
                            }
                        }
                        @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                        {/* ... */

                            token.continuePermissionRequest();
                        }
                    }).check();

        } else if (strButtonText.equals(getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";
//            if (result) {
//                mCallPhotoGallary();
//            }

            Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener()
                    {
                        @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if(report.areAllPermissionsGranted()){

                                mCallPhotoGallary();
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                                showAlert();
                            }
                        }
                        @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                        {/* ... */

                            token.continuePermissionRequest();
                        }
                    }).check();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if(StaticClass.BottomProfileCompany){
//            finish();
//        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        sharedPref.putBottomViewCompany(StaticClass.Menu_profile_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(EditCompanyProfileActivity.this, StaticClass.Menu_profile_company);

        ImageUtils.deleteImageGallery();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(activity, transitionflag);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                activity.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                activity.getResources().getString(R.string.yes),
                activity.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
