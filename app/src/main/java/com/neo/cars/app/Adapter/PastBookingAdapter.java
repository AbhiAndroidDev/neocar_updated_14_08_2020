package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neo.cars.app.BookingInformationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.MyBookingListModel;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.AddRatingActivity;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by joydeep on 7/5/18.
 */

public class PastBookingAdapter  extends RecyclerView.Adapter<PastBookingAdapter.MyViewHolder> {

    private List<MyBookingListModel> myBookingModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircularImageViewBorder civUpcomingBookingPic;
        CustomTextviewTitilliumWebRegular tvLocation, tvCarOwnerName, tvDate, tvBookingRefId;
        ImageView btnCancel, iv_pastbookingstatus;
        RelativeLayout rlParent;
        LinearLayout vehicleRatingll, driverRatingll;
        CustomTitilliumTextViewSemiBold tvAddRating, tvVehicleRating, tvDriverRating;

        public MyViewHolder(View view) {
            super(view);

            civUpcomingBookingPic= view.findViewById(R.id.civUpcomingBookingPic);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvCarOwnerName = view.findViewById(R.id.tvCarOwnerName);
            tvDate = view.findViewById(R.id.tvDate);
            tvBookingRefId = view.findViewById(R.id.tvBookingRefId);
            btnCancel=view.findViewById(R.id.btnCancel);
            iv_pastbookingstatus=view.findViewById(R.id.iv_pastbookingstatus);
            tvAddRating= view.findViewById(R.id.tvAddRating);
            tvVehicleRating= view.findViewById(R.id.tvVehicleRating);
            tvDriverRating= view.findViewById(R.id.tvDriverRating);
            rlParent = view.findViewById(R.id.rlParent);
            vehicleRatingll = view.findViewById(R.id.vehicleRatingll);
            driverRatingll = view.findViewById(R.id.driverRatingll);
        }
    }

    public PastBookingAdapter(Context context,List<MyBookingListModel> myBookingModelList) {
        this.context = context;
        this.myBookingModelList = myBookingModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_booking_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvLocation.setText(myBookingModelList.get(position).getPickup_location());
        holder.tvDate.setText(myBookingModelList.get(position).getBooking_date());
        holder.tvBookingRefId.setText(myBookingModelList.get(position).getBooking_id());
        holder.tvCarOwnerName.setText(myBookingModelList.get(position).getDuration()+"hrs");

        Picasso.get().load(myBookingModelList.get(position).getVehicle_image()).into(holder.civUpcomingBookingPic);

        // status value meaning :  0 => Pending, 1 => Confirmed, 2 => Cancelled, 3 => Completed,4=>Stopped

        if(myBookingModelList.get(position).getStatus().equals("2")){
            holder.iv_pastbookingstatus.setVisibility(View.GONE);
            holder.btnCancel.setVisibility(View.VISIBLE);
            holder.tvAddRating.setVisibility(View.GONE);
            holder.vehicleRatingll.setVisibility(View.GONE);
            holder.driverRatingll.setVisibility(View.GONE);

        }else if (myBookingModelList.get(position).getReview_added().equalsIgnoreCase("Y")) {
                holder.tvAddRating.setVisibility(View.GONE);
                holder.btnCancel.setVisibility(View.GONE);
                holder.vehicleRatingll.setVisibility(View.VISIBLE);
                holder.driverRatingll.setVisibility(View.VISIBLE);
                holder.iv_pastbookingstatus.setVisibility(View.VISIBLE);
                holder.tvVehicleRating.setText(myBookingModelList.get(position).getVehicle_rating());
                holder.tvDriverRating.setText(myBookingModelList.get(position).getDriver_rating());

            } else {
                holder.iv_pastbookingstatus.setVisibility(View.VISIBLE);
                holder.tvAddRating.setVisibility(View.VISIBLE);
                holder.vehicleRatingll.setVisibility(View.GONE);
                holder.driverRatingll.setVisibility(View.GONE);
                holder.btnCancel.setVisibility(View.GONE);

        }

        holder.tvAddRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addRatingIntent = new Intent(context, AddRatingActivity.class);
                addRatingIntent.putExtra("vehicle_id", myBookingModelList.get(position).getVehicle_id());
                addRatingIntent.putExtra("driver_id", myBookingModelList.get(position).getDriver_id());
                addRatingIntent.putExtra("booking_id", myBookingModelList.get(position).getBooking_id());
                context.startActivity(addRatingIntent);
            }
        });

        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent moreIntent = new Intent(context, BookingInformationActivity.class);
                    moreIntent.putExtra("booking_id",myBookingModelList.get(position).getBooking_id());
                    moreIntent.putExtra("booking_Type", StaticClass.MyVehiclePast);
                    context.startActivity(moreIntent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return myBookingModelList.size();
    }
}
