package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.neo.cars.app.Interface.UserVoucherRedeem_Interface;
import com.neo.cars.app.PaytmIntegration.Constants;
import com.neo.cars.app.PaytmIntegration.Paytm;
import com.neo.cars.app.SetGet.PassengerModel;
import com.neo.cars.app.SetGet.VoucherModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.BeforeRedirectPayTmApi;
import com.neo.cars.app.Webservice.ProcessBooking_webservice;
import com.neo.cars.app.Webservice.Urlstring;
import com.neo.cars.app.Webservice.UserVoucherRedeem_Webservice;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by mithilesh on 17/10/18.
 */

public class BookNowPaymentActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback,UserVoucherRedeem_Interface {

    private Context context;
    private ConnectionDetector cd;
    private ScrollView scview;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvLocationName, tvDate, tvDuration, tvStartTime, tvPickUpLoc, tvDropOffLoc, tvName, tvMobNo,
            tvEndTime,  tvAddlReq, tvAddlInfo, tvHirePurpose, tvExpectedTravelItinerary, tvTotalAmount, tvDiscount, tvNightChargesPay,
            tvPayableAmount, tvAddlPassenHeader, tvAddlPassenger;
    private LinearLayout lladdmore_passenger;
    private CustomButtonTitilliumSemibold btnMakePayment;
    private String strBookingId="", strUserId="", strVehicleId="", strDriverId="", strDate="",
            strPickUptime="", strEndtime="", strTotalPassenger="", strBookingHourValue="",  strPickUpLocation="", strDropLocation="",
            strTotalAmount="", strNightCharge = "", strPickUpLocationId="", strDropLocationId="", strAdditionalRequest="", strAdditionalInfo="", strExpectedRoute="",
            strHirePurposeId="", strHirePurpose="", strVoucherCode="", strBookingtype="", strNetPayableAmount="", strParkingCharge = "",
            strDiscountAmount="", addl_passenger="";

    private Bundle bundle;
    private Toolbar toolbar;
    private ArrayList<PassengerModel> arrlistPassenger;
//    private JSONArray jsonArrayPassenger;
    private CustomEditTextTitilliumWebRegular tvApplyCoupon;
    private CustomTextviewTitilliumWebRegular tvApplyCouponThanks, tvParkingCharge;
    private ImageView ivEnter;
    private RelativeLayout rlBackLayout, llApplyCoupon;
    private int transitionflag = StaticClass.transitionflagNext;

    private BottomView bottomview=new BottomView();

    private String strBookingTimeToSend="", strLocation="", addlPassengerCounter = "";
    private LinearLayout llApplyCouponThanks;
    private SharedPrefUserDetails sharedPref;
//    private PassengerModel passengerModel;

    //paytm variables
    private static final String PAYTM_GENERATE_CHECK_SUM = Urlstring.Basepathmain+"paytmApp/generateChecksum.php";

    private Paytm paytm;
    private String customerid = "CUST1234567", amount = "1.00";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booknowpayment);
        sharedPref = new SharedPrefUserDetails(this);

//        passengerModel = new PassengerModel();

        bundle = getIntent().getExtras();
        if(bundle != null){

            strUserId = bundle.getString("UserId");
            strDriverId = bundle.getString("DriverId");
            Log.e("driver_id", strDriverId);
            strVehicleId = bundle.getString("VehicleId");
            strDate = bundle.getString("BookingDate");
            strPickUptime = bundle.getString("start_time");
            sharedPref.setStartTime(strPickUptime);
            strBookingTimeToSend = bundle.getString("StartTimeToSend");
            strEndtime = bundle.getString("EndTime");
            sharedPref.setEndTime(bundle.getString("EndTime"));

            strPickUpLocation = bundle.getString("PickupLoc");
            strDropLocation = bundle.getString("DropLoc");
            strBookingHourValue = bundle.getString("Duration");
            strHirePurposeId = bundle.getString("booking_purpose_id");
            strPickUpLocationId = bundle.getString("PickUpLocId");
            strDropLocationId = bundle.getString("DropLocId");
            strAdditionalRequest = bundle.getString("AddlReq");
            strAdditionalInfo = bundle.getString("AddlInfo");
            strExpectedRoute = bundle.getString("ExpectedRoute");
            strTotalAmount = bundle.getString("TotalAmount");
            strNightCharge = bundle.getString("NightCharge");
            strNetPayableAmount = bundle.getString("NetPayableAmount");
            strParkingCharge = bundle.getString("parking_charge");
            arrlistPassenger = bundle.getParcelableArrayList("PassengerList");
            addl_passenger = bundle.getString("addl_passenger");
            strHirePurpose = bundle.getString("BookingPurpose");
            strHirePurposeId = bundle.getString("BookingPurposeId");
            strLocation = bundle.getString("Location");
            addlPassengerCounter = bundle.getString("addlPassengerCounter");

            //converting additional passenger list (arrlistPassenger) into json array
//            jsonArrayPassenger = new JSONArray();
//
//            for(int i = 0; i < arrlistPassenger.size(); i++) {
//
//                passengerModel = arrlistPassenger.get(i);
//                String name = passengerModel.getPassenger_name();
//                String contact = passengerModel.getPassenger_contact();
//
//                JSONObject jsonObject = new JSONObject();
//
//                try {
//                    jsonObject.put("name", name);
//                    jsonObject.put("contact_no", contact);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                jsonArrayPassenger.put(jsonObject);
//            }

            Log.d("strDatess", strDate);
            Log.d("strBookingTimeToSends", strBookingTimeToSend);
            Log.d("strBookingHourValues", strBookingHourValue);

            Log.d("d", "**arrlistPassenger**"+arrlistPassenger);
            Log.d("d", "**strHirePurposeId**"+strHirePurposeId);

        }

        new AnalyticsClass(BookNowPaymentActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvBookNowHeaderText));

        tvLocationName = findViewById(R.id.tvLocationName);
        tvDate = findViewById(R.id.tvDate);
        tvDuration = findViewById(R.id.tvDuration);
        tvStartTime = findViewById(R.id.tvStartTime);
        tvPickUpLoc = findViewById(R.id.tvPickUpLoc);
        tvDropOffLoc = findViewById(R.id.tvDropOffLoc);
        tvAddlReq = findViewById(R.id.tvAddlReq);
        tvAddlInfo = findViewById(R.id.tvAddlInfo);
        tvHirePurpose = findViewById(R.id.tvHirePurpose);
        tvExpectedTravelItinerary = findViewById(R.id.tvExpectedTravelItinerary);
        tvTotalAmount = findViewById(R.id.tvTotalAmount);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvNightChargesPay = findViewById(R.id.tvNightChargesPay);
        tvPayableAmount = findViewById(R.id.tvPayableAmount);
        llApplyCoupon = findViewById(R.id.llApplyCoupon);
        btnMakePayment = findViewById(R.id.btnMakePayment);
        tvApplyCoupon = findViewById(R.id.tvApplyCoupon);
        tvApplyCouponThanks = findViewById(R.id.tvApplyCouponThanks);
        tvParkingCharge = findViewById(R.id.tvParkingCharge);
        ivEnter = findViewById(R.id.ivEnter);
        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvAddlPassenHeader = findViewById(R.id.tvAddlPassenHeader);
        tvAddlPassenger = findViewById(R.id.tvAddlPassenger);
        lladdmore_passenger = findViewById(R.id.lladdmore_passenger);
        tvEndTime =  findViewById(R.id.tvEndTime);
        llApplyCouponThanks = findViewById(R.id.llApplyCouponThanks);

        if (!"".equals(strDate) && !"null".equals(strDate)){
            tvDate.setText(strDate);
        }else{
            tvDate.setText("N/A");
        }

        if(!"".equals(strLocation) && !"null".equals(strLocation)){
            tvLocationName.setText(strLocation);
        }else{
            tvLocationName.setText("N/A");
        }

        if(!"".equals(strBookingHourValue) && !"null".equals(strBookingHourValue)){
            tvDuration.setText(strBookingHourValue.concat(" hours"));
        }else{
            tvDuration.setText("N/A");
        }

        if(!"".equals(strPickUptime) && !"null".equals(strPickUptime)){
            tvStartTime.setText(strPickUptime);
        }else{
            tvStartTime.setText("N/A");
        }

        if(!"".equals(strEndtime) && !"null".equals(strEndtime)){
            tvEndTime.setText(strEndtime);
        }else {
            tvEndTime.setText("N/A");
        }

        if(!"".equals(strPickUpLocation) && !"null".equals(strPickUpLocation)){
            tvPickUpLoc.setText(strPickUpLocation);
        }else{
            tvPickUpLoc.setText("N/A");
        }

        if(!"".equals(strDropLocation) && !"null".equals(strDropLocation)){
            tvDropOffLoc.setText(strDropLocation);
        }else{
            tvDropOffLoc.setText("N/A");
        }

        if(!"".equals(strAdditionalRequest) && !"null".equals(strAdditionalRequest)){
            tvAddlReq.setText(strAdditionalRequest);
        }else{
            tvAddlReq.setText("N/A");
        }

        if(!"".equals(strAdditionalInfo) && !"null".equals(strAdditionalInfo)){
            tvAddlInfo.setText(strAdditionalInfo);
        }else{
            tvAddlInfo.setText("N/A");
        }

        if(!"".equals(strExpectedRoute) && !"null".equals(strExpectedRoute)){
            tvExpectedTravelItinerary.setText(strExpectedRoute);
        }else{
            tvExpectedTravelItinerary.setText("N/A");
        }

        if(arrlistPassenger != null){
            Log.d("d", "****arrlistPassenger size***"+arrlistPassenger.size());
            if(arrlistPassenger.size() > 0){

                lladdmore_passenger.setVisibility(View.VISIBLE);

//                tvAddlPassenger.setText(""+arrlistPassenger.size());
                tvAddlPassenger.setText(addlPassengerCounter);


                for (int i=0; i<arrlistPassenger.size(); i++){
                    //inflate layout

                    if(arrlistPassenger.get(i).getPassenger_name().equals("") &&
                    arrlistPassenger.get(i).getPassenger_contact().equals("")){

                    }else {

                        lladdmore_passenger.addView(addView2(i));
                    }
                }
            }else{

                if (addlPassengerCounter.equalsIgnoreCase("0")) {

                    tvAddlPassenger.setText("N/A");
                }else{
                    tvAddlPassenger.setText(addlPassengerCounter);
                }


//                tvAddlPassenHeader.setVisibility(View.GONE);
                lladdmore_passenger.setVisibility(View.GONE);
            }
        }

        if(!"".equals(strHirePurpose) && !"null".equals(strHirePurpose)){
            tvHirePurpose.setText(strHirePurpose);
        }else{
            tvHirePurpose.setText("N/A");
        }

        if(!"".equals(strTotalAmount) && !"null".equals(strTotalAmount)){
            tvTotalAmount.setText(getResources().getString(R.string.Rs) + strTotalAmount);
            tvDiscount.setText(getResources().getString(R.string.Rs) + "0");
            tvPayableAmount.setText(getResources().getString(R.string.Rs) + strNetPayableAmount);
        }else{
            tvTotalAmount.setText("N/A");
            tvDiscount.setText("N/A");
            tvPayableAmount.setText("N/A");
        }

        if (!TextUtils.isEmpty(strNightCharge)){

            tvNightChargesPay.setText(getResources().getString(R.string.Rs) + strNightCharge);
        }else{
            tvNightChargesPay.setText("N/A");
        }

        if (!TextUtils.isEmpty(strParkingCharge)){
            tvParkingCharge.setText(strParkingCharge);
        }


        bottomview.BottomView(BookNowPaymentActivity.this,StaticClass.Menu_Explore);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void Listener(){

        btnMakePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //

                if (cd.isConnectingToInternet()) {
                    new BeforeRedirectPayTmApi().beforeRedirectPayTm(BookNowPaymentActivity.this, strVehicleId, strDriverId, strDate,
                            strBookingTimeToSend, strPickUpLocation, strDropLocation, strBookingHourValue, strHirePurpose, strHirePurposeId,
                            strPickUpLocationId, strDropLocationId, addl_passenger, strAdditionalRequest,
                            strAdditionalInfo, strExpectedRoute, strNetPayableAmount, strDiscountAmount, strNightCharge, strVoucherCode, "", "[{}]");
                }else{
                    Intent i = new Intent(BookNowPaymentActivity.this, NetworkNotAvailable.class);
                    startActivity(i);
                }


                Log.d("strNetPayableAmountClk", strNetPayableAmount);

                if (!strNetPayableAmount.equals("0.00") && !strNetPayableAmount.equals("0")) {
                    if (cd.isConnectingToInternet()) {

                        paytm = new Paytm(
                                Constants.M_ID,
                                Constants.CHANNEL_ID,
                                strNetPayableAmount,
                                Constants.WEBSITE,
                                Constants.CALLBACK_URL,
                                Constants.INDUSTRY_TYPE_ID,
                                customerid
                        );

                        Log.d("paytm", new Gson().toJson(paytm));
                        //calling the method generateCheckSum() which will generate the paytm checksum for payment
                        generateCheckSum(paytm);

                    } else {
                        Intent i = new Intent(BookNowPaymentActivity.this, NetworkNotAvailable.class);
                        startActivity(i);
                    }
                }else if (cd.isConnectingToInternet()) {
                    Log.d("strNetPayableAmounts", strNetPayableAmount);
                    new ProcessBooking_webservice().processBooking(BookNowPaymentActivity.this, strVehicleId, strDriverId,  strDate,
                            strBookingTimeToSend, strPickUpLocation, strDropLocation, strBookingHourValue, strHirePurpose,  strHirePurposeId,
                            strPickUpLocationId, strDropLocationId, addl_passenger, strAdditionalRequest,
                            strAdditionalInfo, strExpectedRoute, strNetPayableAmount, strDiscountAmount, strNightCharge, strVoucherCode,"","[{}]");

                }
            }
        });

        ivEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvApplyCoupon.setError(null);
                strVoucherCode = tvApplyCoupon.getText().toString();

                boolean cancel = false;
                View focusView = null;

                if (TextUtils.isEmpty(strVoucherCode)) {
                    /*tvApplyCoupon.setError(getString(R.string.error_field_required));
                    focusView = tvApplyCoupon;
                    cancel = true;*/
                    new CustomToast(BookNowPaymentActivity.this, getResources().getString(R.string.plsEnterCoupon));

                }else{
                    if(cd.isConnectingToInternet()){
                        new UserVoucherRedeem_Webservice().userVoucherRedeem(BookNowPaymentActivity.this, strUserId, strVoucherCode, strBookingtype, strTotalAmount, strNightCharge);

                    }else{
                        Intent i = new Intent(BookNowPaymentActivity.this, NetworkNotAvailable.class);
                        startActivity(i);
                    }
                }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag = StaticClass.transitionflagBack;
                finish();

            }
        });
    }


    private void generateCheckSum(final Paytm paytm){

        StringRequest request = new StringRequest(Request.Method.POST, PAYTM_GENERATE_CHECK_SUM, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("**********"+jsonObject.toString(3));

                    initializePaytmPayment(jsonObject.getString("CHECKSUMHASH"), paytm);

                } catch (JSONException e) {

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error: ", ""+error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();

                param.put("MID", paytm.getmId());
                param.put("ORDER_ID", paytm.getOrderId());
                param.put("CUST_ID", paytm.getCustId());
                param.put("CHANNEL_ID", paytm.getChannelId());
                param.put("TXN_AMOUNT", paytm.getTxnAmount());
                param.put("WEBSITE", paytm.getWebsite());
                param.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
                param.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());

                if (sharedPref.getMobile() !=null ){
                    param.put("MERC_UNQ_REF", sharedPref.getMobile()+"_"+paytm.getCustId());
                }
                else{
                    param.put("MERC_UNQ_REF", sharedPref.getUserid()+"_"+paytm.getCustId());
                }
               // param.put("MERC_UNQ_REF", sharedPref.getMobile()+"_"+paytm.getCustId());

                System.out.println("***generateCheckSum param : " + param.toString());

                return param;
            }
        };

        request.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(BookNowPaymentActivity.this).add(request);
    }


    private void initializePaytmPayment(String checksumHash, Paytm paytm) {

        //getting paytm service
//        PaytmPGService Service = PaytmPGService.getStagingService(); // old
        PaytmPGService Service = PaytmPGService.getStagingService("");

        //use this when using for production
       // PaytmPGService Service = PaytmPGService.getProductionService();

        //creating a hashmap and adding all the values required
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", Constants.M_ID);
        paramMap.put("ORDER_ID", paytm.getOrderId());
        paramMap.put("CUST_ID", paytm.getCustId());
        paramMap.put("CHANNEL_ID", paytm.getChannelId());
        paramMap.put("TXN_AMOUNT", paytm.getTxnAmount());
        paramMap.put("WEBSITE", paytm.getWebsite());
        paramMap.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());

        if (sharedPref.getMobile() !=null ){
            paramMap.put("MERC_UNQ_REF", sharedPref.getMobile()+"_"+paytm.getCustId());
        }
        else{
            paramMap.put("MERC_UNQ_REF", sharedPref.getUserid()+"_"+paytm.getCustId());
        }
        //paramMap.put("MERC_UNQ_REF", sharedPref.getMobile()+"_"+paytm.getCustId());

        System.out.println("****generateCheckSum param : " + paramMap.toString());
        //creating a paytm order object using the hashmap
        PaytmOrder order = new PaytmOrder(paramMap);

        //intializing the paytm service
        Service.initialize(order, null);

        //finally starting the payment transaction
        Service.startPaymentTransaction(this, true, true, this);

    }

    public View addView2(final int position) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.addmore_name_phno_inflate, null);

        CustomTextviewTitilliumWebRegular tvName = v.findViewById(R.id.tvName);
        CustomTextviewTitilliumWebRegular tvMobNo = v.findViewById(R.id.tvMobNo);

        tvName.setText(arrlistPassenger.get(position).getPassenger_name());
        tvMobNo.setText(arrlistPassenger.get(position).getPassenger_contact());

        return v;
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(BookNowPaymentActivity.this, transitionflag);
    }


    @Override
    public void userVoucherRedeem(VoucherModel voucherModel, String status) {

        Log.d("d", "discount::"+voucherModel.getTotal_discount());


        if (status.equalsIgnoreCase(StaticClass.SuccessResult)) {

            llApplyCoupon.setVisibility(View.GONE);
            llApplyCouponThanks.setVisibility(View.VISIBLE);

            if (!"".equals(voucherModel.getTotal_discount()) && !"null".equals(voucherModel.getTotal_discount())) {
                tvDiscount.setText(getResources().getString(R.string.Rs) + voucherModel.getTotal_discount());
            }

            if (!"".equals(voucherModel.getNet_payable_amount()) && !"null".equals(voucherModel.getNet_payable_amount())) {
                tvPayableAmount.setText(getResources().getString(R.string.Rs) + voucherModel.getNet_payable_amount());
                tvApplyCoupon.setText("");
                strNetPayableAmount = voucherModel.getNet_payable_amount();
                strDiscountAmount = voucherModel.getTotal_discount();

                Log.d("strNetPayableAmount", strNetPayableAmount);
            }
        }

        if (status.equalsIgnoreCase(StaticClass.ErrorResult)){
            strDiscountAmount = "";
            strVoucherCode = "";
//            strNightCharge = "";
            tvApplyCoupon.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.BottomExplore){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) BookNowPaymentActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    //Paytm overriden method
    @Override
    public void onTransactionResponse(Bundle bundle) {
//        Toast.makeText(this, bundle.toString(), Toast.LENGTH_LONG).show();
        Log.d("paytmBundle", bundle.toString());

        String strStatus = bundle.getString("STATUS");
        Log.d("strStatus", strStatus);
        String strOrderId = bundle.getString("ORDERID");
        String strTransactionId = bundle.getString("TXNID");
        String strBankTransactionId = bundle.getString("BANKTXNID");
        String strTransactionInfo = "[{\"BANKTXNID\":\""+strBankTransactionId+"\",\"STATUS\":\""+strStatus+"\",\"ORDERID\":\""+strOrderId+"\"}]";

//        "transaction_info": "[{\"BANKTXNID\":\"5556157\",\"STATUS\":\"TXN_SUCCESS\",\"TXNID\":\"20180921111212800110168824600022527\"}]",

        if (Objects.equals(strStatus, "TXN_SUCCESS")) {

            Log.d("strNightCharge***", strNightCharge);
            new ProcessBooking_webservice().processBooking(BookNowPaymentActivity.this, strVehicleId, strDriverId, strDate,
                    strBookingTimeToSend, strPickUpLocation, strDropLocation, strBookingHourValue, strHirePurpose, strHirePurposeId,
                    strPickUpLocationId, strDropLocationId, addl_passenger, strAdditionalRequest,
                    strAdditionalInfo, strExpectedRoute, strNetPayableAmount, strDiscountAmount, strNightCharge, strVoucherCode, strTransactionId, strTransactionInfo);
        }else {
            new CustomToast(BookNowPaymentActivity.this, strStatus);
        }
    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Toast.makeText(this, "Back Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
    }
}
