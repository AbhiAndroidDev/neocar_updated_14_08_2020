package com.neo.cars.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

public class EnlistActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvEnlistYourVehicle, tvEnlistYourDriver, tvMyVehiclesBooking,
            tvUpcomingBookingcalendar,tvNeoCarsGuidelines,tvFaqs, tvPayoutInfo;
    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;
    private SharedPrefUserDetails sharedPref;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enlist);

        new AnalyticsClass(EnlistActivity.this);

        initialize();
        listener();
    }

    private void initialize() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvEnlist));
        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvEnlistYourVehicle = findViewById(R.id.tvEnlistYourVehicle);
        tvEnlistYourDriver = findViewById(R.id.tvEnlistYourDriver);
        tvMyVehiclesBooking = findViewById(R.id.tvMyVehiclesBooking);
        tvPayoutInfo = findViewById(R.id.tvPayoutInfo);
        tvUpcomingBookingcalendar= findViewById(R.id.tvUpcomingBookingcalendar);

        sharedPref = new SharedPrefUserDetails(EnlistActivity.this);
    }

    private void listener() {
        tvEnlistYourVehicle.setOnClickListener(this);
        tvEnlistYourDriver.setOnClickListener(this);
        tvMyVehiclesBooking.setOnClickListener(this);
        rlBackLayout.setOnClickListener(this);
        tvUpcomingBookingcalendar.setOnClickListener(this);
        tvPayoutInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvEnlistYourVehicle:
                Intent intentVehicle = new Intent(EnlistActivity.this, CompanyVehicleListActivity.class);
                startActivity(intentVehicle);
                break;
            case R.id.tvEnlistYourDriver:
                Intent intentDriver = new Intent(EnlistActivity.this, CompanyDriverListActivity.class);
                startActivity(intentDriver);
                break;
            case R.id.tvMyVehiclesBooking:
                Intent intentBooking = new Intent(EnlistActivity.this, CompanyMyBookingActivity.class);
                intentBooking.putExtra("upcommingFlag",false);
                startActivity(intentBooking);
                break;
            case R.id.tvUpcomingBookingcalendar:
                Intent intentBookingcalender = new Intent(EnlistActivity.this, CompanyMyBookingActivity.class);
                intentBookingcalender.putExtra("upcommingFlag",true);
                startActivity(intentBookingcalender);
                break;
            case R.id.tvPayoutInfo:

                Intent payoutInfoIntent = new Intent(EnlistActivity.this, CompanyPayoutInfoActivity.class);
                startActivity(payoutInfoIntent);

                break;

            case R.id.rlBackLayout:
                transitionflag = StaticClass.transitionflagBack;
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        sharedPref.putBottomViewCompany(StaticClass.Menu_profile_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(EnlistActivity.this, StaticClass.Menu_profile_company);
    }
}
