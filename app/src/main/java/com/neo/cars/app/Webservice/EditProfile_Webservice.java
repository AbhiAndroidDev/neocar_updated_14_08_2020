package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.ExploreActivity;
import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.Interface.EditProfileSave_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;


import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 22/3/18.
 */

public class EditProfile_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private UserDocumentModel userDocumentModel;
    private int transitionflag = StaticClass.transitionflagNext;


    public void EditProfile (Activity context, String strFirstName, String strLastName, String strMob,
                             String strDob, String strSelectedGender, String strNationality,
                             String strEmail, String strAddress, String strFathersName,
                             String strEmergencyName, String strEmergencyNo,
                             String strCompany, File fileProPic, String strStateId, String strCityId,
                             String strOccupationId, String strOtherOccupation,
                             File fileAadharCard, File fileAadharCardBack, File fileOtherDoc, File fileOtherDocBack, String strDocumentCode){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        userDocumentModel = new UserDocumentModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        String struserdocdeatils = sharedPref.getObjectFromPreferenceUserDetails();

        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        userDocumentModel = gson.fromJson(struserdocdeatils, UserDocumentModel.class);

        showProgressDialog();

        MultipartEntity entity = new MultipartEntity();
        try{

            entity.addPart("user_id", new StringBody(UserLoginDetails.getId()));
            entity.addPart("email", new StringBody(strEmail));
            entity.addPart("mobile_no", new StringBody(strMob));

            /*if (strName != null){
                entity.addPart("full_name", new StringBody(strName));
            }*/

            if(strFirstName != null){
                entity.addPart("first_name", new StringBody(strFirstName));
            }

            if(strLastName != null){
                entity.addPart("last_name", new StringBody(strLastName));
            }

            if (strDob != null){
                entity.addPart("dob", new StringBody(strDob));
            }

            if(strSelectedGender != null){
                entity.addPart("sex", new StringBody(strSelectedGender));
            }

            if(strNationality != null){
                entity.addPart("nationality", new StringBody(strNationality));
            }

            if(strAddress != null){
                entity.addPart("address", new StringBody(strAddress));
                Log.d("d", "Address: "+strAddress);
            }

            if(strFathersName != null){
                entity.addPart("father_name", new StringBody(strFathersName));
            }

            if(strEmergencyName != null){
                entity.addPart("emergency_contact_name", new StringBody(strEmergencyName));
            }

            if(strStateId != null){
                Log.d("d", "State id:" + strStateId);
                entity.addPart("state_id", new StringBody(strStateId));
            }

            if (strCityId != null){
                Log.d("d", "City id:" + strCityId);
                entity.addPart("city_id", new StringBody(strCityId));
            }

            if(strEmergencyNo != null){
                entity.addPart("emergency_contact_number", new StringBody(strEmergencyNo));
            }



            if(strCompany != null) {
                entity.addPart("company", new StringBody(strCompany));
            }

            if(strOccupationId != null){
                entity.addPart("occupation", new StringBody(strOccupationId));
                Log.d("d", "occupation_id:::"+strOccupationId);
            }

            if(strOtherOccupation != null){
                entity.addPart("other_occupation", new StringBody(strOtherOccupation));
                Log.d("d", "other_occupation:::" + strOtherOccupation);
            }

            if (fileProPic != null){
                Log.d("d", "Profile pic image:::"+fileProPic);
                entity.addPart("profile_pic", new FileBody(fileProPic));
            }

            if (fileAadharCard != null){
                Log.d("d", "aadhar_document:::"+fileAadharCard);
                entity.addPart("aadhar_document", new FileBody(fileAadharCard));
            }

            if (fileAadharCardBack != null){
                Log.d("d", "aadhar_document:::"+fileAadharCardBack);
                entity.addPart("aadhar_document_back", new FileBody(fileAadharCardBack));
            }

            if(strDocumentCode != null){
                entity.addPart("other_document_type", new StringBody(strDocumentCode));
                Log.d("d", "Other doc type: "+strDocumentCode);
            }

            if (fileOtherDoc != null){
                Log.d("d", "other_document:::"+fileOtherDoc);
                entity.addPart("other_document", new FileBody(fileOtherDoc));
            }

            if (fileOtherDocBack != null){
                Log.d("d", "other_document:::"+fileOtherDocBack);
                entity.addPart("other_document_back", new FileBody(fileOtherDocBack));
            }



        }catch (Exception e){
            e.printStackTrace();
        }

        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.edit_profile,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());

                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                //Log.d("Response edit profile::", response);
                System.out.println("**response Edit Profile**"+response);
                Apiparsedata(response);

            }
        }, entity){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };

        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response) {

        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("edit_profile").optString("user_deleted");
            Msg = jobj_main.optJSONObject("edit_profile").optString("message");
            Status= jobj_main.optJSONObject("edit_profile").optString("status");

            JSONObject details=jobj_main.optJSONObject("edit_profile").optJSONObject("details");
            if (Status.equals(StaticClass.SuccessResult)) {

                UserLoginDetails.setId(details.optString("id"));
              //  UserLoginDetails.setFull_name(details.optString("profile_name"));
                UserLoginDetails.setProfile_name(details.optString("profile_name"));
                UserLoginDetails.setSex(details.optString("sex"));
                UserLoginDetails.setMobile(details.optString("mobile_no"));
//                UserLoginDetails.setMobile_verified(details.optString("mobile_verified"));
                UserLoginDetails.setEmail_verified(details.optString("email_verified"));
                UserLoginDetails.setNationality(details.optString("nationality"));
                UserLoginDetails.setEmail(details.optString("email"));
                UserLoginDetails.setAddress(details.optString("address"));
                UserLoginDetails.setFather_name(details.optString("father_name"));
                UserLoginDetails.setEmergency_contact_name(details.optString("emergency_contact_name"));
                UserLoginDetails.setEmergency_contact_number(details.optString("emergency_contact_number"));

                UserLoginDetails.setOccupation(details.optString("occupation"));
                UserLoginDetails.setOccupation_info(details.optString("occupation_info"));
                UserLoginDetails.setOther_occupation(details.optString("other_occupation"));

                UserLoginDetails.setCompany(details.optString("company"));
                UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                UserLoginDetails.setState_id(details.optString("state_id"));
                UserLoginDetails.setCity_id(details.optString("city_id"));

                String strParentDetails = gson.toJson(UserLoginDetails);
                sharedPref.putObjectToPreferenceUserDetails(strParentDetails);
                sharedPref.setUserloginStatus();


            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("edit_profile").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    // added by tb
                    new CustomToast(mcontext, Msg);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }



        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else{
            if (Status.equals(StaticClass.SuccessResult)){
                ((EditProfileSave_Interface)mcontext).editProfileSaveInterface(Msg);

            }else {
//                new CustomToast(mcontext, Msg);
            }


        }

    }


}