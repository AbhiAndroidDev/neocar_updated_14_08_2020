package com.neo.cars.app.SetGet;

/**
 * Created by joydeep on 4/5/18.
 */

public class MyVehicleBookingModel {

    String booking_id,vehicle_id,driver_id,booking_date,start_time,end_time,total_passenger,pickup_location,drop_location,
            customer_id,customer_name,vehicle_image,flag, review_added, rider_rating;

    String have_other_location;
    String booking_status;

    public String getReview_added() {
        return review_added;
    }

    public void setReview_added(String review_added) {
        this.review_added = review_added;
    }

    public String getRider_rating() {
        return rider_rating;
    }

    public void setRider_rating(String rider_rating) {
        this.rider_rating = rider_rating;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTotal_passenger() {
        return total_passenger;
    }

    public void setTotal_passenger(String total_passenger) {
        this.total_passenger = total_passenger;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getHave_other_location() {
        return have_other_location;
    }

    public void setHave_other_location(String have_other_location) {
        this.have_other_location = have_other_location;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }
}
