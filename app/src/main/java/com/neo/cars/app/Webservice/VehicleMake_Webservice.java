package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.VehicleMake_Interface;
import com.neo.cars.app.Interface.Vehicletype_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 2/4/18.
 */

public class VehicleMake_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "";
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private SharedPrefUserDetails sharedPref;
    private VehicleTypeModel vehicleMakeModel;
    ArrayList<VehicleTypeModel> arrlistVehicleMake;
    VehicleMake_Interface Ivehiclemake_Interface;
    JSONObject jsonObjectVehiclemake;

    public void vehicleMakeWebservice(Activity context, VehicleMake_Interface mvehicleMake_Interface){

        mcontext = context;
        Ivehiclemake_Interface=mvehicleMake_Interface;

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistVehicleMake  = new ArrayList<>();
        gson = new Gson();
        vehicleMakeModel = new VehicleTypeModel();

        showProgressDialog();

        StringRequest vehicleMakeRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_make,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        vehicleMakeRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(vehicleMakeRequest);
    }


    private void showProgressDialog() {
       /* try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void hideProgressDialog() {
       /* try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);
            Msg = jobj_main.optJSONObject("vehicle_make").optString("message");
            Status = jobj_main.optJSONObject("vehicle_make").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("vehicle_make").optJSONArray("details");
            for (int i = 0; i < jsonArrayDetails.length(); i++){
                jsonObjectVehiclemake = jsonArrayDetails.optJSONObject(i);

                VehicleTypeModel vehicleTypeModel = new VehicleTypeModel();
                vehicleTypeModel.setId(jsonObjectVehiclemake.optString("id"));
                vehicleTypeModel.setCode(jsonObjectVehiclemake.optString("code"));
                vehicleTypeModel.setTitle(jsonObjectVehiclemake.optString("title"));
                vehicleTypeModel.setIs_active(jsonObjectVehiclemake.optString("is_active"));

                arrlistVehicleMake.add(vehicleTypeModel);
            }
            Log.d("VehicleMake Model size:", "" + arrlistVehicleMake.size());
            Log.e("VehicleMake Model", "" + arrlistVehicleMake);

        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {
            Ivehiclemake_Interface.VehicleMakeList(arrlistVehicleMake);
        }


    }

}
