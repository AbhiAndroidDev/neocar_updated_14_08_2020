package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleTypeModel;
import java.util.ArrayList;

/**
 * Created by parna on 2/4/18.
 */

public interface VehicleMake_Interface {
    void VehicleMakeList(ArrayList<VehicleTypeModel> arrListVehicleMake);
}
