package com.neo.cars.app.Utils;


import java.util.ArrayList;

public class StaticClass {
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static boolean IsMobileNoChanged = false;

    //Fb, Twitter login smartdevindinana@gmail.com
    //Password Matrix321/Matrix@321
    //Google login: matrixdev2017@gmail.com/matrix2017

    public static String TWITTER_KEY = "AECSZuNP7oI20C78SMzTVgcfd";
    public static String TWITTER_SECRET_KEY = "KaUtceWpRbLaVPlEHgXSSjtaxfqDgo4wqPXjVVcVdWPDIjGKDe";

    public static final String API_KEY = "AIzaSyAOAl0P8rnQSpLJlHq4Y12J9e9IGHpvIqk";

    public static int transitionflagBack = 2;
    public static int transitionflagNext = 1;


    public static String alertyes = "yes";
    public static String alertno = "no";

    public static String Menu_profile = "Menu_profile";
    public static String Menu_Explore = "Menu_Explore";
    public static String Menu_Search = "Menu_Search";
    public static String Menu_Messages = "Menu_Messages";
    public static String Menu_Wishlist = "Menu_Wishlist";

    public static String Menu_profile_company = "Menu_profile_company";
    public static String Menu_MyVehicles_company = "Menu_MyVehicles_company";
    public static String Menu_MyBookings_company = "Menu_MyBookings_company";
    public static String Menu_DriverLists_company = "Menu_DriverLists_company";
    public static String Menu_Messages_company = "Menu_Messages_company";


    public static String SuccessResult = "1";
    public static String ErrorResult = "0";

    public static String FacebookLogin = "F";
    public static String GoogleLogin = "G";
    public static String TwitterLogin = "T";

    public static String AadharCardImage = "AadharCard";
    public static String PanCardImage = "PanCard";
    public static String DrivingLicenseImage  = "DrivingLicense";
    public static String VoterCardImage = "VoterCard";

    public static String TermsCondition = "TermsCondition";
    public static String PrivacyPolicy = "PrivacyPolicy";
    public static String HowToUse = "HowToUse";
    public static String FAQ = "FAQ";
    public static String ContactUs = "ContactUs";
    public static String AboutUs = "AboutUs";

    public static String USERTYPE = "user_type";
    public static String USERID = "user_id";
    public static String USER = "user";
    public static String COMPANY = "company";

    public static boolean isLoginFalg=false;

    public static ArrayList<String> Al_Location=new ArrayList<String>();
    public static boolean isVehicleAvailabilitySave = false;
    public static boolean isLocationPermissionCanceled = false;

    public static ArrayList<String> getAl_Location() {
        return Al_Location;
    }

    public static void setAl_Location(ArrayList<String> al_Location) {
        Al_Location = al_Location;
    }

    public static ArrayList<String> Al_Location_company=new ArrayList<String>();

    public static ArrayList<String> getAl_Location_company() {
        return Al_Location_company;
    }

    public static void setAl_Location_company(ArrayList<String> al_Location_company) {
        Al_Location_company = al_Location_company;
    }

    public static void addAl_Location_company(ArrayList<String> al_Location_company) {
        Al_Location_company.addAll(al_Location_company);
    }

    public static void resetAl_Location_company() {
        Al_Location_company.clear();
    }

    public static int SearchCityRequestCode = 501;
    public static int SearchFilterRequestCode = 502;
    public static int BackRequestCode = 500;

    public static boolean MyVehicleAddUpdate=false;
    public static boolean MyBookingListFlag=false;
    public static boolean OverTimeBookingFlag=false;

    public static String MyVehicleCompleted = "C";
    public static String MyVehicleDeleted = "D";
    public static String MyVehicleOngoing = "O";
    public static String MyVehicleUpcoming = "U";
    public static String MyVehiclePast = "P";
    public static String MyVehicleBooking = "B";
    public static String PaymentInfo = "PI";


    public static int minPrice = 0;
//    public static int maxPrice = 1500;
    public static int maxPrice = 10000;

    public static boolean isSeekBarSet = false;


    public static ArrayList<String> Al_NameBooking=new ArrayList<String>();
    public static ArrayList<String> Al_MobileNoBooking=new ArrayList<String>();


    public static ArrayList<String> getAl_NameBooking() {
        return Al_NameBooking;
    }
    public static void setAl_NameBooking(ArrayList<String> al_NameBooking) {
        Al_NameBooking = al_NameBooking;
    }

    public static ArrayList<String> getAl_MobileNoBooking() {
        return Al_MobileNoBooking;
    }
    public static void setAl_MobileNoBooking(ArrayList<String> al_MobileNoBooking) {
        Al_MobileNoBooking = al_MobileNoBooking;
    }

    public static String Wishlist  = "Wishlist";
    public static String ExploreCar = "ExploreCar";
    public static String ExploreOvertimeCar = "ExploreOvertimeCar";


    //********************** Bottom menu Flag   ***********************************//

    public static boolean BottomWishlist = false;
    public static boolean BottomExplore = false;
    public static boolean BottomProfile = false;
    public static boolean BottomSearch = false;
    public static boolean BottomMessages = false;

    //********************** Bottom menu Flag   ***********************************//

    //********************** Bottom menu Flag Company  ***********************************//

    public static boolean BottomProfileCompany = false;
    public static boolean BottomMyVehicleCompany = false;
    public static boolean BottomBookingsCompany = false;
    public static boolean BottomDriverListCompany = false;
    public static boolean BottomMessagesCompany = false;

    //********************** Bottom menu Flag Company  ***********************************//

    public static int StateLitRequestCode = 701;
    public static int CityLitRequestCode = 702;
    public static int  DocumentListRequestCode = 703;
    public static int  NationalityListRequestCode = 704;
    public static int OccupationLitRequestCode = 705;

    public static String canAddVehicle = "Y";

    public static String MyVehicleActiveDeactive = "ActiveDeactive";
    public static String MyVehicleDelete = "Delete";


    public static String Explore_Whishlist_Add = "Add";
    public static String Explore_Whishlist_Delete = "Delete";


    public static boolean MyVehicleIsDelete = false;
    public static boolean EditProfileIsSave = false;

    public static boolean EditProfileIsSaveCompany = false;

    public static boolean IsPaymentDone = false;

    public static String DriverMarried = "Married";
    public static String DriverSingle = "Single";

    public static  String DriverGenderMale = "Male";
    public static  String DriverGenderFemale = "Female";

    public static int book_now_contact_pos=0;

    public static final String[] MONTHS = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    public static boolean isClearClicked = false;

    //used to set mobile no on Otp Login page
    public static String mobileNo = "";
    public static boolean Is_MobileNo = false;

    public static boolean continueExploring = false;

    public static int selectedPosition = -1;
    public static boolean showMoreClicked = false;
    public static boolean resetClicked = false;

    public static boolean isBookNow = true;

}
