package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.neo.cars.app.Adapter.ExploreOvertimeCarAdapter;
import com.neo.cars.app.Adapter.ExploreOvertimeGridCarAdapter;
import com.neo.cars.app.SetGet.OvertimeAvailabilityModel;
import com.neo.cars.app.SetGet.OvertimeAvailabilityVehicleModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;

public class RequestOvertimeCheckActivity extends AppCompatActivity{

    private Context context;
    private Toolbar toolbar;
//    private SwipeRefreshLayout swipeRefreshLayout;
//    private RecyclerView rcvExploreCar;
    private Gson gson;
    private ArrayList<OvertimeAvailabilityModel> overtimeAvailabilityModelList;
    private SharedPrefUserDetails sharedPref;
    private OvertimeAvailabilityModel overtimeAvailabilityModel;
    private ExploreOvertimeCarAdapter exploreOvertimeCarAdapter;
//    private CustomTitilliumTextViewSemiBold tvNoExploreCarFound;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvmsgSorry;
    private LinearLayoutManager layoutManagerVertical;
    private RelativeLayout rlBackLayout;
    private int transitionflag = StaticClass.transitionflagNext;
    private String strCityId = "";
    private BottomView bottomview = new BottomView();
    private String strCityName = "";
    private String strMsg = "";
    private CustomButtonTitilliumSemibold btnContinueExploring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_overtime_check);

        new AnalyticsClass(RequestOvertimeCheckActivity.this);
        initialize();
        listener();
    }

    private void initialize() {

        context = RequestOvertimeCheckActivity.this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        btnContinueExploring = findViewById(R.id.btnContinueExploring);

//        tvNoExploreCarFound = findViewById(R.id.tvNoExploreCarFound);
        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tvmsgSorry = findViewById(R.id.tvmsgSorry);
        sharedPref = new SharedPrefUserDetails(this);
//        rcvExploreCar = findViewById(R.id.rcvExploreCar);
        overtimeAvailabilityModelList = new ArrayList<>();
        overtimeAvailabilityModel = new OvertimeAvailabilityModel();

        tv_toolbar_title.setText("Request Overtime");
        gson = new Gson();
        String overtimeAvailable = getIntent().getStringExtra("overtimeAvailableModel");
        overtimeAvailabilityModel = gson.fromJson(overtimeAvailable, OvertimeAvailabilityModel.class);
        overtimeAvailabilityModelList.add(overtimeAvailabilityModel);

        if (overtimeAvailabilityModel != null) {
            strCityId = overtimeAvailabilityModel.getCity_id();
            strCityName = overtimeAvailabilityModel.getCity_name();

        }

        strMsg = getIntent().getStringExtra("msgOvertime");
        if (strMsg != null && !strMsg.equals("")) {
            tvmsgSorry.setText(strMsg);
        }

//        if (overtimeAvailabilityModel != null){
//
//
//            rcvExploreCar.setVisibility(View.VISIBLE);
//            tvNoExploreCarFound.setVisibility(View.GONE);
//            rcvExploreCar.removeAllViews();
//            if(!strCityId.equals("") && strCityId != null) {
//                Log.d("d", "hfdfhfsfshgfsh");
//                exploreOvertimeCarAdapter = new ExploreOvertimeCarAdapter(context, overtimeAvailabilityModelList);
//                layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
//                rcvExploreCar.setLayoutManager(layoutManagerVertical);
//                rcvExploreCar.setItemAnimator(new DefaultItemAnimator());
//                rcvExploreCar.setHasFixedSize(true);
//                rcvExploreCar.setAdapter(exploreOvertimeCarAdapter);
////                if (pageCount > 0){
////                    rcvExploreCar.smoothScrollToPosition(pageCount*intOffset-1);
////                }
//
//
//            }else{
//                ArrayList<OvertimeAvailabilityVehicleModel> listOfVehical=new ArrayList<>();
//                listOfVehical=overtimeAvailabilityModelList.get(0).getArrListOvertimeVehicle();
//                ExploreOvertimeGridCarAdapter exploreGrideCarAdapter = new ExploreOvertimeGridCarAdapter(RequestOvertimeCheckActivity.this, listOfVehical);
//                  layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
//                // rcvExploreCar.setLayoutManager(layoutManagerVertical);
//                rcvExploreCar.setLayoutManager(new GridLayoutManager(this, 2));
//                rcvExploreCar.setItemAnimator(new DefaultItemAnimator());
//                rcvExploreCar.setHasFixedSize(true);
//                rcvExploreCar.setAdapter(exploreGrideCarAdapter);
//
////                if (pageCount > 0){
////                    rcvExploreCar.smoothScrollToPosition(pageCount*intOffset-1);
////                }
//            }
//
//        }else{
//            rcvExploreCar.setVisibility(View.GONE);
//            tvNoExploreCarFound.setVisibility(View.VISIBLE);
//
//        }
        bottomview.BottomView(RequestOvertimeCheckActivity.this, StaticClass.Menu_Explore);

    }


    private void listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });


        btnContinueExploring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticClass.continueExploring = true;

                transitionflag = StaticClass.transitionflagBack;
//                startActivity(new Intent(RequestOvertimeCheckActivity.this, FilterOnBookActivity.class));
                startActivity(new Intent(RequestOvertimeCheckActivity.this, SearchActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.BottomProfile){
            finish();
        }

        if(StaticClass.continueExploring){
            finish();
        }

        if( StaticClass.MyBookingListFlag){
            finish();

        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagNext;
            finish();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(RequestOvertimeCheckActivity.this, transitionflag);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }
}
