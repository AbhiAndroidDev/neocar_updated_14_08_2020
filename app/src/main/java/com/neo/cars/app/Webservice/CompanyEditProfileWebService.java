package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.EditProfileSave_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.VolleyParser.CustomMultiPartRequest;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 30/10/18.
 */

public class CompanyEditProfileWebService {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private UserDocumentModel userDocumentModel;
    private int transitionflag = StaticClass.transitionflagNext;

    public void companyEditProfile(Activity context, String strNameOfCompany, String strEmailCompany, String strMobileCompany, File fileProfilePic,
                                   String strDeviceType, String strDeviceId, String strRegisteredAddress, String strPhoneNumber, String strGSTNnumber,
                                   String strPANNumber, String strPrimaryContactPName, String strprimayMobileNumber, File filePrimaryContactId,
                                   File filePrimaryContactIdBack, String strSecondaryContactPName, String strSecondaryMobileNumber,
                                   File fileSecondaryContactId, File fileSecondaryContactIdback, File fileCompanyPANCard, String strBankAccountNumber, String strIfscCode,
                                   File fileCancelCheck, String struserId){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        userDocumentModel = new UserDocumentModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        String struserdocdeatils = sharedPref.getObjectFromPreferenceUserDetails();

        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        userDocumentModel = gson.fromJson(struserdocdeatils, UserDocumentModel.class);

        showProgressDialog();

        MultipartEntity entity = new MultipartEntity();

        try{

            entity.addPart("user_id", new StringBody(UserLoginDetails.getId()));
//            entity.addPart("email", new StringBody(strEmail));
//            entity.addPart("mobile_no", new StringBody(strMob));

            if(strNameOfCompany != null){
                entity.addPart("company_name", new StringBody(strNameOfCompany));
            }

            if(strEmailCompany != null){
                entity.addPart("email", new StringBody(strEmailCompany));
            }

            if(strMobileCompany != null){
                entity.addPart("mobile_no", new StringBody(strMobileCompany));
            }

            if (fileProfilePic != null){
                entity.addPart("profile_pic", new FileBody(fileProfilePic));
            }

            if(strDeviceType != null){
                entity.addPart("device_type", new StringBody(strDeviceType));
            }

            if(strDeviceId != null){
                entity.addPart("device_id", new StringBody(strDeviceId));
            }

            if(strRegisteredAddress != null){
                entity.addPart("registered_address", new StringBody(strRegisteredAddress));
                Log.d("d", "Address: "+strRegisteredAddress);
            }

            if(strPhoneNumber != null){
                entity.addPart("phone_number", new StringBody(strPhoneNumber));
            }

            if(strGSTNnumber != null){
                entity.addPart("gst_number", new StringBody(strGSTNnumber));
            }

            if(strPANNumber != null){
                Log.d("d", "pan_number" + strPANNumber);
                entity.addPart("pan_number", new StringBody(strPANNumber));
            }

            if (strPrimaryContactPName != null){
                Log.d("d", "primary_contact_name:" + strPrimaryContactPName);
                entity.addPart("primary_contact_name", new StringBody(strPrimaryContactPName));
            }

            if(strprimayMobileNumber != null){
                entity.addPart("primary_contact_mobile", new StringBody(strprimayMobileNumber));
            }

            if(filePrimaryContactId != null) {
                entity.addPart("primary_contact_id", new FileBody(filePrimaryContactId));
            }

            if(filePrimaryContactIdBack != null) {
                entity.addPart("primary_id_back", new FileBody(filePrimaryContactIdBack));
            }

            if(strSecondaryContactPName != null){
                entity.addPart("secondary_contact_name", new StringBody(strSecondaryContactPName));
                Log.d("d", "strSecondaryContactPName:::"+strSecondaryContactPName);
            }

            if(strSecondaryMobileNumber != null){
                entity.addPart("secondary_contact_mobile", new StringBody(strSecondaryMobileNumber));
                Log.d("d", "strSecondaryMobileNumber:::" + strSecondaryMobileNumber);
            }

            if (fileSecondaryContactId != null){
                Log.d("d", "fileSecondaryContactId image:::"+fileSecondaryContactId);
                entity.addPart("secondary_contact_id", new FileBody(fileSecondaryContactId));
            }

            if (fileSecondaryContactIdback != null){
                Log.d("d", "fileSecondaryContactId image:::"+fileSecondaryContactIdback);
                entity.addPart("secondary_id_back", new FileBody(fileSecondaryContactIdback));
            }

            if (fileCompanyPANCard != null){
                Log.d("d", "fileCompanyPANCard:::"+fileCompanyPANCard);
                entity.addPart("company_pan_card", new FileBody(fileCompanyPANCard));
            }

            if(strBankAccountNumber != null){
                entity.addPart("bank_account_number", new StringBody(strBankAccountNumber));
                Log.d("d", "Other doc type: "+strBankAccountNumber);
            }

            if (strIfscCode != null){
                Log.d("d", "other_document:::"+strIfscCode);
                entity.addPart("ifsc_code", new StringBody(strIfscCode));
            }

            if (fileCancelCheck != null){
                Log.d("d", "fileCancelCheck:::"+fileCancelCheck);
                entity.addPart("cancel_check", new FileBody(fileCancelCheck));
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        RequestQueue rq = Volley.newRequestQueue(mcontext);
        CustomMultiPartRequest customMultiPartRequest = new CustomMultiPartRequest(Urlstring.edit_company_profile,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        Log.d("Error.Response", error.toString());

                    }
                }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                //Log.d("Response edit profile::", response);
                System.out.println("**response Edit Profile**"+response);
                Apiparsedata(response);

            }
        }, entity){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("xsrf-token",sharedPref.getKEY_Access_Token());
                System.out.println("params******getHeaders***" + params);

                return params;
            }
        };

        customMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        customMultiPartRequest.setShouldCache(false);
        // add the request object to the queue to be executed
        rq.add(customMultiPartRequest);

    }


    private void Apiparsedata(String response) {

        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("edit_company_profile").optString("user_deleted");
            Msg = jobj_main.optJSONObject("edit_company_profile").optString("message");
            Status= jobj_main.optJSONObject("edit_company_profile").optString("status");

            JSONObject details=jobj_main.optJSONObject("edit_company_profile").optJSONObject("details");
            if (Status.equals(StaticClass.SuccessResult)) {

                  UserLoginDetails.setId(details.optString("id"));
                  UserLoginDetails.setEmail(details.optString("email"));
                  UserLoginDetails.setCompany_mobile_number(details.optString("mobile_no"));
                  UserLoginDetails.setUser_type(details.optString("user_type"));
                  UserLoginDetails.setIs_active(details.optString("is_active"));
                  UserLoginDetails.setProfile_name(details.optString("profile_name"));
                  UserLoginDetails.setProfile_pic(details.optString("profile_pic"));
                  UserLoginDetails.setReg_type(details.optString("reg_type"));
                  UserLoginDetails.setReferral_code(details.optString("referral_code"));
                  UserLoginDetails.setIs_notification_active(details.optString("is_notification_active"));
                  UserLoginDetails.setIs_profile_complete(details.optString("is_profile_complete"));
//                  UserLoginDetails.setMobile_verified(details.optString("mobile_verified"));
                  UserLoginDetails.setEmail_verified(details.optString("email_verified"));
//                  UserLoginDetails.setStatus(details.optString("update_status"));
                  UserLoginDetails.setProfile_incomplete_message(details.optString("profile_incomplete_message"));
                  UserLoginDetails.setCompany_name(details.optString("company_name"));
                  UserLoginDetails.setCompany_phone_number(details.optString("phone_number"));
                  UserLoginDetails.setCompany_address(details.optString("registered_address"));
                  UserLoginDetails.setCompany_gst_number(details.optString("gst_number"));
                  UserLoginDetails.setCompany_pan_number(details.optString("pan_number"));
                  UserLoginDetails.setPrimary_contact_name(details.optString("primary_contact_name"));
                  UserLoginDetails.setPrimary_contact_mobile(details.optString("primary_contact_mobile"));
                  UserLoginDetails.setPrimary_contact_id(details.optString("primary_contact_id"));
                  UserLoginDetails.setSecondary_contact_name(details.optString("secondary_contact_name"));
                  UserLoginDetails.setSecondary_contact_mobile(details.optString("secondary_contact_mobile"));
                  UserLoginDetails.setSecondary_contact_id(details.optString("secondary_contact_id"));
                  UserLoginDetails.setCompany_pan_card(details.optString("company_pan_card"));
                  UserLoginDetails.setBank_account_number(details.optString("bank_account_number"));
                  UserLoginDetails.setIfsc_code(details.optString("ifsc_code"));
                  UserLoginDetails.setCancel_check(details.optString("cancel_check"));

            }else if (Status.equals("0")){
                details = jobj_main.optJSONObject("edit_company_profile").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
            new CustomToast(mcontext, Msg);
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else{
            if (Status.equals(StaticClass.SuccessResult)){
                ((EditProfileSave_Interface)mcontext).editProfileSaveInterface(Msg);

            }else {
            }
        }
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
