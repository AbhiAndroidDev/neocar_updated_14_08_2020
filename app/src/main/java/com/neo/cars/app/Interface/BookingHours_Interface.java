package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.BookingHoursModel;

import java.util.ArrayList;

/**
 * Created by parna on 7/5/18.
 */

public interface BookingHours_Interface {

    void BookingHoursList(ArrayList<BookingHoursModel> arrlistBookingHours);
}
