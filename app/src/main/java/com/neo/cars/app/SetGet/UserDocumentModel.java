package com.neo.cars.app.SetGet;

/**
 * Created by parna on 21/3/18.
 */

public class UserDocumentModel {

    String id;
    String document_type;
    String document_side;
    String document_file;
    String document_number;
    String document_expiry_date;
    String is_approved;

    public String getDocument_side() {
        return document_side;
    }

    public void setDocument_side(String document_side) {
        this.document_side = document_side;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocument_file() {
        return document_file;
    }

    public void setDocument_file(String document_file) {
        this.document_file = document_file;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    public String getDocument_expiry_date() {
        return document_expiry_date;
    }

    public void setDocument_expiry_date(String document_expiry_date) {
        this.document_expiry_date = document_expiry_date;
    }

    public String getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(String is_approved) {
        this.is_approved = is_approved;
    }
}
