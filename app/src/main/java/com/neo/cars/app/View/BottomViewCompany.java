package com.neo.cars.app.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.neo.cars.app.CompanyDriverListActivity;
import com.neo.cars.app.CompanyHomeProfileActivity;
import com.neo.cars.app.CompanyInboxActivity;
import com.neo.cars.app.CompanyMyBookingActivity;
import com.neo.cars.app.CompanyVehicleListActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by parna on 1/11/18.
 */

public class BottomViewCompany {

    View v;
    Activity mcontext;
    Context ab;

    TextView tvCompanyProfile, tvMyVehiclesCompany, tvMyBookingsCompany, tvDriverListCompany, tvMessagesCompany;
    String sSelectmenu= StaticClass.Menu_profile_company;
    private int transitionflag = StaticClass.transitionflagNext;
    boolean boolWishlist=false;
    private SharedPrefUserDetails sharedPref;
    CustomTextviewTitilliumWebRegular tvBadgeComapny;
    public Timer timer;

    public View BottomViewCompany(Activity context,String Selectmenu){
        mcontext = context;
        ab = context;

        sSelectmenu=Selectmenu;
        sharedPref = new SharedPrefUserDetails(ab);

        Initialize();
        Listener();
        return v;

    }

    private void Initialize() {
        tvCompanyProfile= mcontext.findViewById(R.id.tvCompanyProfile);
        tvMyVehiclesCompany= mcontext.findViewById(R.id.tvMyVehiclesCompany);
        tvMyBookingsCompany= mcontext.findViewById(R.id.tvBookingCompany);
        tvDriverListCompany= mcontext.findViewById(R.id.tvDriverListCompany);
        tvMessagesCompany= mcontext.findViewById(R.id.tvMessagesCompany);
        tvBadgeComapny = mcontext.findViewById(R.id.tvBadgeCompany);

        if(sharedPref.getBottomViewCompany().equals(StaticClass.Menu_profile_company)){
            tvCompanyProfile.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.footer_profile_icon_active_1, 0, 0);
        }else  if(sharedPref.getBottomViewCompany().equals(StaticClass.Menu_MyVehicles_company)){
            tvMyVehiclesCompany.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_vehicle_select, 0, 0);
        }else  if(sharedPref.getBottomViewCompany().equals(StaticClass.Menu_MyBookings_company)){
            tvMyBookingsCompany.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_booking_select, 0, 0);
        }else  if(sharedPref.getBottomViewCompany().equals(StaticClass.Menu_DriverLists_company)){
            tvDriverListCompany.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.driver_list_select, 0, 0);
        }else if(sharedPref.getBottomViewCompany().equals(StaticClass.Menu_Messages_company)){
            tvMessagesCompany.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.inbox_select, 0, 0);
        }


        try {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {

                    mcontext.runOnUiThread(new Runnable(){

                        @Override
                        public void run() {

                            //System.out.println("*****mcontext.getClass().getName()*****"+mcontext.getClass().getName());
                            if(sharedPref.getNewNotificationStatusCompany()) {
                                tvBadgeComapny.setVisibility(View.VISIBLE);
                            }else{
                                tvBadgeComapny.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            },0, 1*1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Listener(){

        tvCompanyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Log.d("BottomView*********", ""+mcontext.getClass().getName().equals(mcontext.getPackageName()));
                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyHomeProfileActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddVehicleActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditCompanyProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditVehicleActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddRiderRatingActivity"))){

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvCompanyProfileListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();

                    }else{
                        tvCompanyProfileListener();
                    }

                }


            }
        });
//        CompanyEditDriverActivity

        tvMyVehiclesCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyVehicleListActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddVehicleActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditCompanyProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditVehicleActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddRiderRatingActivity"))){


                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvMyVehiclesListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();


                    }else{
                        tvMyVehiclesListener();
                    }
                }else{
//                    ( (ExploreReffresh_Interface)mcontext).onExploreReffresh();
                }

            }
        });

        tvMyBookingsCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("BottomView*********", ""+mcontext.getClass().getName());
                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyMyBookingActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddVehicleActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditCompanyProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditVehicleActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditDriverActivity"))){

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvMyBookingListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();

                    }else{
                        tvMyBookingListener();
                    }
                }
            }
        });

        tvDriverListCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyDriverListActivity")){

                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddVehicleActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditCompanyProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditVehicleActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddRiderRatingActivity"))){

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvDriverListListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();
                    }else{
                        tvDriverListListener();
                    }

                }

            }
        });


        tvMessagesCompany.setOnClickListener(new View.OnClickListener() { //CompanyAddRiderRatingActivity
            @Override
            public void onClick(View v) {

                if (!mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyInboxActivity")){
                    if((mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddVehicleActivity"))||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".EditCompanyProfileActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditVehicleActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyEditDriverActivity")) ||
                            (mcontext.getClass().getName().equals(mcontext.getPackageName()+".CompanyAddRiderRatingActivity"))){

                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(mcontext,
                                mcontext.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                                mcontext.getResources().getString(R.string.yes),
                                mcontext.getResources().getString(R.string.no));
                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                                tvMessagesListener();
                            }
                        });

                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialogYESNO.dismiss();
                            }
                        });

                        alertDialogYESNO.show();

                    }else{
                        tvMessagesListener();
                    }
                }
            }
        });

    }

    private void tvCompanyProfileListener(){

        if(!sharedPref.getBottomView().equals(StaticClass.Menu_profile_company)){
            sharedPref.putBottomView(StaticClass.Menu_profile_company);
            new OnPauseSlider(mcontext, transitionflag);

            Intent mybookingIntent = new Intent(mcontext, CompanyHomeProfileActivity.class);
            mcontext.startActivity(mybookingIntent);

//            mcontext.finish();

        }else {
            StaticClass.BottomProfileCompany = true;
            mcontext.finish();

        }
    }

    private void tvMyVehiclesListener(){
        if(!sharedPref.getBottomView().equals(StaticClass.Menu_MyVehicles_company)){
            sharedPref.putBottomView(StaticClass.Menu_MyVehicles_company);
            new OnPauseSlider(mcontext, transitionflag);
            Intent mybookingIntent = new Intent(mcontext, CompanyVehicleListActivity.class);
            mcontext.startActivity(mybookingIntent);
        }else{
            StaticClass.BottomMyVehicleCompany = true;
            mcontext.finish();
        }
    }

    private void tvDriverListListener(){

        if(!sharedPref.getBottomView().equals(StaticClass.Menu_DriverLists_company)){
            sharedPref.putBottomView(StaticClass.Menu_DriverLists_company);
            new OnPauseSlider(mcontext, transitionflag);
            Intent mybookingIntent = new Intent(mcontext, CompanyDriverListActivity.class);
            mcontext.startActivity(mybookingIntent);
        }else{
            StaticClass.BottomDriverListCompany = true;
            mcontext.finish();
        }
    }

    private void tvMyBookingListener(){

        Log.d("sharedPrefBottomView**", mcontext.getClass().getName());
        Log.d("sharedPrefBottomView**", sharedPref.getBottomViewCompany());

        if(!sharedPref.getBottomViewCompany().equals(StaticClass.Menu_MyBookings_company)){
            sharedPref.putBottomViewCompany(StaticClass.Menu_MyBookings_company);
            new OnPauseSlider(mcontext, transitionflag);
            Intent mybookingIntent = new Intent(mcontext, CompanyMyBookingActivity.class);
            mcontext.startActivity(mybookingIntent);
        }else{
            StaticClass.BottomBookingsCompany = true;
            mcontext.finish();
        }
    }

    private void tvMessagesListener(){
        if(!sharedPref.getBottomView().equals(StaticClass.Menu_Messages_company)){
            sharedPref.putBottomView(StaticClass.Menu_Messages_company);
            new OnPauseSlider(mcontext, transitionflag);

            Intent mybookingIntent = new Intent(mcontext, CompanyInboxActivity.class);
            mcontext.startActivity(mybookingIntent);

        }else {
            StaticClass.BottomMessagesCompany = true;
            mcontext.finish();

        }
    }
}
