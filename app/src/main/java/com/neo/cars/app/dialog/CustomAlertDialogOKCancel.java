package com.neo.cars.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.R;

import java.util.Objects;


public class CustomAlertDialogOKCancel extends Dialog {
    Context context;

    RelativeLayout Custom_Alert_Dialog_YES_NO;

    String message,yes,No;

    TextView TV_MESSAGE_CUSTOM_ALERT_DIALOG_YES_NO;

    Button BT_YES_CUSTOM_ALERT_DIALOG_YES_NO, BT_YES_CUSTOM_ALERT_DIALOG_cancel;

    View.OnClickListener onAcceptButtonClickListener;
    View.OnClickListener onCancelButtonClickListener;


    public CustomAlertDialogOKCancel(Context context, String msg, String sYes, String sNo) {
        super(context, android.R.style.Theme_Translucent);
        this.context = context;// init Context
        this.message = msg;
        yes=sYes;
        No=sNo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        context.setTheme(R.style.DialogCustomTheme);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Objects.requireNonNull(getWindow()).addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(context, R.color.black));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customdialog_yes_no);

        TV_MESSAGE_CUSTOM_ALERT_DIALOG_YES_NO=findViewById(R.id.tvMsg);

        TV_MESSAGE_CUSTOM_ALERT_DIALOG_YES_NO.setText(""+message);

        BT_YES_CUSTOM_ALERT_DIALOG_YES_NO=findViewById(R.id.btPositive);

        BT_YES_CUSTOM_ALERT_DIALOG_cancel=findViewById(R.id.btNegative);

        BT_YES_CUSTOM_ALERT_DIALOG_YES_NO.setText(yes);
        BT_YES_CUSTOM_ALERT_DIALOG_cancel.setText(No);

        if("".equals(No)){
            BT_YES_CUSTOM_ALERT_DIALOG_cancel.setVisibility(View.GONE);
        }


        BT_YES_CUSTOM_ALERT_DIALOG_YES_NO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onAcceptButtonClickListener != null)
                    onAcceptButtonClickListener.onClick(v);
            }
        });


        BT_YES_CUSTOM_ALERT_DIALOG_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onCancelButtonClickListener != null)
                    onCancelButtonClickListener.onClick(v);
            }
        });

    }


//    @Override
//    public void show() {
//        try {
//            super.show();
//            // set dialog enter animations
//            //Custom_Alert_Dialog_YES_NO.startAnimation(AnimationUtils.loadAnimation(context, R.anim.dialog_main_show_amination));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    @Override
    public void dismiss() {
        CustomAlertDialogOKCancel.super.dismiss();
    }
    public void setOnAcceptButtonClickListener(
            View.OnClickListener onAcceptButtonClickListener) {
        this.onAcceptButtonClickListener = onAcceptButtonClickListener;
        if(BT_YES_CUSTOM_ALERT_DIALOG_YES_NO != null)
            BT_YES_CUSTOM_ALERT_DIALOG_YES_NO.setOnClickListener(onAcceptButtonClickListener);
    }

    public void setOnCancelButtonClickListener(
            View.OnClickListener onCancelButtonClickListener) {
        this.onCancelButtonClickListener = onCancelButtonClickListener;
        if(BT_YES_CUSTOM_ALERT_DIALOG_cancel != null)
            BT_YES_CUSTOM_ALERT_DIALOG_cancel.setOnClickListener(onCancelButtonClickListener);
    }

}
