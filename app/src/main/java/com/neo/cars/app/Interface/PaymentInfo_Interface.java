package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.PaymentInfoModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 18/5/18.
 */

public interface PaymentInfo_Interface {
     void OnPaymentInfo(ArrayList<PaymentInfoModel> arr_PaymentInfo, String total_record);
}
