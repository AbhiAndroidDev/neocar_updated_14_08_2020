package com.neo.cars.app.dialog;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.neo.cars.app.Interface.CallbackCancelButtonClick;
import com.neo.cars.app.R;

/**
 * Created by parna on 15/5/18.
 */

public class BottomSheetCancel {

    private Context mContext;
    private Activity mActivity;
    private CallbackCancelButtonClick mClickCallBackButton;
    private String mStrMsg, mStrNegative, mStrPositive, mBookingFlag, mBookingType;
    private CallbackCancelButtonClick buttonClick=null;

    public BottomSheetCancel(Context context, Activity activity,
                                             String strMsg,
                                             String strNegative,
                                             String strPositive,
                                             String strBookingFlag,
                                             String strBookingType) {
        this.mContext = context;
        this.mActivity = activity;
        this.mStrMsg = strMsg;
        this.mStrNegative = strNegative;
        this.mStrPositive = strPositive;
        this.mBookingFlag = strBookingFlag;
        this.mBookingType = strBookingType;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }

    public BottomSheetCancel(Context context, Activity activity,
                             CallbackCancelButtonClick buttonClick,
                                             String strMsg,
                                             String strNegative,
                                             String strPositive,
                                             String strBookingFlag,
                                             String strBookingType) {
        this.mContext = context;
        this.mActivity = activity;
        this.buttonClick = buttonClick;
        this.mStrMsg = strMsg;
        this.mStrNegative = strNegative;
        this.mStrPositive = strPositive;
        this.mBookingFlag = strBookingFlag;
        this.mBookingType = strBookingType;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }


    private void callBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(mContext);
        View sheetView = mActivity.getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_positive_negative, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.show();

        TextView tvMsg;
        Button btNegative, btPositive;

        tvMsg = sheetView.findViewById(R.id.tvMsg);
        btNegative = sheetView.findViewById(R.id.btNegative);
        btPositive = sheetView.findViewById(R.id.btPositive);

        tvMsg.setText(mStrMsg);
        btNegative.setText(mStrNegative);
        btPositive.setText(mStrPositive);

        btNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (buttonClick == null) {
                    ((CallbackCancelButtonClick) mActivity).onButtonClick(mStrNegative, mBookingFlag, mBookingType);
                }else {
                    buttonClick.onButtonClick(mStrNegative, mBookingFlag, mBookingType);
                }
            }
        });

        btPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (buttonClick == null) {
                    ((CallbackCancelButtonClick) mActivity).onButtonClick(mStrPositive, mBookingFlag, mBookingType);
                }else {
                    buttonClick.onButtonClick(mStrPositive, mBookingFlag, mBookingType);
                }
            }
        });
    }
}
