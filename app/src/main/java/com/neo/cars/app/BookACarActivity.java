package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.AddRemoveWishlist_Interface;
import com.neo.cars.app.Interface.BookVehicle_Interface;
import com.neo.cars.app.Interface.UserEmailMobileVerifyInterface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleGalleryModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.BookVehicle_Webservice;
import com.neo.cars.app.Webservice.UserMobileEmailVerifyWebService;
import com.neo.cars.app.Webservice.WishlistAdd_webservice;
import com.neo.cars.app.Webservice.WishlistDelete_Webservice;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by parna on 9/5/18.
 */

public class BookACarActivity  extends RootActivity implements BookVehicle_Interface, AddRemoveWishlist_Interface,
        UserEmailMobileVerifyInterface{

    private RelativeLayout rlFirstRelativeLayout, rlCarDetailsLayout, rlBackLayout;
    private ScrollView scvCarDetails;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvTypeName, tvBrandName, tvVehicleMake, tvYrManufacture,
            tvTotalKm, tvMaxPassenger, tvMaxLuggage, tvratingReview, tvHourlyRate, tvLocation;
    private CustomButtonTitilliumSemibold btnBookNow, btnAddToWishList, btnDeleteWishlist;
    private CustomTextviewTitilliumBold tvratingValue;
    private Toolbar toolbar;
    private ImageButton ibNavigateMenu;
    private ViewPager view_pager;
    private Context context;
    private Bundle bundle;
    private ConnectionDetector cd;
    private String strVehicleId="", strDriverId="", strUserId="", strIsComingFrom="", strWishlistId="", strLocation="", strIsInWishlist="",
            strStartDate = "", strDuration = "", strPickUpTime = "";
    private ArrayList<VehicleGalleryModel> listOfVehicleGallery;
    private ArrayList<String>  listOfVehicleImage;
    private MyViewPagerAdapter myViewPagerAdapter;
    private TextView[] dots;
    private LinearLayout dotsLayout;
    private int transitionflag = StaticClass.transitionflagNext;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails sharedPref;
    private BottomView bottomview =new BottomView();
    private RatingBar ratingBar;
    private VehicleTypeModel vehicleTypeModel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookacar);

        bundle = getIntent().getExtras();
        if(bundle != null){
            strVehicleId = bundle.getString("VehicleId");
            strIsComingFrom = bundle.getString("IsComingFrom");
            strStartDate = bundle.getString("FromDate");
            strDuration = bundle.getString("Duration");
            strPickUpTime = bundle.getString("PickUpTime");
            Log.d("d", "Get vehicle id: "+strVehicleId);
            Log.d("d", "Is coming from: "+strIsComingFrom);
        }

        new AnalyticsClass(BookACarActivity.this);

        Initialize();
        Listener();

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        sharedPref = new SharedPrefUserDetails(BookACarActivity.this);
        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
//        strUserId = UserLoginDetails.getId();
        strUserId = sharedPref.getUserid();

        if(cd.isConnectingToInternet()){
            new BookVehicle_Webservice().bookVehicle(BookACarActivity.this, strVehicleId);

        }else {
            Intent i = new Intent(BookACarActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        StaticClass.BottomExplore = false;
        StaticClass.BottomWishlist = false;

        if(StaticClass.IsPaymentDone){
            StaticClass.IsPaymentDone = false;
            finish();

        }

        if(strIsComingFrom.equalsIgnoreCase(StaticClass.Wishlist)){
            if(StaticClass.BottomWishlist){
                finish();
            }

        }else{
            if(StaticClass.BottomExplore){
                finish();
            }
        }

        if(StaticClass.BottomExplore){
//            StaticClass.BottomExplore = false;
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);

//        tv_toolbar_title.setText(getResources().getString(R.string.tvBookVehicleHeader));

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        ibNavigateMenu.setVisibility(View.VISIBLE);
        dotsLayout = findViewById(R.id.layoutDots);
        view_pager = findViewById(R.id.view_pager);
        rlFirstRelativeLayout =  findViewById(R.id.rlFirstRelativeLayout);
        rlCarDetailsLayout =  findViewById(R.id.rlCarDetailsLayout);
        scvCarDetails = findViewById(R.id.scvCarDetails);
        tvTypeName =  findViewById(R.id.tvTypeName);
        tvBrandName =  findViewById(R.id.tvBrandName);
        tvVehicleMake =  findViewById(R.id.tvVehicleMake);
        tvYrManufacture =  findViewById(R.id.tvYrManufacture);
        tvTotalKm =  findViewById(R.id.tvTotalKm);
        tvMaxPassenger =  findViewById(R.id.tvMaxPassenger);
        tvMaxLuggage =  findViewById(R.id.tvMaxLuggage);
        tvratingReview =  findViewById(R.id.tvratingReview);
        tvHourlyRate =  findViewById(R.id.tvHourlyRate);
        tvLocation =  findViewById(R.id.tvLocation);
        btnBookNow =  findViewById(R.id.btnBookNow);
        btnAddToWishList =  findViewById(R.id.btnAddToWishList);
        rlBackLayout =  findViewById(R.id.rlBackLayout);
        btnDeleteWishlist =  findViewById(R.id.btnDeleteWishlist);
        tvratingValue = findViewById(R.id.tvratingValue);
        ratingBar = findViewById(R.id.preRatingGiven);
        ratingBar.setIsIndicator(true);
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag = StaticClass.transitionflagBack;
                finish();

            }
        });

        tvratingReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(BookACarActivity.this, RatingDetailsActivity.class);
                intent.putExtra("vehicle_id",strVehicleId);
                intent.putExtra("user_id",vehicleTypeModel.getUser_id());
                startActivity(intent);

            }
        });

        btnBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new UserMobileEmailVerifyWebService().UserMobileEmailVerification(BookACarActivity.this);

            }
        });

        btnAddToWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cd.isConnectingToInternet()){
                    new WishlistAdd_webservice().wishlistAdd(BookACarActivity.this, strUserId, strVehicleId);

                }else{
                    Intent i = new Intent(BookACarActivity.this, NetworkNotAvailable.class);
                    startActivity(i);
                }
            }
        });


        btnDeleteWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strDeactivateMsg = context.getResources().getString(R.string.DeleteCar);

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                        strDeactivateMsg ,
                        context.getResources().getString(R.string.yes),
                        context.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();

                        //call service to delete wishlist
                        if(cd.isConnectingToInternet()){
                            Log.d("d", "**User id**"+strUserId);
                            Log.d("d", "**strWishlistId**"+strWishlistId);

                            new WishlistDelete_Webservice().wishlistDelete(BookACarActivity.this, strUserId, strWishlistId );

                        }else{
                            new CustomToast(BookACarActivity.this, getResources().getString(R.string.Network_not_availabl));
                        }
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();

            }
        });
    }

    @Override
    public void bookvehicleInterface(VehicleTypeModel vehicletypedetails) {

        vehicleTypeModel = vehicletypedetails;

        strIsInWishlist = vehicletypedetails.getIs_in_wishlist();
        Log.d("d", "**IsInWishlist**"+strIsInWishlist);

        if(!vehicletypedetails.getWishlist_id().equals("") && !vehicletypedetails.getWishlist_id().equals("null")){
            strWishlistId = vehicletypedetails.getWishlist_id();
            Log.d("d", "**strWishlistId**"+strWishlistId);
        }

        if(!vehicletypedetails.getHourly_rate().equals("") && !vehicletypedetails.getHourly_rate().equals("null")){
            tvHourlyRate.setText(vehicletypedetails.getHourly_rate());
        }

        if(!vehicletypedetails.getCity_name().equals("") && !vehicletypedetails.getCity_name().equals("null")){
            tvLocation.setText(vehicletypedetails.getCity_name());
        }

        if(!vehicletypedetails.getVehicle_type_name().equals("") && !vehicletypedetails.getVehicle_type_name().equals("null")){
            tvTypeName.setText(vehicletypedetails.getVehicle_type_name());
        }

        if(!vehicletypedetails.getModel_name().equals("") && !vehicletypedetails.getModel_name().equals("null")){
            tvBrandName.setText(vehicletypedetails.getModel_name());
        }

        if(!vehicletypedetails.getBrand_name().equals("") && !vehicletypedetails.getBrand_name().equals("null")){
            tvVehicleMake.setText(vehicletypedetails.getBrand_name());
        }

        if(!vehicletypedetails.getYear().equals("") && !vehicletypedetails.getYear().equals("null")){
            tvYrManufacture.setText(vehicletypedetails.getYear());
        }

        if(!vehicletypedetails.getKm_travelled().equals("") && !vehicletypedetails.getKm_travelled().equals("null")){
            tvTotalKm.setText(vehicletypedetails.getKm_travelled());
        }

        if(!vehicletypedetails.getMax_passenger().equals("") && !vehicletypedetails.getMax_passenger().equals("null")){
            tvMaxPassenger.setText(vehicletypedetails.getMax_passenger());
        }

        if(!vehicletypedetails.getMax_luggage().equals("") && !vehicletypedetails.getMax_luggage().equals("null")){
            tvMaxLuggage.setText(vehicletypedetails.getMax_luggage());
        }

        if(!vehicletypedetails.getDriver_id().equals("") && !vehicletypedetails.getDriver_id().equals("null")){
            strDriverId = vehicletypedetails.getDriver_id();
        }

        if(!"".equals(vehicletypedetails.getCity_name()) && !"null".equals(vehicletypedetails.getCity_name())){
            strLocation = vehicletypedetails.getCity_name();
        }

        String avg = vehicletypedetails.getNeo_rating();
        Log.d("Average Rating***** : ", avg);
        if (vehicleTypeModel.getNeo_rating() != "" && vehicleTypeModel.getNeo_rating() != null) {

            ratingBar.setRating(Float.parseFloat(vehicleTypeModel.getNeo_rating()));
            String rtng=String.format("%.1f", Float.parseFloat(vehicleTypeModel.getNeo_rating()));
            tvratingValue.setText(rtng);
        }

        if (vehicleTypeModel.getCount_review() != "" && vehicleTypeModel.getCount_review() != null){

            String review = vehicleTypeModel.getCount_review()+" Reviews";
            tvratingReview.setText(Html.fromHtml("<u>"+review+"</u>"));
        }

        if(vehicletypedetails.getUser_id().equals(strUserId)){
            btnBookNow.setBackgroundResource(R.drawable.button);
            btnBookNow.setClickable(false);
        }

        listOfVehicleGallery = vehicletypedetails.getArr_VehicleGalleryModel();
        listOfVehicleImage = new ArrayList<String>();
        for (int i=0;i<listOfVehicleGallery.size();i++) {
            listOfVehicleImage.add(listOfVehicleGallery.get(i).getImage_file());
            Log.d("d", "Vehicle arraylist image:: " + listOfVehicleImage.get(i));
        }

        Log.d("d", "Vehicle arraylist size: "+listOfVehicleImage.size());

        //addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        view_pager.setAdapter(myViewPagerAdapter);
        view_pager.addOnPageChangeListener(viewPagerPageChangeListener);

        if(strIsComingFrom.equalsIgnoreCase(StaticClass.ExploreCar)){
            tv_toolbar_title.setText(getResources().getString(R.string.tvBookVehicleHeader));
            btnBookNow.setVisibility(View.VISIBLE);

            if("Y".equals(strIsInWishlist)){

                btnAddToWishList.setVisibility(View.GONE);
                btnDeleteWishlist.setVisibility(View.VISIBLE);

            }else {
                btnAddToWishList.setVisibility(View.VISIBLE);
                btnDeleteWishlist.setVisibility(View.GONE);
            }

            bottomview.BottomView(BookACarActivity.this,StaticClass.Menu_Explore);

        }else if (strIsComingFrom.equalsIgnoreCase(StaticClass.Wishlist)){
            tv_toolbar_title.setText(getResources().getString(R.string.tvYourWishlistHeader));
            btnBookNow.setVisibility(View.GONE);
            btnAddToWishList.setVisibility(View.GONE);
            btnDeleteWishlist.setVisibility(View.VISIBLE);
            bottomview.BottomView(BookACarActivity.this,StaticClass.Menu_Wishlist);
            strWishlistId = bundle.getString("WishlistId");
            Log.d("d", "Wishlist id****"+strWishlistId);

        } else if (strIsComingFrom.equalsIgnoreCase(StaticClass.ExploreOvertimeCar)){
            tv_toolbar_title.setText(getResources().getString(R.string.tvBookVehicleHeader));
            btnBookNow.setVisibility(View.VISIBLE);

            if("Y".equals(strIsInWishlist)){
                btnAddToWishList.setVisibility(View.GONE);
                btnDeleteWishlist.setVisibility(View.VISIBLE);

            }else {
                btnAddToWishList.setVisibility(View.VISIBLE);
                btnDeleteWishlist.setVisibility(View.GONE);
            }

            bottomview.BottomView(BookACarActivity.this,StaticClass.Menu_Explore);
        }
    }


    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {


        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        // dots = new TextView[layouts.length];

        try {
            dots = new TextView[listOfVehicleImage.size()];

            int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
            int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

            dotsLayout.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(context);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(colorsInactive[currentPage]);
                dotsLayout.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(colorsActive[currentPage]);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void wishlistAddRemoveInterface(String msg,final String strWishListType) {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(context,
                msg ,
                context.getResources().getString(R.string.btn_ok),
                "");
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();

                if(StaticClass.ExploreCar.equals(strIsComingFrom)) {
                    if (cd.isConnectingToInternet()) {
                        new BookVehicle_Webservice().bookVehicle(BookACarActivity.this, strVehicleId);

                    }

                    if (strWishListType.equals(StaticClass.Explore_Whishlist_Delete)) {
                        btnAddToWishList.setVisibility(View.VISIBLE);
                        btnDeleteWishlist.setVisibility(View.GONE);
                    } else {
                        btnAddToWishList.setVisibility(View.GONE);
                        btnDeleteWishlist.setVisibility(View.VISIBLE);
                    }
                }else{
                    finish();
                }
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void userEmailMobileVerify(String status) {
        if (status.equals(StaticClass.SuccessResult)){


                                transitionflag = StaticClass.transitionflagNext;
                                Intent booknowintent = new Intent(BookACarActivity.this, BookNowActivity.class);
                                booknowintent.putExtra("vehicleId", strVehicleId);
                                booknowintent.putExtra("driverId", strDriverId);
                                booknowintent.putExtra("CityName", strLocation);
                                booknowintent.putExtra("FromDate", strStartDate);
                                booknowintent.putExtra("Duration", strDuration);
                                booknowintent.putExtra("PickUpTime", strPickUpTime);

//                                Log.d("d", "**Book now vehicle id send****"+strVehicleId);
//                                Log.d("d", "**Book now driver id send****"+strDriverId);
//                                Log.d("d", "**Book now location city send****"+strLocation);
                                startActivity(booknowintent);
//                                finish();

        }
    }


    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        private ImageView ivVehicleGallery;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container,final int position) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //View view = layoutInflater.inflate(layouts[position], container, false);
            View view = layoutInflater.inflate(R.layout.view_pager_vehicle_gallery, container, false);
            final ImageView ivVehicleGallery = view.findViewById(R.id.ivVehicleGallery);
            ivVehicleGallery.setScaleType(ImageView.ScaleType.FIT_XY);

//            Picasso.get().load(listOfVehicleImage.get(position)).into(ivVehicleGallery);

            Glide.with(BookACarActivity.this)
                    .load(listOfVehicleImage.get(position))
                    .fitCenter()
                    .into(ivVehicleGallery);
            //Log.d("d", "Vehicle gallery: "+listOfVehicleImage);

            ivVehicleGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(BookACarActivity.this, ImageFullActivity.class);
                    i.putExtra("imgUrl", listOfVehicleImage.get(position));
                    startActivity(i);
                }
            });

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return listOfVehicleImage.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
//
//    @Override
//    public void onBackPressed() {
//        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(BookACarActivity.this,
//                BookACarActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
//                BookACarActivity.this.getResources().getString(R.string.yes),
//                BookACarActivity.this.getResources().getString(R.string.no));
//        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialogYESNO.dismiss();
//                transitionflag = StaticClass.transitionflagBack;
//                finish();
//            }
//        });
//
//        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialogYESNO.dismiss();
//            }
//        });
//
//        alertDialogYESNO.show();
//    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(BookACarActivity.this, transitionflag);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) BookACarActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
