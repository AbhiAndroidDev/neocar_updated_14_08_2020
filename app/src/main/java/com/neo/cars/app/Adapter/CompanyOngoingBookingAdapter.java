package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.neo.cars.app.CompanyBookingInformationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CompanyBookingListModel;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CompanyOngoingBookingAdapter extends RecyclerView.Adapter<CompanyOngoingBookingAdapter.MyViewHolder> {

    private List<CompanyBookingListModel> myBookingModelList;
    private int transitionflag = StaticClass.transitionflagNext;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircularImageViewBorder civUpcomingBookingPic;
        CustomTextviewTitilliumWebRegular tvFrom, tvTo, tvBooking, tvTravellerName, tvAssignDriverName, tvBookingRefrence;
        RelativeLayout rlParent;

        public MyViewHolder(View view) {
            super(view);

            civUpcomingBookingPic= view.findViewById(R.id.civUpcomingBookingPic);
            tvFrom = view.findViewById(R.id.tvFrom);
            tvTo = view.findViewById(R.id.tvTo);
            tvBooking = view.findViewById(R.id.tvBooking);
            tvTravellerName = view.findViewById(R.id.tvTravellerName);
            tvAssignDriverName = view.findViewById(R.id.tvAssignDriverName);
            tvBookingRefrence = view.findViewById(R.id.tvBookingRefrence);
            rlParent = view.findViewById(R.id.rlParent);
        }
    }

    public CompanyOngoingBookingAdapter(Context context,List<CompanyBookingListModel> myBookingModelList) {
        this.context = context;
        this.myBookingModelList = myBookingModelList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.company_ongoing_list_item, parent, false);
        return new CompanyOngoingBookingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompanyOngoingBookingAdapter.MyViewHolder holder, final int position) {

        holder.tvFrom.setText(myBookingModelList.get(position).getPickup_location());
        holder.tvTo.setText(myBookingModelList.get(position).getDrop_location());
        holder.tvBooking.setText(myBookingModelList.get(position).getBooking_date());
        holder.tvBookingRefrence.setText(myBookingModelList.get(position).getBooking_id());
        if (!TextUtils.isEmpty(myBookingModelList.get(position).getDriver_name())) {
            holder.tvTravellerName.setText(myBookingModelList.get(position).getDriver_name());
        }else {
            holder.tvTravellerName.setVisibility(View.GONE);
            holder.tvAssignDriverName.setVisibility(View.VISIBLE);
        }


        Picasso.get().load(myBookingModelList.get(position).getVehicle_image()).transform(new CropCircleTransformation()).into(holder.civUpcomingBookingPic);


        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag=StaticClass.transitionflagNext;
                Intent moreIntent = new Intent(context, CompanyBookingInformationActivity.class);
                moreIntent.putExtra("booking_id",myBookingModelList.get(position).getBooking_id());
                moreIntent.putExtra("booking_Type", StaticClass.MyVehicleOngoing);
                context.startActivity(moreIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.myBookingModelList.size();
    }
}
