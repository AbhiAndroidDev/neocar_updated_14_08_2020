package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.CompanyInboxDetails;
import com.neo.cars.app.InboxDetails;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.InboxModal;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 23/7/18.
 */

public class ReadNotification_Webservice {

    Activity mcontext;
    private String Status = "0", Msg = "",count_unread_message="0", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private ArrayList<InboxModal> arr_InboxModal=new ArrayList<>();
    int position;
    private int transitionflag = StaticClass.transitionflagNext;

    public void ReadNotification(Activity context,ArrayList<InboxModal> ar_InboxModal,int pos) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        arr_InboxModal=ar_InboxModal;
        position=pos;
        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.read_notification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",UserLoginDetails.getId());
                params.put("notification_id",arr_InboxModal.get(position).getId());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("read_notification").optString("user_deleted");
            Msg = jobj_main.optJSONObject("read_notification").optString("message");
            Status= jobj_main.optJSONObject("read_notification").optString("status");
            count_unread_message= jobj_main.optJSONObject("read_notification").optString("count_unread_message");

        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                sharedPref.setNewNotificationStatus(false);

          if (sharedPref.getUserType().equalsIgnoreCase("U")) {

              Intent inboxdetails = new Intent(mcontext, InboxDetails.class);
              inboxdetails.putExtra("messagetitle", arr_InboxModal.get(position).getTitle());
              inboxdetails.putExtra("message", arr_InboxModal.get(position).getDescription());
              inboxdetails.putExtra("Datetime", arr_InboxModal.get(position).getCreated_at());
              inboxdetails.putExtra("notificationId", arr_InboxModal.get(position).getId());
              mcontext.startActivity(inboxdetails);

          }else if (sharedPref.getUserType().equalsIgnoreCase("C")){
              Intent inboxdetails = new Intent(mcontext, CompanyInboxDetails.class);
              inboxdetails.putExtra("messagetitle", arr_InboxModal.get(position).getTitle());
              inboxdetails.putExtra("message", arr_InboxModal.get(position).getDescription());
              inboxdetails.putExtra("Datetime", arr_InboxModal.get(position).getCreated_at());
              inboxdetails.putExtra("notificationId", arr_InboxModal.get(position).getId());
              mcontext.startActivity(inboxdetails);

          }

            }else{
                new CustomToast(mcontext, Msg);
            }

        }
    }
}
