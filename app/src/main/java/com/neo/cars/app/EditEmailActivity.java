package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Webservice.EmailEditWebService;

public class EditEmailActivity extends AppCompatActivity {

    private TextView tvPreEmail;
    private EditText etEmail;
    private Button btnSubmit, btnCancel;
    private String email = "", userType = "";
    private ConnectionDetector cd;
    private SharedPrefUserDetails prefs;
    private Gson gson;
    private UserLoginDetailsModel userLoginDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email);
        new AnalyticsClass(EditEmailActivity.this);

        initialize();
        listener();
    }

    private void initialize() {

        gson = new Gson();
        userLoginDetails=new UserLoginDetailsModel();
        prefs = new SharedPrefUserDetails(this);
        String struserdetails = prefs.getObjectFromPreferenceUserDetails();
        userLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        tvPreEmail = findViewById(R.id.tvPreEmail);
        etEmail = findViewById(R.id.etEmail);
        btnCancel = findViewById(R.id.btnCancel);
        btnSubmit = findViewById(R.id.btnSubmit);

        cd = new ConnectionDetector(this);
        prefs = new SharedPrefUserDetails(this);

        userType = userLoginDetails.getUser_type();

    }

    private void listener() {

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void checkValidation() {

        email = etEmail.getText().toString();
        if (!TextUtils.isEmpty(email)){

             if (!(new Emailvalidation().email_validation(email))) {
                 etEmail.setError(getString(R.string.error_email_format));
                 etEmail.requestFocus();

             }else if (cd.isConnectingToInternet()){

                (new EmailEditWebService()).emailEditWebService(this, email);

            }else {
                startActivity(new Intent(this, NetworkNotAvailable.class));
            }
        }else {
            new CustomToast(this, "Please enter your EmailId");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!TextUtils.isEmpty(prefs.getEmail())){
            tvPreEmail.setText(prefs.getEmail());
        }else {

            tvPreEmail.setText(userLoginDetails.getEmail());
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) EditEmailActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
