package com.neo.cars.app.SetGet;

import java.util.ArrayList;

/**
 * Created by parna on 6/4/18.
 */

public class DriverDataModel {

    String id;
    String name;
    String first_name;
    String last_name;
    String dob;
    String gender;
    String address;
    String nationality;
    String in_employment_since;
    String fathers_name;
    String contact_no;
    String marital_status;
    String name_of_spouse;
    String recent_photograph;
    String name_of_childrens;
    String additional_info;
    String driver_email;
    String driver_associated;
    String can_assign_driver;

    public String getCan_assign_driver() {
        return can_assign_driver;
    }

    public void setCan_assign_driver(String can_assign_driver) {
        this.can_assign_driver = can_assign_driver;
    }

    ArrayList<UserDocumentModel> arr_UserDocumentModel;
    ArrayList<CompanyAvailableDrivermodel> arr_CompanyAvailableDrivermodel;

    public String getDriver_associated() {
        return driver_associated;
    }

    public void setDriver_associated(String driver_associated) {
        this.driver_associated = driver_associated;
    }

    public ArrayList<CompanyAvailableDrivermodel> getArr_CompanyAvailableDrivermodel() {
        return arr_CompanyAvailableDrivermodel;
    }

    public void setArr_CompanyAvailableDrivermodel(ArrayList<CompanyAvailableDrivermodel> arr_CompanyAvailableDrivermodel) {
        this.arr_CompanyAvailableDrivermodel = arr_CompanyAvailableDrivermodel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getIn_employment_since() {
        return in_employment_since;
    }

    public void setIn_employment_since(String in_employment_since) {
        this.in_employment_since = in_employment_since;
    }

    public String getFathers_name() {
        return fathers_name;
    }

    public void setFathers_name(String fathers_name) {
        this.fathers_name = fathers_name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getName_of_spouse() {
        return name_of_spouse;
    }

    public void setName_of_spouse(String name_of_spouse) {
        this.name_of_spouse = name_of_spouse;
    }

    public String getRecent_photograph() {
        return recent_photograph;
    }

    public void setRecent_photograph(String recent_photograph) {
        this.recent_photograph = recent_photograph;
    }

    public String getName_of_childrens() {
        return name_of_childrens;
    }

    public void setName_of_childrens(String name_of_childrens) {
        this.name_of_childrens = name_of_childrens;
    }

    public String getAdditional_info() {
        return additional_info;
    }

    public void setAdditional_info(String additional_info) {
        this.additional_info = additional_info;
    }


    public String getDriver_email() {
        return driver_email;
    }

    public void setDriver_email(String driver_email) {
        this.driver_email = driver_email;
    }



    public ArrayList<UserDocumentModel> getArr_UserDocumentModel() {
        return arr_UserDocumentModel;
    }

    public void setArr_UserDocumentModel(ArrayList<UserDocumentModel> arr_UserDocumentModel) {
        this.arr_UserDocumentModel = arr_UserDocumentModel;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
