package com.neo.cars.app.calendar;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.neo.cars.app.EnlistVehicleAvailablityActivity;
import com.neo.cars.app.R;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

@SuppressLint("InflateParams")
public class CalendarAdapterVehicle extends BaseAdapter {

    private Context mContext;
    private java.util.Calendar month;
    public GregorianCalendar pmonth; // calendar instance for previous month
    /**
     * calendar instance for previous month for getting complete view
     */
    public GregorianCalendar pmonthmaxset;
    private GregorianCalendar selectedDate;
    private int firstDay;
    private int maxWeeknumber;
    private int maxP;
    private int calMaxP;
    private int lastWeekDay;
    private int leftDays;
    private int mnthlength;
    private String itemvalue, curentDateString, strVehicleId;
    private DateFormat df;

    private ArrayList<String> items;
    public static List<String> dayString;
    private View previousView;
    private ArrayList<String> unAvailableList;
    private TextView dayView;


    public CalendarAdapterVehicle(Context context, GregorianCalendar monthCalendar, ArrayList<String> unAvailablelist, String _strVrhicleId) {

        CalendarAdapterVehicle.dayString = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        Log.d("selectedDate", ""+selectedDate);
        mContext = context;
//        mCalenderActivity=activity;
        this.unAvailableList = unAvailablelist;
        this.strVehicleId = _strVrhicleId;

        month.set(GregorianCalendar.DAY_OF_MONTH, 1);
        this.items = new ArrayList<String>();
        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());

        Log.d("curentDateString",curentDateString);


        refreshDays();
    }

    @Override
    public int getCount() {
        Log.d("dayStringSize", ""+dayString.size());
        return dayString.size();

    }

    @Override
    public Object getItem(int position) {
        return dayString.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LinearLayout llContainer;
        if (convertView == null) { // if it's not recycled, initialize some
            // attributes
            LayoutInflater vi = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.adapter_calendar_item ,null);

        }
        dayView =  v.findViewById(R.id.adapterCalender_date);

        llContainer = v.findViewById(R.id.adapterCalender_llContainer);


//        dayView.setBackgroundColor(mContext.getResources().getColor(R.color.gray));

        // separates daystring into parts.
        String[] separatedTime = dayString.get(position).split("-");
        // taking last part of date. ie; 2 from 2012-12-02
        String gridvalue = separatedTime[2].replaceFirst("^0*", "");
        //System.out.println("gridvalue :: "+gridvalue);
        // checking whether the day is in current month or not.
        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
            // setting offdays to white color.
//            dayView.setTextColor(mContext.getResources().getColor(R.color.gray));
            dayView.setClickable(false);
            dayView.setFocusable(false);
            dayView.setVisibility(View.GONE);
//            v.setBackgroundResource(R.drawable.circile_white);
        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
//            dayView.setTextColor(mContext.getResources().getColor(R.color.gray));
            dayView.setClickable(false);
            dayView.setFocusable(false);
            dayView.setVisibility(View.GONE);
//            v.setBackgroundResource(R.drawable.circile_white);
        } else {
            // setting curent month's days in black color.

            dayView.setVisibility(View.VISIBLE);
            dayView.setTextColor(Color.WHITE);

            /* check the date is current date or not */


            if (dayString.get(position).equals(curentDateString)) {
                //System.out.println("curentDateString in view : "+curentDateString);
//                dayView.setTextColor(Color.RED);
//                setSelected(v);
//                previousView = v;
                /* if user visit in current month then stop previous button functionality */
//                mCalenderActivity.disablePreviousButton(true);
            } else {
//                v.setBackgroundResource(R.drawable.circile_white);
//                dayView.setBackgroundResource(R.color.colorPrimary);
            }

            for (int i = 0; i < unAvailableList.size(); i++) {

                Log.d("dayStringPos", dayString.get(position));

                if (dayString.get(position).equalsIgnoreCase(unAvailableList.get(i))) {
                    dayView.setBackgroundColor(mContext.getResources().getColor(R.color.red));
                    dayView.setTextColor(Color.WHITE);
                }
            }

//            if (dayString.get(position).equalsIgnoreCase(unAvailableDate)){
//                dayView.setBackgroundColor(Color.RED);
//            }
        }

        /* show month name on first day of every month */

//        if(Integer.parseInt(gridvalue)==1){
//            tvMonth.setText(getMonth(separatedTime[1].replaceFirst("^0*", "")));
//        }

        /* set day value of every month */

        dayView.setText(gridvalue);

        // create date string for comparison
        String date = dayString.get(position);

        if (date.length() == 1) {
            date = "0" + date;
        }

        String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }

        final int index = position;

        dayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date1 = sdf.parse(dayString.get(index));
                    Date date2 = sdf.parse(curentDateString);

                    Intent intent = new Intent(new Intent(mContext, EnlistVehicleAvailablityActivity.class));
                    intent.putExtra("strvehicleId", strVehicleId);
                    intent.putExtra("strDate", dayString.get(index));
                    mContext.startActivity(intent);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        });

        return v;
    }


    public void refreshDays() {
        // clear items
        if (items != null) {
            items.clear();
        }
        dayString.clear();
        Locale.setDefault(Locale.US);
        pmonth = (GregorianCalendar) month.clone();
        // month start day. ie; sun, mon, etc
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
        // finding number of weeks in current month.
        maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        mnthlength = maxWeeknumber * 7;
        maxP = getMaxP(); // previous month maximum day 31,30....
        calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        pmonthmaxset = (GregorianCalendar) pmonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

        /**
         * filling calendar gridview.
         */
        for (int n = 0; n < mnthlength; n++) {

            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(GregorianCalendar.DATE, 1);

            dayString.add(itemvalue);

        }
    }

    private int getMaxP() {
        int maxP;
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            pmonth.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }
        maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return maxP;
    }



    private String getMonth(String month){
        String strMonth="";
        switch (month){
            case "1":
                strMonth="Jan";
                break;
            case "2":
                strMonth="Feb";
                break;
            case "3":
                strMonth="Mar";
                break;
            case "4":
                strMonth="Apr";
                break;
            case "5":
                strMonth="May";
                break;
            case "6":
                strMonth="Jun";
                break;
            case "7":
                strMonth="Jul";
                break;
            case "8":
                strMonth="Aug";
                break;
            case "9":
                strMonth="Sep";
                break;
            case "10":
                strMonth="Oct";
                break;
            case "11":
                strMonth="Nov";
                break;
            case "12":
                strMonth="Dec";
                break;
            default:
                strMonth="";
                break;
        }
        return strMonth;
    }
}
