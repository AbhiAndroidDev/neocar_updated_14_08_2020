package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.neo.cars.app.AddRiderRatingActivity;
import com.neo.cars.app.BookingInformationActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.MyVehicleBookingModel;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by parna on 12/3/18.
 */

public class MyVehicleBokingListAdapter extends RecyclerView.Adapter<MyVehicleBokingListAdapter.MyViewHolder> {

    private ArrayList<MyVehicleBookingModel> vehicleTypeModelList;
    private Context context;
    private int transitionflag = StaticClass.transitionflagNext;

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView civVehiclePic,civ_mybookingstatus;
        CustomTitilliumTextViewSemiBold tvRiderRating, tvAddRiderRating;
        CustomTextviewTitilliumWebRegular tvFrom, tvTo, tvToDate,tvTravellername;
        RelativeLayout rlParent;
        ImageView tvNeedsApproval;
        LinearLayout riderRatingll;

        public MyViewHolder(View view) {
            super(view);
            civVehiclePic = view.findViewById(R.id.civVehiclePic);
            civ_mybookingstatus= view.findViewById(R.id.civ_mybookingstatus);
            tvRiderRating = view.findViewById(R.id.tvRiderRating);
            tvAddRiderRating = view.findViewById(R.id.tvAddRiderRating);
            tvFrom = view.findViewById(R.id.tvFrom);
            rlParent = view.findViewById(R.id.rlParent);
            tvNeedsApproval = view.findViewById(R.id.tvNeedsApproval);

            tvTo = view.findViewById(R.id.tvTo);
            tvToDate = view.findViewById(R.id.tvToDate);
            tvTravellername = view.findViewById(R.id.tvTravellername);

            riderRatingll = view.findViewById(R.id.riderRatingll);
        }
    }

    public MyVehicleBokingListAdapter(Context context, ArrayList<MyVehicleBookingModel> myvehiclelist){
        this.context = context;
        vehicleTypeModelList = myvehiclelist;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.my_vehicle_bookinglist_inflate, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final  int position) {

        holder.tvFrom.setText(vehicleTypeModelList.get(position).getPickup_location());
        holder.tvTo.setText(vehicleTypeModelList.get(position).getDrop_location());
        holder.tvToDate.setText(vehicleTypeModelList.get(position).getBooking_date());
        holder.tvTravellername.setText(vehicleTypeModelList.get(position).getCustomer_name());

        if(vehicleTypeModelList.get(position).getFlag().equals(StaticClass.MyVehicleCompleted)) {
            holder.civ_mybookingstatus.setVisibility(View.VISIBLE);
            holder.civ_mybookingstatus.setImageResource(R.drawable.my_vehicle_booking_icon_1);
            holder.tvNeedsApproval.setVisibility(View.GONE);

            if (vehicleTypeModelList.get(position).getReview_added().equalsIgnoreCase("Y")){
                holder.tvAddRiderRating.setVisibility(View.GONE);
                holder.riderRatingll.setVisibility(View.VISIBLE);
                holder.tvRiderRating.setText(vehicleTypeModelList.get(position).getRider_rating());
            }else {
                holder.tvAddRiderRating.setVisibility(View.VISIBLE);
                holder.riderRatingll.setVisibility(View.GONE);
            }

        }else if (vehicleTypeModelList.get(position).getFlag().equals(StaticClass.MyVehicleDeleted)){
            holder.civ_mybookingstatus.setVisibility(View.VISIBLE);
            holder.civ_mybookingstatus.setImageResource(R.drawable.ic_cancel);
            holder.tvAddRiderRating.setVisibility(View.GONE);
            holder.riderRatingll.setVisibility(View.GONE);
            holder.tvNeedsApproval.setVisibility(View.GONE);

        }else if (vehicleTypeModelList.get(position).getFlag().equals(StaticClass.MyVehicleUpcoming) || vehicleTypeModelList.get(position).getFlag().equals(StaticClass.MyVehicleOngoing)){
            holder.civ_mybookingstatus.setVisibility(View.VISIBLE);
            holder.civ_mybookingstatus.setImageResource(R.drawable.my_vehicle_booking_icon_2);
            holder.tvAddRiderRating.setVisibility(View.GONE);
            holder.riderRatingll.setVisibility(View.GONE);


            if ("0".equals(vehicleTypeModelList.get(position).getBooking_status())){
                holder.civ_mybookingstatus.setVisibility(View.GONE);
                holder.tvNeedsApproval.setVisibility(View.VISIBLE);
                holder.tvNeedsApproval.setImageResource(R.drawable.need_approval);
                holder.tvAddRiderRating.setVisibility(View.GONE);
                holder.riderRatingll.setVisibility(View.GONE);
            }
        }

        Log.d("d", " Booking status *****"+vehicleTypeModelList.get(position).getBooking_status());


        Picasso.get().load(vehicleTypeModelList.get(position).getVehicle_image()).transform(new CropCircleTransformation()).into(holder.civVehiclePic);

        holder.tvAddRiderRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addRatingIntent = new Intent(context, AddRiderRatingActivity.class);
                addRatingIntent.putExtra("rider_id", vehicleTypeModelList.get(position).getCustomer_id());
                addRatingIntent.putExtra("booking_id", vehicleTypeModelList.get(position).getBooking_id());
                context.startActivity(addRatingIntent);
            }
        });

        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag=StaticClass.transitionflagNext;
                Intent moreIntent = new Intent(context, BookingInformationActivity.class);
                moreIntent.putExtra("booking_id",vehicleTypeModelList.get(position).getBooking_id());
                moreIntent.putExtra("booking_Type", StaticClass.MyVehicleBooking); // value = B
                moreIntent.putExtra("booking_flag", vehicleTypeModelList.get(position).getFlag());
                moreIntent.putExtra("have_other_location", vehicleTypeModelList.get(position).getHave_other_location());
                moreIntent.putExtra("booking_status", vehicleTypeModelList.get(position).getBooking_status());
                context.startActivity(moreIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicleTypeModelList.size();
    }
}
