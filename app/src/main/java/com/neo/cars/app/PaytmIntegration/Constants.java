package com.neo.cars.app.PaytmIntegration;

/**
 * Created by sutopa on 18/9/18.
 */

public class Constants {

    //for paytm staging

    public static final String M_ID = "MayurS10245738780961"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WEB"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPSTAGING";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    //for paytm production

/*    public static final String M_ID = "MayurS59364106892614"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail109"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPPROD";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";*/

//    {MID=MayurS59364106892614, CALLBACK_URL=https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=181778983, TXN_AMOUNT=1.00, ORDER_ID=181778983, WEBSITE=APPPROD, INDUSTRY_TYPE_ID=Retail109, CHANNEL_ID=WAP, CUST_ID=CUST123456}

}
