package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.neo.cars.app.Adapter.PayoutInfoAdapter;
import com.neo.cars.app.Interface.PaymentInfo_Interface;
import com.neo.cars.app.SetGet.PaymentInfoModel;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

public class PayoutInformation extends AppCompatActivity implements PaymentInfo_Interface {

    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private RelativeLayout rlBackLayout;
    private RecyclerView rcv_PaymentInformation;
    private LinearLayoutManager layoutManagerVertical;
    private PayoutInfoAdapter payoutinfoAdapter;
    private List<PaymentInfoModel> myPaymentInfoModel = new ArrayList<>();
    private BottomView bottomview = new BottomView();
    private CustomTitilliumTextViewSemiBold tvNoPayoutFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payout_information);

        new AnalyticsClass(PayoutInformation.this);

        Initialize();
        Listener();

    }

    private void Initialize() {

        rcv_PaymentInformation = findViewById(R.id.rcv_PaymentInformation);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.PayoutInformation));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvNoPayoutFound = findViewById(R.id.tvNoPayoutFound);

        bottomview.BottomView(PayoutInformation.this, StaticClass.Menu_profile);

    }

    private void Listener() {
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(PayoutInformation.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        transitionflag = StaticClass.transitionflagBack;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        if (NetWorkStatus.isNetworkAvailable(PayoutInformation.this)) {
//            new PayoutInfo_Webservice().PayoutInfo(PayoutInformation.this); //coming soon...
        } else {
            Intent i = new Intent(PayoutInformation.this, NetworkNotAvailable.class);
            startActivity(i);
        }

    }

    @Override
    public void OnPaymentInfo(ArrayList<PaymentInfoModel> arr_PaymentInfo, String total_record) {

        myPaymentInfoModel=arr_PaymentInfo;

        if(myPaymentInfoModel.size() > 0){
            payoutinfoAdapter = new PayoutInfoAdapter(myPaymentInfoModel,this);
            layoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rcv_PaymentInformation.setLayoutManager(layoutManagerVertical);
            rcv_PaymentInformation.setItemAnimator(new DefaultItemAnimator());
            rcv_PaymentInformation.setHasFixedSize(true);
            rcv_PaymentInformation.setAdapter(payoutinfoAdapter);

        }else{
            rcv_PaymentInformation.setVisibility(View.GONE);
            tvNoPayoutFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) PayoutInformation.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
