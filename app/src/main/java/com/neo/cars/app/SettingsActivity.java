package com.neo.cars.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.CallBackOnDeleteUser;
import com.neo.cars.app.Interface.DeleteUserInterface;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.DeleteUser_Webservice;
import com.neo.cars.app.Webservice.LogoutUser_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;


/**
 * Created by parna on 27/2/18.
 */

public class SettingsActivity extends AppCompatActivity implements CallBackButtonClick, CallBackOnDeleteUser, DeleteUserInterface {

    private Toolbar toolbar;
    private ImageButton ibNavigateMenu;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile, tvEnlistVehicle,  tvNotificationSettings, tvTermsAndCond, tvPrivacyPolicy, tvLogout, tvPaymentInfo, tvDeleteUser;
    private RelativeLayout rlBackLayout;
    private ConnectionDetector cd;

    private int transitionflag = StaticClass.transitionflagNext;
    private BottomView bottomview = new BottomView();
    private SharedPrefUserDetails sharedPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        new AnalyticsClass(SettingsActivity.this);
        cd = new ConnectionDetector(this);
        Initialize();
        Listener();
    }

    private void Initialize(){
        sharedPref = new SharedPrefUserDetails(SettingsActivity.this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvSettings));
        rlBackLayout = findViewById(R.id.rlBackLayout);

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        tvTermsAndCond = findViewById(R.id.tvTermsAndCond);
        tvPrivacyPolicy = findViewById(R.id.tvPrivacyPolicy);
        tvLogout = findViewById(R.id.tvLogout);
        tvPaymentInfo = findViewById(R.id.tvPaymentInfo);
        tvNotificationSettings = findViewById(R.id.tvNotificationSettings);
        tvEnlistVehicle = findViewById(R.id.tvEnlistVehicle);
        tvDeleteUser = findViewById(R.id.tvDeleteUser);

        bottomview.BottomView(SettingsActivity.this,StaticClass.Menu_profile);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        tvTermsAndCond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent termsIntent = new Intent(SettingsActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);

            }
        });

        tvPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent privacyIntent = new Intent(SettingsActivity.this, CmsPageActivity.class);
                privacyIntent.putExtra("cms", StaticClass.PrivacyPolicy);
                startActivity(privacyIntent);

            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new BottomSheetDialogPositiveNegative(SettingsActivity.this,
                        SettingsActivity.this,
                        SettingsActivity.this.getResources().getString(R.string.DoyouWantToLogout),
                        SettingsActivity.this.getResources().getString(R.string.yes),
                        SettingsActivity.this.getResources().getString(R.string.no));
            }
        });

//        tvDeleteUser.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                BottomSheetDialogDeleteUser bsd = new BottomSheetDialogDeleteUser(SettingsActivity.this,
//                        SettingsActivity.this,
//                        SettingsActivity.this.getResources().getString(R.string.deleteUserText),
//                        SettingsActivity.this.getResources().getString(R.string.yes),
//                        SettingsActivity.this.getResources().getString(R.string.no));
//            }
//        });

        tvPaymentInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent paymentinfoIntent = new Intent(SettingsActivity.this, PaymentInformation.class);
                startActivity(paymentinfoIntent);

            }
        });

        tvEnlistVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent enlistVehicleIntent = new Intent(SettingsActivity.this, EnlistVehicleActivity.class);
                startActivity(enlistVehicleIntent);

            }
        });

        tvNotificationSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intentSettings=new Intent(SettingsActivity.this,PushNotificationActivity.class);
               startActivity(intentSettings);

            }
        });
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(SettingsActivity.this, transitionflag);
    }


    @Override
    public void onButtonClick(String strButtonText) {
        if (strButtonText.equals(SettingsActivity.this.getResources().getString(R.string.yes))) {

            if(cd.isConnectingToInternet()){
                new LogoutUser_Webservice().logoutUser(SettingsActivity.this);

            }else {
                Intent i = new Intent(SettingsActivity.this, NetworkNotAvailable.class);
                startActivity(i);
            }

//            StaticClass.isLoginFalg=true;
//            transitionflag = StaticClass.transitionflagBack;
//            finish();

        }
    }

    @Override
    public void onButtonCLickDelUser(String strButtonText) {
        if (strButtonText.equals(SettingsActivity.this.getResources().getString(R.string.yes))) {
            if(cd.isConnectingToInternet()){
                new DeleteUser_Webservice().deleteUser(SettingsActivity.this);

            }else {
                Intent i = new Intent(SettingsActivity.this, NetworkNotAvailable.class);
                startActivity(i);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public void responseOnDelete(String msg) {
//        new CustomToast(SettingsActivity.this, msg);
        if (msg.equals(StaticClass.SuccessResult)) {
            Log.d("d", "***Status 34***"+StaticClass.SuccessResult);
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }
}
