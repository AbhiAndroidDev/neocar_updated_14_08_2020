package com.neo.cars.app.SetGet;

/**
 * Created by parna on 12/3/18.
 */

public class MyVehicleModel {

    String location;
    String driver_name;
    String plate_no;


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }
}
