package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.OccupationModel;
import com.neo.cars.app.SetGet.StateListModel;

import java.util.ArrayList;

/**
 * Created by parna on 12/6/18.
 */

public interface OccupationInterface {

    void OccupationList(ArrayList<OccupationModel> arrlistOccupation);
}
