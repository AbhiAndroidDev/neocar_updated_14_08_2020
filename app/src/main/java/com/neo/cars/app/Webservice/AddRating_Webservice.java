package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.AddRatingInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.AddRatingModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 13/9/18.
 */

public class AddRating_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private AddRatingModel addRatingModel;
    private ArrayList<AddRatingModel> arrListaddRatingModel;

    public void addRating(Activity context, final String booking_id, String riview_by_entity, String riview_by_id,
                          final String strWriteReview, final String strVehicleRating, final String strDriverRating,
                          final String vehicleId, final String driverId, final String isCheckedAnonymous){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
//        vehicleTypeModel = new VehicleTypeModel();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();
//        URL: http://neocars.testyourprojects.biz/api/add_review_rating

        StringRequest deleteUserRequest = new StringRequest(Request.Method.POST, Urlstring.add_review_rating,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("booking_id", booking_id);
                params.put("riview_by_entity", "CS");
                params.put("riview_by_id", UserLoginDetails.getId());
                params.put("comment", strWriteReview);
                params.put("car_rating", strVehicleRating);
                params.put("driver_rating", strDriverRating);
                params.put("vehicle_id", vehicleId);
                params.put("driver_id", driverId);
                params.put("send_anonymously", isCheckedAnonymous);

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        deleteUserRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(deleteUserRequest);

    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;

        try{
            jobj_main = new JSONObject(response);
            strUserDeleted = jobj_main.optJSONObject("add_review_rating").optString("user_deleted");
            Msg = jobj_main.optJSONObject("add_review_rating").optString("message");
            Status= jobj_main.optJSONObject("add_review_rating").optString("status");
            details=jobj_main.optJSONObject("add_review_rating").optJSONObject("details");

            addRatingModel = new AddRatingModel();
            arrListaddRatingModel = new ArrayList<AddRatingModel>();

            if (Status.equals(StaticClass.SuccessResult) && details != null){
                addRatingModel.setRiview_by_entity(details.optString("riview_by_entity"));
                addRatingModel.setRiview_by_id(details.optString("riview_by_id"));
                addRatingModel.setCar_rating(details.optString("car_rating"));
                addRatingModel.setDriver_rating(details.optString("driver_rating"));
                addRatingModel.setComment(details.optString("comment"));
                addRatingModel.setVehicle_id(details.optString("vehicle_id"));
                addRatingModel.setDriver_id(details.optString("driver_id"));
                addRatingModel.setVehicle_review_id(details.optString("vehicle_review_id"));
                addRatingModel.setDriver_review_id(details.optString("driver_review_id"));

                arrListaddRatingModel.add(addRatingModel);

                ((AddRatingInterface) mcontext).onAddRating(arrListaddRatingModel);
            }

            if (Status.equals(StaticClass.SuccessResult)) {
            }else if (Status.equals(StaticClass.ErrorResult)){
                details = jobj_main.optJSONObject("user_soft_delete").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            Log.d("d", "***strUserDeleted***"+strUserDeleted);
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            Log.d("d", "***Status 12***"+StaticClass.SuccessResult);
            if (Status.equals(StaticClass.SuccessResult)) {
                Log.d("d", "***Status 34***"+StaticClass.SuccessResult);
                ((AddRatingInterface)mcontext).onAddRating(arrListaddRatingModel);

                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
                builder.setMessage(Msg)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                mcontext.finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle(mcontext.getResources().getString(R.string.app_name));
                alert.show();

//                new CustomToast(mcontext, Msg);
            }

        }

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext ,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
