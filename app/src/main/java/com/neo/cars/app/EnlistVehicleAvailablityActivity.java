package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.neo.cars.app.Interface.CallBackCustomTimePicker;
import com.neo.cars.app.Interface.VehicleAddEditAvailability_Interface;
import com.neo.cars.app.Interface.VehicleUnAvailableDelete_interface;
import com.neo.cars.app.Interface.VehicleUnAvailableDetail_interface;
import com.neo.cars.app.SetGet.VehicleUnAvailabilityDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.VehicleAddEditAvailabilityWebService;
import com.neo.cars.app.Webservice.VehicleUnAvailabilityDeleteWebService;
import com.neo.cars.app.Webservice.VehicleUnAvailableDetailsWebService;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.CustomTimePickerDialog1;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EnlistVehicleAvailablityActivity extends AppCompatActivity implements CallBackCustomTimePicker,
        VehicleUnAvailableDetail_interface, VehicleAddEditAvailability_Interface, VehicleUnAvailableDelete_interface {

    private Toolbar toolbar;
    private CheckBox enl_drv_chkbtn_avl, enl_drv_allday_chkbtn;
    private TextView enl_drv_chkbtn_avl_txt, enl_drv_chkbtn_unavl_txt;
    private ImageView enl_drv_frm_img, enl_drv_to_img;
    private EditText enl_drv_frm_edt, enl_drv_to_edt;
    private Button enl_drv_btn_save, enl_drv_btn_cancel, enl_drv_btn_save_disable;
    private CustomTimePickerDialog1 customTimePickerDialog;
    private String whichBtn="0";
    private String timeFrom="",timeTo="";
    private Boolean isAvailable=true;
    private int transitionflag = StaticClass.transitionflagNext;
    private Context context;
    private Activity activity;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private RelativeLayout rlBackLayout;
    private BottomViewCompany bottomviewCompany = new BottomViewCompany();
    private SharedPrefUserDetails sharedPrefUser;
    private String strId = "", strvehicleId = "", strDate = "", strStartDate = "", strEndTime = "", strAllDay = "";
    private LinearLayout enl_vic_from_ll1, enl_vic_to_ll2;
    private String strIsAvailable = "", unavailable_id = "", status = "";
    private int hourFrom = 0, minFrom = 0, hourTo = 0, minTo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enlist_driver);
        new AnalyticsClass(EnlistVehicleAvailablityActivity.this);

        initialize();
        listener();
        if (getIntent() != null) {
            strvehicleId = getIntent().getStringExtra("strvehicleId");
            strDate = getIntent().getStringExtra("strDate");
        }
        if (NetWorkStatus.isNetworkAvailable(context)) {
            new VehicleUnAvailableDetailsWebService().getVehicleUnAvailableDetails(context, activity, strvehicleId, strDate);
        } else {
            Intent i = new Intent(context, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    private void initialize() {
        context = EnlistVehicleAvailablityActivity.this;
        activity = EnlistVehicleAvailablityActivity.this;

        sharedPrefUser = new SharedPrefUserDetails(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvEnlistYourVehicle));

        rlBackLayout = findViewById(R.id.rlBackLayout);

        enl_drv_allday_chkbtn= findViewById(R.id.enl_drv_allday_chkbtn);
        enl_drv_chkbtn_avl= findViewById(R.id.enl_drv_chkbtn_avl);
        enl_drv_chkbtn_avl_txt= findViewById(R.id.enl_drv_chkbtn_avl_txt);
        enl_drv_chkbtn_unavl_txt= findViewById(R.id.enl_drv_chkbtn_unavl_txt);

        enl_drv_frm_img= findViewById(R.id.enl_drv_frm_img);
        enl_drv_to_img= findViewById(R.id.enl_drv_to_img);

        enl_drv_frm_edt= findViewById(R.id.enl_drv_frm_edt);
        enl_drv_to_edt= findViewById(R.id.enl_drv_to_edt);

        enl_drv_btn_save= findViewById(R.id.enl_drv_btn_save);
        enl_drv_btn_save_disable  =findViewById(R.id.enl_drv_btn_save_disable);
        enl_drv_btn_cancel= findViewById(R.id.enl_drv_btn_cancel);

        enl_vic_from_ll1 = findViewById(R.id.enl_vic_from_ll1);
        enl_vic_to_ll2 = findViewById(R.id.enl_vic_to_ll2);
    }

    private void listener() {
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();

            }
        });

        enl_drv_chkbtn_avl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                  @Override
                  public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                      if(isChecked){ //ischecked true means Available
                          enl_drv_chkbtn_avl_txt.setVisibility(View.VISIBLE);
                          enl_drv_chkbtn_unavl_txt.setVisibility(View.INVISIBLE);
                          isAvailable=true;

                          if (strIsAvailable.equalsIgnoreCase("Y")) {
                              enl_drv_allday_chkbtn.setChecked(false);
                              enl_drv_allday_chkbtn.setEnabled(false);
                              enl_drv_allday_chkbtn.setClickable(false);
                              enl_drv_btn_save.setVisibility(View.GONE);
                              enl_drv_btn_save_disable.setVisibility(View.VISIBLE);
                              enl_drv_btn_save_disable.setClickable(false);
                              enl_drv_frm_img.setImageResource(R.drawable.time_disable);
                              enl_drv_to_img.setImageResource(R.drawable.time_disable);
                              enl_drv_frm_edt.setText("");
                              enl_drv_to_edt.setText("");
                              enl_drv_frm_img.setClickable(false);
                              enl_drv_to_img.setClickable(false);
                          }else {
                              enl_drv_allday_chkbtn.setChecked(false);
                              enl_drv_allday_chkbtn.setEnabled(false);
                              enl_drv_allday_chkbtn.setClickable(false);
                              enl_drv_frm_img.setImageResource(R.drawable.time_disable);
                              enl_drv_to_img.setImageResource(R.drawable.time_disable);
                              enl_drv_frm_edt.setText("");
                              enl_drv_to_edt.setText("");
                              enl_drv_frm_img.setClickable(false);
                              enl_drv_to_img.setClickable(false);
                          }

                      }else{
                          enl_drv_chkbtn_avl_txt.setVisibility(View.INVISIBLE);
                          enl_drv_chkbtn_unavl_txt.setVisibility(View.VISIBLE);
                          isAvailable=false;
                          if (strIsAvailable.equalsIgnoreCase("Y")) {
                              enl_drv_allday_chkbtn.setChecked(true);
                              enl_drv_allday_chkbtn.setEnabled(true);
                              enl_drv_allday_chkbtn.setClickable(true);
                              enl_drv_btn_save.setVisibility(View.VISIBLE);
                              enl_drv_btn_save_disable.setVisibility(View.GONE);
                          }else {
                              enl_drv_allday_chkbtn.setChecked(true);
                              enl_drv_allday_chkbtn.setEnabled(true);
                              enl_drv_allday_chkbtn.setClickable(true);
                          }

                      }
                  }
              }
        );

        enl_drv_frm_edt.setEnabled(false);
        enl_drv_frm_edt.setClickable(false);
        enl_drv_to_edt.setEnabled(false);
        enl_drv_to_edt.setClickable(false);

        enl_drv_allday_chkbtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                 @Override
                 public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                     if(isChecked){
                         //all day
                         whichBtn="0";
                         enl_drv_frm_img.setImageResource(R.drawable.time_disable);
                         enl_drv_to_img.setImageResource(R.drawable.time_disable);
                         enl_drv_frm_edt.setText("");
                         enl_drv_to_edt.setText("");

                         enl_drv_frm_img.setClickable(false);
                         enl_drv_to_img.setClickable(false);

                     }else{

                         // specific time
                         enl_drv_frm_img.setImageResource(R.drawable.time_passing_enable);
                         enl_drv_to_img.setImageResource(R.drawable.time_passing_enable);
                         enl_drv_frm_img.setClickable(true);
                         enl_drv_to_img.setClickable(true);

                     }
                 }
             }
        );


        enl_drv_frm_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                whichBtn="1";//from
                Log.d("d", "***tvStartTime***");
                customTimePickerDialog = new CustomTimePickerDialog1(EnlistVehicleAvailablityActivity.this, 24,60,false);
                customTimePickerDialog.show();

            }
        });

        enl_drv_to_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                whichBtn="2";//To
                Log.d("d", "***tvStartTime***");
                customTimePickerDialog = new CustomTimePickerDialog1(EnlistVehicleAvailablityActivity.this, 24,60,false);
                customTimePickerDialog.show();
            }
        });

        enl_drv_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (whichBtn.equals("0")) {
                    timeFrom = "";
                    timeTo = "";
                } else {
                    timeFrom = "" + enl_drv_frm_edt.getText();
                    timeTo = "" + enl_drv_to_edt.getText();
                }
                if (strIsAvailable.equalsIgnoreCase("N")) {
                    if (enl_drv_chkbtn_avl.isChecked()) {
                        if (NetWorkStatus.isNetworkAvailable(context)) {

                            //calling to delete means Unavailable to available(Vehicle)
                            new VehicleUnAvailabilityDeleteWebService().vehicleUnAvailableDelete(context, activity, unavailable_id);

                        } else {
                            Intent i = new Intent(context, NetworkNotAvailable.class);
                            startActivity(i);
                        }
                    } else if (NetWorkStatus.isNetworkAvailable(context)) {
                        if (enl_drv_allday_chkbtn.isChecked()) {
                            strAllDay = "Y";
                        } else {
                            strAllDay = "N";
                        }
                        strId = unavailable_id;          //strId will be unavailable_id for edit Unavailablity
                        if (strAllDay.equalsIgnoreCase("N")){
                            if (checkValidation()){
                                //calling for edit in Unavailable where from and to time is mandatory
                                new VehicleAddEditAvailabilityWebService().vehicleAddEditAvailability(context, activity, strId, strvehicleId, strDate, setTimeFormat2(timeFrom), setTimeFormat2(timeTo), strAllDay);
                            }
                        }else{
                            //calling for edit in Unavailable here strAllDay = "Y"
                            new VehicleAddEditAvailabilityWebService().vehicleAddEditAvailability(context, activity, strId, strvehicleId, strDate, setTimeFormat2(timeFrom), setTimeFormat2(timeTo), strAllDay);
                        }

                    } else {
                        Intent i = new Intent(context, NetworkNotAvailable.class);
                        startActivity(i);
                    }
                }else if (strIsAvailable.equalsIgnoreCase("Y")){

                    //enl_drv_chkbtn_avl.isChecked() true means Available false means UnAvailable
                    if (!enl_drv_chkbtn_avl.isChecked()){
                        if (enl_drv_allday_chkbtn.isChecked()) {
                            strAllDay = "Y";
                        } else {
                            strAllDay = "N";
                        }
                        strId = "0";  //strId will be 0 for add Unavailablity
                        if (strAllDay.equalsIgnoreCase("N")){
                            if (checkValidation()){
                                //calling for add Unavailablity strAllDay = N, from and to time is mandatory
                                new VehicleAddEditAvailabilityWebService().vehicleAddEditAvailability(context, activity, strId, strvehicleId, strDate, setTimeFormat2(timeFrom), setTimeFormat2(timeTo), strAllDay);
                            }
                        }else {
                            //calling for add Unavailablity strAllDay = Y
                            new VehicleAddEditAvailabilityWebService().vehicleAddEditAvailability(context, activity, strId, strvehicleId, strDate, setTimeFormat2(timeFrom), setTimeFormat2(timeTo), strAllDay);
                        }
                    }
                }
            }
        });

        enl_drv_btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    private boolean checkValidation() {
        if (!TextUtils.isEmpty(enl_drv_frm_edt.getText()) && !TextUtils.isEmpty(enl_drv_to_edt.getText())){
            return true;
        }else {
            new CustomToast(activity, "please select time");
        }
        return false;
    }

    @Override
    public void onButtonClick(TimePicker mTimePicker, int hour, int minute) {

        String format;
        String time24="";

        // if whichBtn = 1 means from time and 2 means To time
        if (whichBtn.equalsIgnoreCase("1")) {
            hourFrom = hour;
            minFrom = minute;

        }else if (whichBtn.equalsIgnoreCase("2")){

            if (hourFrom == 23 && minFrom ==0){
                if (hour == 12){
                    hourTo = 24;
                }else{
                    hourTo = hour;
                }
            }else {
                if(hourFrom > 12 && hour == 12){
                    hourTo = 24;
                }else {
                    hourTo = hour;
                    minTo = minute;
                }
            }
        }

        System.out.println("*****mTimePicker*****"+mTimePicker);
        System.out.println("*****hour*****"+hour);
        System.out.println("*****minute*****"+minute);

        if(minute == 0){
            minute = 0;
        }else if (minute == 1){
            minute = 15;
        }else if (minute == 2){
            minute = 30;
        }else if (minute == 3){
            minute = 45;
        }

        if (hour == 0) {
            hour += 12;
            format = "AM";

        } else if (hour == 12) {
            format = "PM";

        } else if (hour > 12) {
            hour -= 12;
            format = "PM";

        } else {
            format = "AM";
        }

        if (minute == 0){
            time24 = hour+":0"+minute+" "+format;
            if (hour < 10){
                time24 = "0"+hour+":0"+minute+" "+format;
            }
        }else if (hour < 10){
            time24 = "0"+hour+":"+minute+" "+format;
        }else {
            time24 = hour+":"+minute+" "+format;
        }

        //Code to validate from and to time
        if(whichBtn.equals("1")) {

            if (hourFrom == 23 && minFrom > 0 || hourFrom == 12){
                enl_drv_frm_edt.setText("");
                enl_drv_to_edt.setText("");
                showAlert(getResources().getString(R.string.timeValidMsg));

            }else {
                enl_drv_frm_edt.setText(time24);
                enl_drv_to_edt.setText("");
            }
        }
        else if(whichBtn.equals("2")) {

            if (hourFrom >= hourTo){
                enl_drv_to_edt.setText("");

                showAlert(getResources().getString(R.string.dateValidMsg));

            }else if (hourFrom == hourTo-1){

                if (minTo < minFrom){
                    enl_drv_to_edt.setText("");
                    showAlert(getResources().getString(R.string.dateValidMsg));
                }else {
                    enl_drv_to_edt.setText(time24);
                }

            }else {
                enl_drv_to_edt.setText(time24);
            }
        }
        customTimePickerDialog.dismiss();
    }

    public void showAlert(String msg){

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        android.app.AlertDialog alert = builder.create();
        alert.setTitle(getResources().getString(R.string.app_name));
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void vehicleUnAvailableDetails(VehicleUnAvailabilityDetailsModel arrListVehicleUnAvailabilityModel, String status, String msg) {

        this.status = status;

        if (status.equalsIgnoreCase(StaticClass.SuccessResult)) {

            strIsAvailable = arrListVehicleUnAvailabilityModel.getIs_available();
            unavailable_id = arrListVehicleUnAvailabilityModel.getId();
            if (arrListVehicleUnAvailabilityModel.getIs_available().equalsIgnoreCase("N")) {
                enl_drv_chkbtn_avl.setChecked(false);
                enl_drv_chkbtn_avl_txt.setVisibility(View.INVISIBLE);
                enl_drv_chkbtn_unavl_txt.setVisibility(View.VISIBLE);

                if (arrListVehicleUnAvailabilityModel.getAll_day().equalsIgnoreCase("Y")) {
                    enl_drv_allday_chkbtn.setChecked(true);
                    enl_drv_allday_chkbtn.setEnabled(true);
                    enl_drv_frm_edt.setText("");
                    enl_drv_to_edt.setText("");
                    //from and to time picker will be disable

                } else {
                    enl_drv_allday_chkbtn.setChecked(false);
                    enl_drv_allday_chkbtn.setEnabled(true);
                    enl_drv_frm_edt.setText(setTimeFormat(arrListVehicleUnAvailabilityModel.getStart_time()));
                    enl_drv_to_edt.setText(setTimeFormat(arrListVehicleUnAvailabilityModel.getEnd_time()));
                }

            } else if (arrListVehicleUnAvailabilityModel.getIs_available().equalsIgnoreCase("Y")) {
                enl_drv_chkbtn_avl.setChecked(true);
                enl_drv_chkbtn_avl_txt.setVisibility(View.VISIBLE);
                enl_drv_chkbtn_unavl_txt.setVisibility(View.INVISIBLE);
                enl_drv_btn_save.setVisibility(View.GONE);
                enl_drv_btn_save_disable.setVisibility(View.VISIBLE);
//            enl_drv_allday_chkbtn.setChecked(true);
                enl_drv_allday_chkbtn.setEnabled(false);

                enl_drv_frm_img.setImageResource(R.drawable.time_disable);
                enl_drv_to_img.setImageResource(R.drawable.time_disable);
                enl_drv_frm_edt.setText("");
                enl_drv_to_edt.setText("");
                enl_drv_frm_img.setClickable(false);
                enl_drv_to_img.setClickable(false);
                //check, from and to all will be disable
                isAvailable = true;
            }
        }else if (status.equalsIgnoreCase(StaticClass.ErrorResult)){

            showDialog(msg);

        }
    }

    public String setTimeFormat(String start_time){
        String time =  "";
        if (!TextUtils.isEmpty(start_time)) {
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                final Date dateObj = sdf.parse(start_time);
                time = new SimpleDateFormat("hh:mm a").format(dateObj);

            } catch (final ParseException e) {
                e.printStackTrace();
            }
        }
        return time;
    }

    public String setTimeFormat2(String fromToTime){
        String time = "";
        if (!TextUtils.isEmpty(fromToTime)){
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                final Date dateObj = sdf.parse(fromToTime);
                time = new SimpleDateFormat("H:mm").format(dateObj);

            } catch (final ParseException e) {
                e.printStackTrace();
            }
        }

        return time;
    }

    @Override
    public void vehicleAddEditAvailability(VehicleUnAvailabilityDetailsModel vehicleUnAvailabilityModel, String status, String msg) {
        transitionflag = StaticClass.transitionflagBack;
        //this boolean variable is added to check and finish previous page on save
        StaticClass.isVehicleAvailabilitySave = true;
//        startActivity(new Intent(activity, CompanyVehicleListActivity.class));

        showDialog(msg);

    }

    @Override
    public void vehicleUnAvailableDelete(String status, String msg) {
        transitionflag = StaticClass.transitionflagBack;
        //this boolean variable is added to check and finish previous page on save
        StaticClass.isVehicleAvailabilitySave = true;
//        startActivity(new Intent(activity, CompanyVehicleListActivity.class));
        showDialog(msg);
    }

    public void showDialog(String msg){
        final CustomAlertDialogOKCancel alertDialogYESNO = new CustomAlertDialogOKCancel(this,
                msg ,
                getResources().getString(R.string.btn_ok),
                "");
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                activity.finish();
            }
        });
        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });
        alertDialogYESNO.show();
        alertDialogYESNO.setCancelable(false);
    }

}
