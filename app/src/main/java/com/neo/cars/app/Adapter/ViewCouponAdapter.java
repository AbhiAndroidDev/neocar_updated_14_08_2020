package com.neo.cars.app.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserVoucherModel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.List;

/**
 * Created by parna on 8/3/18.
 */

public class ViewCouponAdapter extends RecyclerView.Adapter<ViewCouponAdapter.MyViewHolder>{


    private List<UserVoucherModel> manageCouponModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextviewTitilliumWebRegular tvCouponCode, tvValue, tvDate;

        public MyViewHolder(View view) {
            super(view);

            tvCouponCode = view.findViewById(R.id.tvCouponCode);
            tvValue = view.findViewById(R.id.tvValue);
            tvDate = view.findViewById(R.id.tvDate);
        }
    }

    public ViewCouponAdapter(List<UserVoucherModel> manageCouponModelList) {
        this.manageCouponModelList = manageCouponModelList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_coupon_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UserVoucherModel manageCouponModel = manageCouponModelList.get(position);
        holder.tvCouponCode.setText(manageCouponModel.getVoucher_code());
        holder.tvValue.setText(manageCouponModel.getDiscount_value());
        holder.tvDate.setText(manageCouponModel.getEnd_date());

    }

    @Override
    public int getItemCount() {

        return manageCouponModelList.size();
    }
}
