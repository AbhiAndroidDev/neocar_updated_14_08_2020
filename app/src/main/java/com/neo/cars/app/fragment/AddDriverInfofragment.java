package com.neo.cars.app.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.BuildConfig;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.DriverAdd_interface;
import com.neo.cars.app.NationalityListActivity;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.DriverAdd_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.dialog.BottomSheetSingleDialog;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.YearMonthPickerDialog;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

//import me.iwf.photopicker.PhotoPicker;

/**
 * Created by parna on 16/3/18.
 */

public class AddDriverInfofragment extends CommonBaseFragment implements CallBackButtonClick, DriverAdd_interface {

    private static final String TAG = AddDriverInfofragment.class.getSimpleName();
    private View mView;
    private Context context;
    private ConnectionDetector cd;
    private RelativeLayout rlSpouseChildrenLayout;
    private CircularImageViewBorder civDriverPic;
    private ImageView ivDrivingLicense, ivDrivingLicenseBack, ivAdhaarCard, ivAdhaarCardBack, ivContactIconDriverInfo;
    private NoDefaultSpinner spnrGender, spnrMaritalStatus;
    private CustomEditTextTitilliumWebRegular etSpouseName, etChildrenName, etName,  etAddress, etFathersname, etMobNo, etAddl;
    private CustomButtonTitilliumSemibold btnProfileSave;
    private CustomTextviewTitilliumBold tvDrivingLicenseextview, tvDrivingLicenseextviewBack, tvAdhaarcardTextview, tvAdhaarcardTextviewBack;
    private CustomTextviewTitilliumWebRegular etDOB, etEmploymntSince, etNationality;

    private String strImage="", userChoosenTask="", imageFilePath="", strDriverPic="", strSelectedDriverPic="", strGender="", strSelectedGender="M", strSelectedMarritalStatus="",  strMaritalStatus="",
            strDriverDob="", strDriverAddress="", strDriverNationality="", strDriverEmploymentSince="", strDriverFathersName="",
            strDriverMobNo="", strDriverSpouseName="", strDriverChildrenName="", strDriverAdditionalInfo="",
            strDrivingLicensePic="", strDrivingLicensePicBack="", strSelectedDrivingLicensePic="", strSelectedDrivingLicensePicBack="",
            strAdhaarcardpic="", strAdhaarcardpicBack="", strSelectedAdhaarCardPic="", strSelectedAdhaarCardPicBack="", strDriverFirstName="", strDriverLastName="";

    private File file, fileDriverPic, fileDrivingLicensePic, fileDrivingLicensePicBack, fileAdhaarcard, fileAdhaarcardBack;
    private Calendar mcalendar;
    private int day,month,year;

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int PICK_IMAGE_REQUEST = 912;
    public static final int NationalityListRequestCode = 704;
    private static final int REQUEST_CODE_PICK_CONTACTS = 103;

    private String[] state = { StaticClass.DriverGenderMale, StaticClass.DriverGenderFemale};
    private String[] maritalStatus = { StaticClass.DriverMarried, StaticClass.DriverSingle} ;
    private ArrayAdapter<String> arrayAdapterGender, arrayAdapterMaritalStatus;
    private String strVehicleId = null;
    private CustomEditTextTitilliumWebRegular etFirstName, etLastName;

    private CustomTextviewTitilliumBold tvFirstNameheader, tvLastNameheader;

    private CustomTitilliumTextViewSemiBold tvSexHeader, tvAddressHeader, tvNationalityHeader,
            tvEmplHeader, tvFathersnameHeader, tvMobNoHeader, tvMaritalStatusHeader, tvSpouseNameHeader, tvDOBHeader;


    private Uri uriContact;
    private String contactID;     // contacts unique ID

    private ProgressDialog dialog;
    private Uri imageUri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_add_driver_info, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            strVehicleId = getArguments().getString("strVehicleId");
            Log.d("d", "Get strVehicleId: "+strVehicleId);
        }else{
            strVehicleId = "";
        }

        Initialize();
        Listener();

        return mView;
    }

    private void Initialize(){
        context = getActivity();
        cd = new ConnectionDetector(getActivity());

        mcalendar = Calendar.getInstance();
        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

        tvFirstNameheader = mView.findViewById(R.id.tvFirstNameheader);
        tvLastNameheader = mView.findViewById(R.id.tvLastNameheader);
        tvSexHeader = mView.findViewById(R.id.tvSexHeader);
        tvAddressHeader = mView.findViewById(R.id.tvAddressHeader);
        tvNationalityHeader = mView.findViewById(R.id.tvNationalityHeader);
        tvEmplHeader = mView.findViewById(R.id.tvEmplHeader);
        tvFathersnameHeader = mView.findViewById(R.id.tvFathersnameHeader);
        tvMobNoHeader = mView.findViewById(R.id.tvMobNoHeader);
        tvMaritalStatusHeader = mView.findViewById(R.id.tvMaritalStatusHeader);
        tvSpouseNameHeader = mView.findViewById(R.id.tvSpouseNameHeader);

        tvDOBHeader = mView.findViewById(R.id.tvDOBHeader);

        civDriverPic = mView.findViewById(R.id.civDriverPic);
        ivDrivingLicense = mView.findViewById(R.id.ivDrivingLicense);
        ivDrivingLicenseBack = mView.findViewById(R.id.ivDrivingLicenseBack);
        ivAdhaarCard = mView.findViewById(R.id.ivAdhaarCard);
        ivAdhaarCardBack = mView.findViewById(R.id.ivAdhaarCardBack);
        //ivContactIconDriverInfo = mView.findViewById(R.id.ivContactIconDriverInfo);
        etFirstName = mView.findViewById(R.id.etFirstName);
        etLastName = mView.findViewById(R.id.etLastName);
        etDOB = mView.findViewById(R.id.etDOB);
        etAddress =  mView.findViewById(R.id.etAddress);
        etNationality = mView.findViewById(R.id.etNationality);
        etEmploymntSince = mView.findViewById(R.id.etEmploymntSince);
        etFathersname = mView.findViewById(R.id.etFathersname);
        etMobNo = mView.findViewById(R.id.etMobNo);
        etAddl = mView.findViewById(R.id.etAddl);
        spnrGender = mView.findViewById(R.id.spnrGender);
        spnrMaritalStatus = mView.findViewById(R.id.spnrMaritalStatus);
        rlSpouseChildrenLayout = mView.findViewById(R.id.rlSpouseChildrenLayout);
        etSpouseName = mView.findViewById(R.id.etSpouseName);
        etChildrenName = mView.findViewById(R.id.etChildrenName);
        btnProfileSave = mView.findViewById(R.id.btnProfileSave);
        tvDrivingLicenseextview = mView.findViewById(R.id.tvDrivingLicenseextview);
        tvDrivingLicenseextviewBack = mView.findViewById(R.id.tvDrivingLicenseextviewBack);
        tvAdhaarcardTextview = mView.findViewById(R.id.tvAdhaarcardTextview);
        tvAdhaarcardTextviewBack = mView.findViewById(R.id.tvAdhaarcardTextviewBack);

        //Gender value
        arrayAdapterGender = new ArrayAdapter<String>(getActivity(), R.layout.countryitem, state);
        arrayAdapterGender.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrGender.setPrompt(getResources().getString(R.string.tvPleaseSelectGender));
        spnrGender.setAdapter(arrayAdapterGender);
        spnrGender.setSelection(0);

        spnrGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                spnrGender.setSelection(position);
                strGender = (String) spnrGender.getSelectedItem();
                Log.d("d", "Selected Gender: "+strGender);

                if (StaticClass.DriverGenderMale.equalsIgnoreCase(strGender)){
                    strSelectedGender = "M";
                }else if (StaticClass.DriverGenderFemale.equalsIgnoreCase(strGender)){
                    strSelectedGender = "F";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Marital status value
        arrayAdapterMaritalStatus = new ArrayAdapter<String>(getActivity(), R.layout.countryitem, maritalStatus);
        arrayAdapterMaritalStatus.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrMaritalStatus.setPrompt(getResources().getString(R.string.tvPleaseselectyourmaritalstatus));
        spnrMaritalStatus.setAdapter(arrayAdapterMaritalStatus);

        spnrMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                spnrMaritalStatus.setSelection(position);
                strMaritalStatus = (String) spnrMaritalStatus.getSelectedItem();
                Log.d("d", "Selected marital status: " + strMaritalStatus);

                if (StaticClass.DriverMarried.equalsIgnoreCase(strMaritalStatus)){
                    Log.d("d", "Married status::");
                    strSelectedMarritalStatus = "Y";
                    Log.d("d", "View marital status1: " + strSelectedMarritalStatus);
                    rlSpouseChildrenLayout.setVisibility(View.VISIBLE);

                }else if (StaticClass.DriverSingle.equalsIgnoreCase(strMaritalStatus)){
                    Log.d("d", "Unmarried status::");
                    strSelectedMarritalStatus = "N";
                    Log.d("d", "View marital status2: " + strSelectedMarritalStatus);
                    rlSpouseChildrenLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        etNationality.setText("India");
    }

    private void Listener(){

        btnProfileSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                etFirstName.setError(null);
                etLastName.setError(null);
                etAddress.setError(null);
                etNationality.setError(null);
                etEmploymntSince.setError(null);
                etFathersname.setError(null);

                boolean cancel = false;
                View focusView = null;

                strDriverFirstName = etFirstName.getText().toString().trim();
                strDriverLastName = etLastName.getText().toString().trim();
                strDriverDob = etDOB.getText().toString().trim();
                strDriverAddress = etAddress.getText().toString().trim();
                strDriverNationality = etNationality.getText().toString().trim();
                strDriverEmploymentSince = etEmploymntSince.getText().toString().trim();
                strDriverFathersName = etFathersname.getText().toString().trim();
                strDriverMobNo = etMobNo.getText().toString().trim();
                strDriverSpouseName = etSpouseName.getText().toString().trim();
                strDriverChildrenName = etChildrenName.getText().toString().trim();
                strDriverAdditionalInfo = etAddl.getText().toString().trim();

                Log.d("d", "Help !! Vehicle id: "+strVehicleId);

                //Driver Mob no validation
                if (!"".equalsIgnoreCase(strDriverMobNo)) {
                    if (!(new Emailvalidation().phone_validation(strDriverMobNo))) {
                        etMobNo.setError(getString(R.string.error_telphone_format));
                        focusView = etMobNo;
                        cancel = true;
                    }
                }

                if (TextUtils.isEmpty(strDriverFirstName)) {
                    etFirstName.setError(getString(R.string.error_field_required));
                    focusView = etFirstName;
                    cancel = true;

                } else if (TextUtils.isEmpty(strDriverLastName)){
                    etLastName.setError(getString(R.string.error_field_required));
                    focusView = etLastName;
                    cancel = true;

                }
//                else if (TextUtils.isEmpty(strDriverDob)){
//                    etDOB.setError(getString(R.string.error_field_required));
//                    focusView = etDOB;
//                    cancel = true;
//
//                }
                else if (TextUtils.isEmpty(strDriverAddress)){
                    etAddress.setError(getString(R.string.error_field_required));
                    focusView = etAddress;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverNationality)){
                    etNationality.setError(getString(R.string.error_field_required));
                    focusView = etNationality;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverEmploymentSince)){
                    etEmploymntSince.setError(getString(R.string.error_field_required));
                    focusView = etEmploymntSince;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverFathersName)){
                    etFathersname.setError(getString(R.string.error_field_required));
                    focusView = etFathersname;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverMobNo)){
                    etMobNo.setError(getString(R.string.error_field_required));
                    focusView = etMobNo;
                    cancel = true;

                }else if (cancel) {
                    // form field with an error.
                    focusView.requestFocus();

                }else {
                    Log.d("d", "Vehicle id: "+strVehicleId);

                    Log.d("d", "FileDriver pic*****"+fileDriverPic);

                   if (strVehicleId == null){
                        //Toast.makeText(getActivity(), "No vehicle id found!", Toast.LENGTH_SHORT).show();
                        new CustomToast(getActivity(), getResources().getString(R.string.VehicleIDnotfound));

                    }else if (fileDriverPic == null){
                        new CustomToast(getActivity(), getResources().getString(R.string.addDriverPic));
                    }
                    else if (fileDrivingLicensePic == null){
                       new CustomToast(getActivity(), getResources().getString(R.string.msgUpldDrivingLicense));
                    }

                    else if (fileDrivingLicensePicBack == null){
                       new CustomToast(getActivity(), getResources().getString(R.string.msgUpldDrivingLicense));
                    }
                    else if (fileAdhaarcard == null){
                       new CustomToast(getActivity(), getResources().getString(R.string.msgUploadAdhaarCard));
                    }
                   else if (fileAdhaarcardBack == null){
                       new CustomToast(getActivity(), getResources().getString(R.string.msgUploadAdhaarCard));
                   }
                    else if(cd.isConnectingToInternet()){
                            new DriverAdd_Webservice().DriverAdd(getActivity(), AddDriverInfofragment.this, strVehicleId, strDriverFirstName, strDriverLastName,  strSelectedGender,
                                    strDriverAddress, strDriverNationality, strDriverEmploymentSince, strDriverFathersName, strDriverMobNo, strSelectedMarritalStatus,
                                    strDriverSpouseName, strDriverChildrenName, strDriverAdditionalInfo, fileDriverPic, strDriverDob, fileAdhaarcard, fileAdhaarcardBack, fileDrivingLicensePic,
                                    fileDrivingLicensePicBack, "");

                        }else{
                            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
                            startActivity(i);
                        }
                }
            }
        });

        civDriverPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "DriverPic";
                // selectImage(getResources().getString(R.string.msgUploadDriverPic), strImage);
                selectDriverPic(getResources().getString(R.string.msgUploadDriverPic), strImage);
            }
        });

        etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DobDialog();
            }
        });

        tvDrivingLicenseextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "DrivingLicense";
                selectImage(getResources().getString(R.string.msgUpldDrivingLicense), strImage);

            }
        });

        tvDrivingLicenseextviewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "DrivingLicenseBack";
                selectImage(getResources().getString(R.string.msgUpldDrivingLicense), strImage);

            }
        });

        tvAdhaarcardTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "AdhaarCard";
                selectImage(getResources().getString(R.string.msgUploadAdhaarCard), strImage);
            }
        });


        tvAdhaarcardTextviewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "AdhaarCardBack";
                selectImage(getResources().getString(R.string.msgUploadAdhaarCard), strImage);
            }
        });

        etEmploymntSince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inEmployementSince();
            }
        });

        etNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent stateintent = new Intent(context, NationalityListActivity.class);
                startActivityForResult(stateintent, NationalityListRequestCode);
            }
        });

    }



    public void DobDialog(){

        etDOB.setInputType(InputType.TYPE_NULL);
        etDOB.setTextIsSelectable(true);

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" +monthOfYear);
                etDOB.setText(year + "-" + (monthOfYear+1) + "-" +dayOfMonth );
                Log.d("d", "String Date::"+etDOB.getText().toString().trim());
            }
        };
        DatePickerDialog dpDialog = new DatePickerDialog(context, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(mcalendar.getTimeInMillis());
        dpDialog.show();

    }

    public void inEmployementSince(){

        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR),01,01);

        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(getActivity(),
                calendar,
                new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int year) {
                        System.out.println("**YearMonthPickerDialog**"+year);
                        etEmploymntSince.setText(String.valueOf(year));
                    }
                });

        yearMonthPickerDialog.show();
    }


    public void selectDriverPic(final String strMessage, String strImage){

        BottomSheetSingleDialog bsd = new BottomSheetSingleDialog(context,
                getActivity(),
                AddDriverInfofragment.this,
                strMessage,
                AddDriverInfofragment.this.getResources().getString(R.string.Camera));

    }


        public void selectImage(final String strMessage, String strImage){

            BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(context,
                    getActivity(),
                    AddDriverInfofragment.this,
                    strMessage,
                    AddDriverInfofragment.this.getResources().getString(R.string.Camera),
                    AddDriverInfofragment.this.getResources().getString(R.string.Gallery));

    }

    //take photo through camera
    private void takePhoto() {

        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

//        try {
//            PhotoPicker.builder()
//                    .setPhotoCount(1)
//                    .setShowCamera(true)
//                    .setShowGif(false)
//                    .setPreviewEnabled(false)
//                    .start(Objects.requireNonNull(getContext()), this, PICK_IMAGE_REQUEST);
//
//
//        } catch (Exception e) {
//            //To catch unknown Null pointer exception inside library
//            e.printStackTrace();
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:
                    dialog = new ProgressDialog(context);
                    try {

                        dialog.setMessage("Image processing...");
                        dialog.show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {
                                    Bitmap fullImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(),imageUri);

                                    //Added to set orientation of camera capturing image in samsung/sony device
                                    InputStream input = context.getContentResolver().openInputStream(imageUri);
                                    ExifInterface ei;
                                    if (Build.VERSION.SDK_INT > 23)
                                        ei = new ExifInterface(input);
                                    else
                                        ei = new ExifInterface(imageUri.getPath());

                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    switch (orientation) {
                                        case ExifInterface.ORIENTATION_ROTATE_90:
                                            fullImage = rotateImage(fullImage, 90);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_180:
                                            fullImage = rotateImage(fullImage, 180);
                                            break;
                                        case ExifInterface.ORIENTATION_ROTATE_270:
                                            fullImage = rotateImage(fullImage, 270);
                                            break;
                                        default:
                                            break;
                                    }

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                    byte[] byteArray = bytes.toByteArray();
                                    Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);
                                    file = ImageUtils.saveImage(scaledBitmap, context);

                                    if (file != null) {

                                        if (strImage == "DriverPic"){
                                            fileDriverPic = file;
                                            Log.d("d", "Camera Driver pic file:" + fileDriverPic);
                                            Picasso.get().load(file).resize(200, 200).into(civDriverPic);

                                        }
                                        else if (strImage == "DrivingLicense"){
                                            fileDrivingLicensePic = file;
                                            Log.d("d", "Camera Driving license pic file: "+fileDrivingLicensePic);
                                            Picasso.get().load(file).resize(200, 200).into(ivDrivingLicense);

                                        }

                                        else if (strImage == "DrivingLicenseBack"){
                                            fileDrivingLicensePicBack = file;
                                            Log.d("d", "Camera Driving license pic file: "+fileDrivingLicensePicBack);
                                            Picasso.get().load(file).resize(200, 200).into(ivDrivingLicenseBack);

                                        }

                                        else if (strImage == "AdhaarCard"){
                                            fileAdhaarcard = file;
                                            Log.d("d", "Camera Adhaar card pic file: "+fileAdhaarcard);
                                            Picasso.get().load(file).resize(200, 200).into(ivAdhaarCard);
                                        }

                                        else if (strImage == "AdhaarCardBack"){
                                            fileAdhaarcardBack = file;
                                            Log.d("d", "Camera Adhaar card pic file: "+fileAdhaarcardBack);
                                            Picasso.get().load(file).resize(200, 200).into(ivAdhaarCardBack);
                                        }

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                }
                                finally{
                                    dialog.dismiss();
                                }
                            }
                        }, 4000);  // 4 sec to allow processing of image captured


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case PICK_IMAGE_REQUEST:

                    try{
//                        ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {
                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }



                        if(strImage == "DriverPic"){
//                            strDriverPic = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strDriverPic);
//                            strSelectedDriverPic = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedDriverPic);
                            fileDriverPic = file;
                            Log.d("d", "Driver pic Image File::" + fileDriverPic);
                            Picasso.get().load(file).resize(200, 200).into(civDriverPic);

                        }

                        else if (strImage == "DrivingLicense"){
//                            strDrivingLicensePic = "file://" + photos.get(0);
//                            Log.d("d", "Driving license pic path : " + strDrivingLicensePic);
//                            strSelectedDrivingLicensePic = photos.get(0);
//                            Log.d("d", "Driving license pic to be uploaded: " + strSelectedDrivingLicensePic);
                            fileDrivingLicensePic = file;
                            Log.d("d", "Driving license Image file: "+fileDrivingLicensePic);
                            Picasso.get().load(file).resize(200, 200).into(ivDrivingLicense);

                        }

                        else if (strImage == "DrivingLicenseBack"){
//                            strDrivingLicensePicBack = "file://" + photos.get(0);
//                            Log.d("d", "Driving license pic path : " + strDrivingLicensePicBack);
//                            strSelectedDrivingLicensePicBack = photos.get(0);
//                            Log.d("d", "Driving license pic to be uploaded: " + strSelectedDrivingLicensePicBack);
                            fileDrivingLicensePicBack = file;
                            Log.d("d", "Driving license Image file: "+fileDrivingLicensePicBack);
                            Picasso.get().load(file).resize(200, 200).into(ivDrivingLicenseBack);

                        }


                        else if (strImage == "AdhaarCard"){
//                            strAdhaarcardpic = "file://" + photos.get(0);
//                            Log.d("d", "Adhaar card pic path : " + strAdhaarcardpic);
//                            strSelectedAdhaarCardPic = photos.get(0);
//                            Log.d("d", "Adhaar card pic to be uploaded: " + strSelectedAdhaarCardPic);
                            fileAdhaarcard = file;
                            Log.d("d", "Adhaar card Image file: "+fileAdhaarcard);
                            Picasso.get().load(file).resize(200, 200).into(ivAdhaarCard);

                        }

                        else if (strImage == "AdhaarCardBack"){
//                            strAdhaarcardpicBack = "file://" + photos.get(0);
//                            Log.d("d", "Adhaar card pic path : " + strAdhaarcardpicBack);
//                            strSelectedAdhaarCardPicBack = photos.get(0);
//                            Log.d("d", "Adhaar card pic to be uploaded: " + strSelectedAdhaarCardPicBack);
                            fileAdhaarcardBack = file;
                            Log.d("d", "Adhaar card Image file: "+fileAdhaarcardBack);
                            Picasso.get().load(file).resize(200, 200).into(ivAdhaarCardBack);

                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;

                case NationalityListRequestCode:

                    if (!data.getExtras().getString("Nationality").equals("")) {
                        etNationality.setText(data.getExtras().getString("Nationality"));
                    }

                    break;

                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();

                    getContactNumber(context, data);
                    break;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void getContactNumber(final Context context, Intent data) {
        String selectedNumber = "";
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";
        etMobNo.setText("");

        List<String> allNumbers = new ArrayList<>();
        int contactIdColumnId, phoneColumnID, nameColumnID;
        try {
            Uri result = data.getData();
//            Log.e(TAG, result.toString());
            String id = result.getLastPathSegment();
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);
                    Log.e("Phone", phoneNumber);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
                    allNumbers.size();
                    cursor.moveToNext();
                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (allNumbers.size() > 1 && !allNumbers.isEmpty()) {
                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        String selectedNumber = items[item].toString().replaceAll("[^0-9\\+]", "");
                        Log.d("numberOnClick", selectedNumber);

                        if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {
                            etMobNo.setText(selectedNumber);
                            etMobNo.requestFocus();

                        } else {
                            new CustomToast(getActivity(), getResources().getString(R.string.txt_message_mobile_no));
                        }
                    }
                });
                android.app.AlertDialog alert = builder.create();
                alert.show();

            } else {
                selectedNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                Log.d("number", selectedNumber);
                if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {
                    etMobNo.setText(selectedNumber);
                    etMobNo.requestFocus();
                } else {
                    new CustomToast(getActivity(), getResources().getString(R.string.txt_message_mobile_no));
                }
            }
        }
    }

    @Override
    public void onButtonClick(String strButtonText) {
        System.out.println("****strButtonText***"+strButtonText);

//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(getActivity().getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {

                Dexter.withActivity(getActivity()).withPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    takePhoto();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert(context);
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }

        } else if (strButtonText.equals(getActivity().getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";

            Dexter.withActivity(getActivity()).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE )
                    .withListener(new MultiplePermissionsListener()
                    {
                        @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if(report.areAllPermissionsGranted()){

                                mCallPhotoGallary();
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                                showAlert(context);
                            }
                        }
                        @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                        {/* ... */

                            token.continuePermissionRequest();
                        }
                    }).check();
//            if (result) {
//                mCallPhotoGallary();
//            }
        }
    }

    @Override
    public void driverAddInterface(String status, String msg) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(getContext(),
                        msg ,
                getResources().getString(R.string.btn_ok),
                "");

                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialogYESNO.dismiss();
                        getActivity().finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
    }
}
